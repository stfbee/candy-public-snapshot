По многочисленным просьбам выкладываю исходники последней публичной версии Candy.

Весь этот код не стоит рассматривать как академическое пособие, т. к. он писался в процессе изучения школьниками Java и содержит очень много неочевидных для нас того времени ошибок.

Может быть, если мне (@stfbee) будет скучно, клиент будет переписан с использованием модных технологий и паттернов.

Из кода удалены все (или почти все) токены и пароли, регайте свои.
История изменений по тем же причинам не сохранена.

Спасибо всем людям, что жертвовали свои деньги на проект, а так же тем, кто всячески помогал в его написании.

Онищенко Влад, январь 2017.

https://vk.com/kadabeldstudios


```
#!plain

Copyright 2016 Onishchenko Vladislav

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```