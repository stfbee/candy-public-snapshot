package ru.kadabeld.candy;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import ru.kadabeld.candy.fragments.SettingsFragment;
import ru.kadabeld.candy.utils.ColorUtils;

public class SettingsActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CandyApplication.setTheme(this);
        super.onCreate(savedInstanceState);
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) supportActionBar.setDisplayHomeAsUpEnabled(true);

            //Делаем отступ в тулбаре под статусбар
            int statusBarHeight = getStatusBarHeight();
            toolbar.setPadding(0, statusBarHeight, 0, 0);
            toolbar.getLayoutParams().height += statusBarHeight;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkAlpha));
            if (sPref.getBoolean("colorizeNavbar", true)) {
                getWindow().setNavigationBarColor(ColorUtils.getThemeColor(R.attr.colorPrimary));
            }
        } else {
            //Затемняем статусбар и навбар на девайсах меньше лоллипопа
            SystemBarTintManager tintManager = new SystemBarTintManager(this);// enable status bar tint
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setNavigationBarTintEnabled(true);
            tintManager.setTintColor(Color.parseColor("#20000000"));
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Fragment settingsFragment = new SettingsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, settingsFragment).commit();
    }
}
