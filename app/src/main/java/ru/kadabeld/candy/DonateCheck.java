package ru.kadabeld.candy;

import android.util.Base64;
import android.util.Log;

import com.loopj.android.http.RequestParams;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ru.kadabeld.candy.account.AccountInfo;
import ru.kadabeld.candy.utils.NetworkUtils;
import rx.functions.Func1;

/**
 * Created by arktic on 11.05.16.
 */
public class DonateCheck implements Func1<AccountInfo, AccountInfo> {

    @Override
    public AccountInfo call(AccountInfo accountInfo) {
        final RequestParams params = new RequestParams();
        String group = "80665401";

        if (group.length() != 8) {
            throw new RuntimeException("пшол в хуй, пират");
        }

        try {
            String b = "aHR0cHM6Ly9hcGkudmsuY29tL21ldGhvZC9ncm91cHMuaXNNZW1iZXI/Z3JvdXBfaWQ9ODA2NjU0MDEmZXh0ZW5kZWQ9MSZ1c2VyX2lkPQ==";
            byte[] getdata = Base64.decode(b, Base64.DEFAULT);
            String text = new String(getdata, "UTF-8");
            params.put("group_id", 0x99DB67 * group.length() + 1);
            params.put("user_id", accountInfo.getUserId());
            params.put("extended", 1);

            OkHttpClient client = NetworkUtils.getInstance().generateDefaultOkHttpClient();

            Response response = client.newCall(new Request.Builder().url(text + accountInfo.getUserId()).build()).execute();
            JSONObject resp = new JSONObject(response.body().string());
            int member = resp.getJSONObject("response").getInt("member");
            accountInfo.setDonate(member == 1);
            return accountInfo;
        } catch (IOException | JSONException e) {
            Log.d("", "Failed to check donate status.", e);
            return null;
        }
    }
}
