package ru.kadabeld.candy.network.robospice.requests;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.account.AccountInfo;
import ru.kadabeld.candy.entities.NewsfeedList;
import ru.kadabeld.candy.utils.NetworkUtils;

/**
 * Created by arktic on 13.05.16.
 */
public class NewsFeedRequest extends CandyRequest<NewsFeedRequest.NewsFeedResponse> {

    public static class NewsFeedResponse extends ArrayList<NewsfeedList> {}

    public static class NewsFeedCacheKey {

        private final long mUserId;

        public NewsFeedCacheKey(long userId) {
            mUserId = userId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof NewsFeedCacheKey)) return false;

            NewsFeedCacheKey that = (NewsFeedCacheKey) o;

            return mUserId == that.mUserId;

        }

        @Override
        public int hashCode() {
            return (int) (mUserId ^ (mUserId >>> 32));
        }

    }

    private final AccountInfo mInfo;

    public NewsFeedRequest(AccountInfo info) {
        super(NewsFeedResponse.class);
        mInfo = info;
    }

    @Override
    public NewsFeedResponse loadDataFromNetwork() throws Exception {
        RequestBody body = new FormEncodingBuilder()
                .add("extended", String.valueOf(1))
                .add("v", API.version)
                .add("access_token", mInfo.getAccessToken())
                .add("lang", CandyApplication.lang)
                .build();

        OkHttpClient client = NetworkUtils.getInstance().generateDefaultOkHttpClient();
        Response response = client.newCall(new Request.Builder().url(API.BASE_without_setonline + API.newsfeedGetLists).post(body).build()).execute();
        if (response.isSuccessful()) {
                JSONObject object = new JSONObject(response.body().string());
                if (object.has("response")) {
                    JSONObject resp = object.getJSONObject("response");
                    JSONArray listsObjects = resp.getJSONArray("items");
                    NewsFeedResponse result = new NewsFeedResponse();
                    for (int i = 0; i < listsObjects.length(); i++) {
                        JSONObject listObject = listsObjects.getJSONObject(i);
                        result.add(NewsfeedList.parseNewsfeedList(listObject));
                    }
                    return result;
                }

        }
        throw new RuntimeException("smth went wrong");
    }

    @Override
    public Object getCacheKey() {
        return new NewsFeedCacheKey(mInfo.getUserId());
    }

    @Override
    public long getCacheDuration() {
        return TimeUnit.HOURS.toMillis(1);
    }
}
