package ru.kadabeld.candy.network.robospice.requests;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by arktic on 21.05.16.
 */
public abstract class CandyRequest<T> extends SpiceRequest<T> {

    public CandyRequest(Class<T> clazz) {
        super(clazz);
    }

    public Object getCacheKey() {
        return null;
    }

    public long getCacheDuration() {
        return DurationInMillis.ALWAYS_EXPIRED;
    }

}
