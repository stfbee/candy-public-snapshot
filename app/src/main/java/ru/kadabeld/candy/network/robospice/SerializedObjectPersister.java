package ru.kadabeld.candy.network.robospice;

import android.app.Application;

import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.exception.CacheLoadingException;
import com.octo.android.robospice.persistence.exception.CacheSavingException;
import com.octo.android.robospice.persistence.file.InFileObjectPersister;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import roboguice.util.temp.Ln;

/**
 * Created by arktic on 13.05.16.
 */
public class SerializedObjectPersister<T> extends InFileObjectPersister<T> {

    public SerializedObjectPersister(Application application, Class<T> clazz, File cacheFolder) throws CacheCreationException {
        super(application, clazz, cacheFolder);
    }

    @Override
    protected T readCacheDataFromFile(File file) throws CacheLoadingException {
        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new FileInputStream(file));
            return (T) in.readObject();
        } catch (FileNotFoundException e) {
            Ln.w("file " + file.getAbsolutePath() + " does not exists", e);
            return null;
        } catch (Exception e) {
            throw new CacheLoadingException(e);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public T saveDataToCacheAndReturnData(final T data, final Object cacheKey) throws CacheSavingException {
        if (!(data instanceof Serializable)) {
            throw new RuntimeException(data + " is not serializable || parcelable");
        }
        if (isAsyncSaveEnabled()) {
            new Thread() {
                @Override
                public void run() {
                    try {
                        saveData(data, cacheKey);
                    } catch (CacheSavingException e) {
                        Ln.e(e, "An error occurred on saving request " + cacheKey + " data asynchronously");
                    }
                }
            }.start();
        } else {
            saveData(data, cacheKey);
        }
        return data;
    }

    private void saveData(T data, Object cacheKey) throws CacheSavingException {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(getCacheFile(cacheKey)));
            out.writeObject(data);
        } catch (IOException e) {
            throw new CacheSavingException(e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
