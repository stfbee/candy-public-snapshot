package ru.kadabeld.candy.network.robospice;

import android.app.Application;

import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.file.InFileObjectPersister;
import com.octo.android.robospice.persistence.file.InFileObjectPersisterFactory;

import java.io.File;
import java.util.List;

/**
 * Created by arktic on 13.05.16.
 */
public class SerializedObjectPersisterFactory extends InFileObjectPersisterFactory {

    public SerializedObjectPersisterFactory(Application application, File cacheFolder)
            throws CacheCreationException {
        this(application, null, cacheFolder);
    }

    public SerializedObjectPersisterFactory(Application application, List<Class<?>> listHandledClasses, File cacheFolder)
            throws CacheCreationException {
        super(application, listHandledClasses, cacheFolder);
    }

    @Override
    public <T> InFileObjectPersister<T> createInFileObjectPersister(Class<T> clazz, File cacheFolder)
            throws CacheCreationException {
        return new SerializedObjectPersister<T>(getApplication(), clazz, cacheFolder);
    }
}