package ru.kadabeld.candy;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;

import com.crashlytics.android.Crashlytics;
import com.loopj.android.http.AsyncHttpClient;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import ru.kadabeld.candy.account.AccountInfo;
import ru.kadabeld.candy.account.AccountsBus;
import ru.kadabeld.candy.routine.service.ForegroundScheduledTasks;
import ru.kadabeld.candy.routine.service.RoutineService;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.NetworkUtils;

public class CandyApplication extends MultiDexApplication {
    public static PrettyTime p;
    public static Typeface light;
    public static Typeface thin;
    public static Typeface regular;

    public static boolean ninja = true;
    public static String lang = "ru";
    public static AsyncHttpClient client;

    private static CandyApplication INSTANCE;

    private AccountsBus mAccountsBus;

    @Deprecated
    public static String access_token() {
        return getInstance()
                .getAccountsBus()
                .current()
                .map(info -> info == null ? null : info.getAccessToken())
                .toBlocking()
                .first();
    }

    @Deprecated
    public static AccountInfo current() {
        return getInstance()
                .getAccountsBus()
                .current()
                .toBlocking()
                .first();
    }

    public static CandyApplication getInstance() {
        return INSTANCE;
    }

    public AccountsBus getAccountsBus() {
        return mAccountsBus;
    }

    @Override
    public void onCreate() {
        INSTANCE = this;
        mAccountsBus = new AccountsBus(getApplicationContext());
        Fabric.with(this, new Crashlytics());

        setTheme(getApplicationContext());
        super.onCreate();

        NetworkUtils.init(this);
        ForegroundScheduledTasks tasks = new ForegroundScheduledTasks();
        tasks.addTask(() -> RoutineService.runPendingTasks(getInstance()));
        tasks.startForegroundOnly(this);

        light = Typeface.createFromAsset(getAssets(), "robotolight.ttf");
        thin = Typeface.createFromAsset(getAssets(), "robotothin.ttf");
        regular = Typeface.createFromAsset(getAssets(), "robotoregular.ttf");
        client = new AsyncHttpClient();
        client.setLoggingEnabled(false);
        client.setURLEncodingEnabled(true);
        client.setMaxRetriesAndTimeout(6, 10000);
        ninja = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("ninja", false);

        ColorUtils.init(this);
        switchLocale(this);
        mAccountsBus
                .current()
                .filter(accountInfo -> accountInfo != null)
                .subscribe(info -> {
                    Crashlytics.setUserIdentifier(String.valueOf(info.getUserId()));
                    Crashlytics.setUserName(info.getFullName());
                });
    }

    public static void setTheme(Context context) {
        //todo theme selector
        context.setTheme(R.style.CandyTheme_Default);
    }

    public static void switchLocale(Context ctx) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        Resources res = ctx.getResources();

        String system_lang = res.getConfiguration().locale.getLanguage();
        String[] stringArray = res.getStringArray(R.array.langvalues);
        String app_lang = "ru"; //default
        if (!sPref.contains("language")) {
            for (String supportable_lang : stringArray) {
                if (supportable_lang.equals(system_lang)) {
                    app_lang = system_lang;
                    sPref.edit().putString("language", app_lang).apply();
                    break;
                }
            }
        } else {
            app_lang = sPref.getString("language", app_lang);
        }
        lang = app_lang.toLowerCase();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = new Locale(lang);
        Locale.setDefault(conf.locale);
        res.updateConfiguration(conf, dm);
        Global.displayDensity = dm.density;

        CandyApplication.p = new PrettyTime(new Locale(app_lang));
    }
}
