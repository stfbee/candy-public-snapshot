package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.PhotoGridAdapter;
import ru.kadabeld.candy.entities.Photo;
import ru.kadabeld.candy.utils.parsers.Attachments;

/**
 * User: Vlad
 * Date: 28.12.2015
 * Time: 0:51
 */
public class PhotosFragment extends Fragment {
    private long user_id = CandyApplication.current().getUserId();
    private int colorPrimary = 0;
    private boolean canColorize;
    private boolean colorized = false;
    private AsymmetricGridView rootLV;
    private ArrayList<PhotoGridAdapter.PhotoItem> photos = new ArrayList<>();
    private PhotoGridAdapter adapter;
    private ProgressBar progressBar;
    private TextView empty;
    private int offset;

    public void init(long id, int color) {
        user_id = id;
        colorPrimary = color;
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photos, container, false);
        ((AbstractActivity) getActivity()).makeActionBar(rootView);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);
        rootLV = (AsymmetricGridView) rootView.findViewById(R.id.listview_parallax);
        rootLV.setRequestedColumnCount(3);
        rootLV.setAllowReordering(true);
        photos = new ArrayList<>();

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) supportActionBar.setTitle(getString(R.string.photos));

        canColorize = colorPrimary != 0;
        if (canColorize) {
            colorize();
            colorized = true;
        }

        adapter = new PhotoGridAdapter(getContext(), photos);
        AsymmetricGridViewAdapter asymmetricAdapter = new AsymmetricGridViewAdapter(getActivity(), rootLV, adapter);
        rootLV.setAdapter(asymmetricAdapter);

        addPhotos();

        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            int previousLastPosition = 0;

            void resetLastIndex() {
                previousLastPosition = 0;
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        addPhotos();
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (colorized && canColorize) {
            colorize();
        }
    }

    private void colorize() {
        ((AbstractActivity) getActivity()).colorize(colorPrimary);
    }

    private void addPhotos() {
        if (photos.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        RequestParams params = new RequestParams();
        params.put("owner_id", user_id);
        params.put("offset", offset);
        params.put("count", 30);
        params.put("extended", 1);
        params.put("sort", 0);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.photosGetAll, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (photos.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                empty.setVisibility(View.GONE);
                try {
                    JSONObject object = new JSONObject(responseString);
                    JSONObject response = object.getJSONObject("response");
                    JSONArray items = response.getJSONArray("items");
                    if (items.length() > 0) {
                        Attachments attachments_parsed = Attachments.parseJson(items);
                        ArrayList<Photo> _photos = attachments_parsed.getPhotos();
                        for (Photo photo : _photos) {
                            PhotoGridAdapter.PhotoItem photoItem = new PhotoGridAdapter.PhotoItem(photos.size(), photo);
                            photos.add(photoItem);
                        }
                        adapter.notifyDataSetChanged();
                        offset = photos.size();
                        progressBar.setVisibility(View.GONE);
                        rootLV.setVisibility(View.VISIBLE);
                    } else {
                        if (photos.size() == 0) {
                            empty.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    if (photos.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        empty.setText(R.string.loading_error);
                        progressBar.setVisibility(View.GONE);
                    }
                    Log.e("LOAD", responseString);
                    e.printStackTrace();
                }
            }
        });
    }
}
