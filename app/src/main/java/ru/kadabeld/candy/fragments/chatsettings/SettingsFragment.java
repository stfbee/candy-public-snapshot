package ru.kadabeld.candy.fragments.chatsettings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.thebluealliance.spectrum.SpectrumPreference;

import org.joda.time.Interval;
import org.joda.time.Minutes;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;

import java.util.Date;
import java.util.Locale;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Dialog;
import ru.kadabeld.candy.utils.fragment.PreferenceFragment;

/**
 * User: Vlad
 * Date: 22.05.2016
 * Time: 21:35
 */

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private long chat_id;
    SharedPreferences sPref;
    private String name = "";
    private String chat_name;
    private String chat_sound;
    private String chat_vibrate;
    private String chat_muted_for;
    private String chat_color;
    private String chat_muted_since;
    private String chat_notify_disabled;
    boolean is_chat;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        chat_id = 2000000000 + getArguments().getLong("chat_id");
        Dialog dialog;
        is_chat = getArguments().getBoolean("is_chat");
        if (is_chat) {
            dialog = Cache.getChat(getArguments().getLong("chat_id"));
            name = dialog.getTitle();
        } else {
            dialog = Cache.getDialog(getArguments().getLong("user_id"));
            name = dialog.getUser().getName();
        }

        chat_name = "chat_" + this.chat_id + "_name";
        chat_sound = "chat_" + this.chat_id + "_sound";
        chat_vibrate = "chat_" + this.chat_id + "_vibrate";
        chat_muted_for = "chat_" + this.chat_id + "_muted_for";
        chat_color = "chat_" + this.chat_id + "_color";
        chat_muted_since = "chat_" + this.chat_id + "_muted_since";
        chat_notify_disabled = "chat_" + this.chat_id + "_notify_disabled";

        sPref = PreferenceManager.getDefaultSharedPreferences(getContext());

        sPref.edit().putString(chat_name, name).apply();

        setPreferenceScreen(createPreferenceHierarchy());
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sPref, String key) {
        Log.d("PREFS", "key: " + key + ", value: " + sPref.getAll().get(key));

        if (key.equals(chat_muted_for)) {
            long muted_for = Long.parseLong(sPref.getString(chat_muted_for, "0"));
            if (muted_for == -1) {
                sPref.edit().putBoolean(chat_notify_disabled, true).apply();
            } else {
                sPref.edit().putBoolean(chat_notify_disabled, false).apply();
                sPref.edit().putLong(chat_muted_since, (new Date().getTime())).apply();
            }
            String summary = "";
            if (muted_for == 0) {
                summary = getContext().getResources().getStringArray(R.array.chat_mute)[0];
            } else if (muted_for == 900000) {
                summary = getContext().getResources().getStringArray(R.array.chat_mute)[1];
            } else if (muted_for == 3600000) {
                summary = getContext().getResources().getStringArray(R.array.chat_mute)[2];
            } else if (muted_for == 86400000) {
                summary = getContext().getResources().getStringArray(R.array.chat_mute)[3];
            } else if (muted_for == 604800000) {
                summary = getContext().getResources().getStringArray(R.array.chat_mute)[4];
            } else if (muted_for == -1) {
                summary = getContext().getResources().getStringArray(R.array.chat_mute)[5];
            }
            findPreference(chat_muted_for).setSummary(summary);
        } else if (key.equals(chat_name)) {
            String new_name = sPref.getString(chat_name, "");
            findPreference(chat_name).setTitle(new_name);

            RequestParams params = new RequestParams();
            params.put("title", new_name);
            params.put("chat_id", chat_id - 2000000000);
            params.put("v", API.version);
            params.put("access_token", CandyApplication.access_token());
            params.put("lang", CandyApplication.lang);
            CandyApplication.client.post(API.BASE() + API.messagesEditChat, params, new JsonHttpResponseHandler());
            // TODO: 05.06.2016 добавить обработку ответа
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * Использую такое заполнение экрана вместо addPreferencesFromResource(R.xml.chat_settings)
     * т.к. нужно задавать каждый раз разные айдишники, через xml пришлось бы вручную обновлять значения
     *
     * @return PreferenceScreen для добавления на экран
     */
    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(getContext());

        if (is_chat) {
            PreferenceCategory chatNameCategory = new PreferenceCategory(getContext());
            chatNameCategory.setTitle(R.string.settings_chat_name);
            root.addPreference(chatNameCategory);

            EditTextPreference chatNamePref = new EditTextPreference(getContext());
            chatNamePref.setTitle(name);
            chatNamePref.setKey(chat_name);
            chatNamePref.setDialogTitle(R.string.settings_chat_name);
            chatNamePref.setDefaultValue(name);
            chatNameCategory.addPreference(chatNamePref);
        }

        PreferenceCategory notificationsCategory = new PreferenceCategory(getContext());
        notificationsCategory.setTitle(R.string.settings_notifications);
        root.addPreference(notificationsCategory);


        CheckBoxPreference chatSoundPref = new CheckBoxPreference(getContext());
        chatSoundPref.setKey(chat_sound);
        chatSoundPref.setDefaultValue(true);
        chatSoundPref.setTitle(R.string.settings_sound);
        notificationsCategory.addPreference(chatSoundPref);


        CheckBoxPreference chatVibratePref = new CheckBoxPreference(getContext());
        chatVibratePref.setKey(chat_vibrate);
        chatVibratePref.setDefaultValue(true);
        chatVibratePref.setTitle(R.string.settings_vibration);
        notificationsCategory.addPreference(chatVibratePref);


        ListPreference chatMuteListPref = new ListPreference(getContext());
        chatMuteListPref.setEntries(R.array.chat_mute);
        chatMuteListPref.setEntryValues(R.array.chat_mute_values);
        chatMuteListPref.setDialogTitle(R.string.settings_mute);
        chatMuteListPref.setKey(chat_muted_for);
        chatMuteListPref.setTitle(R.string.settings_mute);

        String summary;

        long muted_for = Long.parseLong(sPref.getString(chat_muted_for, "0"));
        if (muted_for == 0) {
            summary = getContext().getResources().getStringArray(R.array.chat_mute)[0];
        } else if (muted_for == -1) {
            summary = getContext().getResources().getStringArray(R.array.chat_mute)[5];
        } else {
            long since = sPref.getLong(chat_muted_since, 0);
            long now = new Date().getTime();
            long unmute_time = muted_for + since;
            if (unmute_time > now) {
                Interval interval = new Interval(now, unmute_time);
                Minutes minutes = Minutes.minutesIn(interval);
                String print = PeriodFormat.getDefault().withLocale(Locale.getDefault()).print(new Period(minutes).normalizedStandard());
                summary = String.format(getString(R.string.settings_time_left), print);
            } else {
                summary = getString(R.string.settings_time_is_up);
            }
        }

        chatMuteListPref.setSummary(summary);
        notificationsCategory.addPreference(chatMuteListPref);


        SpectrumPreference chatColorPref = new SpectrumPreference(getContext(), null);
        chatColorPref.setColors(R.array.dash_colors);
        chatColorPref.setDefaultValue(getResources().getColor(R.color.colorPrimary));
        chatColorPref.setKey(chat_color);
        chatColorPref.setTitle(R.string.settings_led_color);
        chatColorPref.setDialogTitle(R.string.settings_led_color);
        notificationsCategory.addPreference(chatColorPref);

        return root;
    }
}
