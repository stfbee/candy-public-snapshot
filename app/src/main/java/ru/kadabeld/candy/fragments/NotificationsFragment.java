package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.entities.meta.Notification;
import ru.kadabeld.candy.views.NotificationView;

/**
 * User: Vlad
 * Date: 10.11.2015
 * Time: 23:59
 */
@SuppressWarnings("deprecation")
public class NotificationsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private ListView rootLV;
    private ProgressBar progressBar;
    private TextView empty;
    private ArrayList<Notification> notifications = new ArrayList<>();
    private NotificationsAdapter notificationsAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private AbstractActivity ctx;
    private int colorPrimary = 0;
    private boolean canColorize;
    private boolean colorized = false;

    private int previousLastPosition = 0;
    private int lastPosition = 0;

    private String next_from = "";
    private Long last_viewed;
    private boolean last_loaded = false;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean animation = sPref.getBoolean("listview_animation", true);

        View rootView = inflater.inflate(R.layout.fragment_listview, container, false);

        ctx = (AbstractActivity) getActivity();
        ctx.makeActionBar(rootView);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(getString(R.string.drawer_feedback));

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(animation ? R.id.refresh_parallax : R.id.refresh);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#d18d88"), Color.parseColor("#b3655f"), Color.parseColor("#f23426"), Color.parseColor("#d23d32"));

        notificationsAdapter = new NotificationsAdapter(getActivity(), notifications);
        rootLV = (ListView) rootView.findViewById(animation ? R.id.listview_parallax : R.id.listview);
        rootLV.setAdapter(notificationsAdapter);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);

        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            void resetLastIndex() {
                previousLastPosition = 0;
                lastPosition = 0;
                next_from = "";
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (rootLV != null && rootLV.getChildCount() > 0) {
                    boolean firstItemVisible = rootLV.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = rootLV.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);

                lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        getActivity().runOnUiThread(() -> addNotifications());
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });

        addNotifications();

        canColorize = colorPrimary != 0;
        if (canColorize) {
            colorize();
            colorized = true;
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ctx = (AbstractActivity) getActivity();
        if (colorized && canColorize) {
            colorize();
        }
    }

    private void colorize() {
        ((AbstractActivity) getActivity()).colorize(colorPrimary);
    }

    private void addNotifications() {
        if (notifications.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        if (last_loaded) return;
        RequestParams params = new RequestParams();
        int loadStep = 20;
        params.put("count", loadStep);
        params.put("start_from", next_from);
//        params.put("filters", "followers,friends");
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.notificationsGet, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (notifications.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                empty.setVisibility(View.GONE);
                try {
                    JSONObject object = new JSONObject(responseString);
                    JSONObject response = object.getJSONObject("response");

                    JSONArray items = response.getJSONArray("items");
                    int length = items.length();
                    if (length > 0) {
                        if (response.has("next_from")) {
                            next_from = response.getString("next_from");
                        } else {
                            last_loaded = true;
                        }
                        if (response.has("last_viewed"))
                            last_viewed = response.getLong("last_viewed");

                        if (mSwipeRefreshLayout.isRefreshing()) {
                            notifications.clear();
                            previousLastPosition = 0;
                            lastPosition = 0;
                            next_from = "";
                        }
                        if (response.has("profiles")) {
                            JSONArray profiles = response.getJSONArray("profiles");
                            for (int i = 0; i < profiles.length(); i++) {
                                User.parseUser(profiles.getJSONObject(i));
                            }
                        }
                        if (response.has("groups")) {
                            JSONArray groups = response.getJSONArray("groups");
                            for (int i = 0; i < groups.length(); i++) {
                                Group.parseGroup(groups.getJSONObject(i));
                            }
                        }

                        for (int pr = 0; pr < items.length(); pr++) {
                            JSONObject docObject = items.getJSONObject(pr);
                            Notification notification = Notification.parseNotification(docObject);
                            notifications.add(notification);
                        }

                        notificationsAdapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);

                        progressBar.setVisibility(View.GONE);
                        rootLV.setVisibility(View.VISIBLE);
                    } else {
                        if (notifications.size() == 0) {
                            empty.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    if (notifications.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        empty.setText(R.string.loading_error);
                        progressBar.setVisibility(View.GONE);
                    }
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        notifications.clear();
        previousLastPosition = 0;
        lastPosition = 0;
        next_from = "";
        addNotifications();
    }

    private static class NotificationsAdapter extends ArrayAdapter<Notification> {

        private ArrayList<Notification> objects;
        private Activity ctx;


        NotificationsAdapter(Activity context, ArrayList<Notification> notifications) {
            super(context, 0, notifications);
            ctx = context;
            objects = notifications;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            NotificationView _view = new NotificationView(ctx);
            _view.setNotification(objects.get(position), position == getCount() - 1 );
            return _view;
        }


        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public Notification getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }
}
