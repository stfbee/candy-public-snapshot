package ru.kadabeld.candy.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AboutActivity;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.BuildConfig;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

/**
 * User: Vlad
 * Date: 09.03.2015
 * Time: 15:06
 */
public class AboutFragment extends Fragment {
    private static final String TAG = AboutFragment.class.getSimpleName();
    View rootView;

    private ArrayList<Donate> specials = new ArrayList<>();
    private ArrayList<Donate> donates = new ArrayList<>();
    private ArrayList<Donate> profiles = new ArrayList<>();
    private ArrayList<Donate> devs = new ArrayList<>();
    private GuysAdapter guysAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_about, container, false);
        ((AboutActivity) getActivity()).makeActionBar(rootView);
        TextView logo = (TextView) rootView.findViewById(R.id.logo);
        TextView version = (TextView) rootView.findViewById(R.id.version_text);
        final TextView build = (TextView) rootView.findViewById(R.id.build_text);
        TextView build_number = (TextView) rootView.findViewById(R.id.build_number_text);
        Button devs_button = (Button) rootView.findViewById(R.id.devs_button);
        Button greetings_button = (Button) rootView.findViewById(R.id.thanks_button);
        Button legacy_button = (Button) rootView.findViewById(R.id.legacy_button);

        String v = BuildConfig.VERSION_NAME;

        devs_button.setOnClickListener(v1 -> showDialog(PageType.DEV));

        greetings_button.setOnClickListener(v1 -> showDialog(PageType.DONATE));

        legacy_button.setOnClickListener(v1 -> ((AbstractActivity) getActivity()).openFragment(new LicensesFragment()));

        if (BuildConfig.DEBUG) {
            build.setText(R.string.settings_build_type_dev);
        } else {
            build.setText(R.string.settings_build_type_public);
        }

        if (v.contains("Night")) {
            build.setText(R.string.settings_build_type_night);
        }

        build_number.setText(v.split("\\(")[1].replace(")", "").replace("Night", ""));
        version.setText(v.split("\\(")[0].trim());

        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "candy_font.ttf");
        logo.setTypeface(type);
        logo.setText(String.format(" %s ", getString(R.string.app_name)));

        return rootView;
    }

    private void showDialog(PageType type) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());

        switch (type) {
            case DEV:
                builderSingle.setTitle(getString(R.string.settings_developers));
                guysAdapter = new GuysAdapter(getActivity(), devs);
                devs.clear();
                devs.add(new Donate(8043960, "Пили @ Употребляй")); //vlad
                devs.add(new Donate(41053069, "Оптимизируй @ Вырезай")); //artem
                devs.add(new Donate(61351294, "Бэкенд")); //kotyah
                devs.add(new Donate(86185582, "А у меня нет компа :с")); //romik
                devs.add(new Donate(138848209, "Кто все эти люди?")); //andrey

                addDevelopers();
                guysAdapter.notifyDataSetChanged();

                break;
            case DONATE:
                builderSingle.setTitle(getString(R.string.settings_thanks));
                guysAdapter = new GuysAdapter(getActivity(), profiles);

                addDonors();
                guysAdapter.notifyDataSetChanged();
                break;
        }

        builderSingle.setAdapter(guysAdapter, null);

        builderSingle.setNegativeButton(
                R.string.ok,
                (dialog, which) -> {
                    dialog.dismiss();
                });

        builderSingle.show();
    }

    private void addDonors() {
        CandyApplication.client.get("http://kadabeld.ru/donors.php", new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray donorsarray = response.getJSONArray("donors");
                    JSONArray specialarray = response.getJSONArray("special");

                    StringBuilder users = new StringBuilder();
                    for (int i = 0; i < donorsarray.length(); i++) {
                        JSONObject user = donorsarray.getJSONObject(i);
                        users.append(user.getString("vkid")).append(",");

                        Donate donate = new Donate();
                        donate.id = Long.valueOf(user.getString("vkid"));
                        donate.text = user.getString("thankyou");
                        donate.total = user.getDouble("total");
                        donates.add(donate);
                    }
                    for (int i = 0; i < specialarray.length(); i++) {
                        JSONObject user = specialarray.getJSONObject(i);
                        users.append(user.getString("vkid")).append(",");

                        Donate special = new Donate();
                        special.id = Long.valueOf(user.getString("vkid"));
                        special.text = user.getString("thankyou");
                        special.special = true;
                        specials.add(special);
                    }

                    RequestParams params = new RequestParams();
                    params.put("fields", API.userFields);
                    params.put("user_ids", users.toString());
                    params.put("v", API.version);
                    params.put("access_token", CandyApplication.access_token());
                    params.put("lang", CandyApplication.lang);
                    CandyApplication.client.get(API.BASE() + API.usersGet, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                            try {
                                JSONArray items = object.getJSONArray("response");
                                for (int pr = 0; pr < items.length(); pr++) {
                                    JSONObject profileObject = items.getJSONObject(pr);
                                    User.parseUser(profileObject);
                                }

                                profiles.addAll(specials);
                                profiles.addAll(donates);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            guysAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            Log.e(TAG, "fail: " + responseString);
                            guysAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void addDevelopers() {
        StringBuilder ids = new StringBuilder();
        for (Donate dev : devs) {
            ids.append(dev.id).append(", ");
        }

        RequestParams params = new RequestParams();
        params.put("fields", API.userFieldsLight);
        params.put("user_ids", ids.toString());
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.usersGet, params,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                        try {
                            JSONArray items = object.getJSONArray("response");
                            for (int pr = 0; pr < items.length(); pr++) {
                                JSONObject profileObject = items.getJSONObject(pr);
                                User.parseUser(profileObject);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        guysAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Log.e(TAG, "fail: " + responseString);
                        guysAdapter.notifyDataSetChanged();
                    }
                });
    }

    private class GuysAdapter extends ArrayAdapter<Donate> {

        private ArrayList<Donate> objects;
        private Picasso picasso;
        private Activity ctx;
        private LayoutInflater lInflater;


        public GuysAdapter(Activity context, ArrayList<Donate> profiles) {
            super(context, R.layout.list_item_cool_guy, profiles);
            ctx = context;
            objects = profiles;
            lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            picasso = Picasso.with(ctx);
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            v = lInflater.inflate(R.layout.list_item_cool_guy, null, false);

            final ImageView avatar = (ImageView) v.findViewById(R.id.avatar);
            final TextView name = (TextView) v.findViewById(R.id.name);
            final TextView cool_text = (TextView) v.findViewById(R.id.cool_text);
            final View hr = v.findViewById(R.id.hr);


            long id = getItem(position).id;
            String string = objects.get(position).text;
            double total = objects.get(position).total;
            boolean special = objects.get(position).special;

            if (special) {
                cool_text.setVisibility(View.VISIBLE);
                cool_text.setText(string);
            } else {
                cool_text.setVisibility(View.VISIBLE);
                String text = total + "\u20BD";
                if (!string.isEmpty()) {
                    text = text + ", " + string;
                }
                cool_text.setText(text);
            }


            final User profile = (User) Cache.getAbstractProfile(id);

            if (profile != null) {
                avatar.setImageDrawable(profile.getAvatarDrawable());

                hr.setVisibility(position == getCount() - 1 ? View.GONE : View.VISIBLE);

                if (!profile.isAvatarDefault()) {
                    picasso.load(profile.getPhotoFor(200)).tag("friends").transform(new CropCircleTransformation()).into(avatar);
                }

                name.setText(profile.getFullName());
            }


            return v;
        }


        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public Donate getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    private class Donate {

        public Donate(long id, String text) {
            this.text = text;
            this.id = id;
            special = true;
        }

        public Donate() {
        }

        String text = "";
        long id = 0;
        double total = 0d;
        boolean special = false;
    }

    private enum PageType {DEV, DONATE}
}
