package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.VideosAdapter;
import ru.kadabeld.candy.entities.Video;
import ru.kadabeld.candy.views.ListView;

@SuppressWarnings("deprecation")
public class VideosFragment extends Fragment implements SearchView.OnQueryTextListener {
    private ListView rootLV;
    private ArrayList<Video> videos = new ArrayList<>();
    private VideosAdapter videosAdapter;
    private ProgressBar progressBar;
    private TextView empty;

    private long user_id = CandyApplication.current().getUserId();
    private int offset = 0;
    private String query;
    private boolean isSearch = false;
    private boolean isLoading = false;
    private long lastSearchTime = 0;
    private boolean hd_qual = false;
    private boolean adult = true;
    private boolean only_vk = false;

    private int colorPrimary = 0;
    private boolean canColorize;
    private boolean colorized = false;


    public void init(long id, int color) {
        user_id = id;
        colorPrimary = color;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_videos, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                isSearch = true;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                isSearch = false;
                videos.clear();
                offset = 0;
                loadVideos();
                return true;
            }
        });

        searchView.setOnQueryTextListener(this);
        final MenuItem filter = menu.findItem(R.id.filter);
        filter.setOnMenuItemClickListener(item -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            TextView durationTV = new TextView(getActivity());
            durationTV.setText(getResources().getString(R.string.duration));

            Spinner duration = new Spinner(getActivity());
            duration.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            ArrayAdapter<String> durationAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.spinner_duration));
            duration.setAdapter(durationAdapter);

            TextView sortTV = new TextView(getActivity());
            sortTV.setText(getResources().getString(R.string.sort));

            Spinner sort = new Spinner(getActivity());
            sort.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            ArrayAdapter<String> sortAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.spinner_sort));
            sort.setAdapter(sortAdapter);
            CheckBox hd = new CheckBox(getActivity());
            hd.setOnCheckedChangeListener((buttonView, isChecked) -> hd_qual = isChecked);
            hd.setChecked(hd_qual);
            hd.setText(getResources().getString(R.string.video_highquality));

            CheckBox adult1 = new CheckBox(getActivity());
            adult1.setChecked(VideosFragment.this.adult);
            adult1.setOnCheckedChangeListener((buttonView, isChecked) -> VideosFragment.this.adult = isChecked);
            adult1.setText(getResources().getString(R.string.safe_search));

            CheckBox onlyvk = new CheckBox(getActivity());
            onlyvk.setChecked(only_vk);
            onlyvk.setOnCheckedChangeListener((buttonView, isChecked) -> only_vk = isChecked);
            onlyvk.setText(getResources().getString(R.string.video_vkonly));

            LinearLayout filterLayout = new LinearLayout(getActivity());
            filterLayout.setOrientation(LinearLayout.VERTICAL);
            if (isSearch) {
                filterLayout.addView(durationTV);
                filterLayout.addView(duration);
                filterLayout.addView(sortTV);
                filterLayout.addView(sort);
                filterLayout.addView(hd);
                filterLayout.addView(adult1);
            }
            //TODO hd for !isSearch
            filterLayout.addView(onlyvk);
            filterLayout.setPadding(20, 20, 20, 20);

            builder.setView(filterLayout);
            builder.setNegativeButton(getResources().getString(R.string.cancel), (dialog, which) -> dialog.cancel());
            builder.setPositiveButton("OK", (dialog, which) -> {
                progressBar.setVisibility(View.VISIBLE);
                rootLV.setVisibility(View.GONE);
                videos.clear();
                offset = 0;
                isLoading = true;
                if (isSearch) {
                    searchVideos(query);
                } else {
                    loadVideos();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
            return true;
        });
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout rootView = (RelativeLayout) inflater.inflate(R.layout.fragment_videos, container, false);
        ((AbstractActivity) getActivity()).makeActionBar(rootView);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);
        rootLV = (ListView) rootView.findViewById(R.id.listview_parallax);

        setHasOptionsMenu(true);
        videosAdapter = new VideosAdapter(getActivity(), videos);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) supportActionBar.setTitle(getString(R.string.drawer_videos));

        rootLV.setAdapter(videosAdapter);
        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            int previousLastPosition = 0;

            void resetLastIndex() {
                previousLastPosition = 0;
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        if (offset != 0 && !isLoading) {
                            if (isSearch) {
                                searchVideos(query);
                            } else {
                                loadVideos();
                            }
                        }
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });

        loadVideos();

        canColorize = colorPrimary != 0;
        if (canColorize) {
            colorize();
            colorized = true;
        }

        return rootView;
    }

    private void addVideos(String responseString) {
        empty.setVisibility(View.GONE);
        try {
            JSONObject object = new JSONObject(responseString);
            JSONObject responseObject = object.getJSONObject("response");

            if (responseObject.getInt("count") > 0) {
                JSONArray items = responseObject.getJSONArray("items");
                for (int i = 0; i < items.length(); i++) {
                    JSONObject item = items.getJSONObject(i);
                    Video vid = Video.parseVideo(item);

                    //TODO: add filter non-vk video
                    if (only_vk && vid.getExternal().isEmpty()) {
                        videos.add(vid);
                    } else if (!only_vk) {
                        videos.add(vid);
                    }
                }
                progressBar.setVisibility(View.GONE);
                rootLV.setVisibility(View.VISIBLE);
                offset = videos.size();
                videosAdapter.notifyDataSetChanged();
                isLoading = false;
            } else {
                if (videos.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            isLoading = false;

            if (videos.size() == 0) {
                empty.setVisibility(View.VISIBLE);
                empty.setText(R.string.loading_error);
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    private void loadVideos() {
        if (videos.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        isSearch = false;
        RequestParams params = new RequestParams();
        params.put("owner_id", user_id);
        params.put("count", 10);
        params.put("v", API.version);
        params.put("offset", offset);
        params.put("extended", 1);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.videoGet, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (videos.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                addVideos(responseString);
            }
        });
    }

    private void searchVideos(String query) {
        RequestParams params = new RequestParams();
        this.query = query;
        params.put("q", query);
        int adult = (this.adult) ? 0 : 1;
        params.put("adult", adult);
        int hd = (this.hd_qual) ? 1 : 0;
        params.put("hd", hd);
        params.put("count", 10);
        params.put("v", API.version);
        params.put("offset", offset);
        params.put("extended", "1");
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.videoSearch, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (videos.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                addVideos(responseString);
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        long actualSearchTime = (Calendar.getInstance()).getTimeInMillis();

        if (actualSearchTime > lastSearchTime + 1000) {
            lastSearchTime = actualSearchTime;
            System.out.println("onQuery");
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
            videos.clear();
            offset = 0;
            isLoading = true;
            searchVideos(s);
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (colorized && canColorize) {
            colorize();
        }
    }

    private void colorize() {
        ((AbstractActivity) getActivity()).colorize(colorPrimary);
    }

}
