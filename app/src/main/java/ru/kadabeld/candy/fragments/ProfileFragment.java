package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TableRow;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.PeriodFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.PostAdapter;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.utils.ApiUtils;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.NumberUtils;
import ru.kadabeld.candy.utils.parsers.Posts;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;
import ru.kadabeld.candy.utils.transformations.PaletteTransformation;
import ru.kadabeld.candy.utils.transformations.StackBlurTransformation;
import ru.kadabeld.candy.views.ColorCircleCounter;
import ru.kadabeld.candy.views.ListView;

@SuppressWarnings("deprecation")
public class ProfileFragment extends Fragment /*implements SwipeRefreshLayout.OnRefreshListener*/ {
    private static final String TAG = ProfileFragment.class.getSimpleName();
    private final ArrayList<Post> wall = new ArrayList<>();
    private RelativeLayout rootLayout;
    private ListView wallView;
    private CoordinatorLayout profile_header;
    private PostAdapter adapter;
    private long id = 0;
    private LayoutInflater inflater;
    private boolean isGroup = false;
    private AbstractProfile profile;
    private TabHost tabs;
    private ProgressBar progress_bar;
    private ImageView cover;
    private int colorPrimary = ColorUtils.getThemeColor(R.attr.colorPrimary);
    private int offset = 0;
    private SharedPreferences sPref;
    private String first_name_gen = "";
    private JSONObject finalCountersObject;
    private AbstractActivity ctx;
    private LinearLayout counters;
    private EditText status_edit;
    private EditText status_audio_edit;
    private EditText note_edit;
    private View note_hr;
    private LinearLayout note_view;
    private ImageView avatar;
    private TextView name;
    private TextView empty_wall;
    private boolean colorized = false;
    private int toolBarAlpha;
    private LinearLayout status_row;
    private LinearLayout status_audio_row;

    private SwipeRefreshLayout refresh;

    private FloatingActionButton join_button;
    private FloatingActionButton add_user_button;
    private FloatingActionButton message_button;

    private TableRow birthday_row;
    private TextView birthday;
    private TableRow city_row;
    private TextView city;
    private TableRow work_row;
    private TextView work;
    private TableRow school_row;
    private TextView school;
    private TableRow lang_row;
    private TextView lang;
    private TableRow desc_row;
    private TableRow desc_row2;
    private TextView desc;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.inflater = inflater;
        ctx = (AbstractActivity) getActivity();
        sPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (id < 0) {
            isGroup = true;
        }

        initNotUI();
        preInitUI();
        initUI();
        initProfile();

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(getResources().getString(isGroup ? R.string.group : R.string.profile));

        return rootLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (colorized) {
            colorize();
        }
    }

    private void initNotUI() {
        colorPrimary = ColorUtils.getThemeColor(R.attr.colorPrimary);
        adapter = new PostAdapter(getActivity(), wall, PostAdapter.ViewType.FEED);
    }

    @SuppressLint("InflateParams")
    private void preInitUI() {
        rootLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_profile, null, false);
        ctx.makeActionBar(rootLayout);

        ctx.colorize(colorPrimary, toolBarAlpha);

        progress_bar = (ProgressBar) rootLayout.findViewById(R.id.progress_bar);
        wallView = (ListView) rootLayout.findViewById(R.id.listview_parallax);
        profile_header = (CoordinatorLayout) inflater.inflate(R.layout.fragment_profile_header, wallView, false);
        tabs = (TabHost) profile_header.findViewById(android.R.id.tabhost);
        empty_wall = (TextView) profile_header.findViewById(R.id.empty_wall);
        wallView.addHeaderView(profile_header);
        wallView.setAdapter(adapter);

        refresh = (SwipeRefreshLayout) rootLayout.findViewById(R.id.refresh);
        refresh.setEnabled(false); // TODO: 08.04.2016 VOnishchenko

        cover = (ImageView) profile_header.findViewById(R.id.cover);
        wallView.addParallaxedView(cover);
        name = (TextView) profile_header.findViewById(R.id.name);
        avatar = (ImageView) profile_header.findViewById(R.id.avatar);
        status_edit = (EditText) profile_header.findViewById(R.id.status_edit);
        status_audio_edit = (EditText) profile_header.findViewById(R.id.status_audio_edit);
        note_hr = profile_header.findViewById(R.id.note_hr);
        note_edit = (EditText) profile_header.findViewById(R.id.note_edit);
        note_view = (LinearLayout) profile_header.findViewById(R.id.note_view);
        status_row = (LinearLayout) profile_header.findViewById(R.id.status_row);
        status_audio_row = (LinearLayout) profile_header.findViewById(R.id.status_audio_row);

        join_button = (FloatingActionButton) profile_header.findViewById(R.id.join_button);
        add_user_button = (FloatingActionButton) profile_header.findViewById(R.id.add_user_button);
        message_button = (FloatingActionButton) profile_header.findViewById(R.id.message_button);

        birthday_row = (TableRow) profile_header.findViewById(R.id.birthday_row);
        birthday = (TextView) profile_header.findViewById(R.id.birthday);
        city_row = (TableRow) profile_header.findViewById(R.id.city_row);
        city = (TextView) profile_header.findViewById(R.id.city);
        work_row = (TableRow) profile_header.findViewById(R.id.work_row);
        work = (TextView) profile_header.findViewById(R.id.work);
        school_row = (TableRow) profile_header.findViewById(R.id.school_row);
        school = (TextView) profile_header.findViewById(R.id.school);
        lang_row = (TableRow) profile_header.findViewById(R.id.lang_row);
        lang = (TextView) profile_header.findViewById(R.id.lang);
        desc_row = (TableRow) profile_header.findViewById(R.id.desc_row);
        desc_row2 = (TableRow) profile_header.findViewById(R.id.desc_row2);
        desc = (TextView) profile_header.findViewById(R.id.desc);
    }

    private void initUI() {
        wallView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int previousLastPosition = 0;

            void resetLastIndex() {
                previousLastPosition = 0;
            }

            int getScrollY() {
                View c = wallView.getChildAt(0);
                if (c == null) {
                    return 0;
                }

                int firstVisiblePosition = wallView.getFirstVisiblePosition();
                int top = c.getTop();

                int headerHeight = 0;
                if (firstVisiblePosition >= 1) {
                    headerHeight = profile_header.getHeight();
                }

                return -top + firstVisiblePosition * c.getHeight() + headerHeight;
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
                final Picasso picasso = Picasso.with(getActivity());
                if (scrollState == SCROLL_STATE_IDLE) {
                    picasso.resumeTag("news");
                } else {
                    picasso.pauseTag("news");
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int scrollY = getScrollY();
                if (scrollY >= 255) {
                    toolBarAlpha = 255;
                } else if (scrollY <= 0) {
                    toolBarAlpha = 0;
                } else {
                    toolBarAlpha = scrollY;
                }

                ((AbstractActivity) getActivity()).colorize(colorPrimary, toolBarAlpha);

                boolean enable = false;
                if (wallView != null && wallView.getChildCount() >= 0) {
                    // check if the first item of the list is visible
                    boolean firstItemVisible = wallView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = wallView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                rootLayout.setEnabled(enable);
                final int lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        getActivity().runOnUiThread(() -> {
                            if (profile.getDeactivated().equals(AbstractProfile.Deactivated.NONE))
                                addToWall();
                        });
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });
    }

    private void initProfile() {
        RequestParams params = new RequestParams();
        String request;
        if (isGroup || id < 0) {
            params.put("group_id", -id);
            isGroup = true;
            request = API.executeGetFullGroupNew;
        } else {
            params.put("user_id", id);
            request = API.executeGetFullProfileNew;
        }
        params.put("v", API.version);
        params.put("lang", CandyApplication.lang);
        params.put("access_token", CandyApplication.access_token());

        counters = (LinearLayout) profile_header.findViewById(R.id.counters);


        final String back = sPref.getString("background", "blur");

        CandyApplication.client.get(API.BASE() + request, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    ApiUtils.ErrorType errorType = ApiUtils.parseError(object);
                    if (errorType != null) {
                        // TODO: 18.03.2016 VOnishchenko отобразить ошибку в UI или
                        Log.e(TAG, "что-то не так в ответе: " + errorType);
                        if (errorType == ApiUtils.ErrorType.TOO_MANY_REQUESTS) {
                            Thread.sleep(1000);
                            initProfile();
                        }
                        return;
                    }

                    JSONObject response = object.getJSONObject("response");
                    if (isGroup) {
                        profile = Group.parseGroup(response);
                    } else {
                        profile = User.parseUser(response);
                    }
                    name.setText(profile.getFullName());
                    status_edit.setTypeface(CandyApplication.light);
                    note_edit.setTypeface(CandyApplication.light);

                    JSONObject countersObject = null;
                    if (profile.getDeactivated().equals(AbstractProfile.Deactivated.NONE)) {
                        try {
                            countersObject = response.getJSONObject("counters");
                        } catch (JSONException ignored) {
                        }
                    }
                    finalCountersObject = countersObject;

                    Callback googlenow = new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap bitmap = ((BitmapDrawable) cover.getDrawable()).getBitmap();
                            Palette p = PaletteTransformation.getPalette(bitmap);
                            colorPrimary = p.getVibrantColor(Color.BLACK);
                            if (colorPrimary == Color.BLACK) {
                                colorPrimary = p.getMutedColor(Color.BLACK);
                            }

                            if (profile.isAvatarDefault()) {
                                avatar.setImageDrawable(profile.getAvatarDrawable(colorPrimary));
                            }
                            afterCallbackWork();
                        }

                        @Override
                        public void onError() {
                        }
                    };

                    Callback blur = new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap bitmap = ((BitmapDrawable) avatar.getDrawable()).getBitmap();
                            Palette p = PaletteTransformation.getPalette(bitmap);
                            colorPrimary = p.getVibrantColor(Color.BLACK);
                            if (colorPrimary == Color.BLACK) {
                                colorPrimary = p.getMutedColor(Color.BLACK);
                            }

                            if (profile.isAvatarDefault()) {
                                avatar.setImageDrawable(profile.getAvatarDrawable(colorPrimary));
                            }
                            afterCallbackWork();
                        }

                        @Override
                        public void onError() {
                        }
                    };

                    LinearLayout deactivated = (LinearLayout) profile_header.findViewById(R.id.deactivated);
                    if (!profile.getDeactivated().equals(AbstractProfile.Deactivated.NONE)) {
                        String reason = response.getString("deactivated");
                        counters.setVisibility(View.GONE);
                        TextView deactivated_text = (TextView) profile_header.findViewById(R.id.deactivated_text);
                        deactivated.setVisibility(View.VISIBLE);
                        if (reason.contains("deleted")) {
                            if (isGroup) {
                                deactivated_text.setText(getActivity().getResources().getString(R.string.profile_deleted_group));
                            } else {
                                String first_name = profile.getName();
                                if (((User) profile).getSex().equals(User.Sex.FEMALE)) {
                                    deactivated_text.setText(String.format(getActivity().getResources().getString(R.string.profile_deleted_female), first_name));
                                } else {
                                    deactivated_text.setText(String.format(getActivity().getResources().getString(R.string.profile_deleted_male), first_name));
                                }
                            }
                        } else if (reason.contains("banned")) {
                            if (isGroup) {
                                deactivated_text.setText(getActivity().getResources().getString(R.string.profile_banned_group));
                            } else {
                                first_name_gen = profile.getName(User.NameCase.GEN);
                                deactivated_text.setText(String.format(getActivity().getResources().getString(R.string.profile_banned), first_name_gen));
                            }
                        }
                    } else {
                        if (response.has("first_name_gen")) {
                            first_name_gen = response.getString("first_name_gen");
                        }
                        deactivated.setVisibility(View.GONE);
                        counters.setVisibility(View.VISIBLE);
                    }


                    if (!profile.isAvatarDefault()) {
                        if (back.equals("googlenow") || profile.getPhotoFor(200).contains("deactivated")) {
                            Picasso.with(ctx).load(profile.getPhotoFor(200)).transform(new CropCircleTransformation()).resize(Global.dp(110), Global.dp(110)).transform(PaletteTransformation.instance()).into(avatar);
                            Picasso.with(ctx).load(Global.getHeader(ctx)).transform(PaletteTransformation.instance()).into(cover, googlenow);
                        } else if (back.equals("blur")) {
                            Picasso.with(ctx).load(profile.getPhotoFor(200)).transform(new CropCircleTransformation()).resize(Global.dp(110), Global.dp(110)).transform(PaletteTransformation.instance()).into(avatar, blur);
                            Picasso.with(ctx).load(profile.getPhotoFor(200)).transform(new StackBlurTransformation(7)).into(cover);
                        }
                    } else {
                        addCounters(countersObject, counters);
                        avatar.setImageDrawable(profile.getAvatarDrawable(colorPrimary));
                        Picasso.with(ctx).load(Global.getHeader(ctx)).transform(PaletteTransformation.instance()).into(cover, googlenow);
                    }

                    message_button.setVisibility(View.GONE);
                    add_user_button.setVisibility(View.GONE);
                    join_button.setVisibility(View.GONE);

                    if (profile instanceof Group) {
                        if (((Group) profile).isCan_message()) {
                            message_button.setVisibility(View.VISIBLE);
                        }
                        join_button.setVisibility(((Group) profile).is_member() ? View.GONE : View.VISIBLE);
                    } else if (profile instanceof User) {
                        if (((User) profile).isCan_write_private_message()) {
                            message_button.setVisibility(View.VISIBLE);
                        }
                        add_user_button.setVisibility(((User) profile).getFriend_status() == User.FriendStatus.YEP ? View.GONE : View.VISIBLE);
                    }

                    if (profile.getId() == CandyApplication.current().getUserId()) {
                        add_user_button.setVisibility(View.GONE);
                    }


//status

                    String status = profile.getStatus();
                    if (status.isEmpty()) {
                        if (id == CandyApplication.current().getUserId()) {
                            status_row.setVisibility(View.VISIBLE);
                            status_edit.setText(getResources().getString(R.string.set_status));
                        } else {
                            status_row.setVisibility(View.GONE);
                        }
                    } else {
                        status_row.setVisibility(View.VISIBLE);
                        status_edit.setText(status);
                    }

                    String title = profile.getStatus_audio().getTitle();
                    if (!title.isEmpty()) {
                        status_row.setVisibility(View.GONE);
                        status_audio_row.setVisibility(View.VISIBLE);
                        status_audio_edit.setText(title);
                    }

                    if (profile.getId() == CandyApplication.current().getUserId() || (isGroup && ((Group) profile).is_admin())) {
                        status_edit.setOnEditorActionListener((v, actionId, event) -> {
                            if (actionId == EditorInfo.IME_ACTION_DONE) {

                                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

                                String status1 = status_edit.getText().toString();
                                profile.setStatus(status1);

                                RequestParams params1 = new RequestParams();
                                params1.put("text", status1);
                                if (isGroup)
                                    params1.put("group_id", -profile.getId());
                                params1.put("v", API.version);
                                params1.put("access_token", CandyApplication.access_token());
                                params1.put("lang", CandyApplication.lang);
                                CandyApplication.client.post(API.BASE() + API.statusSet, params1, new JsonHttpResponseHandler());

                                return true;
                            }
                            return false;
                        });
                    } else {
                        status_edit.setEnabled(false);
                    }

//note
                    if (sPref.getBoolean("profile_notes", true)) {
                        note_view.setVisibility(View.VISIBLE);
                        note_hr.setVisibility(View.VISIBLE);

                        if (profile.getNote().isEmpty()) {
                            note_edit.setText(R.string.profile_note_template);
                        } else {
                            note_edit.setText(profile.getNote());
                        }
                        note_edit.setOnEditorActionListener((v, actionId, event) -> {
                            if (actionId == EditorInfo.IME_ACTION_DONE) {

                                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

                                String note = note_edit.getText().toString();

                                profile.setNote(note);

                                RequestParams params1 = new RequestParams();
                                params1.put("key", "note_" + profile.getId());
                                params1.put("value", note);
                                params1.put("v", API.version);
                                params1.put("access_token", CandyApplication.access_token());
                                params1.put("lang", CandyApplication.lang);
                                CandyApplication.client.post(API.BASE() + API.storageSet, params1, new JsonHttpResponseHandler());

                                return true;
                            }
                            return false;
                        });
                    } else {
                        note_view.setVisibility(View.GONE);
                        note_hr.setVisibility(View.GONE);
                    }

///

                    fillInfo();

                    message_button.setOnClickListener(v -> {
                        ChatFragment f = new ChatFragment();
                        f.init(profile.getFullName(), false, false, 0, profile.getId());
                        ((AbstractActivity) getActivity()).openFragment(f);
                    });

                    join_button.setOnClickListener(v -> {
                        RequestParams params1 = new RequestParams();
                        params1.put("group_id", -profile.getId());
                        params1.put("v", API.version);
                        params1.put("access_token", CandyApplication.access_token());
                        params1.put("lang", CandyApplication.lang);
                        CandyApplication.client.get(API.BASE() + API.groupsJoin, params1, new TextHttpResponseHandler() {
                            @Override
                            public void onFailure(int statusCode1, Header[] headers1, String responseString, Throwable throwable) {
                            }

                            @Override
                            public void onSuccess(int statusCode1, Header[] headers1, String responseString) {

                            }
                        });
                    });

                    add_user_button.setOnClickListener(v -> {
                        RequestParams params1 = new RequestParams();
                        params1.put("user_id", profile.getId());
                        params1.put("text", "");
                        params1.put("v", API.version);
                        params1.put("access_token", CandyApplication.access_token());
                        params1.put("lang", CandyApplication.lang);
                        CandyApplication.client.get(API.BASE() + API.friendsAdd, params1, new TextHttpResponseHandler() {
                            @Override
                            public void onFailure(int statusCode1, Header[] headers1, String responseString, Throwable throwable) {
                            }

                            @Override
                            public void onSuccess(int statusCode1, Header[] headers1, String responseString) {
                              /*Результат
                                После успешного выполнения возвращает одно из следующих значений:
                                1 — заявка на добавление данного пользователя в друзья отправлена;
                                2 — заявка на добавление в друзья от данного пользователя одобрена;
                                4 — повторная отправка заявки.*/
                            }
                        });
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println(object);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void fillInfo() {
        if (!isGroup) {
            User user = ((User) profile);
            String bdate = user.getBdate();
            if (bdate.isEmpty()) {
                birthday_row.setVisibility(View.GONE);
            } else {
                birthday_row.setVisibility(View.VISIBLE);
                try {
                    LocalDate bdate1 = DateTimeFormat.forPattern("dd.MM.yyyy").parseLocalDate(bdate);
                    String _date = bdate1.toString("d MMMM yyyy");

                    Years years = Years.yearsBetween(bdate1.toDateTimeAtStartOfDay(), DateTime.now());
                    _date = _date + " (" + PeriodFormat.getDefault().withLocale(Locale.getDefault()).print(years) + ")";

                    birthday.setText(_date);
                } catch (IllegalArgumentException e) {
                    try {
                        LocalDate bdate2 = DateTimeFormat.forPattern("dd.MM").parseLocalDate(bdate);
                        String _date = bdate2.toString("d MMMM");
                        birthday.setText(_date);
                    } catch (IllegalArgumentException e2) {
                        e2.printStackTrace();
                        birthday.setText(bdate);
                    }
                }
            }

            if (user.getCity().getTitle().isEmpty()) {
                city_row.setVisibility(View.GONE);
            } else {
                city_row.setVisibility(View.VISIBLE);
                city.setText(user.getCity().getTitle());
            }

            switch (user.getOccupation().getType()) {
                case WORK:
                    school_row.setVisibility(View.GONE);
                    work_row.setVisibility(View.VISIBLE);
                    work.setText(user.getOccupation().getName());
                    break;
                case SCHOOL:
                case UNIVERSITY:
                    work_row.setVisibility(View.GONE);
                    school_row.setVisibility(View.VISIBLE);
                    school.setText(user.getOccupation().getName());
                    break;
                default:
                case NOPE:
                    work_row.setVisibility(View.GONE);
                    school_row.setVisibility(View.GONE);
            }

            ArrayList<String> langs = user.getPersonal().getLangs();
            if (langs.isEmpty()) {
                lang_row.setVisibility(View.GONE);
            } else {
                lang_row.setVisibility(View.VISIBLE);
                lang.setText(langs.toString().replace("[", "").replace("]", "").trim());
            }
        } else {
            Group group = (Group) profile;
            String description = group.getDescription();
            if (description.isEmpty()) {
                desc_row.setVisibility(View.GONE);
                desc_row2.setVisibility(View.GONE);
            } else {
                desc_row.setVisibility(View.VISIBLE);
                desc_row2.setVisibility(View.VISIBLE);
                desc.setText(description);
            }
        }
    }

    private void afterCallbackWork() {
        colorized = true;
        colorize();

        try {
            addCounters(finalCountersObject, counters);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (this.isAdded()) {
            initTabs();
            refresh.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.GONE);
        }
    }

    private void colorize() {
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity == null) return;

        activity.colorize(colorPrimary);

        adapter.setColors(colorPrimary);

        join_button.setBackgroundTintList(ColorStateList.valueOf(colorPrimary));
        add_user_button.setBackgroundTintList(ColorStateList.valueOf(colorPrimary));
        message_button.setBackgroundTintList(ColorStateList.valueOf(colorPrimary));
    }

    private void initTabs() {
        tabs.setup();
        TabWidget tabWidget = tabs.getTabWidget();
        tabWidget.setStripEnabled(true);
        tabWidget.setDividerDrawable(R.drawable.transparent);
        tabWidget.setLeftStripDrawable(R.drawable.transparent);
        tabWidget.setRightStripDrawable(R.drawable.transparent);
        TabHost.TabSpec all = tabs.newTabSpec("all");
        TabHost.TabSpec owner = tabs.newTabSpec("owner");
        all.setContent(new EmptyTabFactory());
        owner.setContent(new EmptyTabFactory());
        all.setIndicator(getResources().getString(R.string.all_posts));
        owner.setIndicator(isGroup ? getResources().getString(R.string.community_posts) : String.format(getResources().getString(R.string.user_posts), first_name_gen));
        tabs.addTab(all);
        tabs.addTab(owner);

        for (int i = 0; i < tabWidget.getChildCount(); i++) {
            Resources r = getResources();
            LayerDrawable layerDrawable = (LayerDrawable) r.getDrawable(R.drawable.tab_bar_background_selected);
            if (layerDrawable != null) {
                GradientDrawable gradientDrawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(R.id.grishka_loh);
                gradientDrawable.setStroke(Global.dp(2), colorPrimary);
            }

            StateListDrawable states = new StateListDrawable();
            states.addState(new int[]{android.R.attr.state_selected}, layerDrawable);
            states.addState(new int[]{-android.R.attr.state_selected}, r.getDrawable(R.color.transparent));
            states.addState(new int[]{android.R.attr.state_pressed}, r.getDrawable(R.color.transparent));
            states.addState(new int[0], r.getDrawable(R.color.transparent));
            tabWidget.getChildAt(i).setBackground(states);
            ((TextView) tabWidget.getChildAt(i).findViewById(android.R.id.title)).setTextColor(colorPrimary);
        }
        tabs.setCurrentTab(0);
    }

    private void addCounters(JSONObject countersObject, LinearLayout counters) throws JSONException {
        counters.removeAllViews();
        String[] names;
        String[] getNames;
        TypedArray icons;
        try {
            if (id != CandyApplication.current().getUserId()) {
                if (isGroup) {
                    names = new String[]{"members", "topics", "docs", "photos", "videos", "audios"};
                    getNames = new String[]{getResources().getString(R.string.members), getResources().getString(R.string.topics), getResources().getString(R.string.docs), getResources().getString(R.string.photos), getResources().getString(R.string.videos), getResources().getString(R.string.audios)};
                    icons = getResources().obtainTypedArray(R.array.groupIcons);
                } else {
                    names = new String[]{"friends", "mutual_friends", "followers", "photos", "videos", "audios", "groups"};
                    getNames = new String[]{getResources().getString(R.string.friends), getResources().getString(R.string.mutual_friends), getResources().getString(R.string.followers), getResources().getString(R.string.photos), getResources().getString(R.string.videos), getResources().getString(R.string.audios), getResources().getString(R.string.groups)};
                    icons = getResources().obtainTypedArray(R.array.profileIcons);
                }
            } else {
                names = new String[]{"friends", "followers", "groups", "photos", "videos", "audios"};
                getNames = new String[]{getResources().getString(R.string.friends), getResources().getString(R.string.followers), getResources().getString(R.string.groups), getResources().getString(R.string.photos), getResources().getString(R.string.videos), getResources().getString(R.string.audios)};
                icons = getResources().obtainTypedArray(R.array.myProfileIcons);
            }
            for (int i = 0; i < names.length; i++) {
                ColorCircleCounter item = new ColorCircleCounter(getActivity());
                item.setCircleColor(colorPrimary);
                item.setVisibility(View.GONE);
                if (countersObject != null && !countersObject.isNull(names[i]) && countersObject.getInt(names[i]) > 0) {
                    item.setCount(NumberUtils.getCount(countersObject.getInt(names[i])));
                    item.setVisibility(View.VISIBLE);
                }
                switch (names[i]) {
                    case "docs":
                        item.setOnClickListener(v -> {
                            DocsFragment f = new DocsFragment();
                            f.init(id, colorPrimary);
                            ((AbstractActivity) getActivity()).openFragment(f);
                        });
                        break;
                    case "members":
                        item.setCount(NumberUtils.getCount(((Group) profile).getMembers_count()));
                        item.setVisibility(View.VISIBLE);
                        break;
                    case "friends":
                        item.setOnClickListener(v -> {
                            FriendsFragment f = new FriendsFragment();
                            f.init(id, colorPrimary, FriendsFragment.PageType.FRIENDS);
                            ((AbstractActivity) getActivity()).openFragment(f);
                        });
                        break;
                    case "mutual_friends":
                        item.setOnClickListener(v -> {
                            FriendsFragment f = new FriendsFragment();
                            f.init(id, colorPrimary, FriendsFragment.PageType.MUTUAL);
                            ((AbstractActivity) getActivity()).openFragment(f);
                        });
                        break;
                    case "groups":
                        item.setOnClickListener(v -> {
                            GroupsFragment f = new GroupsFragment();
                            f.init(id, colorPrimary);
                            ((AbstractActivity) getActivity()).openFragment(f);
                        });
                        break;
                    case "audios":
                        item.setOnClickListener(v -> {
                            AudiosFragment f = new AudiosFragment();
                            f.init(id, colorPrimary);
                            ((AbstractActivity) getActivity()).openFragment(f);
                        });
                        break;
                    case "videos":
                        item.setOnClickListener(v -> {
                            VideosFragment f = new VideosFragment();
                            f.init(id, colorPrimary);
                            ((AbstractActivity) getActivity()).openFragment(f);
                        });
                        break;
                    case "photos":
                        item.setOnClickListener(v -> {
                            PhotosFragment f = new PhotosFragment();
                            f.init(id, colorPrimary);
                            ((AbstractActivity) getActivity()).openFragment(f);
                        });
                        break;
                }
                item.setName(getNames[i]);
                item.setDrawable(icons.getDrawable(i));
                counters.addView(item);
            }
            icons.recycle();
            counters.scrollTo(0, 0);
        } catch (IllegalStateException ignored) {

        }
    }

    private void addToWall() {
        RequestParams params = new RequestParams();
        params.put("owner_id", id);
        params.put("filters", "all");
        params.put("count", 10);
        params.put("offset", offset);
        params.put("extended", 1);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.wallGet, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject object = new JSONObject(responseString);
                    JSONObject response = object.getJSONObject("response");
                    //start_from = response.getString("next_from");
                    JSONArray items = response.getJSONArray("items");
                    JSONArray groups = response.getJSONArray("groups");
                    JSONArray profiles = response.getJSONArray("profiles");
//                    if (rootLayout.isRefreshing()) {
//                        wall.clear();
//                    }
                    wall.addAll(new Posts().parseWall(items, groups, profiles, getActivity(), true));
//                    rootLayout.setRefreshing(false);
//                    isLoading = false;
                    offset = wall.size();
                    tabs.setVisibility(offset == 0 ? View.GONE : View.VISIBLE);
                    empty_wall.setVisibility(offset == 0 ? View.VISIBLE : View.GONE);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void init(long user_id) {
        id = user_id;
    }

    private class EmptyTabFactory implements TabHost.TabContentFactory {

        @Override
        public View createTabContent(String tag) {
            return new View(getActivity());
        }

    }

//    @Override
//    public void onRefresh() {
//        rootLayout.setRefreshing(true);
//        offset = 0;
//        if (profile.getDeactivated().isEmpty()) addToWall();
//    }
}
