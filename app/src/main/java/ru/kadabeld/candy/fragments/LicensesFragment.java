package ru.kadabeld.candy.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.larswerkman.licenseview.LicenseView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.R;

/**
 * User: Vlad
 * Date: 13.01.2016
 * Time: 19:52
 */

public class LicensesFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_licenses, null);
        ((AbstractActivity) getActivity()).makeActionBar(rootView);

        LicenseView licenseView = (LicenseView) rootView.findViewById(R.id.licenseview);
        try {
            licenseView.setLicenses(R.xml.licenses);
        } catch (Resources.NotFoundException | XmlPullParserException | IOException e1) {
            e1.printStackTrace();
        }

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(getString(R.string.settings_licenses));

        return rootView;
    }

}
