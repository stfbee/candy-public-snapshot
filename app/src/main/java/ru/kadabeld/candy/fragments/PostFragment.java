package ru.kadabeld.candy.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.library21.custom.SwipeRefreshLayoutBottom;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.CommentsAdapter;
import ru.kadabeld.candy.adapters.PostAdapter;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Comment;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.parsers.Comments;
import ru.kadabeld.candy.utils.parsers.Posts;
import ru.kadabeld.candy.views.MoreButton;
import ru.kadabeld.candy.views.WriteBar;

@SuppressWarnings("deprecation")
public class PostFragment extends Fragment {
    private MoreButton more;
    private SwipeRefreshLayoutBottom refresh;
    private ListView rootLV;
    private ArrayList<Comment> comments = new ArrayList<>();
    private CommentsAdapter commentsAdapter;
    private int offset = 0; //отступ с конца
    private int lastCount = 0; //сколько комментов всего было при последней загрузке
    private int count = 0; //сколько комментов всего
    private int newCount = 0; // добавлены после открытия комментов
    private long oid;
    private long pid;
    private PostAdapter adapter;
    private ArrayList<Post> posts = new ArrayList<>();
    private LinearLayout postLayout;
    private WriteBar writeBar;
    private boolean writeBarEnabled = false;
    private boolean colorized = false;
    private boolean canColorize = false;
    private int colorPrimary = 0;

    private AbstractActivity ctx;

    @Override
    public void onResume() {
        super.onResume();
        if (colorized && canColorize) {
            colorize();
        }
    }

    private void colorize() {
        ctx.colorize(colorPrimary);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ctx = (AbstractActivity) getActivity();
        View root = inflater.inflate(R.layout.fragment_post, container, false);
        ctx.makeActionBar(root);

        rootLV = (ListView) root.findViewById(R.id.listview_parallax);
        refresh = (SwipeRefreshLayoutBottom) root.findViewById(R.id.refresh);
        writeBar = (WriteBar) root.findViewById(R.id.writebar);

        refresh.setColorSchemeColors(Color.parseColor("#d18d88"), Color.parseColor("#b3655f"), Color.parseColor("#f23426"), Color.parseColor("#d23d32"));
        refresh.setEnabled(false);
        refresh.setOnRefreshListener(new SwipeRefreshLayoutBottom.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh.setRefreshing(true);
                addPost();
                offset = 0;
                addComments();
            }
        });

        more = new MoreButton(ctx);
        more.setVisibility(View.GONE);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more.setVisible(false);
                addComments();
            }
        });

        postLayout = new LinearLayout(ctx);
        rootLV.addHeaderView(postLayout);
        rootLV.addHeaderView(more);

        commentsAdapter = new CommentsAdapter(ctx, comments);
        rootLV.setAdapter(commentsAdapter);

        canColorize = colorPrimary != 0;

        adapter = new PostAdapter(ctx, posts, PostAdapter.ViewType.SINGLE);

        ActionBar supportActionBar = ctx.getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(getString(R.string.com_post_on_wall));

        addComments();
        addPost();

        if (writeBarEnabled) {
            writeBar.setVisibility(View.VISIBLE);
            writeBar.setOnSendClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO attachments (if)
                    sendComment();
                }
            });
        } else {
            writeBar.setVisibility(View.GONE);
        }

        if (canColorize) {
            colorize();
            colorized = true;
        }

        return root;
    }

    void sendComment() {
        String regex = writeBar.getEditText().getText().toString().replaceAll("\\*((?:id|club)[0-9-]+) \\(([^\\)]+)\\)", "[$1|$2]");
        AbstractProfile receiver_user;
        Comment comment = writeBar.getComment();
        if (comment != null) {
            receiver_user = Cache.getAbstractProfile(comment.getReply_to_user());

            if (regex.startsWith(receiver_user.getName() + ",")) {
                String var5 = receiver_user.getName() + ",";
                StringBuilder var6 = (new StringBuilder()).append("[");
                String var7;
                if (comment.getFrom_id() > 0) {
                    var7 = "id";
                } else {
                    var7 = "club";
                }

                regex = regex.replace(var5, var6.append(var7).append(Math.abs(comment.getFrom_id())).append("|").append(receiver_user.getName()).append("],").toString());
            }

        }

        this.sendComment(regex, null, true);
    }

    private void sendComment(final String text, String attachment, final boolean clear) {
        if (!writeBar.getEditText().getText().toString().isEmpty()) {
            String apiRequest = API.wallAddComment;
            RequestParams params = new RequestParams();
            params.put("owner_id", oid);
            params.put("post_id", pid);
            params.put("text", text);
            params.put("v", API.version);
            final Comment comment = writeBar.getComment();
            if (comment != null && comment.getReply_to_comment() != 0) {
                params.put("reply_to_comment", comment.getReply_to_comment());
            }
            params.put("access_token", CandyApplication.access_token());
            params.put("lang", CandyApplication.lang);
            writeBar.getSendButton().setClickable(false);
            CandyApplication.client.get(API.BASE() + apiRequest, params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    writeBar.getSendButton().setClickable(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    //Toast toast = Toast.makeText(getActivity(), R.string.comment_add, Toast.LENGTH_SHORT);
                    //toast.show();
                    writeBar.getEditText().getText().clear();
                    Comment comment = new Comment();
//                    comment.setFrom_user(Candy.profile);
                    comment.setText(text.replaceAll("\\[(id|club)([\\d]+)\\|([^\\]]+)\\]", "$3"));
                    //comment.setId(Global.profile.getId());
                    comment.setTime(System.currentTimeMillis() / 1000L);
                    comment.setReply_to_user(comment.getReply_to_user());
//                        comment.setReceiver_user(Cache.getAbstractProfile(writeBar.comment.getReply_to_user()));
                    comment.setReply_to_comment(comment.getReply_to_comment());
                    comments.add(comment);
                    commentsAdapter.notifyDataSetChanged();
                    refresh.setEnabled(true);
                    rootLV.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorBackground));

                    writeBar.getSendButton().setClickable(true);
                    PostFragment.this.rootLV.post(new Runnable() {
                        public void run() {
                            ctx.hideIME();
                            PostFragment.this.rootLV.postDelayed(new Runnable() {
                                public void run() {
                                    PostFragment.this.rootLV.setSelection(99999999);
                                }
                            }, 200L);
                        }
                    });
                }
            });
        }
    }

    void addPost() {
        posts.clear();
        RequestParams params = new RequestParams();
        params.put("posts", String.format("%s_%s", oid, pid));
        params.put("extended", 1);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.wallGetById, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject object = new JSONObject(responseString);
                    JSONObject response = object.getJSONObject("response");
                    JSONArray items = response.getJSONArray("items");
                    JSONArray groups = response.getJSONArray("groups");
                    JSONArray profiles = response.getJSONArray("profiles");
                    if (refresh.isRefreshing()) {
                        posts.clear();
                        postLayout.removeAllViews();
                    }
                    posts.addAll(new Posts().parsePost(items, groups, profiles, getActivity(), true));
                    adapter.notifyDataSetChanged();
                    try {
                        View item = adapter.getView(0, null, null);
                        postLayout.addView(item);
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    Toast toast = Toast.makeText(getActivity(),
                            "" + e, Toast.LENGTH_SHORT);
                    toast.show();
                    e.printStackTrace();
                }
            }
        });
    }

    void addComments() {
        RequestParams requestParams = new RequestParams();
        requestParams.put("owner_id", oid);
        requestParams.put("post_id", pid);
        requestParams.put("offset", Integer.MAX_VALUE);
        requestParams.put("v", API.version);
        requestParams.put("access_token", CandyApplication.access_token());
        requestParams.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.wallGetComments, requestParams, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                more.setVisible(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject rS = new JSONObject(responseString);
                    JSONObject response = rS.getJSONObject("response");
                    count = response.getInt("count");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                RequestParams params = new RequestParams();
                params.put("owner_id", oid);
                params.put("item_id", pid);
                params.put("count", 10);

                if (lastCount != 0 && count != lastCount) {
                    newCount = count - lastCount;
                } else {
                    newCount = 0;
                }

                params.put("offset", offset + newCount);
                params.put("v", API.version);
                params.put("access_token", CandyApplication.access_token());
                params.put("lang", CandyApplication.lang);
                CandyApplication.client.get(API.BASE() + API.executeGetCommentsNew, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        more.setVisible(true);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        try {
                            JSONObject object = new JSONObject(responseString);
                            JSONObject response = object.getJSONObject("response");
                            int count = response.getJSONObject("c").getInt("count");
                            lastCount = count;
                            JSONArray itemsArray = response.getJSONObject("c").getJSONArray("items");
                            JSONArray profilesArray = response.getJSONArray("p1");
                            JSONArray p2u = response.getJSONArray("p2u");
                            JSONArray p2n = response.getJSONArray("p2n");
                            if (refresh.isRefreshing()) {
                                comments.clear();
                            }
                            comments.addAll(0, Comments.parsePostComments(itemsArray, profilesArray, p2u, p2n, oid).getComments());
                            offset = comments.size();
                            refresh.setRefreshing(false);
                            more.setVisible(true);
                            if (comments.size() != 0) {
                                refresh.setEnabled(true);
                            }
                            if (comments.size() >= count) {
                                more.setVisibility(View.GONE);
                            } else {
                                more.setVisibility(View.VISIBLE);
                                more.setCount(count - comments.size());
                            }
                            commentsAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            Log.d("Candy", responseString);
                            more.setVisible(true);
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    public void init(long owner_id, long id, boolean can_comment, int color) {
        oid = owner_id;
        pid = id;
        writeBarEnabled = can_comment;
        colorPrimary = color;
    }
}
