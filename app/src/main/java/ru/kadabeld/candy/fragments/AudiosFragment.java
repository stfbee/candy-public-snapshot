package ru.kadabeld.candy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.AudiosAdapter;
import ru.kadabeld.candy.entities.Audio;

public class AudiosFragment extends Fragment {
    private ArrayList<Audio> playList = new ArrayList<>();
    private AudiosAdapter audiosAdapter;
    private long user_id = CandyApplication.current().getUserId();
    private int offset = 0;
    private AbstractActivity ctx;
    private int colorPrimary = 0;
    private boolean canColorize;
    private boolean colorized = false;
    private ProgressBar progressBar;
    private TextView empty;
    private ListView rootLV;

    public void init(long id, int color) {
        user_id = id;
        colorPrimary = color;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ctx = ((AbstractActivity) getActivity());

        View rootView = inflater.inflate(R.layout.fragment_audios, container, false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);
        ctx.makeActionBar(rootView);

        canColorize = colorPrimary != 0;
        if (canColorize) {
            colorize();
            colorized = true;
        }

        rootLV = (ListView) rootView.findViewById(R.id.listview);

        audiosAdapter = new AudiosAdapter(ctx, playList);
        rootLV.setAdapter(audiosAdapter);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) supportActionBar.setTitle(getString(R.string.drawer_music));

        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            int previousLastPosition = 0;

            void resetLastIndex() {
                previousLastPosition = 0;
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        getActivity().runOnUiThread(() -> {
                            if (!playList.isEmpty()) {
                                addAudios();
                            }
                        });
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });

        addAudios();

        return rootView;
    }

    private void addAudios() {
        if (playList.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        RequestParams params = new RequestParams();
        params.put("owner_id", user_id);
        params.put("offset", offset);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.audioGet, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (playList.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                empty.setVisibility(View.GONE);
                try {
                    JSONObject object = new JSONObject(responseString);
                    JSONObject response = object.getJSONObject("response");
                    JSONArray items = response.getJSONArray("items");
                    int length = items.length();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            JSONObject item = items.getJSONObject(i);
                            Audio audio = Audio.parseAudio(item);
                            playList.add(audio);
                        }
                        audiosAdapter.notifyDataSetChanged();
                        offset = playList.size();
                        progressBar.setVisibility(View.GONE);
                        rootLV.setVisibility(View.VISIBLE);
                    } else {
                        if (playList.size() == 0) {
                            empty.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    if (playList.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        empty.setText(R.string.loading_error);
                        progressBar.setVisibility(View.GONE);
                    }
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        ctx = (AbstractActivity) getActivity();
        if (colorized && canColorize) {
            colorize();
        }
    }

    private void colorize() {
        ((AbstractActivity) getActivity()).colorize(colorPrimary);
    }

}
