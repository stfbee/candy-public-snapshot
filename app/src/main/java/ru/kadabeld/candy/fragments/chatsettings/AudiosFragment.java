package ru.kadabeld.candy.fragments.chatsettings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.AudiosAdapter;
import ru.kadabeld.candy.entities.Audio;
import ru.kadabeld.candy.utils.parsers.Attachments;
import ru.kadabeld.candy.views.ListView;

/**
 * User: Vlad
 * Date: 16.07.2016
 * Time: 16:38
 */

public class AudiosFragment extends Fragment {
    private ArrayList<Audio> audios = new ArrayList<>();

    private String next_from = "";

    private boolean is_chat;
    private boolean is_group;
    private long chat_id;
    private long user_id;

    private ListView rootLV;
    private AudiosAdapter audiosAdapter;
    private ProgressBar progressBar;
    private TextView empty;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        is_chat = getArguments().getBoolean("is_chat");
        is_group = getArguments().getBoolean("is_group");
        chat_id = getArguments().getLong("chat_id");
        user_id = getArguments().getLong("user_id");
        RelativeLayout rootView = (RelativeLayout) inflater.inflate(R.layout.fragment_chat_audios, container, false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);
        rootLV = (ListView) rootView.findViewById(R.id.listview);

        audiosAdapter = new AudiosAdapter(getActivity(), audios);

        rootLV.setAdapter(audiosAdapter);
        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            int previousLastPosition = 0;

            void resetLastIndex() {
                previousLastPosition = 0;
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        loadAudios();
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });

        loadAudios();

        return rootView;
    }


    private void loadAudios() {
        if (next_from.equals("false")) return;
        if (audios.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }

        RequestParams params = new RequestParams();
        if (is_chat) {
            params.put("peer_id", 2000000000 + chat_id);
        } else {
            if (is_group) {
                params.put("peer_id", -user_id);
            } else {
                params.put("peer_id", user_id);
            }
        }
        params.put("start_from", next_from);
        params.put("media_type", "audio");
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.messagesGetHistoryAttachments, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (audios.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                empty.setVisibility(View.GONE);
                try {
                    response = response.getJSONObject("response");
                    if (response.has("next_from")) {
                        next_from = response.getString("next_from");
                    } else {
                        next_from = "false";
                    }
                    JSONArray items = response.getJSONArray("items");
                    Attachments attachments_parsed = Attachments.parseJson(items);
                    ArrayList<Audio> _audios = attachments_parsed.getAudios();

                    if (_audios.size() > 0) {
                        audios.addAll(_audios);
                        progressBar.setVisibility(View.GONE);
                        rootLV.setVisibility(View.VISIBLE);
                        audiosAdapter.notifyDataSetChanged();
                    } else {
                        if (audios.size() == 0) {
                            empty.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                } catch (JSONException e) {
                    if (audios.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        empty.setText(R.string.loading_error);
                        progressBar.setVisibility(View.GONE);
                    }

                    Log.e("LOAD", response.toString());
                    e.printStackTrace();
                }
            }
        });
    }
}
