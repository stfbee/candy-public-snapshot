package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.DocumentsAdapter;
import ru.kadabeld.candy.entities.Document;

/**
 * User: Vlad
 * Date: 10.11.2015
 * Time: 23:58
 */
public class DocsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View rootView;
    private ArrayList<Document> docs = new ArrayList<>();
    private DocumentsAdapter docsAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isLoading = false;
    private long user_id = CandyApplication.current().getUserId();
    private int colorPrimary = 0;
    private boolean canColorize;
    private boolean colorized = false;

    private int previousLastPosition = 0;
    private int lastPosition = 0;

    private int loadStep = 20;
    private int offset = 0;

    private ProgressBar progressBar;
    private TextView empty;
    private ListView rootLV;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_listview, null);
        ((AbstractActivity) getActivity()).makeActionBar(rootView);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#d18d88"), Color.parseColor("#b3655f"), Color.parseColor("#f23426"), Color.parseColor("#d23d32"));
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        docsAdapter = new DocumentsAdapter(getActivity(), docs);
        rootLV = (ListView) rootView.findViewById(R.id.listview);
        rootLV.setAdapter(docsAdapter);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) supportActionBar.setTitle(getString(R.string.docs));

        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {

            void resetLastIndex() {
                previousLastPosition = 0;
                lastPosition = 0;
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (rootLV != null && rootLV.getChildCount() > 0) {
                    boolean firstItemVisible = rootLV.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = rootLV.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);

                lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        getActivity().runOnUiThread(() -> addDocs());
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });

        addDocs();

        canColorize = colorPrimary != 0;
        if (canColorize) {
            colorize();
            colorized = true;
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (colorized && canColorize) {
            colorize();
        }
    }

    private void colorize() {
        ((AbstractActivity) getActivity()).colorize(colorPrimary);
    }

    private void addDocs() {
        if (docs.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        RequestParams params = new RequestParams();
        params.put("count", loadStep);
        params.put("offset", offset);
        params.put("owner_id", user_id);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.docsGet, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (docs.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject object = new JSONObject(responseString);
                    JSONObject response = object.getJSONObject("response");
                    JSONArray items = response.getJSONArray("items");
                    int length = items.length();
                    if (length > 0) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            docs.clear();
                            offset = 0;
                        }
                        for (int pr = 0; pr < items.length(); pr++) {
                            JSONObject docObject = items.getJSONObject(pr);
                            Document document = Document.parseDocument(docObject);
                            docs.add(document);
                        }
                        offset += loadStep;

                        docsAdapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                        rootLV.setVisibility(View.VISIBLE);
                    } else {
                        if (docs.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);}
                    }
                } catch (JSONException e) {
                    if (docs.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        empty.setText(R.string.loading_error);
                        progressBar.setVisibility(View.GONE);
                    }
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        docs.clear();
        addDocs();
    }

    public void init(long id, int color) {
        user_id = id;
        colorPrimary = color;
    }

}
