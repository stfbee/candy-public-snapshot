package ru.kadabeld.candy.fragments.dialogs;

import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import ru.kadabeld.candy.R;

/**
 * User: Vlad
 * Date: 30.08.2015
 * Time: 18:43
 */
public class StickerSizePickerDialog extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_stickersize, null, false);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final int value = sharedPreferences.getInt("stickersize", 128);

        final ImageView sticker = (ImageView) view.findViewById(R.id.sticker);
        final SeekBar size_bar = (SeekBar) view.findViewById(R.id.size_bar);
        final TextView size_value = (TextView) view.findViewById(R.id.size_value);

        final ViewGroup.LayoutParams stickerLayoutParams = sticker.getLayoutParams();

        size_bar.setProgress(value - 64);
        size_value.setText(String.valueOf(value));
        stickerLayoutParams.height = value;
        stickerLayoutParams.width = value;
        sticker.setLayoutParams(stickerLayoutParams);

        size_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                size_value.setText(String.valueOf(progress + 64));
                stickerLayoutParams.height = progress + 64;
                stickerLayoutParams.width = progress + 64;
                sticker.setLayoutParams(stickerLayoutParams);
                sharedPreferences.edit().putInt("stickersize", progress + 64).apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;
    }
}
