package ru.kadabeld.candy.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AboutActivity;
import ru.kadabeld.candy.BuildConfig;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.OTAService;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.fragments.dialogs.StickerSizePickerDialog;
import ru.kadabeld.candy.notifications.NotificationsService;
import ru.kadabeld.candy.utils.fragment.PreferenceFragment;

public class SettingsFragment extends PreferenceFragment {
    SharedPreferences sPref;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(getString(R.string.drawer_settings));
        }

        sPref = PreferenceManager.getDefaultSharedPreferences(getContext());

        Preference ninja = this.findPreference("ninja");
        ninja.setOnPreferenceChangeListener((preference, newValue) -> {
            if ((boolean) newValue) {
                API.setOffline();
            } else {
                API.setOnline();
            }
            return true;
        });
        Preference sidebar = this.findPreference("sidebar");
        sidebar.setOnPreferenceChangeListener((preference, newValue) -> {
            showToast();
            return true;
        });

        Preference darktheme = this.findPreference("darktheme");
        darktheme.setOnPreferenceChangeListener((preference, newValue) -> {
            showToast();
            return true;
        });

        Preference colorizeNavbar = this.findPreference("colorizeNavbar");
        colorizeNavbar.setOnPreferenceChangeListener((preference, newValue) -> {
            showToast();
            return true;
        });

        Preference language = this.findPreference("language");
        language.setOnPreferenceChangeListener((preference, newValue) -> {
            CandyApplication.switchLocale(getActivity().getBaseContext());
            NotificationsService.res = getActivity().getResources();
            showToast();
            return true;
        });

        Preference background = this.findPreference("background");
        background.setOnPreferenceChangeListener((preference, newValue) -> {
            showToast();
            return true;
        });

        Preference about = this.findPreference("about");
        about.setOnPreferenceClickListener(preference -> {
            getActivity().startActivity(new Intent(getActivity(), AboutActivity.class));
            return false;
        });
        about.setSummary(BuildConfig.VERSION_NAME);


        Preference stickersize = this.findPreference("stickersize");
        stickersize.setOnPreferenceClickListener(preference -> {
            StickerSizePickerDialog stickerSizePickerDialog = new StickerSizePickerDialog();
            stickerSizePickerDialog.show(getActivity().getFragmentManager(), "Sticker size picker");
            return false;
        });

        Preference force_update_check = findPreference("force_update_check");
        force_update_check.setOnPreferenceClickListener(preference -> {
            OTAService.forseCheckUpdate();
            return false;
        });


        if (Build.VERSION.SDK_INT < 21) {
            ((PreferenceCategory) findPreference("ux")).removePreference(colorizeNavbar);
        }

        //TODO remove only after themes
        ((PreferenceCategory) findPreference("ux")).removePreference(darktheme);
//        ((PreferenceCategory) findPreference("ux")).removePreference(maincolor);
    }

    private void showToast() {
        Toast.makeText(getActivity(), getResources().getString(R.string.settings_app_reload), Toast.LENGTH_SHORT).show();
    }
}
