package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

@SuppressWarnings("deprecation")
public class GroupsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private ProgressBar progressBar;
    private TextView empty;
    private ListView rootLV;
    private ArrayList<Long> groups = new ArrayList<>();
    private GroupsAdapter groupsAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private long user_id = CandyApplication.current().getUserId();

    private int colorPrimary = 0;
    private boolean canColorize;
    private boolean colorized = false;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean animation = sPref.getBoolean("listview_animation", true);
        View rootView = inflater.inflate(R.layout.fragment_listview, container, false);
        ((AbstractActivity) getActivity()).makeActionBar(rootView);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(animation ? R.id.refresh_parallax : R.id.refresh);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#d18d88"), Color.parseColor("#b3655f"), Color.parseColor("#f23426"), Color.parseColor("#d23d32"));
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(getString(R.string.drawer_communities));
        groupsAdapter = new GroupsAdapter(getActivity(), groups);
        rootLV = (ListView) rootView.findViewById(animation ? R.id.listview_parallax : R.id.listview);
        rootLV.setAdapter(groupsAdapter);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);


        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (rootLV != null && rootLV.getChildCount() > 0) {
                    boolean firstItemVisible = rootLV.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = rootLV.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);
            }
        });

        addGroups();

        canColorize = colorPrimary != 0;
        if (canColorize) {
            colorize();
            colorized = true;
        }

        return rootView;
    }

    private void addGroups() {
        if (groups.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        RequestParams params = new RequestParams();
        params.put("extended", "1");
        params.put("user_id", user_id);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.groupsGet, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (groups.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                empty.setVisibility(View.GONE);
                try {
                    JSONObject object = new JSONObject(responseString);
                    JSONObject response = object.getJSONObject("response");
                    JSONArray items = response.getJSONArray("items");
                    int length = items.length();
                    if (length > 0) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            groups.clear();
                        }
                        for (int pr = 0; pr < items.length(); pr++) {
                            JSONObject groupObject = items.getJSONObject(pr);
                            Group group = Group.parseGroup(groupObject);
                            groups.add(group.getId());
                        }

                        groupsAdapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        rootLV.setVisibility(View.VISIBLE);
                    } else {
                        if (groups.size() == 0) {
                            empty.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (groups.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        empty.setText(R.string.loading_error);
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (colorized && canColorize) {
            colorize();
        }
    }

    private void colorize() {
        ((AbstractActivity) getActivity()).colorize(colorPrimary);
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        groups.clear();
        addGroups();
    }

    public void init(long id, int color) {
        user_id = id;
        colorPrimary = color;
    }

    /**
     * User: Vlad
     * Date: 30.01.2015
     * Time: 21:08
     */
    private static class GroupsAdapter extends ArrayAdapter<Long> {
        private ArrayList<Long> objects;
        private Picasso picasso;
        private Activity ctx;
        private LayoutInflater lInflater;


        GroupsAdapter(Activity context, ArrayList<Long> groups) {
            super(context, 0, groups);
            ctx = context;
            objects = groups;
            lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            picasso = Picasso.with(ctx);
        }

        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            if (v == null) {
                v = lInflater.inflate(R.layout.list_item_group, parent, false);
            }

            final ImageView avatar = ViewHolder.get(v, R.id.avatar);
            final TextView name = ViewHolder.get(v, R.id.name);
            final TextView group_type = ViewHolder.get(v, R.id.group_type);
            final View hr = ViewHolder.get(v, R.id.hr);

            final Group group = (Group) Cache.getAbstractProfile(getItem(position));
            ctx.runOnUiThread(new Thread(() -> {
                if (group != null) {
                    if (group.isAvatarDefault()) {
                        avatar.setImageDrawable(group.getAvatarDrawable());
                    } else {
                        picasso.load(group.getPhotoFor(200)).tag("groups").transform(new CropCircleTransformation()).into(avatar);
                    }
                    hr.setVisibility(position == getCount() - 1 ? View.GONE : View.VISIBLE);
                    name.setText(group.getName());
                    Group.Type type = group.getType();
                    switch (type) {
                        case PAGE:
                            group_type.setText(ctx.getString(R.string.groups_public_page));
                            break;
                        case GROUP:
                            switch (group.getIs_closed()) {
                                case OPEN:
                                    group_type.setText(ctx.getString(R.string.groups_opened_group));
                                    break;
                                case CLOSED:
                                    group_type.setText(ctx.getString(R.string.groups_closed_group));
                                    break;
                                case PRIVATE:
                                    group_type.setText(ctx.getString(R.string.groups_private_group));
                                    break;
                            }
                            break;
                    }
                }
            }
            ));
            v.setOnClickListener(v1 -> {
                ProfileFragment f = new ProfileFragment();
                f.init(group.getId());
                ((AbstractActivity) getContext()).openFragment(f);
            });
            return v;
        }

        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public Long getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        static class ViewHolder {
            // I added a generic return type to reduce the casting noise in client code
            @SuppressWarnings("unchecked")
            public static <T extends View> T get(View view, int id) {
                SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
                if (viewHolder == null) {
                    viewHolder = new SparseArray<>();
                    view.setTag(viewHolder);
                }
                View childView = viewHolder.get(id);
                if (childView == null) {
                    childView = view.findViewById(id);
                    viewHolder.put(id, childView);
                }
                return (T) childView;
            }
        }
    }
}
