package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Dialog;
import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.im.IMService;
import ru.kadabeld.candy.utils.ApiUtils;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.parsers.Attachments;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

/**
 * User: Vlad
 * Date: 10.02.2015
 * Time: 6:35
 */
@SuppressWarnings("deprecation")
public class DialogsFragment extends Fragment {
    private static String TAG = DialogsFragment.class.getSimpleName();
    private ArrayList<Dialog> dialogList = new ArrayList<>();
    private TreeMap<Long, List<Dialog>> dialogTree = new TreeMap<>(Collections.reverseOrder());
    private DialogsAdapter dialogsAdapter;
    private int previousLastPosition = 0;
    private int lastPosition = 0;
    private int offset = 0;
    private ProgressBar progressBar;
    private TextView empty;
    private ListView rootLV;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean animation = sPref.getBoolean("listview_animation", true);

        View rootView = inflater.inflate(R.layout.fragment_dialogs, container, false);

        AbstractActivity ctx = (AbstractActivity) getActivity();
        ctx.makeActionBar(rootView);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(getString(R.string.drawer_messages));

        dialogsAdapter = new DialogsAdapter(getActivity(), dialogList);
        rootLV = (ListView) rootView.findViewById(animation ? R.id.listview_parallax : R.id.listview);
        rootLV.setAdapter(dialogsAdapter);
        rootLV.setVisibility(View.VISIBLE);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);

        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            void resetLastIndex() {
                previousLastPosition = 0;
                lastPosition = 0;
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        getActivity().runOnUiThread(() -> addDialogs());
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });
        addDialogs();

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iFilter = new IntentFilter();
        for (String action : ALLOWED_ACTIONS) {
            iFilter.addAction(action);
        }
        getActivity().registerReceiver(broadcastReceiver, iFilter);

        getActivity().runOnUiThread(() -> {
            offset = 0;
            addDialogs();
        });

        ((AbstractActivity) getActivity()).colorize();
    }

    private void addDialogs() {
        addDialogs(20);
    }

    private void addDialogs(final int count) {
        if (dialogList.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        RequestParams params = new RequestParams();
        params.put("count", count);
        params.put("offset", offset);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.executeGetDialogsWithProfilesNew, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                if (dialogList.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                empty.setVisibility(View.GONE);
                try {
                    ApiUtils.ErrorType errorType = ApiUtils.parseError(object);
                    if (errorType != null) {
                        // TODO: 18.03.2016 VOnishchenko отобразить ошибку в UI или
                        Log.e(TAG, "что-то не так в ответе: " + errorType);
                        if (errorType == ApiUtils.ErrorType.TOO_MANY_REQUESTS) {
                            Thread.sleep(1000);
                            addDialogs(count);
                        }
                        return;
                    }

                    JSONObject response = object.getJSONObject("response");
                    JSONArray users = response.getJSONArray("p");
                    JSONArray dialogs = response.getJSONArray("a");

                    if (dialogs.length() > 0 || users.length() > 0) {
                        for (int userCounter = 0; userCounter < users.length(); userCounter++) {
                            JSONObject userObject = users.getJSONObject(userCounter);
                            AbstractProfile.parse(userObject);
                        }

                        for (int dialogCounter = 0; dialogCounter < dialogs.length(); dialogCounter++) {
                            JSONObject dialogObject = dialogs.getJSONObject(dialogCounter);
                            Dialog dialog = Dialog.parseDialog(dialogObject);
                            for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                                List<Dialog> ds = entry.getValue();
                                for (Iterator<Dialog> iterator = ds.iterator(); iterator.hasNext(); ) {
                                    Dialog d = iterator.next();
                                    if (d.isChat()) {
                                        if (d.getChat_id() == dialog.getChat_id()) {
                                            iterator.remove();
                                            break;
                                        }
                                    } else {
                                        if (d.getUser_id() == dialog.getUser_id()) {
                                            iterator.remove();
                                            break;
                                        }
                                    }
                                }
                            }
                            putDialog(dialog.getMessage().getDate(), dialog);
                        }
                        updateList();
                        offset = dialogTree.size();
                        progressBar.setVisibility(View.GONE);
                        rootLV.setVisibility(View.VISIBLE);
                    } else {
                        if (dialogList.size() == 0) {
                            empty.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    System.out.println(object);
                    e.printStackTrace();

                    if (dialogList.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        empty.setText(R.string.loading_error);
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private static final String[] ALLOWED_ACTIONS = {
            IMService.ACTION_CHAT_WRITING,
            IMService.ACTION_USER_WRITING,
            IMService.ACTION_MESSAGE_NEW,
            IMService.ACTION_FLAGS_REPLACE,
            IMService.ACTION_FLAGS_RESET,
            IMService.ACTION_FLAGS_SET,
            IMService.ACTION_MESSAGE_DELETE,
            IMService.ACTION_USER_ONLINE,
            IMService.ACTION_USER_OFFLINE,
            IMService.ACTION_READ_OUTCOME,
            IMService.ACTION_READ_INCOME,
            IMService.ACTION_CHAT_UPDATE
    };

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case IMService.ACTION_CHAT_WRITING: {
                    long user_id = intent.getLongExtra("user_id", 0);
                    long chat_id = intent.getLongExtra("chat_id", 0);

                    for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                        final List<Dialog> dialogs = entry.getValue();
                        for (final Dialog dialog : dialogs) {
                            if (dialog.isChat() && dialog.getChat_id() == chat_id) {
                                User writer = (User) Cache.getAbstractProfile(user_id);
                                AbstractProfile.ProfileOnLoad onLoad = user -> {
                                    dialog.setWriter((User) user);
                                    dialog.setWriting(true);
                                    Runnable updateTextRunnable = () -> {
                                        dialog.setWriting(false);
                                        updateList();
                                    };
                                    dialog.getHandler().removeCallbacks(updateTextRunnable);
                                    dialog.getHandler().postDelayed(updateTextRunnable, 5500);
                                };

                                if (writer == null) {
                                    User.load(user_id, onLoad);
                                } else {
                                    onLoad.run(writer);
                                }

                                break;
                            }
                        }
                    }

                    updateList();
                    break;
                }
                case IMService.ACTION_USER_WRITING: {
                    long user_id = intent.getLongExtra("user_id", 0);

                    for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                        final List<Dialog> dialogs = entry.getValue();
                        for (final Dialog dialog : dialogs) {
                            if (!dialog.isChat() && dialog.getUser_id() == user_id) {
                                User writer = (User) Cache.getAbstractProfile(user_id);
                                AbstractProfile.ProfileOnLoad onLoad = user -> {
                                    dialog.setWriter((User) user);
                                    dialog.setWriting(true);
                                    Runnable updateTextRunnable = () -> {
                                        dialog.setWriting(false);
                                        updateList();
                                    };
                                    dialog.getHandler().removeCallbacks(updateTextRunnable);
                                    dialog.getHandler().postDelayed(updateTextRunnable, 5500);
                                };

                                if (writer == null) {
                                    User.load(user_id, onLoad);
                                } else {
                                    onLoad.run(writer);
                                }
                                break;
                            }
                        }
                    }

                    updateList();
                    break;
                }
                case IMService.ACTION_MESSAGE_NEW: {
                    int flags = intent.getIntExtra("flags", 0);
                    long from_id = intent.getLongExtra("from_id", 0);
                    long timestamp = intent.getLongExtra("timestamp", 0);
                    String subject = intent.getStringExtra("subject");
                    String text = StringEscapeUtils.unescapeHtml(intent.getStringExtra("text"));
                    JSONObject attachments = null;
                    try {
                        try {
                            attachments = new JSONObject(intent.getStringExtra("attachments"));
                        } catch (JSONException ignored) {
                        }
                        boolean UNREAD = (flags & 1) != 0;
                        boolean OUTBOX = (flags & 2) != 0;
//                    boolean CHAT = (flags & 16) != 0;
                        boolean CHAT = !subject.equals(" ... ");

                        if (CHAT) {
                            if (attachments != null) {
                                from_id = attachments.getLong("from");
                            }
                            Long chat_id = (from_id % 2000000000);
                            int ds = 0;
                            TreeMap<Long, List<Dialog>> safeMap = new TreeMap<>();
                            for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                                final List<Dialog> dialogs = entry.getValue();
                                for (Dialog dialog : dialogs) {
                                    if (dialog.isChat() && dialog.getChat_id() == chat_id) {
                                        dialog.getMessage().setOut(OUTBOX);
//									dialog.getMessage().setUser((User) Cache.getAbstractProfile(from_id));
                                        dialog.getMessage().setDate(timestamp);
                                        dialog.setWriting(false);
                                        dialog.setChat(true);
                                        dialog.getMessage().setBody(text);
                                        dialog.setTitle(subject);
                                        dialog.getMessage().setRead_state(!UNREAD);
                                        dialog.getMessage().FLAGS = flags;
                                        putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                        dialogs.remove(dialog);
                                        ds++;
                                        break;
                                    }
                                }
                            }
                            dialogTree.putAll(safeMap);
                            if (ds == 0) {
                                offset = 0;
                                addDialogs(1);
                            }
                        } else {
                            int ds = 0;
                            TreeMap<Long, List<Dialog>> safeMap = new TreeMap<>();
                            for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                                final List<Dialog> dialogs = entry.getValue();

                                for (Dialog dialog : dialogs) {
                                    if (!dialog.isChat() && dialog.getUser_id() == from_id) {
                                        dialog.getMessage().setOut(OUTBOX);
                                        dialog.getMessage().setDate(timestamp);
                                        dialog.setWriting(false);
                                        dialog.setChat(false);
                                        dialog.getMessage().setBody(text);
                                        dialog.getMessage().setRead_state(!UNREAD);
                                        dialog.getMessage().FLAGS = flags;
                                        putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                        dialogs.remove(dialog);
                                        ds++;
                                        break;
                                    }
                                }
                            }
                            dialogTree.putAll(safeMap);
                            if (ds == 0) {
                                offset = 0;
                                addDialogs(1);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    updateList();
                    break;
                }
                case IMService.ACTION_READ_OUTCOME: {
                    long message_id = intent.getLongExtra("message_id", 0);
                    long from = intent.getLongExtra("from", 0);
                    boolean is_chat = intent.getBooleanExtra("is_chat", false);

                    TreeMap<Long, List<Dialog>> safeMap = new TreeMap<>();
                    for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                        final List<Dialog> dialogs = entry.getValue();
                        for (Dialog dialog : dialogs) {
                            if (is_chat) {
                                if (dialog.getMessage().getId() <= message_id && dialog.isChat() && dialog.getChat_id() == from && dialog.getMessage().isOut()) {
                                    dialog.getMessage().setRead_state(true);
                                    putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                    dialogs.remove(dialog);
                                }
                            } else {
                                if (dialog.getMessage().getId() <= message_id && !dialog.isChat() && dialog.getUser_id() == from && dialog.getMessage().isOut()) {
                                    dialog.getMessage().setRead_state(true);
                                    putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                    dialogs.remove(dialog);
                                }
                            }
                        }
                    }
                    dialogTree.putAll(safeMap);

                    updateList();
                    break;
                }
                case IMService.ACTION_READ_INCOME: {
                    long message_id = intent.getLongExtra("message_id", 0);
                    long from = intent.getLongExtra("from", 0);
                    boolean is_chat = intent.getBooleanExtra("is_chat", false);

                    TreeMap<Long, List<Dialog>> safeMap = new TreeMap<>();
                    for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                        final List<Dialog> dialogs = entry.getValue();
                        for (Dialog dialog : dialogs) {
                            if (is_chat) {
                                if (dialog.getMessage().getId() <= message_id && dialog.isChat() && dialog.getChat_id() == from && !dialog.getMessage().isOut()) {
                                    dialog.getMessage().setRead_state(true);
                                    putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                    dialogs.remove(dialog);
                                }
                            } else {
                                if (dialog.getMessage().getId() <= message_id && !dialog.isChat() && dialog.getUser_id() == from && !dialog.getMessage().isOut()) {
                                    dialog.getMessage().setRead_state(true);
                                    putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                    dialogs.remove(dialog);
                                }
                            }
                        }
                    }
                    dialogTree.putAll(safeMap);

                    updateList();
                    break;
                }
                case IMService.ACTION_FLAGS_REPLACE: {
                    long message_id = intent.getLongExtra("message_id", 0);
                    int flags = intent.getIntExtra("flags", 0);
                    TreeMap<Long, List<Dialog>> safeMap = new TreeMap<>();
                    for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                        final List<Dialog> dialogs = entry.getValue();
                        for (Dialog dialog : dialogs) {
                            if (!dialog.isChat() && dialog.getMessage().getId() == message_id) {
                                dialog.getMessage().FLAGS = flags;
                                boolean UNREAD = (dialog.getMessage().FLAGS & 1) != 0;
                                dialog.getMessage().setRead_state(!UNREAD);
                                putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                dialogs.remove(dialog);
                            }
                        }
                    }
                    dialogTree.putAll(safeMap);

                    updateList();
                    break;
                }
                case IMService.ACTION_FLAGS_SET: {
                    long message_id = intent.getLongExtra("message_id", 0);
                    int mask = intent.getIntExtra("mask", 0);
                    boolean is_chat = intent.getBooleanExtra("is_chat", false);
                    long from = intent.getLongExtra("from", 0);
                    TreeMap<Long, List<Dialog>> safeMap = new TreeMap<>();
                    for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                        final List<Dialog> dialogs = entry.getValue();
                        for (Dialog dialog : dialogs) {
                            if (is_chat) {
                                if (dialog.isChat() && dialog.getChat_id() == from && dialog.getMessage().getId() == message_id) {
                                    dialog.getMessage().FLAGS |= mask;
                                    boolean UNREAD = (dialog.getMessage().FLAGS & 1) != 0;
                                    dialog.getMessage().setRead_state(!UNREAD);
                                    putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                    dialogs.remove(dialog);
                                }
                            } else {
                                if (!dialog.isChat() && dialog.getUser_id() == from && dialog.getMessage().getId() == message_id) {
                                    dialog.getMessage().FLAGS |= mask;
                                    boolean UNREAD = (dialog.getMessage().FLAGS & 1) != 0;
                                    dialog.getMessage().setRead_state(!UNREAD);
                                    putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                    dialogs.remove(dialog);
                                }
                            }
                        }
                    }
                    dialogTree.putAll(safeMap);

                    updateList();
                    break;
                }
                case IMService.ACTION_FLAGS_RESET: {
                    long message_id = intent.getLongExtra("message_id", 0);
                    int mask = intent.getIntExtra("mask", 0);
                    boolean is_chat = intent.getBooleanExtra("is_chat", false);
                    long from = intent.getLongExtra("from", 0);
                    TreeMap<Long, List<Dialog>> safeMap = new TreeMap<>();
                    for (Map.Entry<Long, List<Dialog>> entry : dialogTree.entrySet()) {
                        final List<Dialog> dialogs = entry.getValue();
                        for (Dialog dialog : dialogs) {
                            if (is_chat) {
                                if (dialog.isChat() && dialog.getChat_id() == from && dialog.getMessage().getId() == message_id) {
                                    dialog.getMessage().FLAGS &= ~mask;
                                    boolean UNREAD = (dialog.getMessage().FLAGS & 1) != 0;
                                    dialog.getMessage().setRead_state(!UNREAD);
                                    putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                    dialogs.remove(dialog);
                                }
                            } else {
                                if (!dialog.isChat() && dialog.getUser_id() == from && dialog.getMessage().getId() == message_id) {
                                    dialog.getMessage().FLAGS &= ~mask;
                                    boolean UNREAD = (dialog.getMessage().FLAGS & 1) != 0;
                                    dialog.getMessage().setRead_state(!UNREAD);
                                    putDialog(safeMap, dialog.getMessage().getDate(), dialog);
                                    dialogs.remove(dialog);
                                }
                            }
                        }
                    }
                    dialogTree.putAll(safeMap);

                    updateList();
                    break;
                }
            }

        }
    };


    private void putDialog(long time, Dialog dialog) {
        ArrayList<Dialog> tempList;
        if (dialogTree.containsKey(time)) {
            tempList = (ArrayList<Dialog>) dialogTree.get(time);
            if (tempList == null)
                tempList = new ArrayList<>();
            tempList.add(dialog);
        } else {
            tempList = new ArrayList<>();
            tempList.add(dialog);
        }
        dialogTree.put(time, tempList);
    }

    private void putDialog(TreeMap<Long, List<Dialog>> safeMap, long time, Dialog dialog) {
        ArrayList<Dialog> tempList;
        if (safeMap.containsKey(time)) {
            tempList = (ArrayList<Dialog>) safeMap.get(time);
            if (tempList == null)
                tempList = new ArrayList<>();
            tempList.add(dialog);
        } else {
            tempList = new ArrayList<>();
            tempList.add(dialog);
        }
        safeMap.put(time, tempList);
    }

    private void updateList() {
        dialogList.clear();
        Collection<List<Dialog>> values = dialogTree.values();
        for (List<Dialog> value : values) {
            for (Dialog dialog : value) {
                if (dialog != null) {
                    dialogList.add(dialog);
                }
            }
        }
        dialogsAdapter.notifyDataSetChanged();
    }

    /**
     * User: Vlad
     * Date: 10.02.2015
     * Time: 6:52
     */
    private static class DialogsAdapter extends ArrayAdapter<Dialog> {
        private ArrayList<Dialog> objects;
        private Picasso picasso;
        private Activity ctx;
        private LayoutInflater lInflater;


        DialogsAdapter(Activity context, ArrayList<Dialog> dialogs) {
            super(context, 0, dialogs);
            ctx = context;
            objects = dialogs;
            lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            picasso = Picasso.with(ctx);
        }

        static class ViewHolder {
            // I added a generic return type to reduce the casting noise in client code
            @SuppressWarnings("unchecked")
            public static <T extends View> T get(View view, int id) {
                SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
                if (viewHolder == null) {
                    viewHolder = new SparseArray<>();
                    view.setTag(viewHolder);
                }
                View childView = viewHolder.get(id);
                if (childView == null) {
                    childView = view.findViewById(id);
                    viewHolder.put(id, childView);
                }
                return (T) childView;
            }
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            if (v == null) {
                v = lInflater.inflate(R.layout.list_item_dialog, null, false);
            }

            final ImageView avatar = ViewHolder.get(v, R.id.avatar);
            final ImageView avatar_small = ViewHolder.get(v, R.id.avatar_small);
            final TextView name = ViewHolder.get(v, R.id.name);
            final TextView body = ViewHolder.get(v, R.id.body);
            final TextView time = ViewHolder.get(v, R.id.time);
            final TextView writing_label = ViewHolder.get(v, R.id.writing_label);
            final RelativeLayout contentArea = ViewHolder.get(v, R.id.content);
            final RelativeLayout writing = ViewHolder.get(v, R.id.writing);
            final View hr = ViewHolder.get(v, R.id.hr);

            final Dialog dialog = getItem(position);
            final View finalV = v;
            ctx.runOnUiThread(new Thread(() -> {
                if (dialog != null) {
                    String text = dialog.getMessage().getBody().trim();
                    if (text.isEmpty()) {
                        text = ctx.getResources().getString(R.string.attachments_many);
                        Attachments attachments = dialog.getMessage().getAttachments();
                        if (!attachments.getStickers().isEmpty()) {
                            text = ctx.getResources().getString(R.string.attachments_sticker);
                        } else {
                            int type_count = 0;
                            if (!attachments.getPhotos().isEmpty()) {
                                type_count++;
                                text = ctx.getResources().getString(R.string.attachments_photo);
                            }
                            if (!attachments.getAudios().isEmpty()) {
                                type_count++;
                                text = ctx.getResources().getString(R.string.attachments_audio);
                            }
                            if (!attachments.getVideos().isEmpty()) {
                                type_count++;
                                text = ctx.getResources().getString(R.string.attachments_video);
                            }
                            if (!attachments.getDocuments().isEmpty()) {
                                type_count++;
                                text = ctx.getResources().getString(R.string.attachments_document);
                            }
                            if (!attachments.getLinks().isEmpty()) {
                                type_count++;
                                text = ctx.getResources().getString(R.string.attachments_link);
                            }
                            if (!attachments.getGeos().isEmpty()) {
                                type_count++;
                                text = ctx.getResources().getString(R.string.attachments_geo);
                            }
                            if (!attachments.getPolls().isEmpty()) {
                                type_count++;
                                text = ctx.getResources().getString(R.string.attachments_poll);
                            }
                            if (!attachments.getPosts().isEmpty()) {
                                type_count++;
                                text = ctx.getResources().getString(R.string.attachments_post);
                            }
                            if (type_count > 1) {
                                text = ctx.getResources().getString(R.string.attachments_many);
                            }
                        }
                    }

                    hr.setVisibility(position == getCount() - 1 ? View.GONE : View.VISIBLE);

                    body.setText(text);
                    time.setText(CandyApplication.p.format(new Date(dialog.getMessage().getDate() * 1000L)));

                    finalV.setOnClickListener(v1 -> {
//                                Bundle b = new Bundle();
//                                b.putBoolean("is_chat", dialog.isChat());
//                                b.putBoolean("is_group", dialog.isGroup());
//                                b.putString("title", dialog.getTitle());
//                                if (dialog.isChat()) {
//                                    b.putLong("chat_id", dialog.getChat_id());
//                                } else {
//                                    b.putLong("user_id", dialog.getUser_id());
//                                }
//                                Intent intent = new Intent(ctx, FragmentWrapper.class);
//                                intent.putExtra("class", ChatFragment.class.getCanonicalName());
//                                intent.putExtra("args", b);
//                                ctx.startActivity(intent);

                        ChatFragment f = new ChatFragment();
                        f.init(dialog.getTitle(), dialog.isChat(), dialog.isGroup(), dialog.getChat_id(), dialog.getUser_id());
                        ((AbstractActivity) ctx).openFragment(f);
                    });

                    if (dialog.getMessage().isRead_state()) {
                        contentArea.setBackground(null);
                        finalV.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorLightBackground));
                    } else {
                        if (dialog.getMessage().isOut()) {
                            contentArea.setBackground(ContextCompat.getDrawable(ctx, R.drawable.unreaded_round));
                            finalV.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorLightBackground));
                        } else {
                            contentArea.setBackground(null);
                            finalV.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorUnread));
                        }
                    }

                    AbstractProfile user;

                    AbstractProfile.ProfileOnLoad runnable = profile -> {
                        if (dialog.isChat()) {
                            avatar_small.setImageDrawable(profile.getAvatarDrawable());
                            if (!profile.isAvatarDefault()) {
                                picasso.load(profile.getPhotoFor(200)).tag("dialogs").transform(new CropCircleTransformation()).into(avatar_small);
                            }
                        } else {
                            name.setText(profile.getFullName());
                            if (!dialog.isWriting() && !profile.isAvatarDefault()) {
                                picasso.load(CandyApplication.current().getPhoto200()).tag("dialogs").transform(new CropCircleTransformation()).into(avatar_small);
                            }
                            avatar.setImageDrawable(profile.getAvatarDrawable());
                            if (!profile.isAvatarDefault()) {
                                picasso.load(profile.getPhotoFor(200)).tag("dialogs").transform(new CropCircleTransformation()).into(avatar);
                            }
                        }
                    };

                    User writer = dialog.getWriter();
                    if (dialog.isWriting() && writer == null) {
                        User.load(dialog.getUser_id(), user1 -> writing_label.setText(String.format(ctx.getResources().getString(R.string.chat_typing), user1.getName())));
                    }

                    if (dialog.isGroup()) {
                        user = Cache.getAbstractProfile(dialog.getUser_id());

                        if (user == null) {
                            Group.load(-dialog.getUser_id(), runnable);
                        } else {
                            runnable.run(user);
                        }
                    } else {
                        user = Cache.getAbstractProfile(Math.max(dialog.getMessage().getUser_id(), dialog.getMessage().getFrom_id()));
                    }


                    if (dialog.isChat()) {
                        name.setText(dialog.getTitle());

                        if (user != null) {
                            avatar_small.setImageDrawable(user.getAvatarDrawable());
                            if (!user.isAvatarDefault()) {
                                picasso.load(user.getPhotoFor(200)).tag("dialogs").transform(new CropCircleTransformation()).into(avatar_small);
                            }
                        }
                        if (dialog.isWriting()) {
                            contentArea.setVisibility(View.GONE);
                            writing.setVisibility(View.VISIBLE);

                            if (writer != null) {
                                writing_label.setText(String.format(ctx.getResources().getString(R.string.chat_typing), writer.getName()));
                            }
                        } else {
                            writing.setVisibility(View.GONE);
                            contentArea.setVisibility(View.VISIBLE);
                            avatar_small.setVisibility(View.VISIBLE);
                        }

                        Drawable drawable = dialog.getAvatarDrawable();
                        avatar.setImageDrawable(drawable);
                        if (!dialog.isAvatarDefault()) {
                            picasso.load(dialog.getPhotoFor(200)).tag("dialogs").transform(new CropCircleTransformation()).into(avatar);
                        }
                    } else {
                        if (user != null) name.setText(user.getFullName());
                        if (dialog.isWriting()) {
                            contentArea.setVisibility(View.GONE);
                            writing.setVisibility(View.VISIBLE);
                            if (writer != null) {
                                writing_label.setText(String.format(ctx.getResources().getString(R.string.chat_typing), writer.getName()));
                            }

                        } else {
                            writing.setVisibility(View.GONE);
                            contentArea.setVisibility(View.VISIBLE);
                            if (dialog.getMessage().isOut()) {
                                picasso.load(CandyApplication.current().getPhoto200()).transform(new CropCircleTransformation()).into(avatar_small);
                                if (user != null && !user.isAvatarDefault()) {
                                    picasso.load(CandyApplication.current().getPhoto200()).tag("dialogs").transform(new CropCircleTransformation()).into(avatar_small);
                                }
                                avatar_small.setVisibility(View.VISIBLE);
                            } else {
                                avatar_small.setVisibility(View.GONE);
                            }
                        }
                        if (user != null) {
                            avatar.setImageDrawable(user.getAvatarDrawable());
                            if (!user.isAvatarDefault()) {
                                picasso.load(user.getPhotoFor(200)).tag("dialogs").transform(new CropCircleTransformation()).into(avatar);
                            }
                        }
                    }
                }
            }

            ));
            return v;
        }

        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public Dialog getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


    }
}