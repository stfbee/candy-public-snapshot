package ru.kadabeld.candy.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.PostAdapter;
import ru.kadabeld.candy.entities.NewsfeedList;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.utils.ApiUtils;
import ru.kadabeld.candy.utils.parsers.Posts;

public class NewsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = NewsFragment.class.getSimpleName();
    private View rootView;
    private ObservableListView rootLV;
    private ProgressBar progressBar;
    private TextView empty;
    private ArrayList<Post> news = new ArrayList<>();
    private PostAdapter postAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String start_from = "";
    private boolean isLoading = false;
    private Posts posts;
    private String type = "all_feed";
    private NewsfeedList newsfeedList;


    @Override
    public void onResume() {
        super.onResume();
        ((AbstractActivity) getActivity()).colorize();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_news_feed, null);
        Bundle arguments = getArguments();
        if (arguments != null) {
            type = arguments.getString("type", "all_feed");
            if (type.equals("list")) {
                newsfeedList = (NewsfeedList) arguments.getSerializable("NewsfeedList");
            }
        }
        posts = new Posts();
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) supportActionBar.setTitle(getString(R.string.drawer_news));
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#d18d88"), Color.parseColor("#b3655f"), Color.parseColor("#f23426"), Color.parseColor("#d23d32"));
        rootLV = (ObservableListView) rootView.findViewById(R.id.listview_parallax);
        postAdapter = new PostAdapter(getActivity(), news, PostAdapter.ViewType.FEED);
        rootLV.setAdapter(postAdapter);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);
        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            int previousLastPosition = 0;

            void resetLastIndex() {
                previousLastPosition = 0;
            }

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
                final Picasso picasso = Picasso.with(getActivity());
                if (scrollState == SCROLL_STATE_IDLE) {
                    picasso.resumeTag("news");
                } else {
                    picasso.pauseTag("news");
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (rootLV != null && rootLV.getChildCount() > 0) {
                    boolean firstItemVisible = rootLV.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = rootLV.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);
                final int lastPosition = firstVisibleItem + visibleItemCount;
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        getActivity().runOnUiThread(() -> addNews());
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                }
            }
        });
        addNews();

        Fragment parentFragment = getParentFragment();
        ViewGroup viewGroup = (ViewGroup) parentFragment.getView();
        if (viewGroup != null) {
            rootLV.setTouchInterceptionViewGroup((ViewGroup) viewGroup.findViewById(R.id.container));
            if (parentFragment instanceof ObservableScrollViewCallbacks) {
                rootLV.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentFragment);
            }
        }

        return rootView;
    }

    void addNews() {
        if (news.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        RequestParams params = new RequestParams();
        params.put("filters", "post");
        params.put("count", 10);
        if (!start_from.isEmpty()) {
            params.put("start_from", start_from);
        }
        if (type.equals("list")) {
            params.put("source_ids", "list" + newsfeedList.getId());
        }
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.newsfeedGet, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (news.size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(R.string.loading_error);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                empty.setVisibility(View.GONE);
                try {
                    ApiUtils.ErrorType errorType = ApiUtils.parseError(object);
                    if (errorType != null) {
                        // TODO: 18.03.2016 VOnishchenko отобразить ошибку в UI или
                        Log.e(TAG, "что-то не так в ответе: " + errorType);
                        if (errorType == ApiUtils.ErrorType.TOO_MANY_REQUESTS) {
                            Thread.sleep(1000);
                            addNews();
                        }
                        return;
                    }

                    JSONObject response = object.getJSONObject("response");
                    if (response.has("next_from")) start_from = response.getString("next_from");
                    JSONArray items = response.getJSONArray("items");
                    JSONArray groups = response.getJSONArray("groups");
                    JSONArray profiles = response.getJSONArray("profiles");
                    int length = items.length();
                    if (length > 0) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            news.clear();
                        }
                        news.addAll(new Posts().parse(items, groups, profiles, getActivity()));
                        mSwipeRefreshLayout.setRefreshing(false);
                        isLoading = false;
                        postAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                        rootLV.setVisibility(View.VISIBLE);
                    } else {
                        empty.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    if (news.size() == 0) {
                        empty.setVisibility(View.VISIBLE);
                        empty.setText(R.string.loading_error);
                        progressBar.setVisibility(View.GONE);
                    }
                    System.out.println(object);
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        start_from = "";
        addNews();
    }
}
