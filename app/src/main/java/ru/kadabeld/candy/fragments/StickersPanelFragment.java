package ru.kadabeld.candy.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.astuetz.PagerSlidingTabStrip;
import com.github.ksoichiro.android.observablescrollview.CacheFragmentStatePagerAdapter;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.meta.PurchasedStickers;

/**
 * User: Vlad
 * Date: 15.04.2016
 * Time: 1:35
 */
public class StickersPanelFragment extends Fragment {
    private PagerSlidingTabStrip slidingTabLayout;
    private ProgressBar progressBar;
    private ViewPager mPager;
    private View.OnClickListener onStickerListener;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sticker_panel, container, false);
        slidingTabLayout = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        mPager = (ViewPager) rootView.findViewById(R.id.pager);

        checkStickersAccessibility();

        RequestParams params = new RequestParams();
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.executeGetPurchasedStickers, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    response = response.getJSONObject("response");
                    Cache.setPurchasedStickers(PurchasedStickers.parse(response.getJSONArray("items")));
                    checkStickersAccessibility();
                    final StickerPackAdapter mPagerAdapter = new StickerPackAdapter(getChildFragmentManager());
                    mPager.setAdapter(mPagerAdapter);
                    slidingTabLayout.setViewPager(mPager);
                    mPagerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return rootView;
    }

    private void checkStickersAccessibility() {
        if (Cache.getPurchasedStickers() == null) {
            progressBar.setVisibility(View.VISIBLE);
            slidingTabLayout.setVisibility(View.GONE);
            mPager.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            slidingTabLayout.setVisibility(View.VISIBLE);
            mPager.setVisibility(View.VISIBLE);
        }
    }

    void setOnStickerListener(View.OnClickListener onStickerListener) {
        this.onStickerListener = onStickerListener;
    }

    private class StickerPackAdapter extends CacheFragmentStatePagerAdapter {
        private ArrayList<String> titles = new ArrayList<>();
        private ArrayList<Long> ids = new ArrayList<>();
        private PurchasedStickers purchasedStickers = null;

        StickerPackAdapter(FragmentManager fm) {
            super(fm);

            purchasedStickers = Cache.getPurchasedStickers();
            if (purchasedStickers == null) return;
            ids = purchasedStickers.getStickerPacksIds();
            for (int i = 0; i < ids.size(); i++) {
                long id = ids.get(i);
                PurchasedStickers.StickerPack stickerPack = purchasedStickers.getStickerPack(id);
                titles.add(stickerPack.getTitle());
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return titles.size();
        }

        @Override
        protected Fragment createItem(int position) {
            StickerPackPageFragment fragment = new StickerPackPageFragment();
            fragment.setOnStickerListener(onStickerListener);
            Bundle bundle = new Bundle();
            bundle.putLong("id", ids.get(position));
            fragment.setArguments(bundle);
            return fragment;
        }
    }

    public static class StickerPackPageFragment extends Fragment {
        private View.OnClickListener onStickerListener;

        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_stickers_grid, null);
            GridView grid = (GridView) rootView.findViewById(R.id.stickers_grid);
            ImageView back = (ImageView) rootView.findViewById(R.id.stickers_back);
            long id = getArguments().getLong("id");
            DataAdapter dataAdapter = new DataAdapter(getActivity(), id);
            grid.setAdapter(dataAdapter);

            Picasso.with(getActivity()).load("http://vk.com/images/store/stickers/" + id + "/background.png").into(back);

            return rootView;
        }

        void setOnStickerListener(View.OnClickListener onStickerListener) {
            this.onStickerListener = onStickerListener;
        }

        class DataAdapter extends ArrayAdapter<Long> {

            private long[] stickers_ids;

            private Activity mContext;

            // Конструктор
            DataAdapter(Activity context, long id) {
                super(context, 0, new Long[0]);
                this.stickers_ids = Cache.getPurchasedStickers().getStickerPack(id).getSticker_ids();
                this.mContext = context;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                // TODO Auto-generated method stub

                View view = convertView;
                if (convertView == null) {
                    convertView = mContext.getLayoutInflater().inflate(R.layout.element_sticker, parent, false);
                    view = convertView;
                }
                ImageView image = (ImageView) view.findViewById(R.id.sticker_image);
                long id = stickers_ids[position];

                image.setTag(id);

                image.setOnClickListener(onStickerListener);

                Picasso.with(mContext).load("https://vk.com/images/stickers/" + id + "/128.png").into(image);
                return (convertView);
            }

            // возвращает содержимое выделенного элемента списка
            public Long getItem(int position) {
                return stickers_ids[position];
            }

            @Override
            public int getCount() {
                return stickers_ids.length;
            }
        }
    }
}
