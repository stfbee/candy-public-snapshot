package ru.kadabeld.candy.fragments.chatsettings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

import java.util.ArrayList;

import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Dialog;

/**
 * Created by VOnishchenko
 * 18.01.2016
 * 15:06
 */
public class WrapperFragment extends Fragment {
    private boolean is_chat;
    private boolean is_group;
    private long chat_id;
    private long user_id;
    private Dialog dialog;


    @Override
    public void onResume() {
        super.onResume();
        ((AbstractActivity) getActivity()).colorize();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);
        ((AbstractActivity) getActivity()).makeActionBar(rootView);
        PagerSlidingTabStrip slidingTabLayout = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        ViewPager mPager = (ViewPager) rootView.findViewById(R.id.pager);
        TabsFragmentAdapter mPagerAdapter = new TabsFragmentAdapter(getActivity(), getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        slidingTabLayout.setViewPager(mPager);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (is_chat) {
            if (supportActionBar != null) supportActionBar.setTitle(dialog.getTitle());
        } else {
            if (supportActionBar != null) supportActionBar.setTitle(dialog.getUser().getFullName());
        }

        return rootView;
    }

    public void init(long chat_id, boolean is_chat, long user_id) {
        this.is_chat = is_chat;
        this.chat_id = chat_id;
        this.user_id = user_id;
        if (is_chat) {
            dialog = Cache.getChat(chat_id);
        } else {
            dialog = Cache.getDialog(this.user_id);
        }
        this.is_group = dialog.isGroup();
    }

    private class TabsFragmentAdapter extends FragmentPagerAdapter {

        private ArrayList<String> titles = new ArrayList<>();
        private ArrayList<Fragment> tabs;

        TabsFragmentAdapter(Context context, FragmentManager fm) {
            super(fm);
            initTabsMap(context);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            return tabs.get(position);
        }

        @Override
        public int getCount() {
            return tabs.size();
        }

        private void initTabsMap(Context context) {
            tabs = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                if (i == 2 && !is_chat) continue;
                Bundle bundle = new Bundle();
                bundle.putLong("chat_id", chat_id);
                bundle.putBoolean("is_chat", is_chat);
                bundle.putBoolean("is_group", is_group);
                if (is_chat) {
                    bundle.putLong("chat_id", chat_id);
                } else {
                    bundle.putLong("user_id", user_id);
                }

                Fragment fragment;
                switch (i) {
                    case 0:
                        titles.add(context.getString(R.string.chat_settings));
                        fragment = new SettingsFragment();
                        fragment.setArguments(bundle);
                        tabs.add(fragment);
                        break;
                    case 1:
                        titles.add(context.getString(R.string.chat_members));
                        fragment = new UsersFragment();
                        fragment.setArguments(bundle);
                        tabs.add(fragment);
                        break;
                    case 2:
                        titles.add(context.getString(R.string.photos));
                        fragment = new PhotosFragment();
                        fragment.setArguments(bundle);
                        tabs.add(fragment);
                        break;
                    case 3:
                        titles.add(context.getString(R.string.audios));
                        fragment = new AudiosFragment();
                        fragment.setArguments(bundle);
                        tabs.add(fragment);
                        break;
                    case 4:
                        titles.add(context.getString(R.string.videos));
                        fragment = new VideosFragment();
                        fragment.setArguments(bundle);
                        tabs.add(fragment);
                        break;
                    case 5:
                        titles.add(context.getString(R.string.docs));
                        fragment = new DocumentsFragment();
                        fragment.setArguments(bundle);
                        tabs.add(fragment);
                        break;
                }
            }
        }
    }
}