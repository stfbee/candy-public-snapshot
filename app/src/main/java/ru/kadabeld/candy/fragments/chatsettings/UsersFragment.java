package ru.kadabeld.candy.fragments.chatsettings;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.steamcrafted.materialiconlib.MaterialIconView;

import java.util.ArrayList;

import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Dialog;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.fragments.ProfileFragment;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

/**
 * Created by VOnishchenko
 * 18.01.2016
 * 16:22
 */
public class UsersFragment extends Fragment {

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        boolean is_chat = getArguments().getBoolean("is_chat");
        long chat_id = getArguments().getLong("chat_id");
        long user_id = getArguments().getLong("user_id");
        Dialog dialog;
        if (is_chat) {
            dialog = Cache.getChat(chat_id);
        } else {
            dialog = Cache.getDialog(user_id);
        }

        boolean animation = sPref.getBoolean("listview_animation", true);
        View rootView = inflater.inflate(R.layout.listview_parallax, null);
        SwipeRefreshLayout mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(animation ? R.id.refresh : R.id.refresh_raw);
        mSwipeRefreshLayout.setEnabled(false);
        FriendsAdapter friendsAdapter = new FriendsAdapter(getActivity(), dialog.getChat_active(), chat_id, dialog);
        ListView rootLV = (ListView) rootView.findViewById(animation ? R.id.listview_parallax : R.id.listview);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        rootLV.setAdapter(friendsAdapter);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((AbstractActivity) getActivity()).colorize();
    }

    /**
     * User: Vlad
     * Date: 30.01.2015
     * Time: 16:24
     */
    private static class FriendsAdapter extends ArrayAdapter<Long> {

        private ArrayList<Long> objects;
        private Picasso picasso;
        private Activity ctx;
        private LayoutInflater lInflater;
        private long chat_id;
        private Dialog dialog;


        FriendsAdapter(Activity context, ArrayList<Long> profiles, long chat_id, Dialog dialog) {
            super(context, 0, profiles);
            ctx = context;
            objects = profiles;
            this.chat_id = chat_id;
            this.dialog = dialog;
            lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            picasso = Picasso.with(ctx);
        }


        static class ViewHolder {
            // I added a generic return type to reduce the casting noise in client code
            @SuppressWarnings("unchecked")
            public static <T extends View> T get(View view, int id) {
                SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
                if (viewHolder == null) {
                    viewHolder = new SparseArray<>();
                    view.setTag(viewHolder);
                }
                View childView = viewHolder.get(id);
                if (childView == null) {
                    childView = view.findViewById(id);
                    viewHolder.put(id, childView);
                }
                return (T) childView;
            }
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            if (v == null) {
                v = lInflater.inflate(R.layout.list_item_chat_member, null, false);
            }

            final ImageView avatar = ViewHolder.get(v, R.id.avatar);
            final TextView name = ViewHolder.get(v, R.id.name);
            final TextView invited_text = ViewHolder.get(v, R.id.invited_text);
            final RelativeLayout online = ViewHolder.get(v, R.id.online);
            final MaterialIconView onlineMobile = ViewHolder.get(v, R.id.online_mobile);
            final View hr = ViewHolder.get(v, R.id.hr);

            online.setVisibility(View.GONE);
            onlineMobile.setVisibility(View.GONE);

            final User profile = (User) Cache.getAbstractProfile(getItem(position));
            ctx.runOnUiThread(new Thread(() -> {
                if (profile != null) {
                    avatar.setImageDrawable(profile.getAvatarDrawable());

                    hr.setVisibility(position == getCount() - 1 ? View.GONE : View.VISIBLE);

                    if (!profile.isAvatarDefault()) {
                        picasso.load(profile.getPhotoFor(200)).tag("friends").transform(new CropCircleTransformation()).into(avatar);
                    }

                    name.setText(profile.getFullName());
                    if (profile.isOnline()) {
                        online.setVisibility(View.VISIBLE);
                        if (profile.isOnline_mobile()) {
                            onlineMobile.setVisibility(View.VISIBLE);
                        }
                    }


                    User inviter = (User) Cache.getAbstractProfile(profile.getInvation(chat_id));

                    AbstractProfile.ProfileOnLoad profileOnLoad = profile1 -> {
                        if (profile1.getId() == getItem(position)) {
                            invited_text.setText(ctx.getString(R.string.chat_founder, dialog.getTitle()));
                        } else {
                            invited_text.setText(ctx.getString(R.string.chat_invited_by, ((User) profile1).getFullName(User.NameCase.INS)));
                        }
                    };
                    if (inviter == null) {
                        User.load(profile.getInvation(chat_id), profileOnLoad);
                    } else {
                        profileOnLoad.run(inviter);
                    }

                }
            }
            ));
            v.setOnClickListener(v1 -> {
                ProfileFragment f = new ProfileFragment();
                f.init(profile.getId());
                ((AbstractActivity) getContext()).openFragment(f);
            });
            return v;
        }


        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public Long getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

}
