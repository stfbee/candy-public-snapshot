package ru.kadabeld.candy.fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.adapters.CommentsAdapter;
import ru.kadabeld.candy.entities.Comment;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.utils.parsers.Comments;

public class PhotoCommentsFragment extends ListFragment {
    ArrayList<Comment> comments = new ArrayList<>();
    CommentsAdapter commentsAdapter;
    HashMap<Long, Long> ids_comment = new HashMap<>();
    HashMap<Long, User> usersList = new HashMap<>();

    public static PhotoCommentsFragment newInstance(long owner_id, long photo_id) {
        PhotoCommentsFragment thisFragment = new PhotoCommentsFragment();

        Bundle args = new Bundle();
        args.putLong("owner_id", owner_id);
        args.putLong("photo_id", photo_id);
        thisFragment.setArguments(args);

        return thisFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        commentsAdapter = new CommentsAdapter(getActivity(), comments);
        setListAdapter(commentsAdapter);
        getListView().setDivider(new ColorDrawable(Color.TRANSPARENT));
        addComments();
    }

    void addComments() {
        final Bundle b = getArguments();
        RequestParams params = new RequestParams();
        params.put("owner_id", b.getLong("owner_id"));
        params.put("photo_id", b.getLong("photo_id"));
        params.put("need_likes", 1);
        params.put("extended", 1);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        params.put("v", API.version);
        CandyApplication.client.get(API.BASE() + API.photosGetComments, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject object = new JSONObject(responseString);
                    final JSONObject response = object.getJSONObject("response");
                    JSONArray items = response.getJSONArray("items");
                    JSONArray groups = response.getJSONArray("groups");
                    JSONArray profiles = response.getJSONArray("profiles");
                    comments.addAll(Comments.parsePhotoComments(items, groups, profiles, b.getLong("owner_id")).getComments());
                    commentsAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println(responseString);
                }
            }
        });
    }
}