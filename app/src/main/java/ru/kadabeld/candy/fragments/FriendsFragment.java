package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import net.steamcrafted.materialiconlib.MaterialIconView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.im.IMService;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;
import ru.kadabeld.candy.views.FLinearLayout;

import static ru.kadabeld.candy.fragments.FriendsFragment.PageType.FRIENDS;

@SuppressWarnings("deprecation")
public class FriendsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private ProgressBar progressBar;
    private TextView empty;
    private ListView rootLV;
    private ArrayList<Long> profiles = new ArrayList<>();
    private FriendsAdapter friendsAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private long user_id = CandyApplication.current().getUserId();
    private PageType pageType = FRIENDS;
    private AbstractActivity ctx;
    private int colorPrimary = 0;
    private boolean canColorize;
    private boolean colorized = false;

    public void init(long id, int color, PageType friends) {
        user_id = id;
        pageType = friends;
        colorPrimary = color;
    }

    enum PageType {FRIENDS, MUTUAL}

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean animation = sPref.getBoolean("listview_animation", true);

        View rootView = inflater.inflate(R.layout.fragment_listview, container, false);

        ctx = (AbstractActivity) getActivity();
        ctx.makeActionBar(rootView);

        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        switch (pageType) {
            case FRIENDS:
                if (supportActionBar != null)
                    supportActionBar.setTitle(getString(R.string.friends));
                break;
            case MUTUAL:
                if (supportActionBar != null)
                    supportActionBar.setTitle(getString(R.string.mutual_friends));
                break;
        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(animation ? R.id.refresh_parallax : R.id.refresh);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#d18d88"), Color.parseColor("#b3655f"), Color.parseColor("#f23426"), Color.parseColor("#d23d32"));

        friendsAdapter = new FriendsAdapter(getActivity(), profiles);
        rootLV = (ListView) rootView.findViewById(animation ? R.id.listview_parallax : R.id.listview);
        rootLV.setAdapter(friendsAdapter);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        empty = (TextView) rootView.findViewById(R.id.empty);

        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (rootLV != null && rootLV.getChildCount() > 0) {
                    boolean firstItemVisible = rootLV.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = rootLV.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);
            }
        });

        addFriends();

        canColorize = colorPrimary != 0;
        if (canColorize) {
            colorize();
            colorized = true;
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iFilter = new IntentFilter();
        for (String action : ALLOWED_ACTIONS) {
            iFilter.addAction(action);
        }
        getActivity().registerReceiver(broadcastReceiver, iFilter);
        ctx = (AbstractActivity) getActivity();
        if (colorized && canColorize) {
            colorize();
        }
    }

    private void colorize() {
        ((AbstractActivity) getActivity()).colorize(colorPrimary);
    }

    private void addFriends() {
        if (profiles.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            rootLV.setVisibility(View.GONE);
        }
        switch (pageType) {
            case MUTUAL: {
                RequestParams params = new RequestParams();
                params.put("user_id", user_id);
                params.put("source_user_id", CandyApplication.current().getUserId());
                params.put("v", API.version);
                params.put("access_token", CandyApplication.access_token());
                params.put("lang", CandyApplication.lang);
                CandyApplication.client.get(API.BASE() + API.executeMutualFriends, params, new TextHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (profiles.size() == 0) {
                            empty.setVisibility(View.VISIBLE);
                            empty.setText(R.string.loading_error);
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        empty.setVisibility(View.GONE);
                        try {
                            JSONObject object = new JSONObject(responseString);
                            JSONObject response = object.getJSONObject("response");
                            JSONArray items = response.getJSONArray("items");
                            int length = items.length();
                            if (length > 0) {
                                if (mSwipeRefreshLayout.isRefreshing()) {
                                    profiles.clear();
                                }
                                for (int pr = 0; pr < items.length(); pr++) {
                                    JSONObject profileObject = items.getJSONObject(pr);
                                    User user = User.parseUser(profileObject);
                                    profiles.add(user.getId());
                                }

                                friendsAdapter.notifyDataSetChanged();
                                mSwipeRefreshLayout.setRefreshing(false);

                                progressBar.setVisibility(View.GONE);
                                rootLV.setVisibility(View.VISIBLE);
                            } else {
                                if (profiles.size() == 0) {
                                    empty.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                }
                            }
                        } catch (JSONException e) {
                            if (profiles.size() == 0) {
                                empty.setVisibility(View.VISIBLE);
                                empty.setText(R.string.loading_error);
                                progressBar.setVisibility(View.GONE);
                            }
                            e.printStackTrace();
                        }
                    }
                });
                break;
            }
            case FRIENDS: {
                RequestParams params = new RequestParams();
                params.put("fields", API.userFields);
                params.put("order", "hints");
                params.put("user_id", user_id);
                params.put("v", API.version);
                params.put("access_token", CandyApplication.access_token());
                params.put("lang", CandyApplication.lang);
                CandyApplication.client.get(API.BASE() + API.friendsGet, params, new TextHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if (profiles.size() == 0) {
                            empty.setVisibility(View.VISIBLE);
                            empty.setText(R.string.loading_error);
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        empty.setVisibility(View.GONE);
                        try {
                            JSONObject object = new JSONObject(responseString);
                            JSONObject response = object.getJSONObject("response");
                            JSONArray items = response.getJSONArray("items");
                            int length = items.length();
                            if (length > 0) {
                                if (mSwipeRefreshLayout.isRefreshing()) {
                                    profiles.clear();
                                }
                                for (int pr = 0; pr < items.length(); pr++) {
                                    JSONObject profileObject = items.getJSONObject(pr);
                                    User profile = User.parseUser(profileObject);
                                    profiles.add(profile.getId());
                                }

                                friendsAdapter.notifyDataSetChanged();
                                mSwipeRefreshLayout.setRefreshing(false);
                                progressBar.setVisibility(View.GONE);
                                rootLV.setVisibility(View.VISIBLE);
                            } else {
                                if (profiles.size() == 0) {
                                    empty.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                }
                            }
                        } catch (JSONException e) {
                            if (profiles.size() == 0) {
                                empty.setVisibility(View.VISIBLE);
                                empty.setText(R.string.loading_error);
                                progressBar.setVisibility(View.GONE);
                                e.printStackTrace();
                            }
                        }
                    }
                });
                break;
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().unregisterReceiver(broadcastReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        profiles.clear();
        addFriends();
    }

    private static final String[] ALLOWED_ACTIONS = {
            IMService.ACTION_USER_ONLINE,
            IMService.ACTION_USER_OFFLINE
    };

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case IMService.ACTION_USER_ONLINE: {
                    long user_id = intent.getLongExtra("user_id", 0);
                    for (long profile : profiles) {
                        if (profile == user_id) {
                            ((User) Cache.getAbstractProfile(profile)).setOnline(true);
                            break;
                        }
                    }
                    break;
                }
                case IMService.ACTION_USER_OFFLINE: {
                    long user_id = intent.getLongExtra("user_id", 0);
                    for (long profile : profiles) {
                        if (profile == user_id) {
                            ((User) Cache.getAbstractProfile(profile)).setOnline(false);
                            break;
                        }
                    }
                    break;
                }
            }
        }
    };

    /**
     * User: Vlad
     * Date: 30.01.2015
     * Time: 16:24
     */
    private static class FriendsAdapter extends ArrayAdapter<Long> {

        private ArrayList<Long> objects;
        private Picasso picasso;
        private Activity ctx;
        private LayoutInflater lInflater;


        FriendsAdapter(Activity context, ArrayList<Long> profiles) {
            super(context, 0, profiles);
            ctx = context;
            objects = profiles;
            lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            picasso = Picasso.with(ctx);
        }


        static class ViewHolder {
            // I added a generic return type to reduce the casting noise in client code
            @SuppressWarnings("unchecked")
            public static <T extends View> T get(View view, int id) {
                SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
                if (viewHolder == null) {
                    viewHolder = new SparseArray<>();
                    view.setTag(viewHolder);
                }
                View childView = viewHolder.get(id);
                if (childView == null) {
                    childView = view.findViewById(id);
                    viewHolder.put(id, childView);
                }
                return (T) childView;
            }
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            if (v == null) {
                v = lInflater.inflate(R.layout.list_item_friend, null, false);
            }

            final ImageView avatar = ViewHolder.get(v, R.id.avatar);
            final TextView name = ViewHolder.get(v, R.id.name);
            final RelativeLayout online = ViewHolder.get(v, R.id.online);
            final MaterialIconView onlineMobile = ViewHolder.get(v, R.id.online_mobile);
            final FLinearLayout add_to_friends = ViewHolder.get(v, R.id.add_to_friends);
            final FLinearLayout send_msg = ViewHolder.get(v, R.id.send_msg);
            final View hr = ViewHolder.get(v, R.id.hr);

            online.setVisibility(View.GONE);
            onlineMobile.setVisibility(View.GONE);

            final User profile = (User) Cache.getAbstractProfile(getItem(position));
            ctx.runOnUiThread(new Thread(() -> {
                if (profile != null) {
                    avatar.setImageDrawable(profile.getAvatarDrawable());

                    hr.setVisibility(position == getCount() - 1 ? View.GONE : View.VISIBLE);

                    if (!profile.isAvatarDefault()) {
                        picasso.load(profile.getPhotoFor(200)).tag("friends").transform(new CropCircleTransformation()).into(avatar);
                    }

                    name.setText(profile.getFullName());
                    if (profile.isOnline()) {
                        online.setVisibility(View.VISIBLE);
                        if (profile.isOnline_mobile()) {
                            onlineMobile.setVisibility(View.VISIBLE);
                        }
                    }

                    if (profile.getId() == CandyApplication.current().getUserId()) {
                        add_to_friends.setVisibility(View.GONE);
                    } else {
                        add_to_friends.setVisibility(profile.getFriend_status() == User.FriendStatus.YEP ? View.GONE : View.VISIBLE);
                    }

                    send_msg.setVisibility(profile.isCan_write_private_message() ? View.VISIBLE : View.GONE);

                    send_msg.setOnClickListener(v1 -> {
                        ChatFragment f = new ChatFragment();
                        f.init(profile.getFullName(), false, false, 0, profile.getId());
                        ((AbstractActivity) getContext()).openFragment(f);
                    });

                    add_to_friends.setOnClickListener(v1 -> {
                        RequestParams params = new RequestParams();
                        params.put("user_id", profile.getId());
                        params.put("text", "");
                        params.put("v", API.version);
                        params.put("access_token", CandyApplication.access_token());
                        params.put("lang", CandyApplication.lang);
                        CandyApplication.client.get(API.BASE() + API.friendsAdd, params, new TextHttpResponseHandler() {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                          /*Результат
                            После успешного выполнения возвращает одно из следующих значений:
                            1 — заявка на добавление данного пользователя в друзья отправлена;
                            2 — заявка на добавление в друзья от данного пользователя одобрена;
                            4 — повторная отправка заявки.*/
                            }
                        });
                    });
//
//                    ViewGroup.MarginLayoutParams lpt = (ViewGroup.MarginLayoutParams) avatar.getLayoutParams();
//                    lpt.setMargins(Math.round(Global.convertDpToPixel(ctx, 10)), 15, 0, 0);
//                    avatar.setLayoutParams(lpt);

                }
            }
            ));
            v.setOnClickListener(v12 -> {
                ProfileFragment f = new ProfileFragment();
                f.init(profile.getId());
                ((AbstractActivity) getContext()).openFragment(f);
            });
            return v;
        }


        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public Long getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

}
