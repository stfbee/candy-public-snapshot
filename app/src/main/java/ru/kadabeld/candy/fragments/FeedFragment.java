package ru.kadabeld.candy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.github.ksoichiro.android.observablescrollview.CacheFragmentStatePagerAdapter;
import com.google.common.collect.Lists;

import java.util.List;

import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.NetworkFragment;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.NewsfeedList;
import ru.kadabeld.candy.network.robospice.requests.NewsFeedRequest;
import rx.android.schedulers.AndroidSchedulers;

public class FeedFragment extends NetworkFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);
        ((AbstractActivity) getActivity()).makeActionBar(rootView);
        PagerSlidingTabStrip slidingTabLayout = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        ViewPager mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(4);
        FeedAdapter mPagerAdapter = new FeedAdapter(getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        slidingTabLayout.setViewPager(mPager);
        CandyApplication.getInstance()
                .getAccountsBus()
                .current()
                .filter(info1 -> info1 != null)
                .compose(bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(info -> requestObservable(new NewsFeedRequest(info)))
                .subscribe(mPagerAdapter::update, error -> {
                    Toast.makeText(getContext(), R.string.no_internet, Toast.LENGTH_LONG).show();
                    error.printStackTrace();
                });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AbstractActivity) getActivity()).colorize();
    }

    public class FeedAdapter extends CacheFragmentStatePagerAdapter {
        private List<String> titles = Lists.newArrayList();
        private List<NewsfeedList> lists = Lists.newArrayList();

        public FeedAdapter(FragmentManager fm) {
            super(fm);
        }

        public void update(List<NewsfeedList> lists) {
            titles.clear();
            this.lists.clear();
            this.titles.add(getString(R.string.ab_feed));
            this.lists = lists;
            for (NewsfeedList list : this.lists) {
                this.titles.add(list.getTitle());
            }
            notifyDataSetChanged();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return titles.size();
        }

        @Override
        protected Fragment createItem(int position) {
            NewsFragment newsFragment = new NewsFragment();
            Bundle bundle = new Bundle();
            switch (position) {
                case 0:
                    bundle.putString("type", "all_feed");
                    newsFragment.setArguments(bundle);
                    return newsFragment;
                default:
                    bundle.putString("type", "list");
                    bundle.putSerializable("NewsfeedList", lists.get(position - 1));
                    newsFragment.setArguments(bundle);
                    return newsFragment;
            }
        }
    }
}
