package ru.kadabeld.candy.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Dialog;
import ru.kadabeld.candy.entities.Document;
import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.entities.Message;
import ru.kadabeld.candy.entities.Sticker;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.fragments.chatsettings.WrapperFragment;
import ru.kadabeld.candy.im.IMService;
import ru.kadabeld.candy.utils.ApiUtils;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.HumanTime;
import ru.kadabeld.candy.utils.parsers.Attachments;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;
import ru.kadabeld.candy.views.WriteBar;

import static ru.kadabeld.candy.API.version;

@SuppressWarnings({"StatementWithEmptyBody", "deprecation", "Unchecked"})
@SuppressLint("InflateParams")
public class ChatFragment extends Fragment {
    private static final String TAG = ChatFragment.class.getCanonicalName();
    private final TreeMap<Long, Message> messagesTree = new TreeMap<>();
    private final TreeMap<Long, Message> unsentTree = new TreeMap<>();
    private final ArrayList<Message> messagesList = new ArrayList<>();
    private ListView rootLV;
    private Dialog dialog;
    private long start_message_id = 0;
    private ChatAdapter chatAdapter;
    private int unsentCounter = 0;
    private int onlineCount = 0;
    private int userCount = 0;
    private int normalpos = 0;
    private boolean lockedBottom = true;
    private SharedPreferences sPref;
    private AbstractActivity ctx;
    private ArrayList<Long> profiles = new ArrayList<>();
    private SlidingUpPanelLayout panelLayout;

    private String subtitle = "";
    private String title = "";
    private boolean is_chat;
    private boolean is_group;
    private long chat_id;
    private long user_id;

    private WriteBar writeBar;

    public void init(String title, boolean is_chat, boolean is_group, long chat_id, long user_id) {
        this.is_chat = is_chat;
        this.is_group = is_group;
        this.chat_id = chat_id;
        this.user_id = user_id;
        this.title = title;
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (dialog.isChat()) {
            addChatAvatar(menu);
        } else {
            long user_id = dialog.getUser_id();
            final AbstractProfile user = Cache.getAbstractProfile(user_id);
            AbstractProfile.ProfileOnLoad onLoad = profile -> addActionBarAvatar(menu, profile);

            if (user == null) {
                User.load(user_id, onLoad);
            } else {
                onLoad.run(user);
            }
        }
    }

    private void addActionBarAvatar(Menu menu, AbstractProfile user) {
        final ImageView photo = new ImageView(this.getActivity()) {
            public void onMeasure(int w, int h) {
                int z = MeasureSpec.getSize(h);
                this.setMeasuredDimension(z, z);
            }
        };
        if (user.isAvatarDefault()) {
            photo.setImageDrawable(user.getAvatarDrawable());
        } else {
            Picasso.with(getActivity()).load(user.getPhotoFor(200)).transform(new CropCircleTransformation()).into(photo);
        }

        int p = (int) Global.convertDpToPixel(getActivity(), 5);
        photo.setPadding(p, p, p, p);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        photo.setLayoutParams(params);

        MenuItem avatar = menu.add(0, 0, 0, getActivity().getResources().getString(R.string.profile));
        avatar.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        avatar.setActionView(photo);


        boolean notify_disabled = sPref.getBoolean("chat_" + user_id + "_notify_disabled", false);

        PopupMenu popupMenu = new PopupMenu(getContext(), photo);
        popupMenu.inflate(R.menu.menu_chat_user);
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.profile:
                    ProfileFragment profileFragment = new ProfileFragment();
                    profileFragment.init(dialog.getUser_id());
                    ((AbstractActivity) getActivity()).openFragment(profileFragment);
                    return true;

                case R.id.settings:
                    WrapperFragment settingsFragment = new WrapperFragment();
                    settingsFragment.init(chat_id, is_chat, user_id);
                    ((AbstractActivity) getActivity()).openFragment(settingsFragment);
                    return true;

                case R.id.mute:
                    sPref.edit().putBoolean("chat_" + user_id + "_notify_disabled", !notify_disabled).apply();
                    item.setTitle(getString(!notify_disabled ? R.string.menu_chat_unmute : R.string.menu_chat_mute));
                    return true;

                case R.id.clear:
                    RequestParams clear_params = new RequestParams();
                    clear_params.put("access_token", CandyApplication.access_token());
                    clear_params.put("v", version);
                    clear_params.put("user_id", user.getId());
                    CandyApplication.client.get(API.BASE() + API.messagesDeleteDialog, clear_params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            try {
                                if (response.getInt("response") == 1) {
                                    // TODO: 11.06.2016 alertdialog
                                    chatAdapter.clear();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    return true;

                case R.id.ban:
                    RequestParams ban_params = new RequestParams();
                    ban_params.put("access_token", CandyApplication.access_token());
                    ban_params.put("v", version);
                    ban_params.put("user_id", user.getId());

                    if (((User) user).isBlacklisted_by_me()) {
                        CandyApplication.client.get(API.BASE() + API.accountUnbanUser, ban_params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                try {
                                    if (response.getInt("response") == 1) {
                                        item.setTitle(getString(R.string.menu_chat_ban));
                                        ((User) user).setBlacklisted_by_me(false);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } else {
                        CandyApplication.client.get(API.BASE() + API.accountBanUser, ban_params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                try {
                                    if (response.getInt("response") == 1) {
                                        item.setTitle(getString(R.string.menu_chat_unban));
                                        ((User) user).setBlacklisted_by_me(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    return true;

                default:
                    return false;
            }
        });

        popupMenu.getMenu().findItem(R.id.profile).setTitle(getString(R.string.menu_chat_profile, user.getName(User.NameCase.GEN)));
        popupMenu.getMenu().findItem(R.id.ban).setTitle(getString(((User) user).isBlacklisted_by_me() ? R.string.menu_chat_unban : R.string.menu_chat_ban));
        popupMenu.getMenu().findItem(R.id.mute).setTitle(getString(notify_disabled ? R.string.menu_chat_unmute : R.string.menu_chat_mute));

        photo.setOnClickListener(v -> {
            popupMenu.show();
        });
    }

    private void addChatAvatar(Menu menu) {
        ImageView photo = new ImageView(this.getActivity()) {
            public void onMeasure(int w, int h) {
                int z = MeasureSpec.getSize(h);
                this.setMeasuredDimension(z, z);
            }
        };
        String photoFor = dialog.getPhotoFor(200);
        Drawable drawable = dialog.getAvatarDrawable();
        photo.setImageDrawable(drawable);
        if (!dialog.isAvatarDefault()) {
            Picasso.with(getActivity()).load(photoFor).transform(new CropCircleTransformation()).into(photo);
        }

        int p = (int) Global.convertDpToPixel(getActivity(), 5);
        photo.setPadding(p, p, p, p);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        photo.setLayoutParams(params);

        MenuItem avatar = menu.add(0, 0, 0, getActivity().getResources().getString(R.string.profile));
        avatar.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        avatar.setActionView(photo);
        photo.setOnClickListener(v -> {
            WrapperFragment f = new WrapperFragment();
            f.init(chat_id, is_chat, user_id);
            ((AbstractActivity) getActivity()).openFragment(f);
        });
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        ctx = ((AbstractActivity) getActivity());

        if (is_chat) {
            dialog = Cache.getChat(chat_id);
        } else {
            dialog = Cache.getDialog(user_id);
        }

        Dialog.DialogOnLoad runnable = dialog1 -> {
            // TODO: 14.04.2016 VOnishchenko
        };
        if (is_chat) {
            if (dialog == null) {
                Dialog.load(chat_id, runnable);
            } else {
                runnable.run(dialog);
            }
        } else {
            dialog = new Dialog();
            dialog.setChat(is_chat);
            dialog.setChat_id(chat_id);
            dialog.setUser_id(user_id);
            dialog.setGroup(is_group);
            dialog.setTitle(title);
        }


        View root = inflater.inflate(R.layout.fragment_chat, container, false);
        ctx.makeActionBar(root);

        ActionBar supportActionBar = ctx.getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(dialog.getTitle());
        }

        addUsers();
        setHasOptionsMenu(true);

        ctx.colorize();
        rootLV = (ListView) root.findViewById(R.id.listview_parallax);
        panelLayout = (SlidingUpPanelLayout) root.findViewById(R.id.sliding_layout);

        chatAdapter = new ChatAdapter(ctx, messagesList, is_chat);
        rootLV.setAdapter(chatAdapter);
        rootLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            int previousLastPosition = 0;

            void resetLastIndex() {
                previousLastPosition = 0;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, final int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
                final int lastPosition = firstVisibleItem + visibleItemCount;
                if (rootLV != null && rootLV.getChildCount() > 0) {
                    boolean firstItemVisible = rootLV.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = rootLV.getChildAt(0).getTop() == 0;
                    if (firstItemVisible && topOfFirstItemVisible) {
                        ctx.runOnUiThread(() -> {
                            normalpos = totalItemCount - firstVisibleItem;
                            addMessages();
                        });
                    }
                }
                if (lastPosition == totalItemCount) {
                    if (previousLastPosition != lastPosition) {
                        lockedBottom = true;
                    }
                    previousLastPosition = lastPosition;
                } else if (lastPosition < previousLastPosition - 1) {
                    resetLastIndex();
                    lockedBottom = false;
                }
            }
        });
        rootLV.post(() -> rootLV.smoothScrollToPosition(messagesList.size() - 1));

        writeBar = (WriteBar) root.findViewById(R.id.writebar);

        writeBar.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> panelLayout.setPanelHeight(writeBar.getHeight()));

        panelLayout.setPanelHeight(writeBar.getHeight());

        if (dialog.isWriting()) {
            if (getActivity() != null) {
                subtitle = String.format(getActivity().getResources().getString(R.string.chat_status), userCount, onlineCount);
                if (supportActionBar != null)
                    supportActionBar.setSubtitle(String.format(ctx.getResources().getString(R.string.chat_typing), dialog.getWriter().getName()));
            }
        }

        writeBar.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!sPref.getBoolean("ninja", false)) {
                    API.setActivity(!is_chat ? user_id : 2000000000 + chat_id);
                }
            }
        });

        writeBar.setOnSendClickListener(v -> {
            String text = writeBar.getEditText().getText().toString();
            if (!text.isEmpty()) {
                // TODO: 16.04.2016 add attaches
                sendMessage(text, null);
                writeBar.getEditText().getText().clear();
            }
        });

        writeBar.setOnEmojiClickListener(v -> {
            switch (panelLayout.getPanelState()) {
                case EXPANDED:
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    break;
                case COLLAPSED:
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    break;
            }
        });

        StickersPanelFragment stickersPanelFragment = new StickersPanelFragment();
        stickersPanelFragment.setOnStickerListener(v -> {
            Attachments attachments = new Attachments();
            long sticker_id = (long) v.getTag();
            attachments.getStickers().add(new Sticker(sticker_id));
            sendMessage("", attachments);
        });

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.stickers_fragment, stickersPanelFragment);
        fragmentTransaction.commit();

        return root;
    }

    private void sendMessage(String text, Attachments attachments) {
        final Message message = new Message();

        RequestParams params = new RequestParams();

        message.setChat(dialog.isChat());
        message.setFrom_id(CandyApplication.current().getUserId());
        message.setBody(text);
        params.put("message", text);

        if (dialog.isChat()) {
            message.setChat_id(dialog.getChat_id());
            params.put("chat_id", message.getChat_id());
        } else {
            message.setUser_id(dialog.getUser_id());
            params.put("user_id", message.getUser_id());
        }

        if (attachments != null) {
            message.setAttachments(attachments);
            ArrayList<Sticker> stickers = attachments.getStickers();
            if (!stickers.isEmpty()) {
                params.put("sticker_id", stickers.get(0).getId());
            }
        }

        params.put("text", text);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);

        message.setDate(System.currentTimeMillis() / 1000L);
        message.setOut(true);
        message.setSent(false);

        final long local_id = unsentCounter;
        unsentCounter++;
        message.setId(local_id);
        unsentTree.put(local_id, message);

        updateList();

        CandyApplication.client.get(API.BASE() + API.messagesSend, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    long id = object.getLong("response");

                    if (unsentTree.containsKey(local_id)) {
                        Message m = unsentTree.get(local_id);
                        unsentTree.remove(local_id);
                        m.setId(id);
                        m.setSent(true);
                        addMessage(m);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("JSON FAIL", object.toString());
                }
            }
        });
    }

    private void updateList() {
        messagesList.clear();
        messagesList.addAll(messagesTree.values());
        messagesList.addAll(unsentTree.values());
        chatAdapter.notifyDataSetChanged();
        if (lockedBottom) rootLV.setSelection(messagesList.size());

        if (!sPref.getBoolean("ninja", false)) {
            for (int size = messagesList.size() - 1; size >= 0; size--) {
                if (!messagesList.get(size).isOut()) {
                    long id = messagesList.get(size).getId();
                    if (is_chat) {
                        API.markAsRead(id, 2000000000 + chat_id);
                    } else {
                        API.markAsRead(id, user_id);
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().unregisterReceiver(broadcastReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iFilter = new IntentFilter();
        for (String action : ALLOWED_ACTIONS) {
            iFilter.addAction(action);
        }
        getActivity().registerReceiver(broadcastReceiver, iFilter);

        getActivity().runOnUiThread(() -> {
            start_message_id = 0;
            addMessages();
        });


        ((AbstractActivity) getActivity()).colorize();
    }

    private void addUsers() {
        if (dialog.isChat()) {
            final RequestParams params = new RequestParams();
            params.put("chat_id", dialog.getChat_id());
            params.put("fields", API.userFields);
            params.put("v", version);
            params.put("access_token", CandyApplication.access_token());
            params.put("lang", CandyApplication.lang);
            CandyApplication.client.get(API.BASE() + API.messagesGetChatUsers, params, new JsonHttpResponseHandler() {
                @SuppressLint("StringFormatMatches")
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                    try {
                        JSONArray userObjects = object.getJSONArray("response");
                        userCount = userObjects.length();

                        for (int i = 0; i < userObjects.length(); i++) {
                            JSONObject userObject = userObjects.getJSONObject(i);
                            User user = User.parseUser(userObject, chat_id);
                            profiles.add(user.getId());
                            if (user.isOnline()) onlineCount++;
                        }
                        if (getActivity() != null) {
                            subtitle = String.format(getActivity().getResources().getString(R.string.chat_status), userCount, onlineCount);
                            ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                            if (supportActionBar != null) supportActionBar.setSubtitle(subtitle);
                        }
                        chatAdapter.notifyDataSetChanged();
                        addMessages();
                    } catch (JSONException e) {
                        Log.e(TAG, object.toString());
                        e.printStackTrace();
                    }
                }
            });
        } else {
            addUser(dialog.getUser_id(), true);
        }
    }

    private void addUser(long id, final boolean loadmsgs) {
        AbstractProfile.ProfileOnLoad runnable = profile -> {
            profiles.add(profile.getId());
            ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (supportActionBar != null) {
                if (!dialog.isGroup()) {
                    if (((User) profile).isOnline()) {
                        subtitle = getActivity().getResources().getString(R.string.online);
                        supportActionBar.setSubtitle(subtitle);
                    } else {
                        subtitle = getActivity().getResources().getString(R.string.offline);
                        supportActionBar.setSubtitle(subtitle);
                    }
                }
                supportActionBar.setTitle(profile.getFullName());
            }


            if (loadmsgs) addMessages();
        };

        if (dialog.isGroup()) {
            Group.load(-id, runnable);
        } else {
            User.load(id, runnable);
        }
    }

    private void addMessages() {
        RequestParams params = new RequestParams();
        if (dialog.isChat()) {
            params.put("peer_id", 2000000000 + dialog.getChat_id());
        } else {
            params.put("user_id", dialog.getUser_id());
        }
        if (start_message_id != 0) params.put("start_message_id", start_message_id);
        params.put("count", 40);
        params.put("v", version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.messagesGetHistory, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    ApiUtils.ErrorType errorType = ApiUtils.parseError(object);
                    if (errorType != null) {
                        // TODO: 18.03.2016 VOnishchenko отобразить ошибку в UI или
                        Log.e(TAG, "что-то не так в ответе: " + errorType);
                        if (errorType == ApiUtils.ErrorType.TOO_MANY_REQUESTS) {
                            Thread.sleep(1000);
                            addMessages();
                        }
                        return;
                    }


                    if (object.has("response")) {
                        JSONObject response = object.getJSONObject("response");
                        JSONArray items = response.getJSONArray("items");
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject messageObject = items.getJSONObject(i);
                            Message message = Message.parseMessage(messageObject);
                            message.setChat(dialog.isChat());
                            message.setSent(true);
                            messagesTree.put(message.getId(), message);
                            start_message_id = message.getId() - 1;
                        }

                        messagesList.clear();
                        messagesList.addAll(messagesTree.values());
                        messagesList.addAll(unsentTree.values());
                        chatAdapter.notifyDataSetChanged();
                        rootLV.setSelection(messagesList.size() - normalpos);

                        if (!sPref.getBoolean("ninja", false)) {
                            for (int size = messagesList.size() - 1; size >= 0; size--) {
                                if (!messagesList.get(size).isOut()) {
                                    long id = messagesList.get(size).getId();
                                    if (is_chat) {
                                        API.markAsRead(id, 2000000000 + chat_id);
                                    } else {
                                        API.markAsRead(id, user_id);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    System.out.println(object.toString());
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void addMessage(Message m) {
        if (m.isOut()) {
            for (Map.Entry<Long, Message> entry : unsentTree.entrySet()) {
                Long key = entry.getKey();
                Message msg = entry.getValue();

                if (msg.getBody().equals(m.getBody())) {
                    unsentTree.remove(key);
                    break;
                }
            }
        }

        messagesTree.put(m.getId(), m);
        updateList();
    }

    private static final String[] ALLOWED_ACTIONS = {
            IMService.ACTION_CHAT_WRITING,
            IMService.ACTION_USER_WRITING,
            IMService.ACTION_MESSAGE_NEW,
            IMService.ACTION_FLAGS_REPLACE,
            IMService.ACTION_FLAGS_RESET,
            IMService.ACTION_FLAGS_SET,
            IMService.ACTION_MESSAGE_DELETE,
            IMService.ACTION_USER_ONLINE,
            IMService.ACTION_USER_OFFLINE,
            IMService.ACTION_READ_OUTCOME,
            IMService.ACTION_READ_INCOME,
            IMService.ACTION_CHAT_UPDATE
    };

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @SuppressLint("StringFormatMatches")
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            try {
                final ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                switch (action) {
                    case IMService.ACTION_CHAT_WRITING: {
                        long user_id = intent.getLongExtra("user_id", 0);
                        long chat_id = intent.getLongExtra("chat_id", 0);

                        if (dialog.isChat() && dialog.getChat_id() == chat_id) {
                            User writer = (User) Cache.getAbstractProfile(user_id);
                            AbstractProfile.ProfileOnLoad runnable = user -> {
                                dialog.setWriter((User) user);
                                dialog.setWriting(true);

                                if (supportActionBar != null)
                                    supportActionBar.setSubtitle(String.format(getActivity().getResources().getString(R.string.chat_typing), dialog.getWriter().getName()));

                                Runnable updateTextRunnable = () -> {
                                    dialog.setWriting(false);
                                    if (supportActionBar != null)
                                        supportActionBar.setSubtitle(subtitle);
                                };
                                dialog.getHandler().removeCallbacks(updateTextRunnable);
                                dialog.getHandler().postDelayed(updateTextRunnable, 5500);
                            };
                            if (writer == null) {
                                User.load(user_id, runnable);
                            } else {
                                runnable.run(writer);
                            }
                            break;
                        }

                        chatAdapter.notifyDataSetChanged();
                        break;
                    }
                    case IMService.ACTION_USER_WRITING: {
                        long user_id = intent.getLongExtra("user_id", 0);

                        if (!dialog.isChat() && dialog.getUser_id() == user_id) {
                            User writer = (User) Cache.getAbstractProfile(user_id);
                            AbstractProfile.ProfileOnLoad onLoad = user -> {
                                dialog.setWriter((User) user);
                                dialog.setWriting(true);

                                if (supportActionBar != null)
                                    supportActionBar.setSubtitle(String.format(getActivity().getResources().getString(R.string.chat_typing), dialog.getWriter().getName()));

                                Runnable updateTextRunnable = () -> {
                                    dialog.setWriting(false);
                                    if (supportActionBar != null)
                                        supportActionBar.setSubtitle(subtitle);
                                };
                                dialog.getHandler().removeCallbacks(updateTextRunnable);
                                dialog.getHandler().postDelayed(updateTextRunnable, 5500);
                            };
                            if (writer == null) {
                                User.load(user_id, onLoad);
                            } else {
                                onLoad.run(writer);
                            }
                            break;
                        }

                        chatAdapter.notifyDataSetChanged();
                        break;
                    }
                    case IMService.ACTION_MESSAGE_NEW: {
                        int flags = intent.getIntExtra("flags", 0);
                        long from_id = intent.getLongExtra("from_id", 0);
                        long timestamp = intent.getLongExtra("timestamp", 0);
                        long message_id = intent.getLongExtra("message_id", 0);
                        String subject = intent.getStringExtra("subject");
                        String text = StringEscapeUtils.unescapeHtml(intent.getStringExtra("text"));
                        JSONObject attachments = null;
                        try {
                            attachments = (new JSONObject(intent.getStringExtra("attachments")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        boolean UNREAD = (flags & 1) != 0;
                        boolean OUTBOX = (flags & 2) != 0;
                        boolean CHAT = from_id > 2000000000;


                        if (CHAT) {
                            Long chat_id = (from_id % 2000000000);
                            if (attachments != null) {
                                from_id = attachments.getLong("from");
                            }
                            if (dialog.isChat() && dialog.getChat_id() == chat_id) {
                                dialog.getMessage().setOut(OUTBOX);
//                            dialog.getMessage().setProfile2(Cache.profilesList.get(from_id));
                                dialog.getMessage().setDate(timestamp);
                                dialog.setWriting(false);

                                dialog.getMessage().setBody(text);
                                dialog.setTitle(subject);
                                dialog.getMessage().setRead_state(!UNREAD);
                                dialog.getMessage().FLAGS = flags;

                                Message m = new Message();

                                if (attachments != null) {
                                    if (attachments.has("source_act")) {
                                        m.setAction(Message.Action.valueOf(attachments.getString("source_act").toUpperCase()));
                                        m.setTechnical(true);
                                    }
                                    if (attachments.has("source_text")) {
                                        m.setAction_text(attachments.getString("source_text"));
                                        m.setTechnical(true);
                                    }
                                    if (attachments.has("source_email")) {
                                        m.setAction_email(attachments.getString("source_email"));
                                        m.setTechnical(true);
                                    }
                                    if (attachments.has("source_mid")) {
                                        m.setAction_mid(attachments.getLong("source_mid"));
                                        m.setTechnical(true);
                                    }
                                }

                                m.setId(message_id);
                                m.setChat(true);
                                m.setOut(OUTBOX);
                                m.setBody(text);
                                m.setFrom_id(from_id);
//                            m.setUser((User) Cache.getAbstractProfile(from_id));
                                m.setDate(timestamp);
                                m.setRead_state(!UNREAD);
                                m.setSent(true);
                                m.FLAGS = flags;

                                loadMessage(m);
                                break;
                            }
                        } else {
                            if (!dialog.isChat() && dialog.getUser_id() == from_id) {
                                dialog.getMessage().setOut(OUTBOX);
//                            dialog.getMessage().setProfile2(Cache.profilesList.get(from_id));
                                dialog.getMessage().setDate(timestamp);
                                dialog.setWriting(false);
                                dialog.getMessage().setBody(text);
                                dialog.getMessage().setRead_state(!UNREAD);
                                dialog.getMessage().FLAGS = flags;

                                Message m = new Message();
                                m.setId(message_id);
                                m.setChat(false);
                                m.setOut(OUTBOX);
                                m.setBody(text);
                                m.setFrom_id(from_id);
//                            m.setUser((User) Cache.getAbstractProfile(from_id));
                                m.setDate(timestamp);
                                m.setRead_state(!UNREAD);
                                m.setSent(true);
                                m.FLAGS = flags;

                                loadMessage(m);
                                break;
                            }
                        }

                        break;
                    }
                    case IMService.ACTION_READ_OUTCOME: {
                        long message_id = intent.getLongExtra("message_id", 0);
                        long from = intent.getLongExtra("from", 0);
                        boolean is_chat = intent.getBooleanExtra("is_chat", false);
                        if (is_chat) {
                            if (dialog.isChat() && dialog.getChat_id() == from) {
                                for (Map.Entry<Long, Message> entry : messagesTree.entrySet()) {
                                    final Message message = entry.getValue();
                                    if (message.getId() <= message_id && message.isOut()) {
                                        message.setRead_state(true);
                                        messagesTree.put(message.getId(), message);
                                    }
                                }
                            }
                        } else {
                            if (!dialog.isChat() && dialog.getUser_id() == from) {
                                for (Map.Entry<Long, Message> entry : messagesTree.entrySet()) {
                                    final Message message = entry.getValue();
                                    if (message.getId() <= message_id && message.isOut()) {
                                        message.setRead_state(true);
                                        messagesTree.put(message.getId(), message);
                                    }
                                }
                            }
                        }

                        updateList();
                        break;
                    }
                    case IMService.ACTION_READ_INCOME: {
                        long message_id = intent.getLongExtra("message_id", 0);
                        long from = intent.getLongExtra("from", 0);
                        boolean is_chat = intent.getBooleanExtra("is_chat", false);
                        if (is_chat) {
                            if (dialog.isChat() && dialog.getChat_id() == from) {
                                for (Map.Entry<Long, Message> entry : messagesTree.entrySet()) {
                                    final Message message = entry.getValue();
                                    if (message.getId() <= message_id && !message.isOut()) {
                                        message.setRead_state(true);
                                        messagesTree.put(message.getId(), message);
                                    }
                                }
                            }
                        } else {
                            if (!dialog.isChat() && dialog.getUser_id() == from) {
                                for (Map.Entry<Long, Message> entry : messagesTree.entrySet()) {
                                    final Message message = entry.getValue();
                                    if (message.getId() <= message_id && !message.isOut()) {
                                        message.setRead_state(true);
                                        messagesTree.put(message.getId(), message);
                                    }
                                }
                            }
                        }

                        updateList();
                        break;
                    }
                    case IMService.ACTION_FLAGS_REPLACE: {
                        long message_id = intent.getLongExtra("message_id", 0);
                        int flags = intent.getIntExtra("flags", 0);
                        for (Map.Entry<Long, Message> entry : messagesTree.entrySet()) {
                            final Message message = entry.getValue();
                            if (!dialog.isChat() && dialog.getMessage().getId() == message_id) {
                                message.FLAGS = flags;
                                boolean UNREAD = (message.FLAGS & 1) != 0;
                                message.setRead_state(!UNREAD);
                                messagesTree.put(message.getId(), message);
                            }
                        }

                        updateList();
                        break;
                    }
                    case IMService.ACTION_FLAGS_SET: {
                        long message_id = intent.getLongExtra("message_id", 0);
                        int mask = intent.getIntExtra("mask", 0);
                        boolean is_chat = intent.getBooleanExtra("is_chat", false);
                        long from = intent.getLongExtra("from", 0);
                        for (Map.Entry<Long, Message> entry : messagesTree.entrySet()) {
                            final Message message = entry.getValue();
                            if (is_chat) {
                                if (dialog.isChat() && dialog.getChat_id() == from && dialog.getMessage().getId() == message_id) {
                                    message.FLAGS |= mask;
                                    boolean UNREAD = (message.FLAGS & 1) != 0;
                                    message.setRead_state(!UNREAD);
                                    messagesTree.put(message.getId(), message);
                                }
                            } else {
                                if (!dialog.isChat() && dialog.getUser_id() == from && dialog.getMessage().getId() == message_id) {
                                    message.FLAGS |= mask;
                                    boolean UNREAD = (message.FLAGS & 1) != 0;
                                    message.setRead_state(!UNREAD);
                                    messagesTree.put(message.getId(), message);
                                }
                            }
                        }

                        updateList();
                        break;
                    }
                    case IMService.ACTION_FLAGS_RESET: {
                        long message_id = intent.getLongExtra("message_id", 0);
                        int mask = intent.getIntExtra("mask", 0);
                        boolean is_chat = intent.getBooleanExtra("is_chat", false);
                        long from = intent.getLongExtra("from", 0);
                        for (Map.Entry<Long, Message> entry : messagesTree.entrySet()) {
                            final Message message = entry.getValue();
                            if (is_chat) {
                                if (dialog.isChat() && dialog.getChat_id() == from && dialog.getMessage().getId() == message_id) {
                                    message.FLAGS &= ~mask;
                                    boolean UNREAD = (message.FLAGS & 1) != 0;
                                    message.setRead_state(!UNREAD);
                                    messagesTree.put(message.getId(), message);
                                }
                            } else {
                                if (!dialog.isChat() && dialog.getUser_id() == from && dialog.getMessage().getId() == message_id) {
                                    message.FLAGS &= ~mask;
                                    boolean UNREAD = (message.FLAGS & 1) != 0;
                                    message.setRead_state(!UNREAD);
                                    messagesTree.put(message.getId(), message);
                                }
                            }
                        }

                        updateList();
                        break;
                    }
                    case IMService.ACTION_USER_ONLINE: {
                        long user_id = intent.getLongExtra("user_id", 0);
                        if (profiles.contains(user_id)) {
                            ((User) Cache.getAbstractProfile(user_id)).setOnline(true);
                            onlineCount++;
                            if (dialog.isChat()) {
                                subtitle = String.format(getActivity().getResources().getString(R.string.chat_status), userCount, onlineCount);

                                if (supportActionBar != null)
                                    supportActionBar.setSubtitle(subtitle);
                            } else {
                                subtitle = getActivity().getResources().getString(R.string.online);

                                if (supportActionBar != null)
                                    supportActionBar.setSubtitle(subtitle);
                            }
                        }
                        break;
                    }
                    case IMService.ACTION_USER_OFFLINE: {
                        long user_id = intent.getLongExtra("user_id", 0);
                        if (profiles.contains(user_id)) {
                            ((User) Cache.getAbstractProfile(user_id)).setOnline(false);
                            onlineCount--;
                            if (dialog.isChat()) {
                                subtitle = String.format(getActivity().getResources().getString(R.string.chat_status), userCount, onlineCount);

                                if (supportActionBar != null)
                                    supportActionBar.setSubtitle(subtitle);
                            } else {
                                subtitle = getActivity().getResources().getString(R.string.offline);

                                if (supportActionBar != null)
                                    supportActionBar.setSubtitle(subtitle);
                            }
                        }
                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void loadMessage(final Message m) {
        long message_id = m.getId();

        RequestParams params = new RequestParams();
        params.put("message_ids", message_id);
        params.put("v", version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.messagesGetById, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    if (object.has("response")) {
                        JSONObject response = object.getJSONObject("response");
                        JSONArray msgObjects = response.getJSONArray("items");
                        JSONObject msgObject = msgObjects.getJSONObject(0);
                        if (msgObject.has("attachments")) {
                            JSONArray attachmentObjects = msgObject.getJSONArray("attachments");
                            m.setAttachments(Attachments.parseJson(attachmentObjects));

                            if (dialog.getMessage().getBody().isEmpty()) {
                                dialog.getMessage().setBody(getActivity().getString(R.string.attachments_many));
                            }
                        }
                        if (msgObject.has("fwd_messages")) {
                            JSONArray fwd_messagesObjects = msgObject.getJSONArray("fwd_messages");
                            ArrayList<Message> fwd_messages = new ArrayList<>();
                            for (int i = 0; i < fwd_messagesObjects.length(); i++) {
                                JSONObject o = fwd_messagesObjects.getJSONObject(i);
                                Message m = Message.parseMessage(o);
                                fwd_messages.add(m);
                            }
                            m.setFwd_messages(fwd_messages);

                            if (dialog.getMessage().getBody().isEmpty() && !msgObject.has("attachments")) {
                                dialog.getMessage().setBody(getActivity().getString(R.string.chat_quote));
                            }
                        }
                        addMessage(m);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean onBackPressed() {
        SlidingUpPanelLayout.PanelState state = panelLayout.getPanelState();
        if (state == SlidingUpPanelLayout.PanelState.EXPANDED) {
            panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return false;
        }
        return true;
    }


    /**
     * User: Vlad
     * Date: 26.02.2015
     * Time: 17:46
     */
    private static class ChatAdapter extends ArrayAdapter<Message> {
        Activity ctx;
        LayoutInflater lInflater;
        ArrayList<Message> messages;
        Picasso picasso;
        boolean isChat = false;

        int dp2, dp4, dp5, dp6, dp19, dp40, dp50, dp20, sticker_size;

        ChatAdapter(Activity context, ArrayList<Message> messages, boolean isChat) {
            super(context, 0, messages);
            ctx = context;
            this.isChat = isChat;
            this.messages = messages;
            lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            picasso = Picasso.with(context);

            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            sticker_size = sharedPreferences.getInt("stickersize", 128);

            dp2 = Math.round(Global.convertDpToPixel(ctx, 2));
            dp4 = Math.round(Global.convertDpToPixel(ctx, 4));
            dp5 = Math.round(Global.convertDpToPixel(ctx, 5));
            dp6 = Math.round(Global.convertDpToPixel(ctx, 6));
            dp19 = Math.round(Global.convertDpToPixel(ctx, 19));
            dp20 = Math.round(Global.convertDpToPixel(ctx, 20));
            dp40 = Math.round(Global.convertDpToPixel(ctx, 40));
            dp50 = Math.round(Global.convertDpToPixel(ctx, 50));
        }

        @Override
        public int getCount() {
            return messages.size();
        }

        @Override
        public Message getItem(int position) {
            return messages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        static class ViewHolder {
            // I added a generic return type to reduce the casting noise in client code
            @SuppressWarnings("unchecked")
            public static <T extends View> T get(View view, int id) {
                SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
                if (viewHolder == null) {
                    viewHolder = new SparseArray<>();
                    view.setTag(viewHolder);
                }
                View childView = viewHolder.get(id);
                if (childView == null) {
                    childView = view.findViewById(id);
                    viewHolder.put(id, childView);
                }
                return (T) childView;
            }
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View v, ViewGroup parent) {
            final Message message = messages.get(position);

            int l = messages.size() - 1;

            final boolean drawAvatar;
            boolean drawTime;
            boolean drawCorner;


            boolean last = position == l;
            boolean first = position == 0;

            int threehours = 3 * 60 * 60;

            // не техническое И (сообщение последнее ИЛИ следующее от другого юзера ИЛИ следующее техническое) И разница с следующем сообщением больше трех часов
            //        drawTime

            // не техническое И (сообщение первое ИЛИ предыдущее от другого юзера ИЛИ предыдущее техническое) И чат
            //        drawAvatar

            // то же самое, но без чата в условии
            //        drawCorner

            if (!message.isTechnical()) {
                if (last) {
                    drawTime = true;
                } else {
                    Message next_message = messages.get(position + 1);
                    drawTime = next_message.getFrom_id() != message.getFrom_id() || next_message.isTechnical() || message.getDate() - next_message.getDate() > threehours;
                }

                if (first) {
                    drawCorner = true;
                    drawAvatar = isChat;
                } else {
                    Message prev_message = messages.get(position - 1);
                    drawCorner = prev_message.getFrom_id() != message.getFrom_id() || prev_message.isTechnical();

                    if (prev_message.getFrom_id() != message.getFrom_id()) {
                        drawAvatar = isChat;
                    } else {
                        drawAvatar = prev_message.isTechnical() && isChat;
                    }
                }
            } else {
                drawTime = false;
                drawAvatar = false;
                drawCorner = false;
            }


            v = lInflater.inflate(R.layout.list_item_message, null, false);

            final RelativeLayout container = ViewHolder.get(v, R.id.container);
            final LinearLayout action_layout = ViewHolder.get(v, R.id.action_layout);
            final RelativeLayout message_body = ViewHolder.get(v, R.id.message_body);
            final ImageView avatar = ViewHolder.get(v, R.id.avatar);
            final ImageView sticker = ViewHolder.get(v, R.id.sticker);
            final ImageView newcover = ViewHolder.get(v, R.id.newcover);
            final TextView content = ViewHolder.get(v, R.id.content);
            final TextView action_text = ViewHolder.get(v, R.id.action_text);
            final TextView newtitle = ViewHolder.get(v, R.id.newtitle);
            final LinearLayout block_msg = ViewHolder.get(v, R.id.block_msg);
            final FrameLayout attachmentsFrame = ViewHolder.get(v, R.id.attachments);
            final LinearLayout repostsFrame = ViewHolder.get(v, R.id.reposts);
            final TextView time = ViewHolder.get(v, R.id.time);


            final RelativeLayout.LayoutParams containerLayoutParams = (RelativeLayout.LayoutParams) container.getLayoutParams();
            final RelativeLayout.LayoutParams block_msgLayoutParams = (RelativeLayout.LayoutParams) block_msg.getLayoutParams();
            final RelativeLayout.LayoutParams message_bodyLayoutParams = (RelativeLayout.LayoutParams) message_body.getLayoutParams();
            final RelativeLayout.LayoutParams stickerLayoutParams = (RelativeLayout.LayoutParams) sticker.getLayoutParams();
            final RelativeLayout.LayoutParams timeLayoutParams = (RelativeLayout.LayoutParams) time.getLayoutParams();
            final RelativeLayout.LayoutParams avatarLayoutParams = (RelativeLayout.LayoutParams) avatar.getLayoutParams();

            stickerLayoutParams.height = sticker_size;
            stickerLayoutParams.width = sticker_size;

            if (message.isOut()) {
                v.setPadding(dp50, dp5, dp5, dp5);
                containerLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                avatarLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                stickerLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                block_msgLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                timeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                message_bodyLayoutParams.addRule(RelativeLayout.LEFT_OF, R.id.avatar);
                if (drawCorner) {
                    block_msg.setBackgroundResource(R.drawable.msg_bubble_outgoing);
                } else {
                    block_msg.setBackgroundResource(R.drawable.msg_bubble_outgoing_no_triangle);
                }
            } else {
                v.setPadding(dp5, dp5, dp50, dp5);
                containerLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                block_msgLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                avatarLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                stickerLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                timeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                message_bodyLayoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.avatar);
                if (drawCorner) {
                    block_msg.setBackgroundResource(R.drawable.msg_bubble_incoming);
                } else {
                    block_msg.setBackgroundResource(R.drawable.msg_bubble_incoming_no_triangle);
                }
            }


            if (message.isRead_state()) {
                v.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorBackground));
            } else {
                v.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorUnread));
            }

            newcover.setVisibility(View.GONE);
            newtitle.setVisibility(View.GONE);

            if (message.isTechnical()) {
                v.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorBackground));
                v.setPadding(dp5, dp5, dp5, dp5);
                container.setVisibility(View.GONE);
                action_layout.setVisibility(View.VISIBLE);

                final Message.Action action = message.getAction();
                final User from = ((User) Cache.getAbstractProfile(message.getFrom_id()));
                final User.Sex sex = from.getSex();

                final boolean owner = message.getFrom_id() == CandyApplication.current().getUserId();
                final boolean self = message.getAction_mid() == message.getFrom_id();

                AbstractProfile.ProfileOnLoad onLoad = profile -> {
                    User mid = (User) profile;
                    if (!message.getAction_email().isEmpty()) {
                        mid.setMail(message.getAction_email());
                    }
                    switch (action) {
                        case CHAT_PHOTO_UPDATE:
                            newcover.setVisibility(View.VISIBLE);
                            picasso.load(message.getAttachments().getPhotos().get(0).getPhoto_604()).tag("chat").transform(new CropCircleTransformation()).into(newcover);
                            if (owner) {
                                action_text.setText(ctx.getString(R.string.chat_action_photo_update_you));
                            } else {
                                switch (sex) {
                                    case NONE:
                                    case MALE:
                                        action_text.setText(ctx.getString(R.string.chat_action_photo_update_male, from.getName()));
                                        break;
                                    case FEMALE:
                                        action_text.setText(ctx.getString(R.string.chat_action_photo_update_female, from.getName()));
                                        break;
                                }
                            }
                            break;
                        case CHAT_PHOTO_REMOVE:
                            if (owner) {
                                action_text.setText(ctx.getString(R.string.chat_action_photo_remove_you));
                            } else {
                                switch (sex) {
                                    case NONE:
                                    case MALE:
                                        action_text.setText(ctx.getString(R.string.chat_action_photo_remove_male, from.getName()));
                                        break;
                                    case FEMALE:
                                        action_text.setText(ctx.getString(R.string.chat_action_photo_remove_female, from.getName()));
                                        break;
                                }
                            }
                            break;
                        case CHAT_CREATE:
                            if (owner) {
                                action_text.setText(ctx.getString(R.string.chat_action_create_you));
                            } else {
                                switch (sex) {
                                    case NONE:
                                    case MALE:
                                        action_text.setText(ctx.getString(R.string.chat_action_create_male, from.getName()));
                                        break;
                                    case FEMALE:
                                        action_text.setText(ctx.getString(R.string.chat_action_create_female, from.getName()));
                                        break;
                                }
                            }
                            break;
                        case CHAT_TITLE_UPDATE:
                            newtitle.setVisibility(View.VISIBLE);
                            newtitle.setText(String.format("\"%s\"", message.getAction_text()));
                            if (owner) {
                                action_text.setText(ctx.getString(R.string.chat_action_title_update_you));
                            } else {
                                switch (sex) {
                                    case NONE:
                                    case MALE:
                                        action_text.setText(ctx.getString(R.string.chat_action_title_update_male, from.getName()));
                                        break;
                                    case FEMALE:
                                        action_text.setText(ctx.getString(R.string.chat_action_title_update_female, from.getName()));
                                        break;
                                }
                            }
                            break;
                        case CHAT_INVITE_USER:
                            if (owner) {
                                if (self) {
                                    action_text.setText(ctx.getString(R.string.chat_action_returned_you));
                                } else {
                                    action_text.setText(ctx.getString(R.string.chat_action_invite_user_you, mid.getName(User.NameCase.ACC)));
                                }
                            } else {
                                switch (sex) {
                                    case NONE:
                                    case MALE:
                                        if (self) {
                                            action_text.setText(ctx.getString(R.string.chat_action_returned_male, from.getName()));
                                        } else {
                                            action_text.setText(ctx.getString(R.string.chat_action_invite_user_male, from.getName(), mid.getName(User.NameCase.ACC)));
                                        }
                                        break;
                                    case FEMALE:
                                        if (self) {
                                            action_text.setText(ctx.getString(R.string.chat_action_returned_female, from.getName()));
                                        } else {
                                            action_text.setText(ctx.getString(R.string.chat_action_invite_user_female, from.getName(), mid.getName(User.NameCase.ACC)));
                                        }
                                        break;
                                }
                            }
                            break;
                        case CHAT_KICK_USER:
                            if (owner) {
                                if (self) {
                                    action_text.setText(ctx.getString(R.string.chat_action_left_you));
                                } else {
                                    action_text.setText(ctx.getString(R.string.chat_action_kick_user_you, mid.getName(User.NameCase.ACC)));

                                }
                            } else {
                                switch (sex) {
                                    case NONE:
                                    case MALE:
                                        if (self) {
                                            action_text.setText(ctx.getString(R.string.chat_action_left_male, from.getName()));
                                        } else {
                                            action_text.setText(ctx.getString(R.string.chat_action_kick_user_male, from.getName(), mid.getName(User.NameCase.ACC)));
                                        }
                                        break;
                                    case FEMALE:
                                        if (self) {
                                            action_text.setText(ctx.getString(R.string.chat_action_left_female, from.getName()));
                                        } else {
                                            action_text.setText(ctx.getString(R.string.chat_action_kick_user_female, from.getName(), mid.getName(User.NameCase.ACC)));
                                        }
                                        break;
                                }
                            }
                            break;
                    }
                };
                if (message.getAction_mid() != 0) {
                    User mid = ((User) Cache.getAbstractProfile(message.getAction_mid()));
                    if (mid == null) {
                        User.load(message.getAction_mid(), onLoad);
                    } else {
                        if (mid.getName(User.NameCase.GEN).isEmpty()) {
                            User.load(message.getAction_mid(), onLoad);
                        } else {
                            if (!message.getAction_email().isEmpty()) {
                                mid.setMail(message.getAction_email());
                            }
                            onLoad.run(mid);
                        }
                    }
                } else {
                    onLoad.run(null);
                }


            } else {
                container.setVisibility(View.VISIBLE);
                action_layout.setVisibility(View.GONE);
            }

            if (message.isSent()) {
                v.setAlpha(1f);
            } else {
                v.setAlpha(.7f);
            }

            final AbstractProfile user;

            AbstractProfile.ProfileOnLoad runnable = profile -> {
                if (drawAvatar) {
                    avatar.setImageDrawable(profile.getAvatarDrawable());
                    if (!profile.isAvatarDefault()) {
                        picasso.load(profile.getPhotoFor(200)).tag("chat").transform(new CropCircleTransformation()).into(avatar);
                    }
                    avatar.setOnClickListener(v1 -> {
                        ProfileFragment f = new ProfileFragment();
                        f.init(profile.getId());
                        ((AbstractActivity) getContext()).openFragment(f);
                    });
                }
            };

            if (message.isGroup()) {
                user = Cache.getAbstractProfile(message.getUser_id());

                if (user == null) {
                    Group.load(-message.getUser_id(), runnable);
                } else {
                    runnable.run(user);
                }
            } else {
                user = Cache.getAbstractProfile(Math.max(message.getUser_id(), message.getFrom_id()));
            }

            if (isChat) {
                if (drawAvatar) {
                    avatar.setVisibility(View.VISIBLE);
                    avatarLayoutParams.height = dp40;
                    if (user != null) {
                        avatar.setImageDrawable(user.getAvatarDrawable());
                        if (!user.isAvatarDefault()) {
                            picasso.load(user.getPhotoFor(200)).tag("chat").transform(new CropCircleTransformation()).into(avatar);
                        }
                        avatar.setOnClickListener(v1 -> {
                            ProfileFragment f = new ProfileFragment();
                            f.init(user.getId());
                            ((AbstractActivity) getContext()).openFragment(f);
                        });
                    }
                } else {
                    avatar.setVisibility(View.INVISIBLE);
                    avatarLayoutParams.height = 0;
                }
            } else {
                avatar.setVisibility(View.GONE);
            }

            if (isChat) {
                time.setPadding(dp19 + dp40, 0, dp19 + dp40, 0);
            } else {
                time.setPadding(dp19, 0, dp19, 0);
            }

            if (drawTime) {
                time.setVisibility(View.VISIBLE);
                if (message.isOut()) {
                    avatarLayoutParams.addRule(RelativeLayout.ABOVE, R.id.time);
                }
                HumanTime ht = new HumanTime();
                time.setText(ht.getTime(message.getDate()));
            } else {
                time.setVisibility(View.GONE);
            }

            block_msg.removeAllViews();

            if (!message.getBody().trim().isEmpty()) {
                content.setVisibility(View.VISIBLE);
                content.setText(message.getBody().trim());
                block_msg.addView(content);
            } else {
                content.setVisibility(View.GONE);
            }

            int content_width = Global.getWindowSize(ctx)[0] - (int) Global.convertDpToPixel(ctx, 5 + 50 + 19 + 9);
            if (isChat) {
                content_width -= dp40;
            }

            Attachments attachments = message.getAttachments();
            if (!message.getGeo().getCoordinates().isEmpty()) {
                attachments.addGeos(message.getGeo());
            }
            if (!attachments.isEmpty()) {
                final ArrayList<Sticker> stickers = attachments.getStickers();
                final ArrayList<Document> graffiti = attachments.getGraffiti();

                if (!stickers.isEmpty()) {
                    sticker.setVisibility(View.VISIBLE);
                    block_msg.setVisibility(View.GONE);

                    time.setPadding(0, 0, 0, 0);
                    picasso.load(stickers.get(0).getPhotoFor(sticker_size)).tag("chat").into(sticker);
                } else if (!graffiti.isEmpty()) {
                    sticker.setVisibility(View.VISIBLE);
                    block_msg.setVisibility(View.GONE);
                    time.setPadding(0, 0, 0, 0);

                    Document document = graffiti.get(0);
                    sticker.getLayoutParams().height = document.getHeight();
                    sticker.getLayoutParams().width = document.getWidth();
                    picasso.load(document.getPhoto_o()).tag("chat").into(sticker);
                } else {
                    putHr(block_msg, ctx);

                    if (isChat) {
                        time.setPadding(dp19 + dp40, 0, dp19 + dp40, 0);
                    } else {
                        time.setPadding(dp19, 0, dp19, 0);
                    }

                    block_msg.setVisibility(View.VISIBLE);
                    sticker.setVisibility(View.GONE);
                    attachments.getView(ctx, attachmentsFrame, content_width);
                    block_msg.addView(attachmentsFrame);
                }
            } else {
                attachmentsFrame.setVisibility(View.GONE);
            }

            ArrayList<Message> reposts = message.getFwd_messages();
            if (!reposts.isEmpty()) {
                putHr(block_msg, ctx);
                repostsFrame.setVisibility(View.VISIBLE);
                QuoteAdapter quoteAdapter = new QuoteAdapter(reposts);
                for (int i = 0, repostsSize = reposts.size(); i < repostsSize; i++) {
                    repostsFrame.addView(quoteAdapter.getView(i, content_width));
                }
                block_msg.addView(repostsFrame);
            } else {
                repostsFrame.setVisibility(View.GONE);
            }

            if (attachments.isEmpty() && reposts.isEmpty()) {
                block_msgLayoutParams.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
                block_msgLayoutParams.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
            } else {
                block_msgLayoutParams.width = RelativeLayout.LayoutParams.MATCH_PARENT;
                block_msgLayoutParams.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
            }

            return v;
        }

        class QuoteAdapter {
            private ArrayList<Message> reposts;

            QuoteAdapter(ArrayList<Message> reposts) {
                this.reposts = reposts;
            }

            public View getView(int position, int content_width) {
                LayoutInflater inflater = LayoutInflater.from(ctx);
                View view = inflater.inflate(R.layout.element_quote, null);

                Message message = reposts.get(position);

                boolean drawHeader = (position == 0) || (position > 0 && reposts.get(position - 1).getUser_id() != message.getUser_id());

                final ViewHolder viewHolder = new ViewHolder();
                viewHolder.stickerRepost = (ImageView) view.findViewById(R.id.sticker_repost);
                viewHolder.nameRepost = (TextView) view.findViewById(R.id.repost_name);
                viewHolder.quoteTreeButton = (TextView) view.findViewById(R.id.quote_tree_button);
                viewHolder.timeRepost = (TextView) view.findViewById(R.id.time_repost);
                viewHolder.contentRepost = (TextView) view.findViewById(R.id.content_repost);
                viewHolder.attachmentsRepost = (FrameLayout) view.findViewById(R.id.attachments_repost);
                viewHolder.quoteHeader = (RelativeLayout) view.findViewById(R.id.quote_header);
                viewHolder.quote_body = (LinearLayout) view.findViewById(R.id.quote_body);

                if (drawHeader) {
                    viewHolder.quoteHeader.setVisibility(View.VISIBLE);
                    long user_id = Math.max(message.getUser_id(), message.getFrom_id());
                    final User user = (User) Cache.getAbstractProfile(user_id);
                    AbstractProfile.ProfileOnLoad onLoad = profile -> {
                        viewHolder.nameRepost.setText(profile.getFullName());
                        viewHolder.nameRepost.setOnClickListener(v -> {
                            ProfileFragment f = new ProfileFragment();
                            f.init(profile.getId());
                            ((AbstractActivity) getContext()).openFragment(f);
                        });
                    };
                    if (user == null) {
                        User.load(user_id, onLoad);
                    } else {
                        onLoad.run(user);
                    }
                } else {
                    viewHolder.quoteHeader.setVisibility(View.GONE);
                }

                viewHolder.quote_body.removeAllViews();

                HumanTime ht = new HumanTime();
                viewHolder.timeRepost.setText(ht.getTime(message.getDate()));
                if (!message.getBody().trim().isEmpty()) {
                    viewHolder.contentRepost.setVisibility(View.VISIBLE);
                    viewHolder.contentRepost.setText(message.getBody().trim());
                    viewHolder.quote_body.addView(viewHolder.contentRepost);
                } else {
                    viewHolder.contentRepost.setVisibility(View.GONE);
                }

                Attachments attachments = message.getAttachments();
                if (!message.getGeo().getCoordinates().isEmpty()) {
                    attachments.addGeos(message.getGeo());
                }
                if (!attachments.isEmpty()) {

                    putHr(viewHolder.quote_body, ctx);
                    final ArrayList<Sticker> stickers = attachments.getStickers();
                    final ArrayList<Document> graffiti = attachments.getGraffiti();

                    if (!stickers.isEmpty()) {
                        viewHolder.stickerRepost.setVisibility(View.VISIBLE);
                        viewHolder.stickerRepost.getLayoutParams().height = sticker_size;
                        viewHolder.stickerRepost.getLayoutParams().width = sticker_size;
                        picasso.load(stickers.get(0).getPhotoFor(sticker_size)).tag("chat").into(viewHolder.stickerRepost);
                    } else if (!graffiti.isEmpty()) {
                        viewHolder.stickerRepost.setVisibility(View.VISIBLE);
                        Document document = graffiti.get(0);
                        viewHolder.stickerRepost.getLayoutParams().height = document.getHeight();
                        viewHolder.stickerRepost.getLayoutParams().width = document.getWidth();
                        picasso.load(document.getPhoto_o()).tag("chat").into(viewHolder.stickerRepost);
                    } else {
                        putHr(viewHolder.quote_body, ctx);
                        viewHolder.stickerRepost.setVisibility(View.GONE);

                        content_width -= (int) Global.convertDpToPixel(ctx, 14);

                        attachments.getView(ctx, viewHolder.attachmentsRepost, content_width);
                        viewHolder.quote_body.addView(viewHolder.attachmentsRepost);
                    }
                } else {
                    viewHolder.attachmentsRepost.setVisibility(View.GONE);
                }

                ArrayList<Message> reposts = message.getFwd_messages();
                if (!reposts.isEmpty()) {
                    putHr(viewHolder.quote_body, ctx);
                    viewHolder.quoteTreeButton.setVisibility(View.VISIBLE);

                    viewHolder.quoteTreeButton.setOnClickListener(v -> Toast.makeText(ctx, R.string.dev_not_implemented_yet, Toast.LENGTH_SHORT).show());

                    viewHolder.quote_body.addView(viewHolder.quoteTreeButton);
                } else {
                    viewHolder.quoteTreeButton.setVisibility(View.GONE);
                }

                return view;
            }

            protected class ViewHolder {
                private LinearLayout quote_body;
                private RelativeLayout quoteHeader;
                private TextView nameRepost;
                private TextView timeRepost;
                private TextView contentRepost;
                private ImageView stickerRepost;
                private TextView quoteTreeButton;
                private FrameLayout attachmentsRepost;
            }
        }

        private void putHr(LinearLayout layout, Context context) {
            if (layout.getChildCount() > 0) {
                View view = new View(context);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp4);
                view.setLayoutParams(layoutParams);
                layout.addView(view);
            }
        }
    }
}
