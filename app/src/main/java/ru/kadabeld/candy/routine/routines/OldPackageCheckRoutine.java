package ru.kadabeld.candy.routine.routines;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import java.util.List;

import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import rx.functions.Action0;

/**
 * Created by VOnishchenko
 * 16.05.2016
 * 16:38
 */
public class OldPackageCheckRoutine implements Action0 {

    private static final int NOTIFICATION_ID = OldPackageCheckRoutine.class.hashCode();
    private static final String package_uri = "ru.cd.candy";

    @Override
    public void call() {
        if (isPackageExisted()) {
            notifyNoVersion();
        }
    }

    private boolean isPackageExisted() {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = CandyApplication.getInstance().getApplicationContext().getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(package_uri)) {
                return true;
            }
        }
        return false;
    }

    private void notifyNoVersion() {
        NotificationManager mNotificationManager = (NotificationManager) CandyApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(CandyApplication.getInstance().getApplicationContext());
        mNotificationManager.notify(NOTIFICATION_ID,
                mBuilder.setContentIntent(getUninstallIntent())
                        .setAutoCancel(true)
                        .setOngoing(false)
                        .setSmallIcon(android.R.drawable.ic_dialog_info)
                        .setContentTitle(CandyApplication.getInstance().getApplicationContext().getString(R.string.oldpkg_found))
                        .setContentText(CandyApplication.getInstance().getApplicationContext().getString(R.string.oldpkg_info))
                        .build());
    }

    private PendingIntent getUninstallIntent() {
        Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:" + package_uri));
        return PendingIntent.getActivity(CandyApplication.getInstance().getApplicationContext(), 0, intent, 0);
    }
}
