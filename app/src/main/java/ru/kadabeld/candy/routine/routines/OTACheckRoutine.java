package ru.kadabeld.candy.routine.routines;

import android.preference.PreferenceManager;

import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.OTAService;
import rx.functions.Action0;

/**
 * Created by arktic on 10.05.16.
 */
public class OTACheckRoutine implements Action0 {

    @Override
    public void call() {
        if (PreferenceManager.getDefaultSharedPreferences(
                CandyApplication.getInstance().getApplicationContext()
        ).getBoolean("ota", true)) {
            OTAService.launchCheckUpdate();
        }
    }

}
