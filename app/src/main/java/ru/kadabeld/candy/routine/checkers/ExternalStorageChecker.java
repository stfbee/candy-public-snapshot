package ru.kadabeld.candy.routine.checkers;

import android.Manifest;

import com.tbruyelle.rxpermissions.RxPermissions;

import ru.kadabeld.candy.CandyApplication;
import rx.functions.Action0;

/**
 * Created by arktic on 10.05.16.
 */
public class ExternalStorageChecker implements Action0 {

    private final Action0 mAction;

    public ExternalStorageChecker(Action0 action) {
        mAction = action;
    }

    @Override
    public void call() {
        if (RxPermissions.getInstance(CandyApplication.getInstance().getApplicationContext())
                .isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            mAction.call();
        }
    }
}