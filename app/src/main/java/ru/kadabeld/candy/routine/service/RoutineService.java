package ru.kadabeld.candy.routine.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.common.collect.Lists;

import java.util.List;

import ru.kadabeld.candy.routine.checkers.ExternalStorageChecker;
import ru.kadabeld.candy.routine.checkers.NetworkChecker;
import ru.kadabeld.candy.routine.checkers.TimedChecker;
import ru.kadabeld.candy.routine.routines.AccountsCheckRoutine;
import ru.kadabeld.candy.routine.routines.DonateCheckRoutine;
import ru.kadabeld.candy.routine.routines.OTACheckRoutine;
import ru.kadabeld.candy.routine.routines.OldPackageCheckRoutine;
import rx.functions.Action0;

/**
 * Created by arktic on 10.05.16.
 */
public class RoutineService extends IntentService {

    private static final String TAG = RoutineService.class.getSimpleName();
    private static final String ACTION_DO_WORK = RoutineService.class.getCanonicalName() + ".do.work";

    private static final List<Action0> TASKS = Lists.newArrayList(
            new ExternalStorageChecker(new NetworkChecker(new TimedChecker(new OTACheckRoutine()))),
            new NetworkChecker(new TimedChecker(new DonateCheckRoutine())),
            new NetworkChecker(new AccountsCheckRoutine()),
            new OldPackageCheckRoutine()
    );

    public static void runPendingTasks(Context context) {
        context.startService(
                new Intent(context, RoutineService.class)
                        .setAction(ACTION_DO_WORK));
    }


    public RoutineService() {
        super(RoutineService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null) {
            Log.e(TAG, "onHandleIntent(null)", new NullPointerException("null intent"));
            return;
        }
        if (!ACTION_DO_WORK.equals(intent.getAction())) {
            throw new RuntimeException("WTF??");
        }
        for (Action0 action : TASKS) {
            Log.d(TAG, "Before running: " + action);
            try {
                action.call();
                Log.d(TAG, "After running: " + action);
            } catch (Exception e) {
                Log.e(TAG, "Failed running: " + action, e);
            }
        }
    }

}