package ru.kadabeld.candy.routine.service;

import android.app.Application;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by arktic on 10.05.16.
 */
public class ForegroundScheduledTasks extends ForegroundStateObserver {

    private static final String TAG = ForegroundScheduledTasks.class.getSimpleName();
    private static final long PERIOD_DEFAULT = TimeUnit.MINUTES.toMillis(5);

    private final long mPeriod;
    private volatile long mDelay;
    private volatile long mDelaySuspendedAt;
    private boolean mStarted;
    private boolean mRegisteredToLifecycleListening;
    private final ScheduledExecutorService mService = Executors.newSingleThreadScheduledExecutor();
    private final List<Runnable> mTasks = new CopyOnWriteArrayList<Runnable>();
    private ScheduledFuture<?> mScheduledFuture;

    public void addTask(@NonNull Runnable task) {
        mTasks.add(task);
    }

    public void removeTask(@NonNull Runnable task) {
        mTasks.remove(task);
    }

    public ForegroundScheduledTasks() {
        this(PERIOD_DEFAULT);
    }

    public ForegroundScheduledTasks(long period) {
        if (period <= 0) {
            throw new RuntimeException("Period must be greater than 0");
        }
        mPeriod = period;
    }

    public void startForegroundOnly(@NonNull Application application) {
        if (!mRegisteredToLifecycleListening) {
            application.registerActivityLifecycleCallbacks(this);
            mRegisteredToLifecycleListening = true;
        }
    }

    public void start() {
        if (mStarted) {
            stop();
        }
        Log.d(TAG, "starting tasks");
        final TaskRunner taskRunner = new TaskRunner();
        if (mDelaySuspendedAt > 0) {
            final long spentInBackground = SystemClock.elapsedRealtime() - mDelaySuspendedAt;
            Log.d(TAG, "time spent in background, sec: " + TimeUnit.MILLISECONDS.toSeconds(spentInBackground));
            mDelay -= spentInBackground;
            if (mDelay < 0) {
                mDelay = 0;
            }
            mDelaySuspendedAt = 0;
        }
        mScheduledFuture = mService.scheduleAtFixedRate(taskRunner, mDelay, mPeriod, TimeUnit.MILLISECONDS);
        mStarted = true;
    }

    public void stop() {
        Log.d(TAG, "stopping tasks");
        if (mScheduledFuture != null) {
            mScheduledFuture.cancel(false);
        }
        mDelay = mPeriod;
        mStarted = false;
    }

    public void suspend() {
        Log.d(TAG, "suspending tasks");
        if (mScheduledFuture != null) {
            long delay = mScheduledFuture.getDelay(TimeUnit.MILLISECONDS);
            mDelay = delay >= 0 ? delay : 0;
            mDelaySuspendedAt = SystemClock.elapsedRealtime();
            mScheduledFuture.cancel(false);
        }
        mStarted = false;
    }

    @Override
    protected void onForegroundState() {
        if (!mStarted) {
            start();
        }
    }

    @Override
    protected void onBackgroundState() {
        suspend();
    }

    private class TaskRunner implements Runnable {
        @Override
        public void run() {
            Log.d(TAG, String.format("Running %d scheduled tasks.", mTasks.size()));
            for (Runnable task : mTasks) {
                task.run();
            }
        }
    }

}
