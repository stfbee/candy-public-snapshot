package ru.kadabeld.candy.routine.checkers;

import ru.kadabeld.candy.utils.NetworkUtils;
import rx.functions.Action0;

/**
 * Created by arktic on 10.05.16.
 */
public class NetworkChecker implements Action0 {


    private final Action0 mAction;

    public NetworkChecker(Action0 action) {
        mAction = action;
    }

    @Override
    public void call() {
        if (NetworkUtils.getInstance().isConnected()) {
            mAction.call();
        }
    }
}