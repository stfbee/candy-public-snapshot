package ru.kadabeld.candy.routine.service;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by arktic on 10.05.16.
 */
public abstract class ForegroundStateObserver implements Application.ActivityLifecycleCallbacks, Runnable {

    private static final Handler HANDLER = new Handler(Looper.getMainLooper());

    private boolean mForeground;

    @Override
    public final void onActivityResumed(Activity activity) {
        mForeground = true;
        checkState();
    }

    @Override
    public final void onActivityPaused(Activity activity) {
        mForeground = false;
        checkState();
    }

    private void checkState() {
        HANDLER.removeCallbacks(this);
        HANDLER.postDelayed(this, 100);
    }

    @Override
    public final void run() {
        if (mForeground) {
            onForegroundState();
        } else {
            onBackgroundState();
        }
    }

    protected abstract void onForegroundState();

    protected abstract void onBackgroundState();


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }
}