package ru.kadabeld.candy.routine.routines;

import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.DonateCheck;
import ru.kadabeld.candy.account.AccountsBus;
import rx.Observable;
import rx.functions.Action0;

/**
 * Created by arktic on 11.05.16.
 */
public class DonateCheckRoutine implements Action0 {

    @Override
    public void call() {
        final AccountsBus bus = CandyApplication.getInstance().getAccountsBus();
        bus.accounts()
                .first()
                .flatMap(Observable::from)
                .map(new DonateCheck())
                .filter(accountInfo -> accountInfo != null)
                .subscribe(bus::update);
    }
}
