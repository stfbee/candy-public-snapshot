package ru.kadabeld.candy.routine.checkers;

import java.util.concurrent.TimeUnit;

import rx.functions.Action0;

/**
 * Created by arktic on 10.05.16.
 */
public class TimedChecker implements Action0 {

    private long mLastRunTime = 0;
    private final long mRunInterval;
    private final Action0 mAction;

    public TimedChecker(Action0 action) {
        this(TimeUnit.MINUTES.toMillis(15), action);
    }

    public TimedChecker(long runInterval, Action0 action) {
        mAction = action;
        mRunInterval = runInterval;
    }

    @Override
    public void call() {
        if (System.currentTimeMillis() - mLastRunTime > mRunInterval) {
            mAction.call();
            mLastRunTime = System.currentTimeMillis();
        }
    }
}