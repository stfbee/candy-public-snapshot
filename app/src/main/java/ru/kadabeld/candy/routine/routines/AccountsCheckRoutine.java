package ru.kadabeld.candy.routine.routines;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.account.AccountInfo;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.utils.ApiUtils;
import ru.kadabeld.candy.utils.NetworkUtils;
import rx.Observable;
import rx.functions.Action0;

/**
 * Created by arktic on 13.05.16.
 */
public class AccountsCheckRoutine implements Action0 {

    @Override
    public void call() {
        CandyApplication.getInstance().getAccountsBus()
                .accounts()
                .first()
                .flatMap(Observable::from)
                .subscribe(this::checkAccount);
    }

    private void checkAccount(AccountInfo info) {
        try {
            // Регаем юзера в статистике
            // https://vk.com/dev/stats.trackVisitor
            RequestParams params = new RequestParams();
            params.put("v", API.version);
            params.put("access_token", info.getAccessToken());
            params.put("lang", CandyApplication.lang);
            new SyncHttpClient().post(API.BASE_without_setonline + API.statsTrackVisitor, params, new JsonHttpResponseHandler());

            RequestBody body = new FormEncodingBuilder()
                    .add("v", API.version)
                    .add("fields", API.userFields)
                    .add("access_token", info.getAccessToken())
                    .add("lang", CandyApplication.lang)
                    .build();

            OkHttpClient client = NetworkUtils.getInstance().generateDefaultOkHttpClient();
            Response response = client.newCall(new Request.Builder().url(API.BASE_without_setonline + API.usersGet).post(body).build()).execute();
            if (response.isSuccessful()) {
                JSONObject object = new JSONObject(response.body().string());
                ApiUtils.ErrorType errorType = ApiUtils.parseError(object);
                if (errorType != null) {
                    switch (errorType) {
                        case FLOOD_CONTROL:
                        case INTERNAL_ERROR:
                        case NEEDS_CAPTCHA:
                        case UNKNOWN_ERROR:
                            break;
                        default:
                            CandyApplication.getInstance().getAccountsBus().remove(info);
                            Log.d(AccountsCheckRoutine.class.getName(), "REMOVED ACCOUNT " + info.getUserId());
                    }
                } else if (object.has("response")) {
                    JSONObject userObject = object.getJSONArray("response").getJSONObject(0);
                    User user = User.parseUser(userObject);
                    info.setFirstName(user.getName());
                    info.setLastName(user.getSurname());
                    info.setPhoto200(user.getPhotoFor(200));
                    CandyApplication.getInstance().getAccountsBus().update(info);
                }
            }
        } catch (IOException | JSONException e) {
            Log.e(AccountsCheckRoutine.class.getName(), "Failed to check accountInfo");
        }
    }
}
