package ru.kadabeld.candy;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

/**
 * Created by arktic on 10.05.16.
 */
public class OTAService extends IntentService {

    private static final String TAG = OTAService.class.getName();
    private static final String ACTION_CHECK_UPDATE = OTAService.class.getCanonicalName() + ".CHECK_OTA";
    private static final String ACTION_UPDATE = OTAService.class.getCanonicalName() + ".UPDATE_OTA";
    private static final String EXTRA_FORCE_CHECK = "force";

    private static final String PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    private static final String PROJECT = BuildConfig.DONATE ? "Candy%20Night" : "Candy";
    private static final String BUILD = "http://host.vladonishchenko.ru/job/" + PROJECT + "/lastSuccessfulBuild/artifact/app/build/outputs/apk/candy_lastbuild_debug_donate.apk";
    private static final String VERSION = "http://host.vladonishchenko.ru/job/" + PROJECT + "/lastSuccessfulBuild/artifact/app/version.properties";

    private static final int NOTIFICATION_ID = OTAService.class.hashCode();

    public static void launchCheckUpdate() {
        Context context = CandyApplication.getInstance().getApplicationContext();
        context.startService(new Intent(context, OTAService.class).setAction(ACTION_CHECK_UPDATE));
    }

    public static void forseCheckUpdate() {
        Context context = CandyApplication.getInstance().getApplicationContext();
        context.startService(new Intent(context, OTAService.class)
                .setAction(ACTION_CHECK_UPDATE)
                .putExtra(EXTRA_FORCE_CHECK, true)
        );
    }

    private final NotificationManager mNotificationManager;
    private final NotificationCompat.Builder mBuilder;

    public OTAService() {
        super(TAG);
        mNotificationManager = (NotificationManager) CandyApplication.getInstance()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (ACTION_CHECK_UPDATE.equals(intent.getAction())) {
            checkUpdate(intent.getBooleanExtra(EXTRA_FORCE_CHECK, false));
        }
        if (ACTION_UPDATE.equals(intent.getAction())) {
            update();
        }
    }

    private void checkUpdate(final boolean forced) {

        new SyncHttpClient().get(VERSION, new TextHttpResponseHandler() {
            int new_version = 0;

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (forced) {
                    notifyCheckFailed();
                }
                Log.e("CANDYOTA", "не удалось проверить версию");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Properties prop = new Properties();
                try {
                    prop.load(new StringReader(responseString));
                    String version_build = prop.getProperty("VERSION_BUILD");
                    if (version_build != null) {
                        new_version = Integer.parseInt(version_build);
                    }
                    int my_version = BuildConfig.VERSION_BUILD;
                    Log.d("CANDYOTA", version_build);

                    if (new_version > my_version) {
                        Log.d("CANDYOTA", "новая версия на сайте!");

                        notifyNewVersion();
                    } else if (forced) {
                        notifyNoVersion();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    notifyCheckFailed();
                }
            }
        });
    }

    private void update() {
        notifyUpdateProgress(0);
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(BUILD);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                notifyUpdateFailed();
                return;
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();


            File outputFile;
            outputFile = new File(PATH, "candy1337.apk");
            output = new FileOutputStream(outputFile);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            int preProgress = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                if (fileLength > 0) {
                    int progress = (int) (total * 100 / fileLength);
                    if (preProgress != progress) {
                        notifyUpdateProgress(progress);
                    }
                    preProgress = progress;
                }
                output.write(data, 0, count);
            }

        } catch (Exception e) {
            e.printStackTrace();
            notifyUpdateFailed();
        } finally {
            try {
                if (output != null) output.close();
                if (input != null) input.close();
            } catch (IOException ignored) {
            }
            if (connection != null) connection.disconnect();
        }

        notifyUpdateDownloaded();
    }

    private void notifyCheckFailed() {
        mNotificationManager.notify(NOTIFICATION_ID,
                mBuilder.setContentIntent(null)
                        .setAutoCancel(true)
                        .setOngoing(false)
                        .setSmallIcon(android.R.drawable.ic_dialog_alert)
                        .setContentTitle(getString(R.string.ota_cant_check_new_version))
                        .setContentText(null)
                        .setProgress(0, 0, false)
                        .build());
    }

    private void notifyNewVersion() {
        Intent intent = new Intent(this, OTAService.class).setAction(ACTION_UPDATE);
        mNotificationManager.notify(NOTIFICATION_ID,
                mBuilder.setContentIntent(PendingIntent.getService(this, 0, intent, 0))
                        .setAutoCancel(true)
                        .setOngoing(false)
                        .setSmallIcon(android.R.drawable.ic_dialog_info)
                        .setContentTitle(getString(R.string.ota_new_version_available))
                        .setContentText(getString(R.string.ota_download_file_question))
                        .setProgress(0, 0, false)
                        .build());
    }

    private void notifyNoVersion() {
        mNotificationManager.notify(NOTIFICATION_ID,
                mBuilder.setContentIntent(null)
                        .setAutoCancel(true)
                        .setOngoing(false)
                        .setSmallIcon(android.R.drawable.ic_dialog_info)
                        .setContentTitle(getString(R.string.ota_no_version_available))
                        .setContentText(null)
                        .setProgress(0, 0, false)
                        .build());
    }

    private void notifyUpdateProgress(int progress) {
        Log.d(TAG, "progress: " + progress);
        mNotificationManager.notify(NOTIFICATION_ID,
                mBuilder.setContentIntent(null)
                        .setAutoCancel(true)
                        .setOngoing(true)
                        .setSmallIcon(android.R.drawable.stat_sys_download)
                        .setContentTitle(getString(R.string.ota_loading))
                        .setContentText(null)
                        .setProgress(progress, progress == 0 ? 0 : 100, progress == 0)
                        .build());
    }

    private void notifyUpdateDownloaded() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(PATH, "candy1337.apk")), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mNotificationManager.notify(NOTIFICATION_ID,
                mBuilder.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0))
                        .setAutoCancel(true)
                        .setOngoing(false)
                        .setSmallIcon(android.R.drawable.stat_sys_download_done)
                        .setContentTitle(getString(R.string.ota_downloaded))
                        .setContentText(getString(R.string.ota_download_install))
                        .setProgress(0, 0, false)
                        .build());
    }

    private void notifyUpdateFailed() {
        mNotificationManager.notify(NOTIFICATION_ID,
                mBuilder.setContentIntent(null)
                        .setAutoCancel(true)
                        .setOngoing(false)
                        .setSmallIcon(android.R.drawable.stat_sys_download_done)
                        .setContentTitle(getString(R.string.ota_download_failed))
                        .setContentText(null)
                        .setProgress(0, 0, false)
                        .build());
    }

}
