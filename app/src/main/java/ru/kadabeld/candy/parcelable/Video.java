package ru.kadabeld.candy.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

@Deprecated
public class Video implements Parcelable {

    public static final Parcelable.Creator<Video> CREATOR = new Parcelable.Creator<Video>() {
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        public Video[] newArray(int size) {
            return new Video[size];
        }
    };
    public String quality;
    public String src;

    public Video(String quality, String src) {
        this.quality = quality;
        this.src = src;
    }

    // конструктор, считывающий данные из Parcel
    private Video(Parcel parcel) {
        quality = parcel.readString();
        src = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(quality);
        parcel.writeString(src);
    }

}