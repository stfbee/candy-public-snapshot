package ru.kadabeld.candy.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.TypedValue;

/**
 * User: Vlad
 * Date: 28.04.2015
 * Time: 10:32
 */
public class ColorUtils {
    private static Context context;

    public static int getThemeColor(int attr) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(attr, typedValue, true);
        return typedValue.data;
    }

    public static int darker(int c) {
        int r = Color.red(c);
        int b = Color.blue(c);
        int g = Color.green(c);

        //was .7
        return Color.rgb((int) (r * .82), (int) (g * .82), (int) (b * .82));
    }

    public static void init(Context context) {
        ColorUtils.context = context;
    }
}
