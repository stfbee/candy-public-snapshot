package ru.kadabeld.candy.utils.parsers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.entities.User;

public class Posts {
    Context ctx;
    boolean isWall = false;
    boolean isPost = false;
    ArrayList<Post> news = new ArrayList<>();

    public ArrayList<Post> parseWall(JSONArray items, JSONArray groups, JSONArray profiles, Context ctx, boolean isWall) {
        this.isWall = isWall;
        return parse(items, groups, profiles, ctx);
    }

    public ArrayList<Post> parsePost(JSONArray items, JSONArray groups, JSONArray profiles, Context ctx, boolean isPost) {
        this.isPost = isPost;
        return parse(items, groups, profiles, ctx);
    }

    public ArrayList<Post> parse(JSONArray items, JSONArray groups, JSONArray profiles, Context ctx) {
        this.ctx = ctx;
        try {
            for (int gr = 0; gr < groups.length(); gr++) {
                JSONObject groupObject = groups.getJSONObject(gr);
                Group.parseGroup(groupObject);
            }
            for (int pr = 0; pr < profiles.length(); pr++) {
                JSONObject profileObject = profiles.getJSONObject(pr);
                User.parseUser(profileObject);
            }
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                Post post = Post.parsePost(item);
//                String type;
//                if (item.isNull("type")) {
//                    type = item.getString("post_type");
//                } else {
//                    type = item.getString("type");
//                }
////                String post_type = item.getString("post_type");
//                if (type.equals("post")) {
//                    String text = item.getString("text");
//                    JSONObject likesObject = item.getJSONObject("likes");
//                    JSONObject repostsCount = item.getJSONObject("reposts");
//                    JSONObject commentsObject = item.getJSONObject("comments");
//                    post.setCountLikes(likesObject.getInt("count"));
//                    boolean liked = false;
//                    if (likesObject.getInt("user_likes") == 1) {
//                        liked = true;
//                    }
//                    long source_id;
//                    if (isWall || isPost) {
//                        source_id = item.getLong("owner_id");
//                    } else {
//                        if (item.isNull("source_id")) {
//                            source_id = item.getLong("owner_id");
//                        } else {
//                            source_id = item.getLong("source_id");
//                        }
//                    }
//                    long post_id;
//                    if (item.isNull("post_id")) {
//                        post_id = item.getLong("id");
//                    } else {
//                        post_id = item.getLong("post_id");
//                    }
//                    if (item.has("is_pinned")) post.setPinned(item.getInt("is_pinned") == 1);
//                    post.setTime(item.getLong("date"));
//                    post.setSid(source_id);
//                    post.setPid(post_id);
//                    post.setLiked(liked);
//                    post.setCountShares(repostsCount.getInt("count"));
//                    post.setCountComments(commentsObject.getInt("count"));
//                    boolean can_post = false;
//                    if (commentsObject.getInt("can_post") == 1) {
//                        can_post = true;
//                    }
//                    post.setCanPost(can_post);
//                    post.setContent(text);
//                    Profile profile;
//                    if (isWall || isPost) {
//                        profile = profilesList.get(item.getLong("from_id"));
//                    } else {
//                        profile = profilesList.get(source_id);
//                    }
//                    post.setName(profile.getFullName());
//                    post.setAvatar(profile.getAvatar());
//                    if (profile.isAvatarDefault()) {
//                        post.setAvatarDefault(profile.isAvatarDefault());
//                        post.setAvatarDrawable(profile.getAvatarDrawable());
//                    }
//
//                    if (!item.isNull("signer_id")) {
//                        Profile signer = profilesList.get(item.getLong("signer_id"));
//                        post.setSigner(signer.getFullName());
//                    }
//                    if (!item.isNull("post_source")) {
//                        JSONObject post_source = item.getJSONObject("post_source");
//                        if (!post_source.isNull("data") && post_source.getString("data").equals("profile_photo")) {
//                            if (profile.getId() > 0) {
//                                //profile
//                                if (profile.getSex() == 1) {
//                                    post.setContent(String.format(ctx.getResources().getString(R.string.feed_updated_photo_female), profile.getFullName()));
//                                } else if (profile.getSex() == 2) {
//                                    post.setContent(String.format(ctx.getResources().getString(R.string.feed_updated_photo_male), profile.getFullName()));
//                                }
//                            } else {
//                                //community
//                                post.setContent(ctx.getResources().getString(R.string.feed_updated_photo_community));
//                            }
//                        }
//                    }
//                    if (!item.isNull("copy_history")) {
//                        JSONObject copy_histroy = item.getJSONArray("copy_history").getJSONObject(0);
//                        post.setRepost(true);
//                        //String copy_post_type = item.getString("post_type");
//                        long copy_owner_id = copy_histroy.getLong("owner_id");
//                        long copy_post_id = copy_histroy.getLong("id");
//                        String copy_text = copy_histroy.getString("text");
//                        post.setRepostText(copy_text);
//                        post.setRepostOwnerSid(copy_owner_id);
//                        post.setRepostOwnerPid(copy_post_id);
//                        Profile repost = profilesList.get(copy_owner_id);
//                        post.setRepostOwnerName(repost.getFullName());
//                        post.setRepostOwnerPhoto(repost.getAvatar());
//
//                        if (repost.isAvatarDefault()) {
//                            post.setRepostOwnerAvatarDefault(repost.isAvatarDefault());
//                            post.setRepostOwnerAvatarDrawable(repost.getAvatarDrawable());
//                        }
//                        post.setRepostTime(copy_histroy.getLong("date"));
//                        if (!copy_histroy.isNull("attachments")) {
//                            JSONArray attachments = copy_histroy.getJSONArray("attachments");
//                            addAttachments(post, attachments, post.isRepost(), true);
//                        }
//                    }
//                    if (!item.isNull("attachments")) {
//                        JSONArray attachments = item.getJSONArray("attachments");
//                        addAttachments(post, attachments, post.isRepost(), false);
//                    }
//                }
                news.add(post);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return news;
    }

//    void addAttachments(Post post, JSONArray attachments, boolean isRepost, boolean fromRepost) throws JSONException {
//        if (isRepost) {
//            if (fromRepost) {
//                ArrayList<Photo> photos = new ArrayList<>();
//                ArrayList<Video> videos = new ArrayList<>();
//                ArrayList<AudioP> audios = new ArrayList<>();
//                ArrayList<Document> documents = new ArrayList<>();
//                for (int at = 0; at < attachments.length(); at++) {
//                    JSONObject attachment = attachments.getJSONObject(at);
//                    String typeAttachment = attachment.getString("type");
//                    if (typeAttachment.equals("photo")) {
//                        photos.add(addPhotos(attachment, at));
//                    }
//                    if (typeAttachment.equals("video")) {
//                        videos.add(addVideos(attachment, at));
//                    }
//                    if (typeAttachment.equals("link")) {
//                        JSONObject linkObject = attachment.getJSONObject("link");
//                        post.setLink_title(linkObject.getString("title"));
//                        post.setLink(linkObject.getString("url"));
//                        if (!linkObject.isNull("image_src")) {
//                            post.setLink_image(linkObject.getString("image_src"));
//                        } else {
//                            post.setLink_image(Global.resIdToUri(ctx, R.drawable.ic_launcher).toString());
//                        }
//                    }
//
//                    if (typeAttachment.equals("audio")) {
//                        audios.add(addAudios(attachment));
//                    }
//                    if (typeAttachment.equals("doc")) {
//                        documents.add(addDocuments(attachment));
//                    }
//                }
//                post.setPhotos(photos);
//                post.setVideos(videos);
//                post.setAudios(audios);
//                post.setDocuments(documents);
//            } else {
//                JSONObject attachment = attachments.getJSONObject(0);
//                String typeAttachment = attachment.getString("type");
//                if (typeAttachment.equals("photo")) {
//                    post.setPhotoRepost(addPhotos(attachment, 0));
//                }
//                if (typeAttachment.equals("video")) {
//                    post.setVideoRepost(addVideos(attachment, 0));
//                }
//                if (typeAttachment.equals("link")) {
//                    JSONObject linkObject = attachment.getJSONObject("link");
//                    post.setLink_title(linkObject.getString("title"));
//                    post.setLink(linkObject.getString("url"));
//                    if (!linkObject.isNull("image_src")) {
//                        post.setLink_image(linkObject.getString("image_src"));
//                    } else {
//                        post.setLink_image(Global.resIdToUri(ctx, R.drawable.ic_launcher).toString());
//                    }
//                }
//
//                if (typeAttachment.equals("audio")) {
//
//                    post.setAudioRepost(addAudios(attachment));
//                }
//                if (typeAttachment.equals("doc")) {
//                    post.setDocumentRepost(addDocuments(attachment));
//                }
//            }
//        } else {
//            ArrayList<Photo> photos = new ArrayList<>();
//            ArrayList<Video> videos = new ArrayList<>();
//            ArrayList<AudioP> audios = new ArrayList<>();
//            ArrayList<Document> documents = new ArrayList<>();
//            for (int at = 0; at < attachments.length(); at++) {
//                JSONObject attachment = attachments.getJSONObject(at);
//                String typeAttachment = attachment.getString("type");
//                if (typeAttachment.equals("photo")) {
//                    photos.add(addPhotos(attachment, at));
//                }
//                if (typeAttachment.equals("video")) {
//                    videos.add(addVideos(attachment, at));
//                }
//                if (typeAttachment.equals("link")) {
//                    JSONObject linkObject = attachment.getJSONObject("link");
//                    post.setLink_title(linkObject.getString("title"));
//                    post.setLink(linkObject.getString("url"));
//                    if (!linkObject.isNull("image_src")) {
//                        post.setLink_image(linkObject.getString("image_src"));
//                    }
//                }
//
//                if (typeAttachment.equals("audio")) {
//                    audios.add(addAudios(attachment));
//                }
//                if (typeAttachment.equals("doc")) {
//                    documents.add(addDocuments(attachment));
//                }
//            }
//            post.setPhotos(photos);
//            post.setVideos(videos);
//            post.setAudios(audios);
//            post.setDocuments(documents);
//        }
//    }
//
//    Photo addPhotos(JSONObject attachment, int at) throws JSONException {
//        JSONObject photoObject = attachment.getJSONObject("photo");
//        Photo photo = Photo.parsePhoto(photoObject);
//        photo.setPosition(at);
//        return photo;
//    }
//
//    Video addVideos(JSONObject attachment, int at) throws JSONException {
//        JSONObject videoObject = attachment.getJSONObject("video");
//        Video video = Video.parseVideo(videoObject);
//        video.setPosition(at);
//        return video;
//    }
//
//    AudioP addAudios(JSONObject attachment) throws JSONException {
//        return new AudioP(attachment.getJSONObject("audio"));
//    }
//
//    Document addDocuments(JSONObject attachment) throws JSONException {
//        JSONObject documentObject = attachment.getJSONObject("doc");
//        return Document.parseDocument(documentObject);
//    }
}
