package ru.kadabeld.candy.utils;

/**
 * User: Vlad
 * Date: 10.03.2015
 * Time: 21:27
 */
public class NumberUtils {
    public static String getCount(long count) {
        if (count >= 1000) {
            if (count >= 1000000) {
                return (count / 1000000) + "KK";
            } else {
                return (count / 1000) + "K";
            }
        } else {
            return String.valueOf(count);
        }
    }
}
