package ru.kadabeld.candy.utils.parsers;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.PhotoActivity;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.AbstractAttachment;
import ru.kadabeld.candy.entities.Audio;
import ru.kadabeld.candy.entities.Document;
import ru.kadabeld.candy.entities.Link;
import ru.kadabeld.candy.entities.Photo;
import ru.kadabeld.candy.entities.Poll;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.entities.Sticker;
import ru.kadabeld.candy.entities.Video;
import ru.kadabeld.candy.entities.meta.Geo;
import ru.kadabeld.candy.views.AudioView;
import ru.kadabeld.candy.views.DocView;
import ru.kadabeld.candy.views.LinkView;
import ru.kadabeld.candy.views.LocationView;
import ru.kadabeld.candy.views.PollView;
import ru.kadabeld.candy.views.RepostView;
import ru.kadabeld.candy.views.VideoImageView;

/**
 * User: Vlad
 * Date: 23.08.2015
 * Time: 15:45
 */
public class Attachments {
    private ArrayList<Photo> photos = new ArrayList<>();
    private ArrayList<Video> videos = new ArrayList<>();
    private ArrayList<Document> documents = new ArrayList<>();
    private ArrayList<Audio> audios = new ArrayList<>();
    private ArrayList<Sticker> stickers = new ArrayList<>();
    private ArrayList<Document> graffiti = new ArrayList<>();
    private ArrayList<Link> links = new ArrayList<>();
    private ArrayList<Poll> polls = new ArrayList<>();
    private ArrayList<Post> posts = new ArrayList<>();
    private ArrayList<Geo> geos = new ArrayList<>();

    public static Attachments parseJson(JSONArray jsonArray) {
        Attachments attachments = new Attachments();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject item;
            try {
                item = jsonArray.getJSONObject(i);
                String type = item.getString("type");
                switch (type) {
                    case "photo": {
                        JSONObject photoObject = item.getJSONObject(type);
                        Photo photo = Photo.parsePhoto(photoObject);
                        photo.setPosition(i);
                        attachments.addPhoto(photo);
                        break;
                    }
                    case "video": {
                        JSONObject videoObject = item.getJSONObject(type);
                        Video video = Video.parseVideo(videoObject);
                        video.setPosition(i);
                        attachments.addVideo(video);
                        break;
                    }
                    case "audio": {
                        JSONObject audioObject = item.getJSONObject(type);
                        attachments.addAudio(Audio.parseAudio(audioObject));
                        break;
                    }
                    case "sticker": {
                        JSONObject stickerObject = item.getJSONObject(type);
                        attachments.addSticker(Sticker.parseSticker(stickerObject));
                        break;
                    }
                    case "doc": {
                        JSONObject documentObject = item.getJSONObject(type);
                        Document document = Document.parseDocument(documentObject);
                        if(document.getType()== Document.Type.GRAFFITI){
                            attachments.addGraffiti(document);
                        }else {
                            attachments.addDocument(document);
                        }
                        break;
                    }
                    case "wall": {
                        JSONObject wallObject = item.getJSONObject(type);
                        attachments.addPost(Post.parsePost(wallObject));
                        break;
                    }
                    case "link": {
                        JSONObject linkObject = item.getJSONObject(type);
                        attachments.addLink(Link.parseLink(linkObject));
                        break;
                    }
                    case "poll": {
                        JSONObject pollObject = item.getJSONObject(type);
                        attachments.addPoll(Poll.parsePoll(pollObject));
                        break;
                    }
                    case "wall_reply": {
                        // вы блять серъёзно?
                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return attachments;
    }

    private void addPost(Post post) {
        posts.add(post);
    }

    private void addSticker(Sticker sticker) {
        stickers.add(sticker);
    }

    private void addAudio(Audio audio) {
        audios.add(audio);
    }

    private void addVideo(Video video) {
        videos.add(video);
    }

    private void addDocument(Document document) {
        documents.add(document);
    }

    private void addGraffiti(Document document) {
        graffiti.add(document);
    }

    private void addLink(Link link) {
        links.add(link);
    }

    private void addPoll(Poll poll) {
        polls.add(poll);
    }

    public void addPhoto(Photo photo) {
        photos.add(photo);
    }

    public void addGeos(Geo geo) {
        geos.add(geo);
    }

    public ArrayList<Geo> getGeos() {
        return geos;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public ArrayList<Link> getLinks() {
        return links;
    }

    public ArrayList<Poll> getPolls() {
        return polls;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public ArrayList<Audio> getAudios() {
        return audios;
    }

    public ArrayList<Sticker> getStickers() {
        return stickers;
    }

    public ArrayList<Document> getGraffiti() {
        return graffiti;
    }

    public boolean isEmpty() {
        return photos.isEmpty() &&
                videos.isEmpty() &&
                documents.isEmpty() &&
                audios.isEmpty() &&
                stickers.isEmpty() &&
                links.isEmpty() &&
                polls.isEmpty() &&
                posts.isEmpty() &&
                graffiti.isEmpty() &&
                geos.isEmpty();
    }

    public void addPhotos(ArrayList<AbstractAttachment> attachments) {
        for (AbstractAttachment attachment : attachments) {
            photos.add((Photo) attachment);
        }
    }

    public void addAudios(ArrayList<AbstractAttachment> attachments) {
        for (AbstractAttachment attachment : attachments) {
            audios.add((Audio) attachment);
        }
    }

    public void addDocs(ArrayList<AbstractAttachment> attachments) {
        for (AbstractAttachment attachment : attachments) {
            documents.add((Document) attachment);
        }
    }

    public void addVideos(ArrayList<AbstractAttachment> attachments) {
        for (AbstractAttachment attachment : attachments) {
            videos.add((Video) attachment);
        }
    }

    public void addGraffitis(ArrayList<AbstractAttachment> attachments) {
        for (AbstractAttachment attachment : attachments) {
            graffiti.add((Document) attachment);
        }
    }

    public enum Filter {
        ALL, PHOTO, VIDEO, AUDIO, DOC, POST, STICKER, LINK, POLL, GEO
    }

    public void getView(final Context ctx, FrameLayout attachments_frame, int content_width) {
        getView(ctx, attachments_frame, content_width, Filter.ALL);
    }

    public void getView(final Context ctx, FrameLayout attachments_frame, int content_width, Filter filter) {
        Picasso picasso = Picasso.with(ctx);
        LayoutInflater inflater = LayoutInflater.from(ctx);

        attachments_frame.setVisibility(View.VISIBLE);

        final LinearLayout attachmentsLayout = (LinearLayout) inflater.inflate(R.layout.element_attachments, null);

        final LinearLayout photosLayout = (LinearLayout) attachmentsLayout.findViewById(R.id.attach_images);
        final LinearLayout videosLayout = (LinearLayout) attachmentsLayout.findViewById(R.id.attach_videos);
        final LinearLayout filesLayout = (LinearLayout) attachmentsLayout.findViewById(R.id.attach_files);
        final LinearLayout audiosLayout = (LinearLayout) attachmentsLayout.findViewById(R.id.attach_music);
        final LinearLayout linksLayout = (LinearLayout) attachmentsLayout.findViewById(R.id.attach_links);
        final LinearLayout postsLayout = (LinearLayout) attachmentsLayout.findViewById(R.id.attach_post);
        final LinearLayout pollsLayout = (LinearLayout) attachmentsLayout.findViewById(R.id.attach_polls);
        final LinearLayout geosLayout = (LinearLayout) attachmentsLayout.findViewById(R.id.attach_geos);
        final HorizontalScrollView scroll_photos = new HorizontalScrollView(ctx);
        final HorizontalScrollView scroll_videos = new HorizontalScrollView(ctx);
        final LinearLayout photos_layout = new LinearLayout(ctx);
        final LinearLayout videos_layout = new LinearLayout(ctx);
        final LinearLayout audio_layout = new LinearLayout(ctx);
        final LinearLayout doc_layout = new LinearLayout(ctx);
        final LinearLayout links_layout = new LinearLayout(ctx);
        final LinearLayout posts_layout = new LinearLayout(ctx);
        final LinearLayout polls_layout = new LinearLayout(ctx);
        final LinearLayout geos_layout = new LinearLayout(ctx);
        final ArrayList<Photo> photosList = getPhotos();
        final ArrayList<Video> videosList = getVideos();
        final ArrayList<Audio> audiosList = getAudios();
        final ArrayList<Document> filesList = getDocuments();
        final ArrayList<Link> linksList = getLinks();
        final ArrayList<Post> postsList = getPosts();
        final ArrayList<Poll> pollsList = getPolls();
        final ArrayList<Geo> geosList = getGeos();


        audio_layout.setOrientation(LinearLayout.VERTICAL);
        doc_layout.setOrientation(LinearLayout.VERTICAL);
        links_layout.setOrientation(LinearLayout.VERTICAL);
        posts_layout.setOrientation(LinearLayout.VERTICAL);
        polls_layout.setOrientation(LinearLayout.VERTICAL);
        geos_layout.setOrientation(LinearLayout.VERTICAL);


        attachmentsLayout.removeAllViews();


        int layoutHeight = Math.round(Global.convertDpToPixel(ctx, 165));

        //
        // Photos
        //
        if (filter == Filter.ALL || filter == Filter.PHOTO) {
            photosLayout.removeAllViews();
            if (!photosList.isEmpty()) {
                int size_photos = photosList.size();
                final ArrayList<String> photosArray = new ArrayList<>();
                for (int i = 0, photosListSize = photosList.size(); i < photosListSize; i++) {
                    Photo photo = photosList.get(i);
                    photosArray.add(photo.getStringId());
                }

                photosLayout.setVisibility(View.VISIBLE);
                attachmentsLayout.addView(photosLayout);
                photosLayout.addView(scroll_photos);
                scroll_photos.addView(photos_layout);

                for (final Photo photo : photosList) {
                    final ImageView imageView = new ImageView(ctx);
                    int w, h;
                    if (size_photos == 1) {
                        scroll_photos.setOnTouchListener((v, event) -> true);
                        w = (int) Math.min(content_width, Global.convertDpToPixel(ctx, photo.getWidth()));
                        h = Global.getResizedDimensions(photo.getWidth(), photo.getHeight(), w, 0, true).second;
                    } else {
                        scroll_photos.setOnTouchListener((v, event) -> false);
                        w = Global.getResizedDimensions(photo.getWidth(), photo.getHeight(), 0, layoutHeight, false).first;
                        h = layoutHeight;
                        imageView.setAdjustViewBounds(true);
                    }
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(w, h));
                    imageView.setOnClickListener(v -> {
                        Intent intent = new Intent(ctx, PhotoActivity.class);
                        intent.putStringArrayListExtra("photos_layout", photosArray);
                        intent.putExtra("photo_position", photo.getPosition());
                        ctx.startActivity(intent);
                    });
                    if (w > 0 && h > 0) {
                        picasso.load(photo.getMaxImage()).resize(w, h).tag("chat").into(imageView);
                        photos_layout.addView(imageView);
                        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }
                }
            } else {
                photosLayout.setVisibility(View.GONE);
            }
        }

        //
        // Videos
        //
        if (filter == Filter.ALL || filter == Filter.VIDEO) {
            videosLayout.removeAllViews();
            if (!videosList.isEmpty()) {

                videosLayout.setVisibility(View.VISIBLE);
                videosLayout.addView(scroll_videos);

                putHr(attachmentsLayout, ctx);
                attachmentsLayout.addView(videosLayout);

                scroll_videos.addView(videos_layout);
                int size_videos = videosList.size();
                //countVideos.setText(size_videos + " видео");
                for (Video video : videosList) {
                    VideoImageView videoView = new VideoImageView(ctx);
                    videoView.setVideo(video);
                    videoView.setTag("dialogs");
                    int w, h;
                    if (size_videos == 1) {
                        scroll_videos.setOnTouchListener((v, event) -> true);
                        w = content_width;
                        h = Global.getResizedDimensions(320, 240, w, 0, true).second;
                    } else {
                        scroll_videos.setOnTouchListener((v, event) -> false);
                        w = Global.getResizedDimensions(320, 240, 0, layoutHeight, false).first;
                        h = layoutHeight;
                        videoView.setAdjustViewBnds(true);
                    }
                    if (w > 0 && h > 0) {
                        videoView.setLayoutParams(new LinearLayout.LayoutParams(w, h));
                        videoView.setImage(video.getMaxImage(), w, h);

                        videos_layout.addView(videoView);
                    }
                }
            }
        }

        //
        // Audios
        //
        if (filter == Filter.ALL || filter == Filter.AUDIO) {
            audiosLayout.removeAllViews();
            if (!audiosList.isEmpty()) {
                for (Audio audio : audiosList) {
                    AudioView audio_item = new AudioView(ctx);
                    audio_item.setAudio(audio);
                    audio_layout.addView(audio_item);
                }

                putHr(attachmentsLayout, ctx);
                audiosLayout.setVisibility(View.VISIBLE);
                audiosLayout.addView(audio_layout);

                attachmentsLayout.addView(audiosLayout);

                audio_layout.setBackgroundResource(R.drawable.border_button);
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) audio_layout.getLayoutParams();
                audio_layout.setLayoutParams(params);
            }
        }

        //
        // Files
        //
        if (filter == Filter.ALL || filter == Filter.DOC) {
            filesLayout.removeAllViews();
            if (!filesList.isEmpty()) {
                for (Document doc : filesList) {
                    DocView docView = new DocView(ctx);
                    docView.setTag("news");
                    docView.setDocument(doc);
                    doc_layout.addView(docView);
                }

                putHr(attachmentsLayout, ctx);
                filesLayout.setVisibility(View.VISIBLE);
                filesLayout.addView(doc_layout);

                attachmentsLayout.addView(filesLayout);

                doc_layout.setBackgroundResource(R.drawable.border_button);
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) doc_layout.getLayoutParams();
                doc_layout.setLayoutParams(params);
            }
        }

        //
        // Links
        //
        if (filter == Filter.ALL || filter == Filter.LINK) {
            linksLayout.removeAllViews();
            if (!linksList.isEmpty()) {
                for (Link link : linksList) {
                    LinkView linkView = new LinkView(ctx);
                    linkView.setLink(link, content_width);
                    linkView.setTag("news");
                    links_layout.addView(linkView);
                }

                putHr(attachmentsLayout, ctx);
                linksLayout.setVisibility(View.VISIBLE);
                linksLayout.addView(links_layout);

                attachmentsLayout.addView(linksLayout);
            }
        }

        //
        // Poll
        //
        if (filter == Filter.ALL || filter == Filter.POLL) {
            pollsLayout.removeAllViews();
            if (!pollsList.isEmpty()) {
                PollView pollView = new PollView(ctx);
                Poll poll = pollsList.get(0);
                pollView.setPoll(poll);
                pollView.setTag("poll");
//            if (attachmentsLayout.getChildCount() == 0) pollView.removePadding();
                polls_layout.addView(pollView);

                putHr(attachmentsLayout, ctx);
                pollsLayout.setVisibility(View.VISIBLE);
                pollsLayout.addView(polls_layout);

                attachmentsLayout.addView(pollsLayout);
            }
        }

        //
        // Repost
        //
        if (filter == Filter.ALL || filter == Filter.POST) {
            postsLayout.removeAllViews();
            if (!postsList.isEmpty()) {
                RepostView repostView = new RepostView(ctx);
                Post post = postsList.get(0);
                repostView.setPost(post, content_width, post.getCopy_history(), -1);
                repostView.setTag("news");
                if (attachmentsLayout.getChildCount() == 0) repostView.removePadding();
                posts_layout.addView(repostView);

                putHr(attachmentsLayout, ctx);
                postsLayout.setVisibility(View.VISIBLE);
                postsLayout.addView(posts_layout);

                attachmentsLayout.addView(postsLayout);
            }
        }

        //
        // Geos
        //
        if (filter == Filter.ALL || filter == Filter.GEO) {
            geosLayout.removeAllViews();
            if (!geosList.isEmpty()) {
                LocationView geoView = new LocationView(ctx);
                Geo geo = geosList.get(0);
                geoView.setLocation(geo, content_width);
                geoView.setTag("chat_geo");
                geos_layout.addView(geoView);


                putHr(attachmentsLayout, ctx);
                geosLayout.setVisibility(View.VISIBLE);
                geosLayout.addView(geos_layout);

                attachmentsLayout.addView(geosLayout);
            }
        }

        attachments_frame.addView(attachmentsLayout);
    }

    private void putHr(LinearLayout attachmentsLayout, Context context) {
        if (attachmentsLayout.getChildCount() > 0) {
            View view = new View(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) Global.convertDpToPixel(context, 4));
            view.setLayoutParams(layoutParams);
            attachmentsLayout.addView(view);
        }
    }
}