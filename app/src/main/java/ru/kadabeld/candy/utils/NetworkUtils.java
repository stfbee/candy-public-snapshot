package ru.kadabeld.candy.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

/**
 * Created by arktic on 10.05.16.
 */
public class NetworkUtils {

    private static final long INTERVAL = TimeUnit.MINUTES.toMillis(1);

    private static NetworkUtils INSTANCE;

    public static void init(@NonNull Context context) {
        INSTANCE = new NetworkUtils(context);
    }

    @NonNull
    public static NetworkUtils getInstance() {
        if (INSTANCE == null) {
            throw new RuntimeException("NetworkUtils not initialised");
        }
        return INSTANCE;
    }

    @NonNull
    private Context ctx;
    private long lastChecked = 0L;
    private boolean lastConnected;

    private NetworkUtils(@NonNull Context ctx) {
        this.ctx = ctx;
    }

    private boolean isAnyConnectionAvailable() {
        ConnectivityManager connMan = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMan.getActiveNetworkInfo();

        return networkInfo != null && (networkInfo.getState() == NetworkInfo.State.CONNECTED);
    }

    public boolean isConnectedNow() {
        lastConnected = isAnyConnectionAvailable();
        lastChecked = System.currentTimeMillis();
        return lastConnected;
    }

    public boolean isConnected() {
        if (System.currentTimeMillis() - lastChecked > INTERVAL) {
            return isConnectedNow();
        }
        return lastConnected;
    }

    public OkHttpClient generateDefaultOkHttpClient() {
        return new OkHttpClient();
    }

}
