package ru.kadabeld.candy.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.kadabeld.candy.CandyApplication;

public class HumanTime {
    public String getTime(long unixtime) {
        String string;
        long now = System.currentTimeMillis() / 1000L;
        long diff = now - unixtime;
        if (diff <= 12 * 60 * 60) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", new Locale(CandyApplication.lang));
            string = sdf.format(new Date(unixtime * 1000L));
            return string;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("d MMMM", new Locale(CandyApplication.lang));
            string = sdf.format(new Date(unixtime * 1000L));
            return string;
        }
    }
}
