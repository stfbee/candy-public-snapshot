package ru.kadabeld.candy.utils.parsers;

import android.os.StrictMode;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Comment;
import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.entities.User;

public class Comments {
    private ArrayList<Comment> comments = new ArrayList<>();

    public static Comments parsePostComments(JSONArray items, JSONArray profiles, JSONArray p2u, JSONArray p2n, long post_owner) {
        Comments comments = new Comments();
        try {
            for (int pr = 0; pr < profiles.length(); pr++) {
                JSONObject userObject = profiles.getJSONObject(pr);
                AbstractProfile.parse(userObject);
            }
            for (int pr = 0; pr < p2u.length(); pr++) {
                User user = (User) Cache.getAbstractProfile(p2u.getLong(pr));
                String name = p2n.getString(pr);
                if (user != null)
                    user.setName(User.NameCase.DAT, name);
            }
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                Comment comment = Comment.parseComment(item);
                comment.setWhere_is(post_owner);
                comments.comments.add(0, comment);
            }
            return comments;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return comments;
    }

    public static Comments parsePhotoComments(JSONArray commentObjects, JSONArray groupObjects, JSONArray profileObjects, long photo_owner) {
        Comments comments = new Comments();
        try {
            for (int gr = 0; gr < groupObjects.length(); gr++) {
                JSONObject groupObject = groupObjects.getJSONObject(gr);
                Group.parseGroup(groupObject);
            }
            for (int pr = 0; pr < profileObjects.length(); pr++) {
                JSONObject userObject = profileObjects.getJSONObject(pr);
                User.parseUser(userObject);
            }
            for (int i = 0; i < commentObjects.length(); i++) {
                JSONObject commentObject = commentObjects.getJSONObject(i);
                Comment comment = Comment.parseComment(commentObject);
                comments.comments.add(0, comment);
                if (comment.getReply_to_user() > 0) {
                    RequestParams params = new RequestParams();
                    params.put("code", "var user_id = " + comment.getReply_to_user() + ";\n" +
                            "var ret = API.users.get({\"user_id\":user_id,\"name_case\":\"gen\",\"fields\":\"\"})[0];\n" +
                            "\n" +
                            "var info;\n" +
                            "\n" +
                            "info = API.users.get({\"user_id\":user_id,\"name_case\":\"nom\"})[0];\n" +
                            "ret.last_name_nom = info.last_name;\n" +
                            "ret.first_name_nom = info.first_name;\n" +
                            "info = API.users.get({\"user_id\":user_id,\"name_case\":\"gen\"})[0];\n" +
                            "ret.last_name_gen = info.last_name;\n" +
                            "ret.first_name_gen = info.first_name;\n" +
                            "info = API.users.get({\"user_id\":user_id,\"name_case\":\"dat\"})[0];\n" +
                            "ret.last_name_dat = info.last_name;\n" +
                            "ret.first_name_dat = info.first_name;\n" +
                            "info = API.users.get({\"user_id\":user_id,\"name_case\":\"acc\"})[0];\n" +
                            "ret.last_name_acc = info.last_name;\n" +
                            "ret.first_name_acc = info.first_name;\n" +
                            "info = API.users.get({\"user_id\":user_id,\"name_case\":\"ins\"})[0];\n" +
                            "ret.last_name_ins = info.last_name;\n" +
                            "ret.first_name_ins = info.first_name;\n" +
                            "info = API.users.get({\"user_id\":user_id,\"name_case\":\"abl\"})[0];\n" +
                            "ret.last_name_abl = info.last_name;\n" +
                            "ret.first_name_abl = info.first_name;\n" +
                            "\n" +
                            "return ret;");
                    params.put("v", API.version);
                    params.put("lang", CandyApplication.lang);
                    SyncHttpClient client = new SyncHttpClient();
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    client.get(API.BASE() + API.usersGet, params, new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                            try {
                                JSONObject response = new JSONObject(responseString);
                                User.parseUser(response);
                            } catch (Exception ignored) {
                            }
                        }
                    });
                }
            }
            return comments;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return comments;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }
}
