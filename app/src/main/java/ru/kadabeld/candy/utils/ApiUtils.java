package ru.kadabeld.candy.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by VOnishchenko
 * 18.03.2016
 * 16:51
 */
public class ApiUtils {
    public static ErrorType parseError(JSONObject object) throws JSONException {
        if (object.has("error")) {
            JSONObject error = object.getJSONObject("error");
            if (error.has("error_code")) {
                int error_code = error.getInt("error_code");
                switch (error_code) {
                    default:
                    case 1:
                        return ErrorType.UNKNOWN_ERROR;
                    case 2:
                        return ErrorType.APP_DISABLED;
                    case 3:
                        return ErrorType.UNKNOWN_METHOD;
                    case 4:
                        return ErrorType.BAD_SIGN;
                    case 5:
                        return ErrorType.NO_USER_AUTH;
                    case 6:
                        return ErrorType.TOO_MANY_REQUESTS;
                    case 7:
                        return ErrorType.NO_RIGHTS;
                    case 8:
                        return ErrorType.BAD_REQUEST;
                    case 9:
                        return ErrorType.FLOOD_CONTROL;
                    case 10:
                        return ErrorType.INTERNAL_ERROR;
                    case 11:
                        return ErrorType.TEST_MODE_ERROR;
                    case 14:
                        return ErrorType.NEEDS_CAPTCHA;
                    case 15:
                        return ErrorType.NO_ACCESS;
                    case 16:
                        return ErrorType.HTTPS_NEEDED;
                    case 17:
                        return ErrorType.VALIDATION_REQUIRED;
                    case 20:
                        return ErrorType.PERMISSION_DENIED_1;
                    case 21:
                        return ErrorType.PERMISSION_DENIED_2;
                    case 23:
                        return ErrorType.method_disabled;
                    case 24:
                        return ErrorType.Confirmation_required;
                    case 100:
                        return ErrorType.BAD_PARAMETER;
                    case 101:
                        return ErrorType.BAD_APP_ID;
                    case 113:
                        return ErrorType.BAD_USER_ID;
                    case 150:
                        return ErrorType.BAD_TIMESTAMP;
                    case 200:
                        return ErrorType.NO_ACCESS_ALBUM;
                    case 201:
                        return ErrorType.NO_ACCESS_AUDIO;
                    case 203:
                        return ErrorType.NO_ACCESS_GROUP;
                    case 300:
                        return ErrorType.ALBUM_FULL;
                    case 500:
                        return ErrorType.NO_MONEY_NO_HONEY;
                    case 600:
                        return ErrorType.NO_AD_RIGHTS;
                    case 603:
                        return ErrorType.AD_ERROR;
                }
            }
        }
        return null;
    }

    public enum ErrorType {
        UNKNOWN_ERROR,        //Произошла неизвестная ошибка.
        APP_DISABLED,        //Приложение выключено.
        UNKNOWN_METHOD,        //Передан неизвестный метод.
        BAD_SIGN,        //Неверная подпись.
        NO_USER_AUTH,        //Авторизация пользователя не удалась.
        TOO_MANY_REQUESTS,        //Слишком много запросов в секунду.
        NO_RIGHTS,        //Нет прав для выполнения этого действия.
        BAD_REQUEST,        //Неверный запрос.
        FLOOD_CONTROL,        //Слишком много однотипных действий.
        INTERNAL_ERROR,       //Произошла внутренняя ошибка сервера.
        TEST_MODE_ERROR,       //В тестовом режиме приложение должно быть выключено или пользователь должен быть залогинен.
        NEEDS_CAPTCHA,       //Требуется ввод кода с картинки (Captcha).
        NO_ACCESS,       //Доступ запрещён.
        HTTPS_NEEDED,       //Требуется выполнение запросов по протоколу HTTPS, т.к. пользователь включил настройку, требующую работу через безопасное соединение.
        VALIDATION_REQUIRED,       //Требуется валидация пользователя.
        PERMISSION_DENIED_1,       //Данное действие запрещено для не Standalone приложений.
        PERMISSION_DENIED_2,       //Данное действие разрешено только для Standalone и Open API приложений.
        method_disabled,       //Метод был выключен.
        Confirmation_required,       //Требуется подтверждение со стороны пользователя.
        BAD_PARAMETER,      //Один из необходимых параметров был не передан или неверен.
        BAD_APP_ID,      //Неверный API ID приложения.
        BAD_USER_ID,      //Неверный идентификатор пользователя.
        BAD_TIMESTAMP,      //Неверный timestamp.
        NO_ACCESS_ALBUM,      //Доступ к альбому запрещён.
        NO_ACCESS_AUDIO,      //Доступ к аудио запрещён.
        NO_ACCESS_GROUP,      //Доступ к группе запрещён.
        ALBUM_FULL,      //Альбом переполнен.
        NO_MONEY_NO_HONEY,      //Действие запрещено. Вы должны включить переводы голосов в настройках приложения.
        NO_AD_RIGHTS,      //Нет прав на выполнение данных операций с рекламным кабинетом.
        AD_ERROR,      //Произошла ошибка при работе с рекламным кабинетом
    }
}
