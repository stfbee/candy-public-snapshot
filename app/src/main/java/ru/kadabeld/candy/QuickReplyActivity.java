package ru.kadabeld.candy;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import org.apache.http.Header;

import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.notifications.NewMessageNotification;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

public class QuickReplyActivity extends RxAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(BIND_AUTO_CREATE);
        CandyApplication.setTheme(this);
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        final boolean is_chat = extras.getBoolean("is_chat");
        final long chat_id = extras.getLong("chat_id");
        final long user_id = extras.getLong("user_id");
        final String message = extras.getString("message");
        final String chat_title = extras.getString("chat_title", "");
        final String name = extras.getString("name");
        final Long message_id = extras.getLong("message_id");
        final Long timestamp = extras.getLong("timestamp");
        final User user = (User) Cache.getAbstractProfile(user_id);

        setContentView(R.layout.activity_quick_reply);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) supportActionBar.hide();

        final TextView name_text = (TextView) findViewById(R.id.name_text);
        final TextView message_text = (TextView) findViewById(R.id.message_text);
        final TextView location_text = (TextView) findViewById(R.id.location_text);
        final EditText writeBar = (EditText) findViewById(R.id.writebar_text);
        final ImageView avatar = (ImageView) findViewById(R.id.avatar);
        name_text.setText(name);
        message_text.setText(emhtml(message));
        location_text.setText(is_chat ? emhtml(chat_title) : getResources().getString(R.string.nt_quick_reply_private));

        getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        FrameLayout writebar_send = (FrameLayout) findViewById(R.id.writebar_send);
        avatar.setImageDrawable(user.getAvatarDrawable());
        if (!user.isAvatarDefault()) {
            Picasso.with(this).load(user.getPhotoFor(200)).transform(new CropCircleTransformation()).into(avatar);
        }

        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (!sPref.getBoolean("ninja", false)) {
            long id = message_id;
            if (is_chat) {
                API.markAsRead(id, 2000000000 + chat_id);
            } else {
                API.markAsRead(id, user_id);
            }
        }

        writebar_send.setOnClickListener(v -> {
            if (!writeBar.getText().toString().isEmpty()) {

                final String text = writeBar.getText().toString();
                writeBar.getText().clear();

                RequestParams params = new RequestParams();
                if (is_chat) {
                    params.put("chat_id", chat_id % 2000000000);
                } else {
                    params.put("user_id", user_id);
                }
                params.put("message", text);
                params.put("v", API.version);
                params.put("access_token", CandyApplication.access_token());
                params.put("lang", CandyApplication.lang);

//                    final long local_id = unsentCounter;
//                    unsentCounter++;
//                    message.setId(local_id);
//                    unsentTree.put(local_id, message);
//
//                    updateList();


                CandyApplication.client.get(API.BASE() + API.messagesSend, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        System.out.println(responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        System.out.println(responseString);
                        Toast.makeText(getApplicationContext(), R.string.sent, Toast.LENGTH_SHORT).show();
                        NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                        nm.cancel(NewMessageNotification.NOTIFICATION_TAG, 0);
                        finish();
                    }
                });
//                            client.get(API.BASE() + API.messagesSend, params, new JsonHttpResponseHandler() {
//                                @Override
//                                public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                                    Toast.makeText(getApplicationContext(), R.string.sent, Toast.LENGTH_SHORT).show();
//                                    NotificationManager nm = (android.app.NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                                    nm.cancel(NewMessageNotification.NOTIFICATION_TAG, 0);
//                                    System.out.println(responseString);
//                                    finish();
//                                }
//
//                                @Override
//                                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                                    System.out.println(responseString);
//                                }
//                            });
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    private static CharSequence emhtml(String s) {
        return android.text.Html.fromHtml(s).toString().trim();
    }
}
