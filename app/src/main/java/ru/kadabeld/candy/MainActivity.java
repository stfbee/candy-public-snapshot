package ru.kadabeld.candy;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tbruyelle.rxpermissions.RxPermissions;

import ru.kadabeld.candy.fragments.ProfileFragment;
import ru.kadabeld.candy.im.IMService;
import ru.kadabeld.candy.notifications.NotificationsService;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends AbstractActivity {
    @Override
    protected void onDestroy() {
        super.onDestroy();
        API.setOffline();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();//TODO: shit
        if (bundle != null) {
            String action = bundle.getString("action");
            if (action != null) {
                if (action.equals("open_profile")) {
                    ProfileFragment f = new ProfileFragment();
                    f.init(bundle.getLong("user_id", 0));
                    openFragment(f, false);
                }
            } else {
                onNavigationItemSelected(navigationView.getMenu().getItem(0));
                navigationView.getMenu().findItem(R.id.drawer_news).setChecked(true);
            }
        } else {
            onNavigationItemSelected(navigationView.getMenu().getItem(0));
            navigationView.getMenu().findItem(R.id.drawer_news).setChecked(true);
        }

        RxPermissions.getInstance(this)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .flatMap(granted ->
                        CandyApplication.getInstance().getAccountsBus().current()
                ).observeOn(AndroidSchedulers.mainThread())
                .compose(bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(currentUser -> {
                    if (currentUser == null) {
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                    } else if (BuildConfig.DONATE && !currentUser.isDonate()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle(R.string.login_important)
                                .setMessage(R.string.login_not_donate)
                                .setCancelable(false)
                                .setPositiveButton(R.string.login_group,
                                        (dialog, which) -> {
                                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/kadabeldstudios_donate"));
                                            startActivity(browserIntent);
                                        })
                                .setNegativeButton(R.string.ok_bitch,
                                        (dialog, id) -> {
                                            dialog.cancel();
// TODO:                                    finish();
                                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                });

        startService(new Intent(this, IMService.class));
        startService(new Intent(this, NotificationsService.class));
    }

    public static class FragmentIndex extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            TextView text = new TextView(this.getActivity());
            text.setText(R.string.not_working);
            text.setGravity(Gravity.CENTER);

            ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (supportActionBar != null) supportActionBar.setTitle(getString(R.string.app_name));
            return text;
        }
    }
}