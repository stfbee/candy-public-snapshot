package ru.kadabeld.candy.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import net.steamcrafted.materialiconlib.MaterialIconView;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.fragments.PostFragment;
import ru.kadabeld.candy.fragments.ProfileFragment;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.parsers.Attachments;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;
import ru.kadabeld.candy.views.FLinearLayout;
import ru.kadabeld.candy.views.RepostView;

@SuppressWarnings("deprecation")
public class PostAdapter extends ArrayAdapter<Post> {
    Activity ctx;
    LayoutInflater lInflater;
    ArrayList<Post> objects;
    Picasso picasso;
    int layoutWidth = 0;
    ViewType viewType = ViewType.FEED;
    SharedPreferences sPref;
    int colorPrimary = 0;

    public enum ViewType {
        FEED, SINGLE, MESSAGE
    }

    public PostAdapter(Activity context, ArrayList<Post> news, ViewType viewType) {
        super(context, 0, news);
        ctx = context;
        objects = news;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        picasso = Picasso.with(ctx);
        this.viewType = viewType;
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public PostAdapter(Activity context, ArrayList<Post> news, ViewType viewType, int colorPrimary, int colorPrimaryDark) {
        this(context, news, viewType);
        this.colorPrimary = colorPrimary;
    }

    public int getColorPrimary() {
        return colorPrimary;
    }

    public void setColors(int colorPrimary) {
        this.colorPrimary = colorPrimary;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        if (v == null) {
            v = lInflater.inflate(R.layout.list_item_post, parent, false);
        }

        //init widgets
        final FLinearLayout post = ViewHolder.get(v, R.id.post);

        final RelativeLayout attach_info = ViewHolder.get(v, R.id.attach_info);

        final RelativeLayout header = ViewHolder.get(v, R.id.header);
        final ImageView avatar = ViewHolder.get(v, R.id.avatar);
        final TextView name = ViewHolder.get(v, R.id.name);
        final TextView time = ViewHolder.get(v, R.id.time);
        final MaterialIconView drop_menu = ViewHolder.get(v, R.id.drop_menu);

        final LinearLayout body = ViewHolder.get(v, R.id.body);
        final LinearLayout content_wrapper = ViewHolder.get(v, R.id.content_wrapper);
        final TextView text = ViewHolder.get(v, R.id.text);
        final FLinearLayout read_more_content = ViewHolder.get(v, R.id.read_more_content);
        final TextView read_more_content_text = ViewHolder.get(v, R.id.read_more_content_text);
        final FrameLayout attachments_frame = ViewHolder.get(v, R.id.attachments_frame);
        final FrameLayout repost_frame = ViewHolder.get(v, R.id.repost_frame);

        final LinearLayout footer = ViewHolder.get(v, R.id.footer);
        final TextView signer = ViewHolder.get(v, R.id.signer);
        final RelativeLayout bottom_buttons = ViewHolder.get(v, R.id.bottom_buttons);
        final ImageView like = ViewHolder.get(v, R.id.like);
        final ImageView share = ViewHolder.get(v, R.id.share);
        final ImageView comment = ViewHolder.get(v, R.id.comment);
        final TextView like_count = ViewHolder.get(v, R.id.like_count);
        final TextView share_count = ViewHolder.get(v, R.id.share_count);
        final TextView comment_count = ViewHolder.get(v, R.id.comment_count);
        final LinearLayout like_layout = ViewHolder.get(v, R.id.like_layout);
        final LinearLayout share_layout = ViewHolder.get(v, R.id.share_layout);
        final LinearLayout comment_layout = ViewHolder.get(v, R.id.comment_layout);

        // colorize elements
        post.getBackground().setColorFilter(ColorUtils.getThemeColor(R.attr.colorLightBackground), PorterDuff.Mode.MULTIPLY);

        // setting default states
        attach_info.setVisibility(View.GONE);
        content_wrapper.setVisibility(View.GONE);
        read_more_content.setVisibility(View.GONE);
        attachments_frame.setVisibility(View.GONE);
        repost_frame.setVisibility(View.GONE);
        signer.setVisibility(View.GONE);
        like_count.setVisibility(View.GONE);
        share_count.setVisibility(View.GONE);
        comment_count.setVisibility(View.GONE);
        bottom_buttons.setVisibility(View.VISIBLE);
        repost_frame.removeAllViews();
        attachments_frame.removeAllViews();

        if (viewType == ViewType.FEED) {
            if (Build.VERSION.SDK_INT <= 19) {
                post.setForeground(ContextCompat.getDrawable(ctx, R.drawable.highlight));
            }
        }

        final Post n = getItem(position);

        layoutWidth = Global.getWindowSize(ctx)[0];
        // TODO: 10.03.2015 добавить centerVertical и норм
        //        layoutWidth = Math.min(Global.getWindowSize(ctx)[0],Global.getWindowSize(ctx)[1]);

        ctx.runOnUiThread(new Thread(new Runnable() {
            public void run() {
                if (n != null) {
                    if (viewType.equals(ViewType.FEED)) {
                        attach_info.setVisibility(n.is_pinned() ? View.VISIBLE : View.GONE);
                    }

                    long from_id = n.getFrom_id();
                    if (from_id == 0) from_id = n.getOwner_id();
                    AbstractProfile from_user = Cache.getAbstractProfile(from_id);
                    avatar.setImageDrawable(from_user.getAvatarDrawable());
                    if (!from_user.isAvatarDefault()) {
                        picasso.load(from_user.getPhotoFor(100)).tag("news").transform(new CropCircleTransformation()).into(avatar);
                    }
                    final long finalFrom_id = from_id;
                    avatar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ProfileFragment f = new ProfileFragment();
                            f.init(finalFrom_id);
                            ((AbstractActivity) getContext()).openFragment(f);
                        }
                    });

                    name.setText(from_user.getFullName());
                    time.setText(CandyApplication.p.format(new Date(n.getDate() * 1000L)));

                    body.removeAllViews();

                    if (viewType == ViewType.SINGLE) {
                        if (!n.getText().isEmpty()) {
                            content_wrapper.setVisibility(View.VISIBLE);
                            text.setText(n.getText());
                            body.addView(content_wrapper);
                        }
                    } else {
                        if (!n.getText().isEmpty()) {
                            content_wrapper.setVisibility(View.VISIBLE);
                            String small = String.valueOf(truncatePost(n.getText()));
                            text.setText(small);
                            read_more_content_text.setText((sPref.getBoolean("expandtext", false)) ? ctx.getResources().getString(R.string.expand_text) : ctx.getResources().getString(R.string.read_more));
                            read_more_content.setVisibility(small.equals(n.getText()) ? View.GONE : View.VISIBLE);
                            read_more_content.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (sPref.getBoolean("expandtext", false)) {
                                        text.setText(n.getText());
                                        read_more_content.setVisibility(View.GONE);
                                    } else {
                                        PostFragment f = new PostFragment();
                                        f.init(n.getOwner_id(), n.getId(), n.getComments().isCan_post(), colorPrimary);
                                        ((AbstractActivity) ctx).openFragment(f);
                                    }
                                }
                            });
                            body.addView(content_wrapper);
                        }
                    }

                    Attachments attachments = n.getAttachments();
                    if (!n.getGeo().getCoordinates().isEmpty()) {
                        attachments.addGeos(n.getGeo());
                    }
                    if (!attachments.isEmpty()) {
                        putHr(body, ctx);
                        attachments.getView(ctx, attachments_frame, Global.getWindowSize(ctx)[0]);
                        body.addView(attachments_frame);
                    }


                    if (n.has_repost()) {
                        putHr(body, ctx);
                        repost_frame.setVisibility(View.VISIBLE);
                        RepostView repostView = new RepostView(ctx);
                        repostView.setPost(n.getCopy_history().get(0), Global.getWindowSize(ctx)[0], n.getCopy_history(), 0);
                        repost_frame.addView(repostView);
                        body.addView(repost_frame);
                    }

                    if (viewType.equals(ViewType.MESSAGE)) {
                        bottom_buttons.setVisibility(View.GONE);
                    } else {
                        bottom_buttons.setVisibility(View.VISIBLE);

                        like.setColorFilter(null);

                        if (n.getLikes().getCount() > 0) {
                            like_count.setText(String.valueOf(n.getLikes().getCount()));
                            like_count.setVisibility(View.VISIBLE);
                        }

                        like(like, like_layout, n.getLikes().isUser_likes());

                        like_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                n.getLikes().like();
                                like(like, like_layout, n.getLikes().isUser_likes());
                                like_count.setVisibility(n.getLikes().getCount() == 0 ? View.GONE : View.VISIBLE);
                                like_count.setText(String.valueOf(n.getLikes().getCount()));

                                RequestParams params = new RequestParams();
                                params.put("type", "post");
                                params.put("owner_id", n.getOwner_id());
                                params.put("item_id", n.getId());
                                params.put("access_token", CandyApplication.access_token());
                                params.put("lang", CandyApplication.lang);
                                CandyApplication.client.get(API.BASE() + API.likes(n.getLikes().isUser_likes()), params, new JsonHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                        try {
                                            response = response.getJSONObject("response");
                                            if (response.has("likes")) {
                                                int likes = response.getInt("likes");
                                                n.getLikes().setCount(likes);
                                                like_count.setText(String.valueOf(likes));
                                            } else {
                                                n.getLikes().like();
                                                like(like, like_layout, n.getLikes().isUser_likes());
                                                like_count.setVisibility(n.getLikes().getCount() == 0 ? View.GONE : View.VISIBLE);
                                                like_count.setText(String.valueOf(n.getLikes().getCount()));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }


                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                        n.getLikes().like();
                                        like(like, like_layout, n.getLikes().isUser_likes());
                                        like_count.setVisibility(n.getLikes().getCount() == 0 ? View.GONE : View.VISIBLE);
                                        like_count.setText(String.valueOf(n.getLikes().getCount()));
                                    }
                                });
                            }
                        });

                        if (n.getReposts().getCount() > 0) {
                            share_count.setText(String.valueOf(n.getReposts().getCount()));
                            share_count.setVisibility(View.VISIBLE);
                        }
                        share_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(final View v) {
                                final ArrayList<String> actions = new ArrayList<>();
                                actions.clear();
                                actions.add(ctx.getResources().getString(R.string.repost_on_wall));
                                actions.add(ctx.getResources().getString(R.string.repost_to_community));
                                actions.add(ctx.getResources().getString(R.string.repost_private));
                                ListAdapter adapter = new ArrayAdapter<String>(ctx, R.layout.select_dialog_item, android.R.id.text1, actions) {
                                };
                                final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                                builder.setTitle(ctx.getString(R.string.action_share_post))
                                        .setAdapter(adapter, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int item) {
                                                switch (item) {
                                                    case 0:
                                                        RequestParams params = new RequestParams();
                                                        params.put("object", String.format("wall%s_%s", n.getOwner_id(), n.getId()));
                                                        params.put("message", "");
                                                        params.put("access_token", CandyApplication.access_token());
                                                        params.put("lang", CandyApplication.lang);
                                                        params.put("v", API.version);
                                                        CandyApplication.client.get(API.BASE() + API.wallRepost, params, new TextHttpResponseHandler() {
                                                            @Override
                                                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                                                            }

                                                            @Override
                                                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                                                Toast toast = Toast.makeText(getContext(), "Пост опубликован", Toast.LENGTH_SHORT);
                                                                toast.show();
                                                            }
                                                        });
                                                        break;
                                                    case 1:
                                                        Toast toast = Toast.makeText(getContext(), "Ещё не готово", Toast.LENGTH_SHORT);
                                                        toast.show();
                                                        break;
                                                    case 2:
                                                        Toast message = Toast.makeText(getContext(), "Ещё не готово", Toast.LENGTH_SHORT);
                                                        message.show();
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        })
                                        .setCancelable(true);
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });

                        if (n.getComments().getCount() > 0) {
                            comment_count.setText(String.valueOf(n.getComments().getCount()));
                            comment_count.setVisibility(View.VISIBLE);
                        }
                    }

                    if (viewType == ViewType.FEED) {
                        post.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PostFragment f = new PostFragment();
                                f.init(n.getOwner_id(), n.getId(), n.getComments().isCan_post(), colorPrimary);
                                ((AbstractActivity) ctx).openFragment(f);
                            }
                        });
                        comment_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PostFragment f = new PostFragment();
                                f.init(n.getOwner_id(), n.getId(), n.getComments().isCan_post(), colorPrimary);
                                ((AbstractActivity) ctx).openFragment(f);
                            }
                        });
                    }

                    signer.setVisibility(View.GONE);
                    if (n.getSigner_id() > 0) {
                        final User signer_user = (User) Cache.getAbstractProfile(n.getSigner_id());
                        AbstractProfile.ProfileOnLoad onLoad = new AbstractProfile.ProfileOnLoad() {
                            @Override
                            public void run(AbstractProfile profile) {
                                signer.setText(signer_user.getFullName());
                            }
                        };
                        if (signer_user == null) {
                            signer.setText(ctx.getResources().getString(R.string.loading));
                            User.load(n.getSigner_id(), onLoad);
                        } else {
                            onLoad.run(signer_user);
                        }
                        signer.setVisibility(View.VISIBLE);
                    }
                }
            }
        }));
        return v;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Post getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    void like(ImageView like, LinearLayout like_layout, boolean liked) {
        Drawable drawable = ContextCompat.getDrawable(ctx, R.drawable.button_news);
        like.setColorFilter(null);
        if (liked && drawable != null) {
            drawable.setColorFilter(ColorUtils.getThemeColor(R.attr.colorPrimary), PorterDuff.Mode.MULTIPLY);
            like.setColorFilter(new LightingColorFilter(ColorUtils.getThemeColor(R.attr.colorLightText), ColorUtils.getThemeColor(R.attr.colorLightText)));
        }
        like_layout.setBackgroundDrawable(drawable);
    }

    public static CharSequence truncatePost(CharSequence text) {
        int textSize = -1;
        if (text.toString().split("\n").length > 6) {
            int var7 = 0;
            String var8 = text.toString();

            for (int var9 = 0; var9 < 6; ++var9) {
                var7 = var8.indexOf(10, var7 + 1);
            }

            textSize = var7;
        }

        if (text.length() > 280) {
            textSize = Math.min(text.toString().indexOf(32, 280), 300);
            if (textSize == -1) {
                textSize = 280;
            }
        }

        if (textSize == -1) {
            return text;
        } else {
            SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
            stringBuilder.append(text, 0, textSize);
            stringBuilder.append("...");
            return stringBuilder;
        }
    }

    public static class ViewHolder {
        // I added a generic return type to reduce the casting noise in client code
        @SuppressWarnings("unchecked")
        public static <T extends View> T get(View view, int id) {
            SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
            if (viewHolder == null) {
                viewHolder = new SparseArray<>();
                view.setTag(viewHolder);
            }
            View childView = viewHolder.get(id);
            if (childView == null) {
                childView = view.findViewById(id);
                viewHolder.put(id, childView);
            }
            return (T) childView;
        }
    }

    private void putHr(LinearLayout layout, Context context) {
        if (layout.getChildCount() > 0) {
            View view = new View(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) Global.convertDpToPixel(context, 4));
            view.setLayoutParams(layoutParams);
            layout.addView(view);
        }
    }
}