package ru.kadabeld.candy.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.kadabeld.candy.PhotoActivity;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Photo;

import static ru.kadabeld.candy.R.id.photo;

/**
 * User: Vlad
 * Date: 02.07.2016
 * Time: 1:31
 */
public class PhotoGridAdapter extends ArrayAdapter<PhotoGridAdapter.PhotoItem> {
    private final LayoutInflater layoutInflater;

    public static class PhotoItem implements AsymmetricItem {

        private int columnSpan;
        private int rowSpan;
        private int position;
        private String path;
        private Photo photo;

        public PhotoItem(int position, Photo photo) {
            this.position = position;
            this.photo = photo;
            if (photo.getHeight() > photo.getWidth()) {
                this.rowSpan = Math.random() < 0.2f ? 2 : 1;
                this.columnSpan = 1;
            } else if (photo.getHeight() < photo.getWidth()) {
                this.columnSpan = Math.random() < 0.2f ? 2 : 1;
                this.rowSpan = 1;
            } else {
                this.columnSpan = Math.random() < 0.2f ? 2 : 1;
                this.rowSpan = columnSpan;
            }

            path = photo.getMaxImage();
        }

        @Override
        public int getColumnSpan() {
            return columnSpan;
        }

        @Override
        public int getRowSpan() {
            return rowSpan;
        }

        public int getPosition() {
            return position;
        }

        PhotoItem(Parcel in) {
            readFromParcel(in);
        }

        public PhotoItem(int columnSpan, int rowSpan, int position, String s) {
            this.columnSpan = columnSpan;
            this.rowSpan = rowSpan;
            this.position = position;
            path = s;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        private void readFromParcel(Parcel in) {
            columnSpan = in.readInt();
            rowSpan = in.readInt();
            position = in.readInt();
        }

        @Override
        public void writeToParcel(@NonNull Parcel dest, int flags) {
            dest.writeInt(columnSpan);
            dest.writeInt(rowSpan);
            dest.writeInt(position);
        }

        /* Parcelable interface implementation */
        public final Creator<PhotoItem> CREATOR = new Creator<PhotoItem>() {

            @Override
            public PhotoItem createFromParcel(@NonNull Parcel in) {
                return new PhotoItem(in);
            }

            @Override
            @NonNull
            public PhotoItem[] newArray(int size) {
                return new PhotoItem[size];
            }
        };

        public String getPath() {
            return path;
        }

        public Photo getPhoto() {
            return photo;
        }
    }

    private static class ViewHolder {
        ImageView placeholder;
        ImageView photo;
    }

    public PhotoGridAdapter(Context context, List<PhotoItem> objects) {
        super(context, 0, objects);
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PhotoItem item = getItem(position);
        ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.gallery_item_photo, parent, false);
            holder = new ViewHolder();
            holder.placeholder = (ImageView) convertView.findViewById(R.id.placeholder);
            holder.photo = (ImageView) convertView.findViewById(photo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        holder.photo.setImageDrawable(null);
        Picasso.with(getContext()).load(item.getPath()).into(holder.photo);

        holder.photo.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), PhotoActivity.class);
            ArrayList<String> value = new ArrayList<>();
            value.add(item.getPhoto().getStringId());
            intent.putStringArrayListExtra("photos_layout", value);
            intent.putExtra("photo_position", 0);
            getContext().startActivity(intent);
        });

        return convertView;
    }

    private void appendItems(List<PhotoItem> newItems) {
        addAll(newItems);
        notifyDataSetChanged();
    }

    public void setItems(List<PhotoItem> moreItems) {
        clear();
        appendItems(moreItems);
    }


}
