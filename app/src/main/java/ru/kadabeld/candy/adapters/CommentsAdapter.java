package ru.kadabeld.candy.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import net.steamcrafted.materialiconlib.MaterialIconView;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Comment;
import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.entities.Sticker;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.fragments.ProfileFragment;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.parsers.Attachments;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;
import ru.kadabeld.candy.views.WriteBar;

public class CommentsAdapter extends ArrayAdapter<Comment> {
    private Activity ctx;
    private LayoutInflater lInflater;
    private ArrayList<Comment> objects;
    private Picasso picasso;

    private int sticker_size;

    public CommentsAdapter(Activity context, ArrayList<Comment> comments) {
        super(context, 0, comments);
        ctx = context;
        objects = comments;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        picasso = Picasso.with(context);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sticker_size = sharedPreferences.getInt("stickersize", 128);
    }

    public int getCount() {
        return objects.size();
    }

    @Override
    public Comment getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View row, ViewGroup parent) {
        // TODO: 03.03.2016 вернуть правильный вьюхолдер
        row = lInflater.inflate(R.layout.list_item_comment, null, false);

        final ImageView avatar = (ImageView) row.findViewById(R.id.avatar);
        final ImageView sticker = (ImageView) row.findViewById(R.id.sticker);
        final TextView name = (TextView) row.findViewById(R.id.name);
        final TextView time = (TextView) row.findViewById(R.id.time);
        final TextView content = (TextView) row.findViewById(R.id.content);
        final TextView reply_for = (TextView) row.findViewById(R.id.reply_for);
        final TextView likes_count = (TextView) row.findViewById(R.id.likes_count);
        final LinearLayout layout_likes = (LinearLayout) row.findViewById(R.id.layout_like);
        final LinearLayout layout_answer = (LinearLayout) row.findViewById(R.id.layout_answer);
        final LinearLayout layout_reply = (LinearLayout) row.findViewById(R.id.layout_reply);
        final FrameLayout attachments_view = (FrameLayout) row.findViewById(R.id.attachments);
        final MaterialIconView like_icon = (MaterialIconView) row.findViewById(R.id.like_icon);

        final LinearLayout.LayoutParams stickerLayoutParams = (LinearLayout.LayoutParams) sticker.getLayoutParams();
        stickerLayoutParams.height = sticker_size;
        stickerLayoutParams.width = sticker_size;
        final Comment comment = objects.get(position);

        AbstractProfile.ProfileOnLoad profileOnLoad = new AbstractProfile.ProfileOnLoad() {
            @Override
            public void run(final AbstractProfile from_user) {
                avatar.setImageDrawable(from_user.getAvatarDrawable());
                if (!from_user.isAvatarDefault()) {
                    picasso.load(from_user.getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                }
                avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ProfileFragment f = new ProfileFragment();
                        f.init(comment.getFrom_id());
                        ((AbstractActivity) getContext()).openFragment(f);
                    }
                });
                name.setText(from_user.getName());
                time.setText(CandyApplication.p.format(new Date(comment.getDate() * 1000L)));

                if (comment.getText().isEmpty()) {
                    content.setVisibility(View.GONE);
                } else {
                    content.setVisibility(View.VISIBLE);
                    content.setText(comment.getText());
                }

                Attachments attachments = comment.getAttachments();
                if (!attachments.isEmpty()) {
                    final ArrayList<Sticker> stickers = attachments.getStickers();

                    if (!stickers.isEmpty()) {
                        sticker.setVisibility(View.VISIBLE);

                        picasso.load(stickers.get(0).getPhotoFor(sticker_size)).tag("comments").into(sticker);
                    } else {
                        sticker.setVisibility(View.GONE);
                        attachments.getView(ctx, attachments_view, (int) (Global.getWindowSize(ctx)[0] - Global.convertDpToPixel(ctx, 103)));
                        attachments_view.setVisibility(View.VISIBLE);
                    }
                }


                layout_answer.setVisibility(View.GONE);
                long reply_to_user = comment.getReply_to_user();
                if (reply_to_user != 0) {
                    AbstractProfile.ProfileOnLoad onLoad = new AbstractProfile.ProfileOnLoad() {
                        @Override
                        public void run(final AbstractProfile profile) {
                            layout_answer.setVisibility(View.VISIBLE);
                            reply_for.setText(profile.getName());
                            layout_answer.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ProfileFragment f = new ProfileFragment();
                                    f.init(profile.getId());
                                    ((AbstractActivity) getContext()).openFragment(f);
                                }
                            });

                        }
                    };

                    AbstractProfile receiver_user = Cache.getAbstractProfile(reply_to_user);
                    if (receiver_user == null) {
                        reply_for.setText(R.string.loading);
                        if (reply_to_user > 0) {
                            User.load(reply_to_user, onLoad);
                        } else {
                            Group.load(reply_to_user, onLoad);
                        }
                    } else {
                        onLoad.run(receiver_user);
                    }
                }

                layout_likes.setVisibility(View.VISIBLE);
                likes_count.setVisibility(View.VISIBLE);
                likes_count.setText(String.valueOf(comment.getCountLikes()));


                like_icon.setColor(ColorUtils.getThemeColor(comment.isLiked() ? R.attr.colorPrimary : R.attr.colorDisabledText));

                layout_likes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        comment.like();
                        like_icon.setColor(ColorUtils.getThemeColor(comment.isLiked() ? R.attr.colorPrimary : R.attr.colorDisabledText));
                        likes_count.setText(String.valueOf(comment.getCountLikes()));

                        RequestParams params = new RequestParams();
                        params.put("type", "comment");
                        params.put("owner_id", comment.getWhere_is());
                        params.put("item_id", comment.getId());
                        params.put("access_token", CandyApplication.access_token());
                        params.put("lang", CandyApplication.lang);
                        CandyApplication.client.get(API.BASE() + API.likes(objects.get(position).isLiked()), params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                try {
                                    response = response.getJSONObject("response");
                                    if (response.has("likes")) {
                                        int likes = response.getInt("likes");
                                        comment.setCountLikes(likes);
                                        likes_count.setText(String.valueOf(likes));
                                    } else {
                                        comment.like();
                                        likes_count.setText(String.valueOf(comment.getCountLikes()));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                comment.like();
                                likes_count.setText(String.valueOf(comment.getCountLikes()));
                                like_icon.setColor(ColorUtils.getThemeColor(comment.isLiked() ? R.attr.colorPrimary : R.attr.colorDisabledText));
                            }
                        });
                    }
                });

                layout_reply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final WriteBar writeBar = (WriteBar) ctx.findViewById(R.id.writebar);
                        if (writeBar != null) {
                            if (writeBar.getEditText().getText().toString().isEmpty()) {
                                writeBar.getEditText().setText(String.format("%s, ", from_user.getName()));
                            }
                            comment.setReply_to_comment(comment.getId());
                            comment.setReply_to_user(from_user.getId());
                            writeBar.setComment(comment);
                            ((InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(writeBar.getEditText(), InputMethodManager.SHOW_FORCED);
                        }
                    }
                });
            }
        };


        final AbstractProfile from_user = Cache.getAbstractProfile(comment.getFrom_id());
        if (from_user == null) {
            if (comment.getFrom_id() > 0) {
                User.load(comment.getFrom_id(), profileOnLoad);
            } else {
                Group.load(comment.getFrom_id(), profileOnLoad);
            }
        } else {
            profileOnLoad.run(from_user);
        }

        return row;
    }
}