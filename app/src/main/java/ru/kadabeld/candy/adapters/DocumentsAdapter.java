package ru.kadabeld.candy.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;
import net.steamcrafted.materialiconlib.MaterialIconView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Document;

/**
 * User: Vlad
 * Date: 16.07.2016
 * Time: 19:22
 */

public class DocumentsAdapter extends ArrayAdapter<Document> {
    private ArrayList<Document> objects;

    public DocumentsAdapter(Activity context, ArrayList<Document> documents) {
        super(context, 0, documents);
        objects = documents;
    }


    private String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = getContext().getResources().getStringArray(R.array.sizes);
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_item_document, parent, false);
            holder.info = (TextView) convertView.findViewById(R.id.info);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.thumb = (MaterialIconView) convertView.findViewById(R.id.thumb);
            holder.hr = convertView.findViewById(R.id.hr);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Document document = getItem(position);

        holder.title.setText(document.getTitle());
        holder.info.setText(getContext().getString(R.string.document_info, readableFileSize(document.getSize()), CandyApplication.p.format(new Date(document.getDate() * 1000))));

        holder.hr.setVisibility(position == getCount() - 1 ? View.GONE : View.VISIBLE);

        convertView.setOnClickListener(v -> {
            final ArrayList<String> actions = new ArrayList<>();

            String ext = getItem(position).getExt();
            if (ext.equals("gif") || ext.equals("png") || ext.equals("jpg")) {
                actions.add(getContext().getString(R.string.view));
            }

            actions.add(getContext().getString(R.string.download));
            actions.add(getContext().getString(R.string.add_documents));

            ListAdapter adapter = new ArrayAdapter<String>(getContext(), android.R.layout.select_dialog_item, android.R.id.text1, actions) {
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(getContext().getString(R.string.chs_action))
                    .setAdapter(adapter, (dialog, item) -> {
                        if (actions.get(item).equals(getContext().getString(R.string.view))) {
                            Global.gif(getContext(), getItem(position).getUrl());
                        }
                    })
                    .setCancelable(true);
            AlertDialog alert = builder.create();
            alert.show();
        });

        if (document.getPhoto_m().isEmpty()) {
            MaterialDrawableBuilder.IconValue iconValue = MaterialDrawableBuilder.IconValue.FILE;
            switch (document.getType()) {
                case TEXT:
                    iconValue = MaterialDrawableBuilder.IconValue.FILE_DOCUMENT;
                    break;
                case ZIP:
                    iconValue = MaterialDrawableBuilder.IconValue.FILE_LOCK;
                    break;
                case GIF:
                    iconValue = MaterialDrawableBuilder.IconValue.FILE_IMAGE;
                    break;
                case IMAGE:
                    iconValue = MaterialDrawableBuilder.IconValue.FILE_IMAGE;
                    break;
                case AUDIO:
                    iconValue = MaterialDrawableBuilder.IconValue.FILE_MUSIC;
                    break;
                case VIDEO:
                    iconValue = MaterialDrawableBuilder.IconValue.FILE_VIDEO;
                    break;
                case BOOK:
                    iconValue = MaterialDrawableBuilder.IconValue.FILE_DOCUMENT;
                    break;
                case OTHER:
                    iconValue = MaterialDrawableBuilder.IconValue.FILE;
                    break;
            }

            ((MaterialIconView) holder.thumb).setIcon(iconValue);
        } else {
            Picasso.with(getContext()).load(document.getPhoto_m()).into(holder.thumb);
        }


        return convertView;
    }

    private static class ViewHolder {
        ImageView thumb;
        TextView title;
        TextView info;
        View hr;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Document getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
