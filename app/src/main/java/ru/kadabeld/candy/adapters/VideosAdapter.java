package ru.kadabeld.candy.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.steamcrafted.materialiconlib.MaterialIconView;

import java.util.ArrayList;
import java.util.Date;

import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Group;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.entities.Video;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

import static ru.kadabeld.candy.CandyApplication.p;

/**
 * User: Vlad
 * Date: 02.07.2016
 * Time: 1:16
 */
public class VideosAdapter extends ArrayAdapter<Video> {
    private ArrayList<Video> objects;
    private LayoutInflater inflater;
    private Activity ctx;

    public VideosAdapter(Activity context, ArrayList<Video> videos) {
        super(context, 0, videos);
        ctx = context;
        objects = videos;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Video getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_video, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Video video = getItem(position);
        AbstractProfile abstractProfile = Cache.getAbstractProfile(video.getOwner_id());

        AbstractProfile.ProfileOnLoad runnable = profile -> {
            holder.video_author.setText(profile.getFullName());
            holder.avatar.setImageDrawable(profile.getAvatarDrawable());
            if (!profile.isAvatarDefault()) {
                Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(holder.avatar);
            }
        };

        Picasso.with(ctx).load(video.getMaxImage()).into(holder.video_frame);
        holder.avatar.setImageResource(R.drawable.avatar);
        holder.title.setText(video.getTitle());
        holder.duration.setText(Global.getMinutes(video.getDuration()));
        holder.info.setText(String.format(ctx.getString(R.string.video_views_date), video.getViews(), p.format(new Date(video.getDate() * 1000))));
        holder.more.setOnClickListener(v1 -> Toast.makeText(getContext(), R.string.dev_not_implemented_yet, Toast.LENGTH_SHORT).show());
        convertView.setOnClickListener(v1 -> Toast.makeText(getContext(), R.string.dev_videos_is_disabled, Toast.LENGTH_SHORT).show());

        if (abstractProfile != null) {
            runnable.run(abstractProfile);
        } else {
            long owner_id = video.getOwner_id();
            if (owner_id > 0) {
                User.load(owner_id, runnable);
            } else {
                Group.load(owner_id, runnable);
            }
        }

        return convertView;
    }

    static class ViewHolder {
        final ImageView video_frame;
        final ImageView avatar;
        final TextView title;
        final TextView video_author;
        final TextView info;
        final TextView duration;
        final MaterialIconView more;

        public ViewHolder(View convertView) {
            video_frame = (ImageView) convertView.findViewById(R.id.video_frame);
            avatar = (ImageView) convertView.findViewById(R.id.avatar);
            title = (TextView) convertView.findViewById(R.id.title);
            video_author = (TextView) convertView.findViewById(R.id.video_author);
            info = (TextView) convertView.findViewById(R.id.info);
            duration = (TextView) convertView.findViewById(R.id.duration);
            more = (MaterialIconView) convertView.findViewById(R.id.more);
        }
    }
}
