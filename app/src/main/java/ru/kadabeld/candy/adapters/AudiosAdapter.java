package ru.kadabeld.candy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Audio;

/**
 * User: Vlad
 * Date: 30.01.2015
 * Time: 21:08
 */
public class AudiosAdapter extends ArrayAdapter<Audio> {

    private ArrayList<Audio> objects;
    private LayoutInflater lInflater;

    public AudiosAdapter(Context context, ArrayList<Audio> audios) {
        super(context, 0, audios);
        objects = audios;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = lInflater.inflate(R.layout.list_item_audio, parent, false);
            holder = new ViewHolder();

            holder.duration = (TextView) convertView.findViewById(R.id.duration);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.artist = (TextView) convertView.findViewById(R.id.artist);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Audio audio = getItem(position);

        holder.duration.setText(Global.getMinutes(audio.getDuration()));
        holder.title.setText(audio.getTitle());
        holder.artist.setText(audio.getArtist());

        return convertView;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Audio getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView duration;
        TextView title;
        TextView artist;
    }
}