package ru.kadabeld.candy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.kadabeld.candy.im.IMService;
import ru.kadabeld.candy.notifications.NotificationsService;

public class CandyReceiver extends BroadcastReceiver {
    public CandyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent myIntent = new Intent(context, IMService.class);
        context.startService(myIntent);
        myIntent = new Intent(context, NotificationsService.class);
        context.startService(myIntent);
    }
}
