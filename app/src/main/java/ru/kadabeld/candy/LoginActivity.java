package ru.kadabeld.candy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.trello.rxlifecycle.ActivityEvent;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.kadabeld.candy.account.AccountInfo;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.utils.ColorUtils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@SuppressLint("SetJavaScriptEnabled")
public class LoginActivity extends RxAppCompatActivity {
    AlertDialog loadingAlert;
    private WebView webView;

    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.signup)
    Button signup;
    @BindView(R.id.forgot_password)
    TextView forgot_password;
    @BindView(R.id.login)
    EditText login;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.logo)
    TextView logo;
    @BindView(R.id.statusBar)
    FrameLayout statusBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CandyApplication.setTheme(this);
        super.onCreate(savedInstanceState);

        CandyApplication.getInstance().getAccountsBus()
                .current()
                .compose(bindToLifecycle())
                .filter(info -> info != null)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(info1 -> {
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                });
//        setTheme(R.style.CandyTheme_Default);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkAlpha));
            statusBar.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorPrimaryDark));
            getWindow().setNavigationBarColor(ColorUtils.getThemeColor(R.attr.colorPrimary));
        } else if (Build.VERSION.SDK_INT == 19) {
            statusBar.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorPrimaryDark));
        }
        Typeface type = Typeface.createFromAsset(getAssets(), "candy_font.ttf");
        logo.setTypeface(type);
        logo.setText(String.format(" %s ", getString(R.string.app_name)));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        loginButton.setOnClickListener(v -> {
            if (login.getText().toString().equals("")) {
                login.setError(getResources().getString(R.string.login_fill_field));
            } else if (password.getText().toString().equals("")) {
                password.setError(getResources().getString(R.string.login_fill_field));
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage(R.string.loading).setCancelable(true);
                loadingAlert = builder.create();
                loadingAlert.show();
                doAuth(login.getText().toString(), password.getText().toString(), false);
            }
        });

        signup.setOnClickListener(v -> {
            String url = "https://vk.com/join";
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(i);
        });

        forgot_password.setOnClickListener(v -> {
            String url = "https://vk.com/restore";
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(i);
        });
    }

    @SuppressLint("AddJavascriptInterface")
    void doAuth(final String login, final String password, boolean captcha) {
        RequestParams params = new RequestParams();
//        params.put("grant_type", "password");
        params.put("scope", "notify,friends,photos,audio,video,docs,notes,pages,status,wall,groups,messages,email,notifications,offline,stats");
//        params.put("client_id", "2274003");
//        params.put("client_secret", "hHbZxrka2uZ6jB1inYsH"); //gvk
        params.put("client_id", «insert_here»);
        params.put("client_secret", «insert_here»);
        params.put("display", "mobile");
        params.put("redirect_uri", "https://oauth.vk.com/blank.html");
        params.put("response_type", "token");
        params.put("v", API.version);
        params.put("lang", CandyApplication.lang);

        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.setTitle(getString(R.string.login_check));

        if (Build.VERSION.SDK_INT < 21) {
            int alertTitleDividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = dialog.findViewById(alertTitleDividerId);
            if (divider != null)
                divider.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorPrimary));
        }
        if (Build.VERSION.SDK_INT < 22) {
            int alertTitleId = dialog.getContext().getResources().getIdentifier("android:id/title", null, null);
            TextView title = (TextView) dialog.findViewById(alertTitleId);
            if (title != null) {
                title.setPadding(Global.dp(18), Global.dp(18), Global.dp(18), Global.dp(18));
                title.setBackgroundColor(ColorUtils.getThemeColor(R.attr.colorPrimary));
                title.setTextColor(ColorUtils.getThemeColor(R.attr.colorLightText));
            }
        }

        LayoutInflater inflater = LayoutInflater.from(LoginActivity.this);
        RelativeLayout authLayout = (RelativeLayout) inflater.inflate(R.layout.dialog_webview, null);
        webView = (WebView) authLayout.findViewById(R.id.webview);
        final ProgressBar progress_bar = (ProgressBar) authLayout.findViewById(R.id.progress_bar);

        progress_bar.setVisibility(View.VISIBLE);
        webView.setVisibility(View.INVISIBLE);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.addJavascriptInterface(this, "MyApp");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.contains("oauth.vk.com")) {
                    if (url.contains("access_token=")) {
                        progress_bar.setVisibility(View.VISIBLE);
                        webView.setVisibility(View.INVISIBLE);
                        StringTokenizer stringTokenizer = new StringTokenizer(url);
                        String accessToken = "";
                        String userID = "";
                        String secret = "";
                        while (stringTokenizer.hasMoreTokens()) {
                            String tokenizer = stringTokenizer.nextToken("&");
                            if (tokenizer.contains("access_token")) {
                                tokenizer = tokenizer.replace("https://oauth.vk.com/blank.html#access_token=", "");
                                accessToken = tokenizer;
                            }
                            if (tokenizer.contains("user_id")) {
                                tokenizer = tokenizer.replace("user_id=", "");
                                userID = tokenizer;
                            }
                            if (tokenizer.contains("secret")) {
                                tokenizer = tokenizer.replace("secret=", "");
                                secret = tokenizer;
                            }
                        }
                        dialog.dismiss();
                        final String finalAccessToken = accessToken;
                        final String finalSecret = secret;
                        User.load(Long.parseLong(userID), profile ->
                                addAccountToDB(finalAccessToken, profile.getId(), finalSecret, profile.getFullName(), profile.getPhotoFor(200)));
                    } else if (url.contains("act=authcheck")) {
                        progress_bar.setVisibility(View.GONE);
                        webView.setVisibility(View.VISIBLE);
                        js(view, "MyApp.resize(document.body.getBoundingClientRect().height)");
                    } else {
                        progress_bar.setVisibility(View.GONE);
                        webView.setVisibility(View.VISIBLE);
                        js(view, "MyApp.resize(document.body.getBoundingClientRect().height)");
                        js(view, "document.getElementsByName(\"email\")[0].value = \"" + login + "\";");
                        js(view, "document.getElementsByName(\"pass\")[0].value = \"" + password + "\";");
//                        js(view, "document.forms[0].getElementsByClassName(\"button\")[0].click();");
                    }
                }
            }
        });
        String url = "https://oauth.vk.com/authorize?" + params.toString();
        System.out.println(url);
        webView.loadUrl(url);
        dialog.setContentView(authLayout);
        loadingAlert.dismiss();
        dialog.show();
    }


    @JavascriptInterface
    public void resize(final float height) {
        runOnUiThread(() -> {
            webView.getLayoutParams().height = (int) (height * getResources().getDisplayMetrics().density);
//          webView.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDisplayMetrics().widthPixels, (int) (height * getResources().getDisplayMetrics().density)));
        });
    }

    void addAccountToDB(final String accessToken, final long user_id, final String secret, String fullName, String photoFor) {
        AccountInfo info = new AccountInfo(accessToken, user_id, fullName.split(" ")[0], fullName.split(" ")[1], photoFor);
        Observable.just(info)
                .observeOn(Schedulers.newThread())
                .map(new DonateCheck())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindUntilEvent(ActivityEvent.DESTROY))
                .doOnNext(info0 -> loadingAlert.dismiss())
                .doOnNext(info1 -> {
                    if (info1 == null) {
                        Toast.makeText(LoginActivity.this, R.string.no_internet, Toast.LENGTH_LONG).show();
                    }
                })
                .filter(info2 -> info2 != null)
                .subscribe(checked -> {
                    CandyApplication.getInstance().getAccountsBus().setCurrent(info);
                });
    }

    public void js(WebView webView, String js) {
        if (Build.VERSION.SDK_INT >= 19) {
            webView.evaluateJavascript(js, null);
        } else {
            webView.loadUrl("javascript:(function() {" + js + "})()");
        }
    }


}