package ru.kadabeld.candy;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONObject;

public class API {
    private final static String BASE = "https://api.vk.com/method/";
    public final static String BASE_without_setonline = BASE;

    public final static String usersGet = "users.get";

    public final static String friendsGet = "friends.get";
    public final static String friendsAdd = "friends.add";

    public final static String newsfeedGet = "newsfeed.get";
    public final static String newsfeedGetLists = "newsfeed.getLists";

    public final static String groupsGet = "groups.get";
    public final static String groupsJoin = "groups.join";
    public final static String groupsGetById = "groups.getById";
    public final static String groupsIsMember = "groups.isMember";

    public final static String messagesSend = "messages.send";
    public final static String messagesGetById = "messages.getById";
    public final static String messagesSetActivity = "messages.setActivity";
    public final static String messagesMarkAsRead = "messages.markAsRead";
    public final static String messagesDeleteDialog = "messages.deleteDialog";
    public final static String messagesGetHistory = "messages.getHistory";
    public final static String messagesGetChat = "messages.getChat";
    public final static String messagesEditChat = "messages.editChat";
    public final static String messagesGetChatUsers = "messages.getChatUsers";
    public final static String messagesGetLongPollServer = "messages.getLongPollServer";
    public final static String messagesGetHistoryAttachments = "messages.getHistoryAttachments";

    public final static String docsGet = "docs.get";

    public final static String notificationsGet = "notifications.get";

    public final static String audioGet = "audio.get";

    public final static String photosGet = "photos.get";
    public final static String photosGetById = "photos.getById";
    public final static String photosGetUserPhotos = "photos.getUserPhotos";
    public final static String photosGetAll = "photos.getAll";
    public final static String photosGetComments = "photos.getComments";

    public final static String videoGet = "video.get";
    public final static String videoSearch = "video.search";

    public final static String likesAdd = "likes.add";
    public final static String likesDelete = "likes.delete";

    public final static String wallGet = "wall.get";
    public final static String wallGetById = "wall.getById";
    public final static String wallRepost = "wall.repost";
    public final static String wallAddComment = "wall.addComment";
    public final static String wallGetComments = "wall.getComments";

    public final static String pollsGetById = "polls.getById";
    public final static String pollsAddVote = "polls.addVote";
    public final static String pollsDeleteVote = "polls.deleteVote";

    public final static String accountSetOnline = "account.setOnline";
    public final static String accountSetOffline = "account.setOffline";
    public final static String accountBanUser = "account.banUser";
    public final static String accountUnbanUser = "account.unbanUser";

    public final static String statsTrackVisitor = "stats.trackVisitor";

    public final static String storageSet = "storage.set";

    public final static String statusSet = "status.set";

    public final static String execute = "execute";
    public final static String executeMutualFriends = "execute.mutualFriends";
    public final static String executeGetDialogsWithProfilesNew = "execute.getDialogsWithProfilesNew";
    public final static String executeGetCommentsNew = "execute.getCommentsNew";
    public final static String executeGetFullGroupNew = "execute.getFullGroupNew";
    public final static String executeGetFullProfileNew = "execute.getFullProfileNew";
    public final static String executeGetPurchasedStickers = "execute.getPurchasedStickers";


    public final static String version = "5.52";
    public final static String userFields = "can_write_private_message,status,friend_status,photo_100,photo_200,photo_400_orig,online,last_seen,first_name_nom,first_name_abl,first_name_acc,first_name_dat,first_name_gen,first_name_ins,last_name_nom,last_name_abl,last_name_acc,last_name_dat,last_name_gen,last_name_ins";
    public final static String userFieldsLight = "photo_100,photo_200,photo_400_orig,online,first_name_nom,last_name_nom";

    public static String likes(boolean like) {
        return like ? likesAdd : likesDelete;
    }

    public static String BASE() {
        if (!CandyApplication.ninja) {
            setOnline();
        }
        return BASE;
    }

    public static void setOffline() {
        RequestParams params = new RequestParams();
        params.put("access_token", CandyApplication.access_token());
        params.put("v", version);
        CandyApplication.client.get(BASE + accountSetOffline, params, new JsonHttpResponseHandler() {
        });
    }

    public static void setOnline() {
        RequestParams params = new RequestParams();
        params.put("access_token", CandyApplication.access_token());
        params.put("v", version);
        CandyApplication.client.get(BASE + accountSetOnline, params, new JsonHttpResponseHandler() {
        });
    }

    public static void markAsRead(long start_message_id, long peer) {
        RequestParams params = new RequestParams();
        params.put("start_message_id", start_message_id);
        params.put("message_ids", start_message_id);
        params.put("peer_id", peer);
        params.put("access_token", CandyApplication.access_token());
        params.put("v", version);
        CandyApplication.client.get(BASE + messagesMarkAsRead, params, new JsonHttpResponseHandler() {
        });
    }

    public static void setActivity(long user_id) {
        RequestParams params = new RequestParams();
        params.put("user_id", user_id);
        params.put("type", "typing");
        params.put("access_token", CandyApplication.access_token());
        params.put("v", version);
        CandyApplication.client.get(BASE + messagesSetActivity, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            }
        });

    }
}
