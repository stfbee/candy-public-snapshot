package ru.kadabeld.candy;

import android.os.Bundle;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.trello.rxlifecycle.components.support.RxFragment;

import ru.kadabeld.candy.network.robospice.CandySpiceService;
import ru.kadabeld.candy.network.robospice.requests.CandyRequest;
import rx.Observable;

/**
 * Created by arktic on 13.05.16.
 */
public abstract class NetworkFragment extends RxFragment {

    private SpiceManager spiceManager = new SpiceManager(CandySpiceService.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        spiceManager.start(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        spiceManager.shouldStop();
    }

    protected <T> void reqestSend(CandyRequest<T> candyRequest) {
        spiceManager.execute(candyRequest, candyRequest.getCacheKey(), candyRequest.getCacheDuration(), new RequestListener<T>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {}

            @Override
            public void onRequestSuccess(T t) {}
        });
    }

    protected <T> Observable<T> requestObservable(CandyRequest<T> candyRequest) {
        return Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed()) {
                return;
            }
            spiceManager.execute(candyRequest, candyRequest.getCacheKey(), candyRequest.getCacheDuration(), new RequestListener<T>() {
                @Override
                public void onRequestFailure(SpiceException spiceException) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(spiceException);
                    }
                }

                @Override
                public void onRequestSuccess(T data) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(data);
                        subscriber.onCompleted();
                    }
                }
            });
        });
    }

}
