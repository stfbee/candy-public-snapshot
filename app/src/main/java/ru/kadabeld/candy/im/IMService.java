package ru.kadabeld.candy.im;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.utils.ApiUtils;

/**
 * User: Vlad
 * Date: 10.02.2015
 * Time: 6:33
 */
@SuppressWarnings({"StatementWithEmptyBody", "deprecation"})
public class IMService extends Service {
    public static final String ACTION_CHAT_WRITING = "ru.kadabeld.candy.ACTION_CHAT_WRITING";         //62
    public static final String ACTION_USER_WRITING = "ru.kadabeld.candy.ACTION_USER_WRITING";         //61
    public static final String ACTION_MESSAGE_NEW = "ru.kadabeld.candy.ACTION_MESSAGE_NEW";           //4
    public static final String ACTION_MESSAGE_DELETE = "ru.kadabeld.candy.ACTION_MESSAGE_DELETE";     //0
    public static final String ACTION_READ_OUTCOME = "ru.kadabeld.candy.ACTION_READ_OUTCOME";         //7
    public static final String ACTION_READ_INCOME = "ru.kadabeld.candy.ACTION_READ_INCOME";           //6
    public static final String ACTION_FLAGS_SET = "ru.kadabeld.candy.ACTION_FLAGS_SET";               //2
    public static final String ACTION_FLAGS_REPLACE = "ru.kadabeld.candy.ACTION_FLAGS_REPLACE";       //1
    public static final String ACTION_FLAGS_RESET = "ru.kadabeld.candy.ACTION_FLAGS_RESET";           //3
    public static final String ACTION_USER_ONLINE = "ru.kadabeld.candy.ACTION_USER_ONLINE";           //8
    public static final String ACTION_USER_OFFLINE = "ru.kadabeld.candy.ACTION_USER_OFFLINE";         //9
    public static final String ACTION_CHAT_UPDATE = "ru.kadabeld.candy.ACTION_CHAT_UPDATE";           //51
    public static final String ACTION_CALL = "ru.kadabeld.candy.ACTION_CALL";                         //70
    public static final String ACTION_COUNTER = "ru.kadabeld.candy.ACTION_COUNTER";                   //80


    // TODO:    10 11 12
    private static final int MESSAGE_DELETE = 0;
    private static final int FLAGS_REPLACE = 1;
    private static final int FLAGS_SET = 2;
    private static final int FLAGS_RESET = 3;
    private static final int MESSAGE_NEW = 4;
    private static final int READ_INCOME = 6;
    private static final int READ_OUTCOME = 7;
    private static final int USER_ONLINE = 8;
    private static final int USER_OFFLINE = 9;
    private static final int CHAT_UPDATE = 51;
    private static final int USER_WRITING = 61;
    private static final int CHAT_WRITING = 62;
    private static final int CALL = 70;
    private static final int COUNTER = 80;
    private static final int NOTIFICATIONS = 114;


    private static final String TAG = IMService.class.getSimpleName();
    private static String key;
    private static String server;
    private static long ts;
    private static AsyncHttpClient client = new SyncHttpClient();

    @Override
    public void onCreate() {
        super.onCreate();
        addNewServer();
        Log.d(TAG, "Сервис сообщенек создан");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Сервис сообщенек уничтожен");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Сервис сообщенек запущен");
        return Service.START_STICKY;
    }

    private void addNewServer() {
        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();

                while (TextUtils.isEmpty(CandyApplication.access_token())) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ignored) {
                    }
                }
                if (!TextUtils.isEmpty(CandyApplication.access_token())) {
                    Log.d(TAG, "И даже токен уже есть!");
                }

                try {
                    RequestParams params = new RequestParams();
                    params.put("v", API.version);
                    params.put("access_token", CandyApplication.access_token());
                    params.put("lang", CandyApplication.lang);
                    SyncHttpClient syncHttpClient = new SyncHttpClient();
                    syncHttpClient.get(API.BASE_without_setonline + API.messagesGetLongPollServer, params,
                            new JsonHttpResponseHandler() {
                                @Override
                                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                    Log.d(TAG, "onFailure: " + responseString, throwable);
                                    addNewServer();
                                }

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject obj) {
                                    try {
                                        ApiUtils.ErrorType errorType = ApiUtils.parseError(obj);
                                        if (errorType != null) {
                                            Log.e(TAG, "что-то не так в ответе: " + errorType);
                                            if (errorType == ApiUtils.ErrorType.TOO_MANY_REQUESTS) {
                                                Thread.sleep(1000);
                                                addNewServer();
                                            }
                                            return;
                                        }
                                        JSONObject response = obj.getJSONObject("response");
                                        key = response.getString("key").split("&")[0];
                                        server = response.getString("server");
                                        ts = response.getLong("ts");
                                        Log.i(TAG, "server: " + server);
                                        client.setTimeout(1000 * 30);
                                        check();

                                    } catch (JSONException | InterruptedException e) {
                                        Log.d(TAG, "Exception: ", e);
                                    }
                                }
                            });
                } catch (StackOverflowError | NullPointerException e) {
                    stopSelf();
                    startService(new Intent(getApplicationContext(), this.getClass()));
                }
            }
        }).start();
    }

    private void check() {
        RequestParams params = new RequestParams();
        params.put("act", "a_check");
        params.put("key", key);
        params.put("ts", ts);
        params.put("wait", 25);
        params.put("mode", 2 + 8 + 64 + 128);
        TextHttpResponseHandler responseHandler = new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e(TAG, responseString, throwable);
                throwable.printStackTrace();
                check();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject object = new JSONObject(responseString);
                    Log.i(TAG, responseString);

                    if (object.has("failed")) {
                        addNewServer();
                    } else {
                        ts = object.getLong("ts");
                        JSONArray updates = object.getJSONArray("updates");
                        for (int i = 0; i < updates.length(); i++) {
                            JSONArray update = updates.getJSONArray(i);
                            int type = update.getInt(0);

                            switch (type) {
                                case MESSAGE_DELETE: {
                                    long message_id = update.getInt(1);
                                    Log.d(TAG, "Удаление сообщения с указанным " + message_id);
                                    Intent broadcast = new Intent(ACTION_MESSAGE_DELETE);
                                    broadcast.putExtra("message_id", message_id);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case FLAGS_REPLACE: {
                                    long message_id = update.getInt(1);
                                    int flags = update.getInt(2);
                                    Log.d(TAG, "Замена флагов сообщения " + message_id + " на " + flags);
                                    Intent broadcast = new Intent(ACTION_FLAGS_REPLACE);
                                    broadcast.putExtra("message_id", message_id);
                                    broadcast.putExtra("flags", flags);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case FLAGS_SET: {
                                    long message_id = update.getInt(1);
                                    int mask = update.getInt(2);
                                    long from = 0;
                                    boolean is_chat = false;
                                    if (update.length() == 4) {
                                        from = update.getInt(3);
                                        is_chat = (from >= 2000000000);
                                        from = from % 2000000000;
                                    }
                                    Log.d(TAG, "Установка флагов сообщения (FLAGS|=$mask)");
                                    Intent broadcast = new Intent(ACTION_FLAGS_SET);
                                    broadcast.putExtra("message_id", message_id);
                                    broadcast.putExtra("mask", mask);
                                    if (update.length() == 4) {
                                        broadcast.putExtra("is_chat", is_chat);
                                        broadcast.putExtra("from", from);
                                    }
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case FLAGS_RESET: {
                                    long message_id = update.getInt(1);
                                    int mask = update.getInt(2);
                                    long from = 0;
                                    boolean is_chat = false;
                                    if (update.length() == 4) {
                                        from = update.getInt(3);
                                        is_chat = (from >= 2000000000);
                                        from = from % 2000000000;
                                    }
                                    Log.d(TAG, "Сброс флагов сообщения (FLAGS&=~$mask)");
                                    Intent broadcast = new Intent(ACTION_FLAGS_RESET);
                                    broadcast.putExtra("message_id", message_id);
                                    broadcast.putExtra("mask", mask);
                                    if (update.length() == 4) {
                                        broadcast.putExtra("is_chat", is_chat);
                                        broadcast.putExtra("from", from);
                                    }
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case MESSAGE_NEW: {
                                    long message_id = update.getLong(1);
                                    int flags = update.getInt(2) % 8192;
                                    long from_id = update.getLong(3);
                                    long timestamp = update.getLong(4);
                                    String subject = update.getString(5);
                                    String text = update.getString(6);
                                    String attachments = update.getString(7);
                                    Log.d(TAG, "Добавление нового сообщения from_id=" + from_id + " subj=" + subject + " text=" + text);
                                    Intent broadcast = new Intent(ACTION_MESSAGE_NEW);
                                    broadcast.putExtra("message_id", message_id);
                                    broadcast.putExtra("flags", flags);
                                    broadcast.putExtra("from_id", from_id);
                                    broadcast.putExtra("timestamp", timestamp);
                                    broadcast.putExtra("subject", subject);
                                    broadcast.putExtra("text", text);
                                    broadcast.putExtra("attachments", attachments);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case READ_INCOME: {
                                    long message_id = update.getInt(2);
                                    long from = update.getInt(1);
                                    boolean is_chat = (from >= 2000000000);
                                    from = from % 2000000000;
                                    Log.d(TAG, "Прочтение всех входящих сообщений с " + from + " вплоть до " + message_id + " включительно");
                                    Intent broadcast = new Intent(ACTION_READ_INCOME);
                                    broadcast.putExtra("message_id", message_id);
                                    broadcast.putExtra("is_chat", is_chat);
                                    broadcast.putExtra("from", from);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case READ_OUTCOME: {
                                    long message_id = update.getInt(2);
                                    long from = update.getInt(1);
                                    boolean is_chat = (from >= 2000000000);
                                    from = from % 2000000000;
                                    Log.d(TAG, "Прочтение всех исходящих сообщений с " + from + " вплоть до " + message_id + " включительно");
                                    Intent broadcast = new Intent(ACTION_READ_OUTCOME);
                                    broadcast.putExtra("message_id", message_id);
                                    broadcast.putExtra("is_chat", is_chat);
                                    broadcast.putExtra("from", from);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case USER_ONLINE: {
                                    long user_id = update.getLong(1);
                                    Log.d(TAG, "Друг " + user_id + " стал онлайн");
                                    Intent broadcast = new Intent(ACTION_USER_ONLINE);
                                    broadcast.putExtra("user_id", -user_id);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case USER_OFFLINE: {
                                    long user_id = update.getLong(1);
                                    Log.d(TAG, "Друг " + user_id + " стал оффлайн");
                                    Intent broadcast = new Intent(ACTION_USER_OFFLINE);
                                    broadcast.putExtra("user_id", -user_id);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case CHAT_UPDATE: {
                                    long chat_id = update.getLong(1);
                                    Log.d(TAG, "Один из параметров (состав, тема) беседы " + chat_id + " были изменены");
                                    Intent broadcast = new Intent(ACTION_CHAT_UPDATE);
                                    broadcast.putExtra("chat_id", chat_id);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case USER_WRITING: {
                                    long user_id = update.getLong(1);
                                    Log.d(TAG, "Пользователь " + user_id + " начал набирать текст в диалоге");
                                    Intent broadcast = new Intent(ACTION_USER_WRITING);
                                    broadcast.putExtra("user_id", user_id);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case CHAT_WRITING: {
                                    long user_id = update.getLong(1);
                                    long chat_id = update.getLong(2);
                                    Log.d(TAG, "Пользователь " + user_id + " начал набирать текст в беседе " + chat_id);
                                    Intent broadcast = new Intent(ACTION_CHAT_WRITING);
                                    broadcast.putExtra("user_id", user_id);
                                    broadcast.putExtra("chat_id", chat_id);
                                    sendBroadcast(broadcast);
                                    break;
                                }
                                case CALL: {
                                    long user_id = update.getLong(1);
                                    long call_id = update.getLong(2);
                                    Log.d(TAG, "Пользователь " + user_id + " совершил звонок имеющий идентификатор " + call_id);
                                    Intent broadcast = new Intent(ACTION_CALL);

                                    sendBroadcast(broadcast);

                                    //кто вообще этим пользуется?
                                    break;
                                }
                                case COUNTER: {
                                    int count = update.getInt(1);
                                    Intent broadcast = new Intent(ACTION_COUNTER);
                                    broadcast.putExtra("count", count);
                                    sendBroadcast(broadcast);
                                    Log.d(TAG, "Новый счетчик непрочитанных в левом меню стал равен " + update.getInt(1));
                                    Intent intent = new Intent(AbstractActivity.DRAWER_UPDATE_COUNTERS);
                                    intent.putExtra("MESSAGES_COUNT", update.getInt(1));
                                    sendBroadcast(intent);
                                    break;
                                }
                                case NOTIFICATIONS: {
                                    break;
                                }
                                default:
                                    Log.e(TAG, "Код не обработан: " + type);
                                    Answers.getInstance().logCustom(new CustomEvent("[LongPool] Не обработан код").putCustomAttribute("code", type));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                check();
            }
        };


        try {
            client.get("https://" + server, params, responseHandler);
        } catch (StackOverflowError | NullPointerException e) {
            Log.e(TAG, "Exception, LOL");
            stopSelf();
            startService(new Intent(getApplicationContext(), this.getClass()));
        }
    }
}


