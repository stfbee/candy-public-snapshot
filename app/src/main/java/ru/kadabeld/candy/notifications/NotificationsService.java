package ru.kadabeld.candy.notifications;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.im.IMService;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

/**
 * User: Vlad
 * Date: 29.03.2015
 * Time: 1:53
 */
public class NotificationsService extends Service {
    private Picasso picasso;
    private static final String TAG = "NotificationsService";
    public static Resources res;
    private int counter;
    private ArrayList<Long> necro_list;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        res = this.getResources();
        necro_list = new ArrayList<>();
        picasso = Picasso.with(this);
        IntentFilter iFilter = new IntentFilter();
        for (String action : ALLOWED_ACTIONS) {
            iFilter.addAction(action);
        }
        this.registerReceiver(broadcastReceiver, iFilter);

        Log.d(TAG, "Сервис уведомлений создан");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Сервис уведомлений запущен");
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Сервис уведомлений уничтожен");
        this.unregisterReceiver(broadcastReceiver);
    }

    private static final String[] ALLOWED_ACTIONS = {
            IMService.ACTION_USER_ONLINE,
            IMService.ACTION_USER_OFFLINE,
            IMService.ACTION_MESSAGE_NEW,
            IMService.ACTION_COUNTER
    };

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case IMService.ACTION_USER_ONLINE: {
                    long user_id = intent.getLongExtra("user_id", 0);
                    if (user_id != 0) showToast(user_id, true);
                    break;
                }
                case IMService.ACTION_USER_OFFLINE: {
                    long user_id = intent.getLongExtra("user_id", 0);
                    if (user_id != 0) showToast(user_id, false);
                    break;
                }
                case IMService.ACTION_MESSAGE_NEW: {
                    int flags = intent.getIntExtra("flags", 0);
                    long from_id = intent.getLongExtra("from_id", 0);
                    long timestamp = intent.getLongExtra("timestamp", 0);
                    long message_id = intent.getLongExtra("message_id", 0);
                    String subject = intent.getStringExtra("subject");
                    String text = StringEscapeUtils.unescapeHtml(intent.getStringExtra("text"));
                    try {
                        JSONObject attachments = (new JSONObject(intent.getStringExtra("attachments")));
                        from_id = attachments.getLong("from");
                    } catch (JSONException ignored) {
                    }
                    boolean UNREAD = (flags & 1) != 0;
                    boolean OUTBOX = (flags & 2) != 0;
                    boolean CHAT = !subject.equals(" ... ");
                    if (UNREAD && !OUTBOX) {
                        showNotification(from_id, timestamp, message_id, text, CHAT, intent.getLongExtra("from_id", 0), subject);
                    }
                    break;
                }
                case IMService.ACTION_COUNTER: {
                    counter = intent.getIntExtra("count", 1);
                    break;
                }
            }
        }
    };

    private void showNotification(long from_id, long timestamp, long message_id, String text, boolean isChat, long chat_id, String subject) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(this);

        /**
         * system_sound - использовать системный звук уведомления вместо звука из R.raw
         * chat_sound_enabled - озвучивать ли новые сообщения в беседе
         * chat_vibrate_enabled - вибрация в беседе
         * chat_muted_for - на сколько уведомления отключены в мс: неделя, сутки, час, 15 минут, если 0, то можно показывать
         * chat_muted_since - с какого момента вести отчет в мс
         * chat_color - цвет индикатора в беседе
         * now - текущее время
         */

        boolean system_sound = sPref.getBoolean("system_sound", false);
        boolean chat_sound_enabled = sPref.getBoolean("chat_" + chat_id + "_sound", true);
        boolean chat_vibrate_enabled = sPref.getBoolean("chat_" + chat_id + "_vibrate", true);
        boolean chat_notify_disabled = sPref.getBoolean("chat_" + chat_id + "_notify_disabled", false);
        long chat_muted_for = Long.parseLong(sPref.getString("chat_" + chat_id + "_muted_for", "0"));
        long chat_muted_since = sPref.getLong("chat_" + chat_id + "_muted_since", 0);
        int chat_color = sPref.getInt("chat_" + chat_id + "_color", getResources().getColor(R.color.colorPrimary));
        long now = (new Date()).getTime();

        if (!chat_notify_disabled && now - (chat_muted_since + chat_muted_for) > 0) {
            User.load(from_id, user -> NewMessageNotification.notify(NotificationsService.this, (User) user, text, counter, timestamp, message_id, isChat, chat_id, subject, system_sound, chat_sound_enabled, chat_vibrate_enabled, chat_color));
        }
    }

    private void showToast(final long user_id, final boolean online) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (sPref.getBoolean("show_toasts", true)) {
            boolean use_whitelist = sPref.getBoolean("use_whitelist", false);
            if (use_whitelist) {
                String lists = sPref.getString("whitelist_ids", "");
                for (String s1 : lists.split(",")) {
                    if (!s1.trim().isEmpty()) {
                        necro_list.add(Long.valueOf(s1.trim()));
                    }
                }
            }

            if (use_whitelist && !necro_list.contains(user_id)) {
                return;
            }
            User.load(user_id, user -> {
                Toast toast = new Toast(NotificationsService.this);
                toast.setDuration(Toast.LENGTH_SHORT);
                LayoutInflater inflater = LayoutInflater.from(NotificationsService.this);
                View layout = inflater.inflate(R.layout.toast_layout, null);

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                TextView name = (TextView) layout.findViewById(R.id.name);
                TextView status = (TextView) layout.findViewById(R.id.status);

                image.setImageDrawable(user.getAvatarDrawable());
                if (!user.isAvatarDefault()) {
                    if (user.getPhotoFor(200).isEmpty()) {
                        Log.e(TAG, user.toString());
                    }
                    picasso.load(user.getPhotoFor(200)).tag("toast").transform(new CropCircleTransformation()).into(image);
                }
                name.setText(user.getFullName());
                status.setText(res.getString(online ? R.string.online : R.string.offline));

                toast.setView(layout);
                toast.show();
            });
        }
    }
}
