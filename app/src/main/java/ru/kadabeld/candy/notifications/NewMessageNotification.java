package ru.kadabeld.candy.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import ru.kadabeld.candy.MainActivity;
import ru.kadabeld.candy.QuickReplyActivity;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.fragments.ChatFragment;
import ru.kadabeld.candy.utils.ImageUtils;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

public class NewMessageNotification {
    public static final String NOTIFICATION_TAG = "NewMessage";

    static void notify(final Context context, final User profile, String message, final int count, long timestamp, long message_id, boolean isChat, long chat_id, String chat_title, boolean system_sound, boolean chat_sound_enabled, boolean chat_vibrate_enabled, int chat_color) {
        Bundle b = new Bundle();
        b.putBoolean("is_chat", isChat);
        if (isChat) {
            b.putLong("chat_id", chat_id % 2000000000);
            b.putString("chat_title", chat_title);
        } else {
            b.putLong("user_id", profile.getId());
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : profile.getFullName().toUpperCase().split(" ")) {
            stringBuilder.append(s.charAt(0));
        }
        stringBuilder.append(": ").append(emhtml(message));

        final String ticker = stringBuilder.toString();

        Bundle bundle = new Bundle();
        bundle.putString("name", profile.getFullName());
        bundle.putString("message", message);
        bundle.putBoolean("is_chat", isChat);
        bundle.putLong("chat_id", chat_id);
        bundle.putLong("user_id", profile.getId());
        bundle.putString("avatar", profile.getPhotoFor(200));
        bundle.putString("chat_title", chat_title);
        bundle.putLong("message_id", message_id);
        bundle.putLong("timestamp", timestamp);
        Intent ActivityIntent = new Intent(context, QuickReplyActivity.class).setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME).putExtras(bundle);

        Uri sound_uri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.bb2);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_mail_white_36dp)
                .setContentTitle(profile.getFullName())
                .setContentText(emhtml(message))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setTicker(ticker)
                .setNumber(count)
                .setAutoCancel(true)
                .setLights(chat_color, 3000, 3000)
                .setColor(context.getResources().getColor(R.color.colorPrimary))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(emhtml(message))
                        .setBigContentTitle(profile.getFullName())
                        .setSummaryText(context.getString(R.string.nt_new_messages)))
                .setContentIntent(
                        PendingIntent.getActivity(
                                context,
                                0,
                                new Intent(context, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                        .putExtra("fragment", ChatFragment.class.getCanonicalName())
                                        .putExtra("extra", b),
                                PendingIntent.FLAG_UPDATE_CURRENT))
                .addAction(
                        R.drawable.ic_reply_grey600_36dp,
                        context.getString(R.string.nt_quick_reply),
                        PendingIntent.getActivity(
                                context,
                                0,
                                ActivityIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT));

        if (chat_sound_enabled) {
            if (!system_sound) {
                builder.setSound(sound_uri);
            } else {
                builder.setDefaults(Notification.DEFAULT_SOUND);
            }
        } else {
            builder.setSound(null);
        }

        if (chat_vibrate_enabled) {
            builder.setVibrate(new long[]{0, 150, 50, 150});
        } else {
            builder.setVibrate(new long[0]);
        }

        Picasso.with(context).load(profile.getPhotoFor(200)).placeholder(profile.getAvatarDrawable()).transform(new CropCircleTransformation()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                builder.setLargeIcon(bitmap);
                Notification notification = builder.build();
                NewMessageNotification.notify(context, notification);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                builder.setLargeIcon(ImageUtils.drawableToBitmap(profile.getAvatarDrawable()));
                Notification notification = builder.build();
                NewMessageNotification.notify(context, notification);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(NOTIFICATION_TAG, 0, notification);

    }

    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(NOTIFICATION_TAG, 0);
    }

    private static CharSequence emhtml(String s) {
        return android.text.Html.fromHtml(s).toString().trim();
    }
}