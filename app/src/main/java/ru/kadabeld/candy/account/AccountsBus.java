package ru.kadabeld.candy.account;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.f2prateek.rx.preferences.Preference;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.google.common.collect.Collections2;

import java.util.Collection;
import java.util.List;

import rx.Observable;

/**
 * Created by arktic on 08.05.16.
 */
public class AccountsBus {

    private static final String CURRENT_USER = "current_user";

    private final AccountsStorage mAccountsStorage;
    private final Preference<Long> mCurrentUser;

    public AccountsBus(Context context) {
        mAccountsStorage = AccountsStorage.with(context);
        mCurrentUser = RxSharedPreferences.create(context.getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE))
                .getLong(CURRENT_USER);
    }

    public Observable<List<AccountInfo>> accounts() {
        return mAccountsStorage.accounts();
    }

    @Deprecated
    public Observable<AccountInfo> current() {
        return mCurrentUser.asObservable()
                .flatMap(id -> id == null ? Observable.just(null) : mAccountsStorage.accounts().map(list -> {
                    Collection<AccountInfo> collection = Collections2.filter(list, item -> id == item.getUserId());
                    return !collection.isEmpty() ? collection.iterator().next() : null;
                })).distinctUntilChanged();
    }

    public void update(@NonNull AccountInfo info) {
        mAccountsStorage.put(info);
    }

    public void remove(@NonNull AccountInfo info) {
        mAccountsStorage.remove(info);
    }

    @Deprecated
    public void setCurrent(@Nullable AccountInfo info) {
        if (info != null) {
            update(info);
        } else {
            Long currentId = mCurrentUser.get();
            if (currentId != null) {
                mAccountsStorage.remove(currentId);
            }
        }
        mCurrentUser.set(info != null ? info.getUserId() : null);
    }
}
