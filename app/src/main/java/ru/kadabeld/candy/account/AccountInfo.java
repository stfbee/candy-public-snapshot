package ru.kadabeld.candy.account;

import java.io.Serializable;

/**
 * Created by arktic on 08.05.16.
 */
public class AccountInfo implements Serializable {

    private String accessToken;
    private long userId;
    private String firstName;
    private String lastName;
    private String photo200;

    private boolean donate = true;

    public AccountInfo() {
    }

    public AccountInfo(String accessToken, long userId, String firstName, String lastName, String photo200) {
        this.accessToken = accessToken;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo200 = photo200;
    }

    public String getFullName() {
        return String.format("%s %s", firstName, lastName);
    }

    public boolean isAvatarDefault() {
        return photo200 == null || (photo200.contains("camera_") || photo200.contains("contact_info") || photo200.contains(".gif")) && !photo200.contains("deactivated");
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoto200() {
        return photo200;
    }

    public void setPhoto200(String photo200) {
        this.photo200 = photo200;
    }

    public boolean isDonate() {
        return donate;
    }

    public void setDonate(boolean donate) {
        this.donate = donate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountInfo)) return false;

        AccountInfo that = (AccountInfo) o;

        if (userId != that.userId) return false;
        if (donate != that.donate) return false;
        if (accessToken != null ? !accessToken.equals(that.accessToken) : that.accessToken != null)
            return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null)
            return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null)
            return false;
        return photo200 != null ? photo200.equals(that.photo200) : that.photo200 == null;

    }

    @Override
    public int hashCode() {
        int result = accessToken != null ? accessToken.hashCode() : 0;
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (photo200 != null ? photo200.hashCode() : 0);
        result = 31 * result + (donate ? 1 : 0);
        return result;
    }
}
