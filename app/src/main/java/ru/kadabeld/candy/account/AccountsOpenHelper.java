package ru.kadabeld.candy.account;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by arktic on 08.05.16.
 */
//package local!!! DO NOT USE
class AccountsOpenHelper extends SQLiteOpenHelper {

    private static final String ACCOUNTS_DB = "accounts.db";
    private static final int VERSION = 2;

    static {
        cupboard().register(AccountInfo.class);
    }

    public AccountsOpenHelper(Context context) {
        super(context, ACCOUNTS_DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
    }
}
