package ru.kadabeld.candy.account;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import nl.qbusict.cupboard.DatabaseCompartment;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by arktic on 08.05.16.
 */
class AccountsStorage {

    private static AccountsStorage INSTANCE;

    public static AccountsStorage with(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new AccountsStorage(context);
        }
        return INSTANCE;
    }

    @NonNull
    private final AccountsOpenHelper mAccountsOpenHelper;

    private final BehaviorSubject<List<AccountInfo>> mAccounts;

    private AccountsStorage(@NonNull Context context) {
        mAccountsOpenHelper = new AccountsOpenHelper(context);
        mAccounts = BehaviorSubject.create(
                cupboard().withDatabase(mAccountsOpenHelper.getReadableDatabase())
                        .query(AccountInfo.class).list()
        );
    }

    public Observable<List<AccountInfo>> accounts() {
        return mAccounts.subscribeOn(Schedulers.io()).distinctUntilChanged();
    }

    public void put(@NonNull AccountInfo accountInfo) {
        DatabaseCompartment cup = cupboard().withDatabase(mAccountsOpenHelper.getWritableDatabase());
        cup.delete(AccountInfo.class, "userId = ?", String.valueOf(accountInfo.getUserId()));
        cup.put(accountInfo);
        mAccounts.onNext(cup.query(AccountInfo.class).list());
    }

    public void remove(@NonNull AccountInfo info) {
        remove(info.getUserId());
    }

    public void remove(@NonNull Long id) {
        DatabaseCompartment cup = cupboard().withDatabase(mAccountsOpenHelper.getWritableDatabase());
        cup.delete(AccountInfo.class, "userId = ?", String.valueOf(id));
        mAccounts.onNext(cup.query(AccountInfo.class).list());
    }
}
