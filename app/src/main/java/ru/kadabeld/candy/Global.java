package ru.kadabeld.candy;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.Calendar;
import java.util.Random;

@SuppressWarnings({"unchecked", "SpellCheckingInspection"})
public class Global {
    static final String[][] nowHeaders = {{
            "ftsjpDr",
            "iaF2GsW",
            "SG0grgt",
            "NvuCPnR"
    }, {
            "VfsJmJG",
            "R5xDcsU",
            "iYgGarG",
            "GrUycsf"
    }, {
            "KRpg95V",
            "6k6F2lW",
            "ns7a8n8",
            "FLpumTc"
    }, {
            "pwAi38H",
            "D7MpPzn",
            "IRxHnwV",
            "x13xlLT"
    }, {
            "p5smgKb",
            "0pv0NNb",
            "INHRnzI",
            "2H2cF2u"
    }, {
            "Zc9jT9o",
            "w0JRn7f",
            "DluzGt7",
            "v2fzFMQ"
    }, {
            "VbUj30p",
            "cN6W8pM",
            "AIinDXM",
            "KidM749"
    }, {
            "AaEeq4q",
            "5PDecWl",
            "eaF13Di",
            "jY0A3oA"
    }, {
            "BAhoz39",
            "hOBT4fn",
            "dv1dEhc",
            "Qrz9CV3"
    }, {
            "DMuvOsq",
            "IGfvvOQ",
            "dK3xC0F",
            "I8RXYkD"
    }, {
            "kUA9Vb5",
            "zaB2Md9",
            "SS0YSXi",
            "N4hhi3W"
    }};

    public static boolean isSmall = false;
    public static float displayDensity;
    public static boolean isTablet = false;

    public static int scale(float scale) {
        return Math.round(displayDensity * scale);
    }

    public static int getStatusBarHeight(Context ctx) {
        int result = 0;
        int resourceId = ctx.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = ctx.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static float convertDpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    public static void RunOnUIThread(Runnable runnable) {
        Handler UIHandler = new Handler(Looper.getMainLooper());
        UIHandler.post(runnable);
    }

    public static int dp(int value) {
        return (int) (Math.max(1, displayDensity)) * value;
    }

    public static int dpf(float value) {
        return (int) Math.ceil(displayDensity * value);
    }

    public static int[] getWindowSize(Activity ctx) {
        int sizeOf[] = new int[2];
        Window window = ctx.getWindow();
        sizeOf[0] = window.getDecorView().getWidth();
        sizeOf[1] = window.getDecorView().getHeight();
        return sizeOf;
    }

    public static int[] getScreenSize(Activity ctx) {
        int sizeOf[] = new int[2];
        Display display = ctx.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        sizeOf[0] = size.x;
        sizeOf[1] = size.y;
        return sizeOf;
    }

    public static Pair<Integer, Integer> getResizedDimensions(int width, int height, int i, int layout_height, boolean b) {
        int w;
        int h;
        if (b) {
            h = (int) ((float) i / (float) width * (float) height);
            w = i;
        } else {
            w = (int) ((float) layout_height / (float) height * (float) width);
            h = layout_height;
        }

        return new Pair(w, h);
    }

    public static String getMinutes(long time) {
        //int hours = time / 3600;
        long minutes = (time % 3600) / 60;
        long seconds = time % 60;

        return String.format("%02d:%02d", minutes, seconds);
    }

    public static Uri resIdToUri(Context context, int resId) {
        return Uri.parse("android.resource://" + context.getPackageName()
                + "/" + resId);
    }

    public static void gif(Context ctx, String url) {
        try {
            final Dialog dialog = new Dialog(ctx);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00996633")));
            LinearLayout gifLayout = new LinearLayout(ctx);
            gifLayout.setOrientation(LinearLayout.VERTICAL);
            Button button = new Button(ctx);
            button.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            button.setAlpha(0.4f);
            button.setText(R.string.close);
            WebView webView = new WebView(ctx);
            webView.setWebViewClient(new GifWebView(null));
            webView.loadUrl(url);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            gifLayout.addView(webView);
            gifLayout.addView(button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.setContentView(gifLayout);
            dialog.show();

        } catch (Exception ignored) {
        }
    }

    public static String getHeader(Activity ctx) {
        Calendar c = Calendar.getInstance();
        Random random = new Random();
        int image = random.nextInt(nowHeaders.length);
        int time = 0;

        int hour = c.get(Calendar.HOUR_OF_DAY);
        if (hour < 4 || hour >= 22) time = 3;
        else if (hour < 10 && hour >= 4) time = 0;
        else if (hour < 16 && hour >= 10) time = 1;
        else if (hour < 22 && hour >= 16) time = 2;

        return "http://i.imgur.com/" + nowHeaders[image][time] + ".png";
    }

    public static int getNavigationBarHeight(Context var0) {
        Resources var1 = var0.getResources();
        int var2 = var1.getIdentifier("navigation_bar_height", "dimen", "android");
        return var2 > 0 ? var1.getDimensionPixelOffset(var2) : 0;
    }

    private static class GifWebView extends WebViewClient {

        private GifWebView() {
        }

        GifWebView(Object object) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView var1, String var2) {
            return false;
        }
    }
}