package ru.kadabeld.candy.entities.meta;

import android.util.LongSparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * User: Vlad
 * Date: 15.04.2016
 * Time: 1:16
 */
public class PurchasedStickers {
    private LongSparseArray<StickerPack> stickerPacks = new LongSparseArray<>();
    private ArrayList<Long> stickerPacksIds = new ArrayList<>();

    public static PurchasedStickers parse(JSONArray array) {
        final PurchasedStickers stickers = new PurchasedStickers();

        try {
            for (int i = 0; i < array.length(); i++) {
                StickerPack stickerPack = new StickerPack();
                JSONObject spObject = array.getJSONObject(i);
                if (spObject.has("id")) stickerPack.setId(spObject.getLong("id"));
                if (spObject.has("purchase_date"))
                    stickerPack.setPurchase_date(spObject.getLong("purchase_date"));
                if (spObject.has("title")) stickerPack.setTitle(spObject.getString("title"));
                if (spObject.has("stickers")) {
                    JSONObject stickers1 = spObject.getJSONObject("stickers");
                    if (stickers1.has("sticker_ids")) {
                        JSONArray sticker_idsObjects = stickers1.getJSONArray("sticker_ids");
                        int length = sticker_idsObjects.length();
                        long[] sticker_ids = new long[length];
                        for (int i1 = 0; i1 < length; i1++) {
                            sticker_ids[i1] = sticker_idsObjects.getLong(i1);
                        }
                        stickerPack.setSticker_ids(sticker_ids);
                    }
                }
                stickers.addStickerPack(stickerPack);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return stickers;
    }

    private void addStickerPack(StickerPack stickerPack) {
        long id = stickerPack.getId();
        stickerPacksIds.add(id);
        stickerPacks.put(id, stickerPack);
    }

    public LongSparseArray<StickerPack> getStickerPacks() {
        return stickerPacks;
    }

    public ArrayList<Long> getStickerPacksIds() {
        return stickerPacksIds;
    }

    public void setStickerPacksIds(ArrayList<Long> stickerPacksIds) {
        this.stickerPacksIds = stickerPacksIds;
    }

    public StickerPack getStickerPack(long id) {
        return stickerPacks.get(id);
    }

    public static class StickerPack {
        private long id;
        private String title = "";
        private long purchase_date;
        private long[] sticker_ids;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public long getPurchase_date() {
            return purchase_date;
        }

        public void setPurchase_date(long purchase_date) {
            this.purchase_date = purchase_date;
        }

        public long[] getSticker_ids() {
            return sticker_ids;
        }

        public void setSticker_ids(long[] sticker_ids) {
            this.sticker_ids = sticker_ids;
        }
    }
}
