package ru.kadabeld.candy.entities;

import org.json.JSONException;
import org.json.JSONObject;

import ru.kadabeld.candy.entities.meta.Product;
import ru.kadabeld.candy.entities.meta.Rating;

/**
 * User: Vlad Onishchenko
 * Date: 31.08.2015
 * Time: 15:58
 */
public class Link implements AbstractAttachment {
    private String url = "";
    private String title = "";
    private String description = "";
    private Photo photo;
    private String preview_page = "";
    private String preview_url = "";
    private String caption = "";
    private boolean is_external;
    private Button button;
    private Product product;
    private Rating rating;

    public static Link parseLink(JSONObject linkObject) {
        Link link = new Link();
        try {
            if (linkObject.has("url")) link.setUrl(linkObject.getString("url"));
            if (linkObject.has("title")) link.setTitle(linkObject.getString("title"));
            if (linkObject.has("caption")) link.setCaption(linkObject.getString("caption"));
            if (linkObject.has("button"))
                link.setButton(Button.parseButton(linkObject.getJSONObject("button")));
            if (linkObject.has("description"))
                link.setDescription(linkObject.getString("description"));
            if (linkObject.has("preview_url"))
                link.setPreview_url(linkObject.getString("preview_url"));
            if (linkObject.has("preview_page"))
                link.setPreview_page(linkObject.getString("preview_page"));
            if (linkObject.has("photo"))
                link.setPhoto(Photo.parsePhoto(linkObject.getJSONObject("photo")));
            if (linkObject.has("product"))
                link.setProduct(Product.parseProduct(linkObject.getJSONObject("product")));
            if (linkObject.has("rating"))
                link.setRating(Rating.parseRating(linkObject.getJSONObject("rating")));
            if (linkObject.has("is_external"))
                link.setIs_external(linkObject.getInt("is_external") == 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return link;
    }

    public String getUrl() {
        return url;
    }

    public Link setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Link setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Link setDescription(String description) {
        this.description = description;
        return this;
    }

    public Photo getPhoto() {
        return photo;
    }

    public Link setPhoto(Photo photo) {
        this.photo = photo;
        return this;
    }

    public String getPreview_page() {
        return preview_page;
    }

    public Link setPreview_page(String preview_page) {
        this.preview_page = preview_page;
        return this;
    }

    public String getPreview_url() {
        return preview_url;
    }

    public Link setPreview_url(String preview_url) {
        this.preview_url = preview_url;
        return this;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public boolean is_external() {
        return is_external;
    }

    public void setIs_external(boolean is_external) {
        this.is_external = is_external;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    @Override
    public long getDate() {
        return 0;
    }

    public static class Button {
        private String title = "";
        private String url = "";

        public static Button parseButton(JSONObject object) {
            Button button = new Button();
            try {
                if (object.has("url")) button.setUrl(object.getString("url"));
                if (object.has("title")) button.setTitle(object.getString("title"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return button;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
