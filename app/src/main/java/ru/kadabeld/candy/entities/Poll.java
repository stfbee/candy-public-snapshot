package ru.kadabeld.candy.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * User: Vlad Onishchenko
 * Date: 31.08.2015
 * Time: 16:11
 */
public class Poll {

    private long id;
    private long owner_id;
    private long from_id;
    private long created;
    private String question = "";
    private long votes;
    private int answer_id;
    private boolean anonymous;
    private ArrayList<Answer> answers = new ArrayList<>();

    public static Poll parsePoll(JSONObject object) {
        Poll poll = new Poll();
        try {
            if (object.has("id")) poll.setId(object.getLong("id"));
            if (object.has("owner_id")) poll.setOwner_id(object.getLong("owner_id"));
            if (object.has("from_id")) poll.setFrom_id(object.getLong("from_id"));
            if (object.has("created")) poll.setCreated(object.getLong("created"));
            if (object.has("question")) poll.setQuestion(object.getString("question"));
            if (object.has("votes")) poll.setVotes(object.getLong("votes"));
            if (object.has("answer_id")) poll.setAnswer_id(object.getInt("answer_id"));
            if (object.has("anonymous")) poll.setAnonymous(object.getInt("anonymous") == 1);
            if (object.has("answers")) {
                JSONArray answersObjects = object.getJSONArray("answers");
                for (int i = 0; i < answersObjects.length(); i++) {
                    poll.addAnswer(Answer.parseAnswer(answersObjects.getJSONObject(i)));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return poll;
    }

    public long getId() {
        return id;
    }

    public Poll setId(long id) {
        this.id = id;
        return this;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public Poll setOwner_id(long owner_id) {
        this.owner_id = owner_id;
        return this;
    }

    public long getCreated() {
        return created;
    }

    public Poll setCreated(long created) {
        this.created = created;
        return this;
    }

    public String getQuestion() {
        return question;
    }

    public Poll setQuestion(String question) {
        this.question = question;
        return this;
    }

    public long getVotes() {
        return votes;
    }

    public Poll setVotes(long votes) {
        this.votes = votes;
        return this;
    }

    public int getAnswer_id() {
        return answer_id;
    }

    public Poll setAnswer_id(int answer_id) {
        this.answer_id = answer_id;
        return this;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public Poll setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
        return this;
    }

    public Poll addAnswers(ArrayList<Answer> answers) {
        this.answers.addAll(answers);
        return this;
    }

    public Poll addAnswer(Answer answer) {
        this.answers.add(answer);
        return this;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public long getFrom_id() {
        return from_id;
    }

    public void setFrom_id(long from_id) {
        this.from_id = from_id;
    }

    public static class Answer {
        private long id;
        private String text = "";
        private long votes;
        private double rate;

        public static Answer parseAnswer(JSONObject object) {
            Answer answer = new Answer();
            try {
                if (object.has("id")) answer.setId(object.getLong("id"));
                if (object.has("text")) answer.setText(object.getString("text"));
                if (object.has("votes")) answer.setVotes(object.getLong("votes"));
                if (object.has("rate")) answer.setRate(object.getDouble("rate"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return answer;
        }

        public long getId() {
            return id;
        }

        public Answer setId(long id) {
            this.id = id;
            return this;
        }

        public String getText() {
            return text;
        }

        public Answer setText(String text) {
            this.text = text;
            return this;
        }

        public long getVotes() {
            return votes;
        }

        public Answer setVotes(long votes) {
            this.votes = votes;
            return this;
        }

        public double getRate() {
            return rate;
        }

        public Answer setRate(double rate) {
            this.rate = rate;
            return this;
        }
    }

}
