package ru.kadabeld.candy.entities;

import org.json.JSONException;
import org.json.JSONObject;

import ru.kadabeld.candy.Cache;

public class Photo implements AbstractAttachment {
    private String text = "";
    private String access_key = "";
    private String photo_75 = "";
    private String photo_130 = "";
    private String photo_604 = "";
    private String photo_807 = "";
    private String photo_1280 = "";
    private String photo_2560 = "";
    private int width;
    private int height;
    private int position;
    private long id;
    private long album_id;
    private long owner_id;
    private long user_id;
    private long date;

    private long post_id;
    private double lat;
    private double lon;
    private boolean can_comment;
    private long count_comments;
    private long count_tags;
    private Likes likes = new Likes();

    public static Photo parsePhoto(JSONObject photoObject) {
        Photo photo = null;

        try {
            long id = 0;
            long owner_id = 0;
            if (photoObject.has("id")) id = photoObject.getLong("id");
            if (photoObject.has("owner_id")) owner_id = photoObject.getLong("owner_id");
            photo = Cache.getPhoto(owner_id, id);
            if (photo == null) photo = new Photo();

            if (photoObject.has("width")) photo.setWidth(photoObject.getInt("width"));
            if (photoObject.has("height")) photo.setHeight(photoObject.getInt("height"));
            if (photoObject.has("text")) photo.setText(photoObject.getString("text"));
            if (photoObject.has("access_key"))
                photo.setAccess_key(photoObject.getString("access_key"));
            if (photoObject.has("photo_75")) photo.setPhoto_75(photoObject.getString("photo_75"));
            if (photoObject.has("photo_130"))
                photo.setPhoto_130(photoObject.getString("photo_130"));
            if (photoObject.has("photo_604"))
                photo.setPhoto_604(photoObject.getString("photo_604"));
            if (photoObject.has("photo_807"))
                photo.setPhoto_807(photoObject.getString("photo_807"));
            if (photoObject.has("photo_1280"))
                photo.setPhoto_1280(photoObject.getString("photo_1280"));
            if (photoObject.has("photo_2560"))
                photo.setPhoto_2560(photoObject.getString("photo_2560"));
            if (photoObject.has("id")) photo.setId(photoObject.getLong("id"));
            if (photoObject.has("album_id")) photo.setAlbum_id(photoObject.getLong("album_id"));
            if (photoObject.has("owner_id")) photo.setOwner_id(photoObject.getLong("owner_id"));
            if (photoObject.has("user_id")) photo.setUser_id(photoObject.getLong("user_id"));
            if (photoObject.has("date")) photo.setDate(photoObject.getLong("date"));
            if (photoObject.has("access_key"))
                photo.setAccess_key(photoObject.getString("access_key"));
            if (photoObject.has("lat")) photo.setLat(photoObject.getDouble("lat"));
            if (photoObject.has("long")) photo.setLon(photoObject.getDouble("long"));
            if (photoObject.has("post_id")) photo.setPost_id(photoObject.getLong("post_id"));
            if (photoObject.has("can_comment"))
                photo.setCan_comment(photoObject.getInt("can_comment") == 1);


            if (photoObject.has("likes")) {
                photo.setLikes(Likes.parseLikes(photoObject.getJSONObject("likes")));
            }

            if (photoObject.has("comments")) {
                JSONObject comments = photoObject.getJSONObject("comments");
                if (comments.has("count")) photo.setCount_comments(comments.getLong("count"));
            }

            if (photoObject.has("tags")) {
                JSONObject tags = photoObject.getJSONObject("tags");
                if (tags.has("count")) photo.setCount_tags(tags.getLong("count"));
            }
//			photo.setUser(Cache.getAbstractProfile(photoObject.getLong("owner_id")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Cache.putPhoto(photo);
        return photo;
    }

    public String getPhoto_75() {
        return photo_75;
    }

    public void setPhoto_75(String photo_75) {
        this.photo_75 = photo_75;
    }

    public String getPhoto_130() {
        return photo_130;
    }

    public void setPhoto_130(String photo_130) {
        this.photo_130 = photo_130;
    }

    public String getPhoto_604() {
        return photo_604;
    }

    public void setPhoto_604(String photo_604) {
        this.photo_604 = photo_604;
    }

    public String getPhoto_807() {
        return photo_807;
    }

    public void setPhoto_807(String photo_807) {
        this.photo_807 = photo_807;
    }

    public String getPhoto_1280() {
        return photo_1280;
    }

    public void setPhoto_1280(String photo_1280) {
        this.photo_1280 = photo_1280;
    }

    public String getPhoto_2560() {
        return photo_2560;
    }

    public void setPhoto_2560(String photo_2560) {
        this.photo_2560 = photo_2560;
    }

    public long getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(long album_id) {
        this.album_id = album_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
//
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public boolean isCan_comment() {
        return can_comment;
    }

    public void setCan_comment(boolean can_comment) {
        this.can_comment = can_comment;
    }

    public long getCount_comments() {
        return count_comments;
    }

    public void setCount_comments(long count_comments) {
        this.count_comments = count_comments;
    }

    public long getCount_tags() {
        return count_tags;
    }

    public void setCount_tags(long count_tags) {
        this.count_tags = count_tags;
    }

    public long getPost_id() {
        return post_id;
    }

    public void setPost_id(long post_id) {
        this.post_id = post_id;
    }


    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public String getMaxImage() {
        if (!photo_2560.isEmpty()) return photo_2560;
        if (!photo_1280.isEmpty()) return photo_1280;
        if (!photo_807.isEmpty()) return photo_807;
        if (!photo_604.isEmpty()) return photo_604;
        if (!photo_130.isEmpty()) return photo_130;
        return photo_75;
    }

    public String getStringId() {
        return String.format("%s_%s", getOwner_id(), getId());
    }

    public static class Likes {
        private long count;
        private boolean user_likes;

        public static Likes parseLikes(JSONObject object) {
            Likes likes = new Likes();
            try {
                if (object.has("count")) likes.setCount(object.getLong("count"));
                if (object.has("user_likes")) likes.setUser_likes(object.getInt("user_likes") == 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return likes;
        }

        public void like() {
            user_likes = !user_likes;
            if (user_likes) {
                count++;
            } else {
                count--;
            }
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public boolean isUser_likes() {
            return user_likes;
        }

        public void setUser_likes(boolean user_likes) {
            this.user_likes = user_likes;
        }
    }
}