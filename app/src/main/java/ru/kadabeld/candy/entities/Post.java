package ru.kadabeld.candy.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.entities.meta.Geo;
import ru.kadabeld.candy.utils.parsers.Attachments;

/**
 * User: Vlad
 * Date: 06.09.2015
 * Time: 1:04
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
public class Post {
    private long id;
    private long owner_id;
    private long from_id;
    private long date;
    private String text;
    private long reply_owner_id;
    private long reply_post_id;
    private long signer_id;
    private boolean friends_only;
    private boolean can_pin;
    private boolean is_pinned;
    private boolean has_repost;
    private Attachments attachments = new Attachments();
    private Comments comments = new Comments();
    private Geo geo = new Geo();
    private Likes likes = new Likes();
    private Reposts reposts = new Reposts();
    private PostType post_type = PostType.POST;
    private PostSource post_source = new PostSource();
    private ArrayList<Post> copy_history = new ArrayList<>();

    public static Post parsePost(JSONObject object) {
        Post post = new Post();
        try {
            if (object.has("id")) post.setId(object.getLong("id"));
            if (object.has("post_id")) post.setId(object.getLong("post_id"));
            if (object.has("owner_id")) post.setOwner_id(object.getLong("owner_id"));
            if (object.has("source_id")) post.setOwner_id(object.getLong("source_id"));
            if (object.has("to_id")) post.setOwner_id(object.getLong("to_id"));
            if (object.has("from_id")) post.setFrom_id(object.getLong("from_id"));
            if (object.has("date")) post.setDate(object.getLong("date"));
            if (object.has("text")) post.setText(object.getString("text"));
            if (object.has("reply_owner_id"))
                post.setReply_owner_id(object.getLong("reply_owner_id"));
            if (object.has("reply_post_id")) post.setReply_post_id(object.getLong("reply_post_id"));
            if (object.has("signer_id")) post.setSigner_id(object.getLong("signer_id"));
            if (object.has("friends_only"))
                post.setFriends_only(object.getInt("friends_only") == 1);
            if (object.has("can_pin")) post.setCan_pin(object.getInt("can_pin") == 1);
            if (object.has("is_pinned")) post.setIs_pinned(object.getInt("is_pinned") == 1);

            if (object.has("attachments")) {
                post.setAttachments(Attachments.parseJson(object.getJSONArray("attachments")));
            }
            if (object.has("comments")) {
                post.setComments(Comments.parseComments(object.getJSONObject("comments")));
            }
            if (object.has("geo")) {
                post.setGeo(Geo.parseGeo(object.getJSONObject("geo")));
            }
            if (object.has("likes")) {
                post.setLikes(Likes.parseLikes(object.getJSONObject("likes")));
            }
            if (object.has("reposts")) {
                post.setReposts(Reposts.parseReposts(object.getJSONObject("reposts")));
            }
            if (object.has("post_source")) {
                post.setPost_source(PostSource.parsePostSource(object.getJSONObject("post_source")));
            }
            if (object.has("post_type")) {
                post.setPost_type(PostType.valueOf(object.getString("post_type").toUpperCase()));
            }

            if (object.has("copy_history")) {
                JSONArray historyObjects = object.getJSONArray("copy_history");
                for (int i = 0; i < historyObjects.length(); i++) {
                    post.addCopyHistory(Post.parsePost(historyObjects.getJSONObject(i)));
                }
                post.has_repost(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return post;
    }

    public void addCopyHistory(Post post) {
        copy_history.add(post);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public long getFrom_id() {
        return from_id;
    }

    public void setFrom_id(long from_id) {
        this.from_id = from_id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getReply_owner_id() {
        return reply_owner_id;
    }

    public void setReply_owner_id(long reply_owner_id) {
        this.reply_owner_id = reply_owner_id;
    }

    public long getReply_post_id() {
        return reply_post_id;
    }

    public void setReply_post_id(long reply_post_id) {
        this.reply_post_id = reply_post_id;
    }

    public boolean has_repost() {
        return has_repost;
    }

    public void has_repost(boolean has_repost) {
        this.has_repost = has_repost;
    }

    public long getSigner_id() {
        return signer_id;
    }

    public void setSigner_id(long signer_id) {
        this.signer_id = signer_id;
    }

    public boolean isFriends_only() {
        return friends_only;
    }

    public void setFriends_only(boolean friends_only) {
        this.friends_only = friends_only;
    }

    public boolean isCan_pin() {
        return can_pin;
    }

    public void setCan_pin(boolean can_pin) {
        this.can_pin = can_pin;
    }

    public boolean is_pinned() {
        return is_pinned;
    }

    public void setIs_pinned(boolean is_pinned) {
        this.is_pinned = is_pinned;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public Reposts getReposts() {
        return reposts;
    }

    public void setReposts(Reposts reposts) {
        this.reposts = reposts;
    }

    public PostType getPost_type() {
        return post_type;
    }

    public void setPost_type(PostType post_type) {
        this.post_type = post_type;
    }

    public PostSource getPost_source() {
        return post_source;
    }

    public void setPost_source(PostSource post_source) {
        this.post_source = post_source;
    }

    public Attachments getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }

    public Geo getGeo() {
        return geo;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    public ArrayList<Post> getCopy_history() {
        return copy_history;
    }

    public void setCopy_history(ArrayList<Post> copy_history) {
        this.copy_history = copy_history;
    }

    enum PostType {
        POST, COPY, REPLY, POSTPONE, SUGGEST, VIDEO, PHOTO
    }

    public static class Reposts {
        private long count;
        private boolean user_reposted;

        public static Reposts parseReposts(JSONObject object) {
            Reposts reposts = new Reposts();
            try {
                if (object.has("count")) reposts.setCount(object.getLong("count"));
                if (object.has("user_reposted"))
                    reposts.setUser_reposted(object.getInt("user_reposted") == 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return reposts;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public boolean isUser_reposted() {
            return user_reposted;
        }

        public void setUser_reposted(boolean user_reposted) {
            this.user_reposted = user_reposted;
        }
    }

    public static class Comments {
        private long count;
        private boolean can_post;

        public static Comments parseComments(JSONObject object) {
            Comments comments = new Comments();
            try {
                if (object.has("count")) comments.setCount(object.getLong("count"));
                if (object.has("can_post")) comments.setCan_post(object.getInt("can_post") == 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return comments;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public boolean isCan_post() {
            return can_post;
        }

        public void setCan_post(boolean can_post) {
            this.can_post = can_post;
        }
    }

    public static class Likes {
        private long count;
        private boolean user_likes;
        private boolean can_like;
        private boolean can_publish;

        public static Likes parseLikes(JSONObject object) {
            Likes likes = new Likes();
            try {
                if (object.has("count")) likes.setCount(object.getLong("count"));
                if (object.has("user_likes")) likes.setUser_likes(object.getInt("user_likes") == 1);
                if (object.has("can_like")) likes.setCan_like(object.getInt("can_like") == 1);
                if (object.has("can_publish"))
                    likes.setCan_publish(object.getInt("can_publish") == 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return likes;
        }

        public void like() {
            user_likes = !user_likes;
            if (user_likes) {
                count++;
            } else {
                count--;
            }
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public boolean isUser_likes() {
            return user_likes;
        }

        public void setUser_likes(boolean user_likes) {
            this.user_likes = user_likes;
        }

        public boolean isCan_like() {
            return can_like;
        }

        public void setCan_like(boolean can_like) {
            this.can_like = can_like;
        }

        public boolean isCan_publish() {
            return can_publish;
        }

        public void setCan_publish(boolean can_publish) {
            this.can_publish = can_publish;
        }
    }

    public static class PostSource {
        private String url = "";
        private Type type;
        private Platform platform;
        private Data data;

        public static PostSource parsePostSource(JSONObject object) {
            PostSource postSource = new PostSource();
            try {
                if (object.has("url")) postSource.setUrl(object.getString("url"));

                if (object.has("type")) {
                    postSource.setType(Type.valueOf(object.getString("type").toUpperCase()));
                }
                if (object.has("platform")) {
                    postSource.setPlatform(Platform.valueOf(object.getString("platform").toUpperCase()));
                }
                if (object.has("data")) {
                    postSource.setData(Data.valueOf(object.getString("data").toUpperCase()));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return postSource;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public Platform getPlatform() {
            return platform;
        }

        public void setPlatform(Platform platform) {
            this.platform = platform;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        enum Data {
            PROFILE_ACTIVITY, PROFILE_PHOTO, COMMENTS, LIKE, POLL
        }

        enum Type {
            VK, WIDGET, API, RSS, SMS, MVK
        }

        enum Platform {
            ANDROID, IPHONE, WPHONE, INSTAGRAM, IPAD, CHRONICLE, WINDOWS
        }
    }
}
