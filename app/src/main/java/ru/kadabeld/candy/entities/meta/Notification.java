package ru.kadabeld.candy.entities.meta;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.entities.Comment;
import ru.kadabeld.candy.entities.Photo;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.entities.Video;
import ru.kadabeld.candy.utils.parsers.Attachments;

/**
 * User: Vlad
 * Date: 12.11.2015
 * Time: 1:27
 */
public class Notification {
    private Type type;
    private long date;
    private Object parent = null;
    private Reply reply = null;
    private Feedback feedback = null;

    public static Notification parseNotification(JSONObject object) {
        Notification notification = new Notification();
        try {
            if (object.has("type")) {
                notification.setType(Type.valueOf(object.getString("type").toUpperCase()));
            }
            if (object.has("date")) notification.setDate(object.getLong("date"));
            if (object.has("parent")) {
                switch (notification.getType()) {
                    case FOLLOW:
                    case FRIEND_ACCEPTED:
                    case MENTION:
                    case WALL:
                    case WALL_PUBLISH:
                    default:
                        break;

                    case MENTION_COMMENTS:
                    case COMMENT_POST:
                    case LIKE_POST:
                    case COPY_POST:
                        notification.setParent(Post.parsePost(object.getJSONObject("parent")));
                        break;

                    case COMMENT_PHOTO:
                    case LIKE_PHOTO:
                    case COPY_PHOTO:
                    case MENTION_COMMENT_PHOTO:
                        notification.setParent(Photo.parsePhoto(object.getJSONObject("parent")));
                        break;

                    case COMMENT_VIDEO:
                    case LIKE_VIDEO:
                    case COPY_VIDEO:
                    case MENTION_COMMENT_VIDEO:
                        notification.setParent(Video.parseVideo(object.getJSONObject("parent")));
                        break;

                    case REPLY_COMMENT:
                    case REPLY_COMMENT_PHOTO:
                    case REPLY_COMMENT_VIDEO:
                    case LIKE_COMMENT:
                    case LIKE_COMMENT_PHOTO:
                    case LIKE_COMMENT_VIDEO:
                    case LIKE_COMMENT_TOPIC:
                        notification.setParent(Comment.parseComment(object.getJSONObject("parent")));
                        break;

                    case REPLY_TOPIC:
                        //topic
                        // TODO: 12.11.2015
                        break;

                }
            }
            if (object.has("reply")) {
                notification.setReply(Reply.parseReply(object.getJSONObject("reply")));
            }
            if (object.has("feedback")) {
                notification.setFeedback(Feedback.parseFeedback(object.getJSONObject("feedback")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return notification;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Object getParent() {
        return parent;
    }

    public void setParent(Object parent) {
        this.parent = parent;
    }

    public Reply getReply() {
        return reply;
    }

    public void setReply(Reply reply) {
        this.reply = reply;
    }

    public enum Type {
        FOLLOW, FRIEND_ACCEPTED, MENTION, MENTION_COMMENTS, WALL, WALL_PUBLISH, COMMENT_POST, COMMENT_PHOTO, COMMENT_VIDEO,
        REPLY_COMMENT, REPLY_COMMENT_PHOTO, REPLY_COMMENT_VIDEO, REPLY_TOPIC, LIKE_POST, LIKE_COMMENT, LIKE_PHOTO, LIKE_VIDEO,
        LIKE_COMMENT_PHOTO, LIKE_COMMENT_VIDEO, LIKE_COMMENT_TOPIC, COPY_POST, COPY_PHOTO, COPY_VIDEO, MENTION_COMMENT_PHOTO, MENTION_COMMENT_VIDEO
    }

    public static class Reply {
        private long id;
        private String text = "";
        private long date;

        public static Reply parseReply(JSONObject object) {
            Reply reply = new Reply();
            try {
                if (object.has("date")) reply.setDate(object.getLong("date"));
                if (object.has("id")) reply.setId(object.getLong("id"));
                if (object.has("text")) reply.setText(object.getString("text"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return reply;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }
    }

    public static class Feedback {
        private long id;
        private long to_id;
        private long from_id;
        private long count;
        private String text = "";
        private ArrayList<Long> from_ids = new ArrayList<>();
        private Post.Likes likes = null;
        private Attachments attachments = null;

        public static Feedback parseFeedback(JSONObject object) {
            Feedback feedback = new Feedback();
            try {
                if (object.has("items")) {
                    JSONArray items = object.getJSONArray("items");
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject object_item = items.getJSONObject(i);
                        if (object_item.has("from_id"))
                            feedback.addFrom_Id(object_item.getLong("from_id"));
                    }
                }

                if (object.has("count")) feedback.setCount(object.getLong("count"));
                if (object.has("id")) feedback.setId(object.getLong("id"));
                if (object.has("to_id")) feedback.setTo_id(object.getLong("to_id"));
                if (object.has("from_id")) feedback.setFrom_id(object.getLong("from_id"));
                if (object.has("text")) feedback.setText(object.getString("text"));
                if (object.has("likes"))
                    feedback.setLikes(Post.Likes.parseLikes(object.getJSONObject("likes")));
                if (object.has("attachments"))
                    feedback.setAttachments(Attachments.parseJson(object.getJSONArray("attachments")));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return feedback;
        }

        public ArrayList<Long> getFrom_ids() {
            return from_ids;
        }

        public void setFrom_ids(ArrayList<Long> from_ids) {
            this.from_ids = from_ids;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public void addFrom_Id(long from_id) {
            this.from_ids.add(from_id);
        }

        public long getTo_id() {
            return to_id;
        }

        public void setTo_id(long to_id) {
            this.to_id = to_id;
        }

        public long getFrom_id() {
            return from_id;
        }

        public void setFrom_id(long from_id) {
            this.from_id = from_id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Post.Likes getLikes() {
            return likes;
        }

        public void setLikes(Post.Likes likes) {
            this.likes = likes;
        }

        public Attachments getAttachments() {
            return attachments;
        }

        public void setAttachments(Attachments attachments) {
            this.attachments = attachments;
        }
    }
}
