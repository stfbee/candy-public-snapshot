package ru.kadabeld.candy.entities;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;

/**
 * User: Vlad
 * Date: 07.09.2015
 * Time: 3:03
 */
public class Dialog {
    //in-app working fields
    private boolean writing;
    private boolean chat;
    private boolean group;
    private User writer;
    private boolean isAvatarDefault;

    private String photo_50 = "";
    private String photo_100 = "";
    private String photo_200 = "";
    private String title = "";
    private long admin_id;
    private long user_id;
    private long chat_id;

    private Message message;
    private AbstractProfile user;

    private ArrayList<Long> chat_active = new ArrayList<>();
    private Handler handler = new Handler();
    private ArrayList<? extends AbstractAttachment> attachments = new ArrayList<>();

    public static Dialog parseDialog(JSONObject object) {
        Dialog dialog = null;
        try {
            Message message = Message.parseMessage(object);
            if (message.isChat()) {
                dialog = Cache.getChat(message.getChat_id());
            } else {
                dialog = Cache.getDialog(message.getUser_id());
            }

            if (dialog == null) dialog = new Dialog();
//            dialog.setChat(message.isChat());

            if (object.has("photo_50")) dialog.setPhoto_50(object.getString("photo_50"));
            if (object.has("photo_100")) dialog.setPhoto_100(object.getString("photo_100"));
            if (object.has("photo_200")) dialog.setPhoto_200(object.getString("photo_200"));
            if (object.has("title")) dialog.setTitle(object.getString("title"));
            if (object.has("user_id")) dialog.setUser_id(object.getLong("user_id"));
            if (object.has("admin_id")) dialog.setUser_id(object.getLong("admin_id"));
            if (object.has("chat_id")) {
                dialog.setChat_id(object.getLong("chat_id"));
                dialog.setChat(true);
            }

            if (object.has("chat_active")) {
                JSONArray chat_activeObjects = object.getJSONArray("chat_active");
                dialog.setChat_active(new ArrayList<Long>());
                for (int i = 0; i < chat_activeObjects.length(); i++) {
                    dialog.addChat_active((chat_activeObjects.getLong(i)));
                }
            }

            dialog.setMessage(message);

            dialog.setUser(Cache.getAbstractProfile(dialog.getUser_id()));

            dialog.setIsAvatarDefault(dialog.getPhotoFor(1000).isEmpty());

            if (dialog.isChat()) {
                Cache.putChat(dialog);
            } else {
                Cache.putDialog(dialog);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dialog;
    }


    public interface DialogOnLoad {
        void run(Dialog dialog);
    }

    public static void load(long chat_id, final DialogOnLoad runnable) {
        RequestParams params = new RequestParams();
        params.put("chat_id", chat_id);
        params.put("fields", API.userFields);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.messagesGetChat, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    if (object.has("response")) {
                        // TODO: 28.02.2016 оно пашет ваще?
                        JSONObject response = object.getJSONObject("response");
                        runnable.run(parseDialog(response));
                    } else {
                        Thread.sleep(1000);
                        sendRetryMessage(10);
                    }
                } catch (JSONException e) {
                    Log.e("NET", object.toString());
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        if (user_id < 0) {
            group = true;
        }
        this.user_id = user_id;
    }

    public boolean isWriting() {
        return writing;
    }

    public void setWriting(boolean writing) {
        this.writing = writing;
    }

    public boolean isChat() {
        return chat;
    }

    public void setChat(boolean chat) {
        this.chat = chat;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public String getPhoto_50() {
        return photo_50;
    }

    public void setPhoto_50(String photo_50) {
        this.photo_50 = photo_50;
    }

    public String getPhoto_100() {
        return photo_100;
    }

    public void setPhoto_100(String photo_100) {
        this.photo_100 = photo_100;
    }

    public String getPhoto_200() {
        return photo_200;
    }

    public void setPhoto_200(String photo_200) {
        this.photo_200 = photo_200;
    }

    public String getPhotoFor(int size) {
        if (size > 100) if (!photo_200.isEmpty()) return photo_200;
        if (size > 50) if (!photo_100.isEmpty()) return photo_100;
        return photo_50;
    }


    public Message getMessage() {
        if (message == null) message = new Message();
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public User getWriter() {
        if (message == null) message = new Message();
        return writer;
    }

    public void setWriter(User writer) {
        this.writer = writer;
    }

    public long getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(long admin_id) {
        this.admin_id = admin_id;
    }

    public ArrayList<Long> getChat_active() {
        return chat_active;
    }

    public void setChat_active(ArrayList<Long> chat_active) {
        this.chat_active = chat_active;
    }

    public void addChat_active(Long chat_active) {
        this.chat_active.add(chat_active);
    }

    public AbstractProfile getUser() {
        return user;
    }

    public void setUser(AbstractProfile user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getChat_id() {
        return chat_id;
    }

    public void setChat_id(long chat_id) {
        this.chat_id = chat_id;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public boolean isAvatarDefault() {
        return isAvatarDefault;
    }

    public void setIsAvatarDefault(boolean isAvatarDefault) {
        this.isAvatarDefault = isAvatarDefault;
    }

    public Drawable getAvatarDrawable() {
        ColorGenerator colorGenerator = ColorGenerator.MATERIAL;
        return TextDrawable.builder().buildRound((getTitle().isEmpty() ? "?" : getTitle().substring(0, 1)), colorGenerator.getColor(chat_id));
    }

    public Drawable getAvatarDrawable(int color) {
        return TextDrawable.builder().buildRound((getTitle().isEmpty() ? "?" : getTitle().substring(0, 1)), color);
    }

    public ArrayList<? extends AbstractAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(ArrayList<? extends AbstractAttachment> attachments) {
        this.attachments = attachments;
    }
}
