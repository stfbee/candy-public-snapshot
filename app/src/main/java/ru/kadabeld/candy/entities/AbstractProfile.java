package ru.kadabeld.candy.entities;

import android.graphics.drawable.Drawable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * User: Vlad
 * Date: 08.09.2015
 * Time: 0:09
 */
public abstract class AbstractProfile {
    private String note;

    public abstract String getName();

    public abstract String getName(User.NameCase nameCase);

    public abstract String getFullName();

    public abstract Deactivated getDeactivated();

    public abstract String getStatus();

    public abstract void setStatus(String string);

    public abstract void setStatus_audio(Audio status_audio);

    public abstract Audio getStatus_audio();

    public abstract boolean isAvatarDefault();

    public abstract Drawable getAvatarDrawable(int colorPrimary);

    public abstract Drawable getAvatarDrawable();

    public abstract long getId();

    public abstract void setId(long id);

    public abstract String getPhotoFor(int size);

    public static AbstractProfile parse(JSONObject object) {
        if (object.has("id")) try {
            long id = object.getLong("id");
            if (id > 0) {
                if (id > 1000000000) {
                    //groups
                    return Group.parseGroup(object);
                }
                //users
                return User.parseUser(object);
            } else {
                if (id < -2000000000) {
                    //emails
                    return User.parseUser(object);
                }
                //groups too
                return Group.parseGroup(object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public enum Deactivated {NONE, DELETED, BANNED}

    public static class Country {
        private long id;
        private String title = "";

        public static Country parseCountry(JSONObject object) {
            Country country = new Country();
            try {
                if (object.has("id")) country.setId(object.getLong("id"));
                if (object.has("title")) country.setTitle(object.getString("title"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return country;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class City {
        private long id;
        private String title = "";

        public static City parseCity(JSONObject object) {
            City city = new City();
            try {
                if (object.has("id")) city.setId(object.getLong("id"));
                if (object.has("title")) city.setTitle(object.getString("title"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return city;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class Status {
        Audio audio = new Audio();
        String text = "";

        public Audio getAudio() {
            return audio;
        }

        public void setAudio(Audio audio) {
            this.audio = audio;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public static Status parseStatus(JSONObject object) {
            Status status = new Status();

            try {
                if (object.has("audio")) {
                    status.setAudio(Audio.parseAudio(object.getJSONObject("audio")));
                }

                if (object.has("text")) status.setText(object.getString("text"));
            } catch (JSONException e) {
                System.out.println(object.toString());
                e.printStackTrace();
            }
            return status;
        }
    }

    public interface ProfileOnLoad {
        void run(AbstractProfile profile);
    }
}
