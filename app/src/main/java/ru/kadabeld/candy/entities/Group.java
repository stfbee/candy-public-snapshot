package ru.kadabeld.candy.entities;

import android.graphics.drawable.Drawable;
import android.util.Log;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;

/**
 * User: Vlad
 * Date: 04.09.2015
 * Time: 2:48
 */
@SuppressWarnings({"SpellCheckingInspection", "unused"})
public class Group extends AbstractProfile {
    private long id;
    private long fixed_post;
    private long main_album_id;
    private long members_count;
    private City city;
    private Country country;
    private long start_date;
    private long finish_date;
    private String name = "";
    private String screen_name = "";
    private String photo_50 = "";
    private String photo_100 = "";
    private String photo_200 = "";
    private String description = "";
    private String wiki_page = "";
    private String foundation_date = "";
    private String activity = "";
    private String status = "";
    private String contacts = "";
    private String links = "";
    private String site = "";
    private boolean is_member;
    private boolean is_admin;
    private boolean can_post;
    private boolean can_message;
    private boolean can_see_all_posts;
    private boolean can_upload_doc;
    private boolean can_upload_video;
    private boolean can_create_topic;
    private boolean verified;
    private boolean is_favorite;
    private Closed is_closed = Closed.OPEN;
    private Deactivated deactivated = Deactivated.NONE;
    private Type type = Type.GROUP;
    private AdminLevel admin_level = AdminLevel.NONE;

    public Audio getStatus_audio() {
        return status_audio;
    }

    public void setStatus_audio(Audio status_audio) {
        this.status_audio = status_audio;
    }

    private Audio status_audio = new Audio();
    private boolean isAvatarDefault = true;

    @Override
    public String toString() {
        return getId() + " @ " + getName();
    }

    public static Group parseGroup(JSONObject object) {
        Group group = null;

        try {
            // id
            long id = 0;
            String msg = "Ромик, иди нахуй, сам пиши";
            if (object.has("id")) id = object.getLong("id") % 1000000000;
            if (id > 0) id = -id;
            group = (Group) Cache.getAbstractProfile(id);
            if (group == null) group = new Group();

            if (object.has("id")) group.setId(-object.getLong("id"));
            if (object.has("fixed_post")) group.setFixed_post(object.getLong("fixed_post"));
            if (object.has("main_album_id"))
                group.setMain_album_id(object.getLong("main_album_id"));
            if (object.has("members_count"))
                group.setMembers_count(object.getLong("members_count"));
            if (object.has("city")) group.setCity(City.parseCity(object.getJSONObject("city")));
            if (object.has("country"))
                group.setCountry(Country.parseCountry(object.getJSONObject("country")));
            if (object.has("start_date")) group.setStart_date(object.getLong("start_date"));
            if (object.has("finish_date")) group.setFinish_date(object.getLong("finish_date"));

            if (object.has("name")) group.setName(object.getString("name"));
            if (object.has("screen_name")) group.setScreen_name(object.getString("screen_name"));
            if (object.has("photo_50")) group.setPhoto_50(object.getString("photo_50"));
            if (object.has("photo_100")) group.setPhoto_100(object.getString("photo_100"));
            if (object.has("photo_200")) group.setPhoto_200(object.getString("photo_200"));
            if (object.has("description")) group.setDescription(object.getString("description"));
            if (object.has("wiki_page")) group.setWiki_page(object.getString("wiki_page"));
            if (object.has("foundation_date"))
                group.setFoundation_date(object.getString("foundation_date"));
            if (object.has("activity")) group.setActivity(object.getString("activity"));

            if (object.has("status"))
                group.setStatus(object.getString("status"));
            if (object.has("status_audio"))
                group.setStatus_audio(Audio.parseAudio(object.getJSONObject("status_audio")));

            if (object.has("contacts")) group.setContacts(object.getString("contacts"));
            if (object.has("links")) group.setLinks(object.getString("links"));
            if (object.has("site")) group.setSite(object.getString("site"));

            if (object.has("is_member")) group.setIs_member(object.getInt("is_member") == 1);
            if (object.has("is_admin")) group.setIs_admin(object.getInt("is_admin") == 1);
            if (object.has("can_post")) group.setCan_post(object.getInt("can_post") == 1);
            if (object.has("can_message")) group.setCan_message(object.getInt("can_message") == 1);
            if (object.has("can_see_all_posts"))
                group.setCan_see_all_posts(object.getInt("can_see_all_posts") == 1);
            if (object.has("can_upload_doc"))
                group.setCan_upload_doc(object.getInt("can_upload_doc") == 1);
            if (object.has("can_upload_video"))
                group.setCan_upload_video(object.getInt("can_upload_video") == 1);
            if (object.has("can_create_topic"))
                group.setCan_create_topic(object.getInt("can_create_topic") == 1);
            if (object.has("verified")) group.setVerified(object.getInt("verified") == 1);
            if (object.has("is_favorite")) group.setIs_favorite(object.getInt("is_favorite") == 1);

            if (object.has("is_closed")) {
                for (Closed closed : Closed.values()) {
                    if (closed.ordinal() == object.getInt("is_closed")) {
                        group.setIs_closed(closed);
                        break;
                    }
                }
            }

            if (object.has("deactivated")) {
                for (Deactivated deactivated : Deactivated.values()) {
                    group.setDeactivated(Deactivated.valueOf(object.getString("deactivated").toUpperCase()));
                }
            }

            if (object.has("type")) {
                group.setType(Type.valueOf(object.getString("type").toUpperCase()));
            }

            if (object.has("admin_level")) {
                for (AdminLevel admin_level : AdminLevel.values()) {
                    if (admin_level.ordinal() == object.getInt("admin_level")) {
                        group.setAdmin_level(admin_level);
                        break;
                    }
                }
            }

            if (object.has("note")) group.setNote(object.getString("note"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Cache.putAbstractProfile(group);
        return group;
    }

    /**
     * Загружает группу и выполняет
     *
     * @param runnable код
     */
    public static void load(Runnable runnable) {
        // TODO: 14.10.2015
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public Drawable getAvatarDrawable() {
        ColorGenerator colorGenerator = ColorGenerator.MATERIAL;
        return TextDrawable.builder().buildRound(getName().isEmpty() ? "?" : getName().substring(0, 1), colorGenerator.getColor(id));
    }

    public Drawable getAvatarDrawable(int color) {
        ColorGenerator colorGenerator = ColorGenerator.MATERIAL;
        return TextDrawable.builder().buildRound(getName().isEmpty() ? "?" : getName().substring(0, 1), color);
    }

    public long getFixed_post() {
        return fixed_post;
    }

    public void setFixed_post(long fixed_post) {
        this.fixed_post = fixed_post;
    }

    public long getMain_album_id() {
        return main_album_id;
    }

    public void setMain_album_id(long main_album_id) {
        this.main_album_id = main_album_id;
    }

    public String getName() {
        return name;
    }

    public String getName(User.NameCase nameCase) {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getFullName() {
        return name;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public String getPhoto_50() {
        return photo_50;
    }

    public void setPhoto_50(String photo_50) {
        this.photo_50 = photo_50;
        isAvatarDefault = (photo_50.contains("camera_") || photo_50.contains(".gif") || photo_50.contains("deactivated") || photo_50.contains("community"));
    }

    public String getPhoto_100() {
        return photo_100;
    }

    public void setPhoto_100(String photo_100) {
        this.photo_100 = photo_100;
        isAvatarDefault = photo_100.contains("camera_") || photo_100.contains(".gif") || photo_100.contains("deactivated") || photo_100.contains("community");
    }

    public String getPhoto_200() {
        return photo_200;
    }

    public void setPhoto_200(String photo_200) {
        this.photo_200 = photo_200;
        isAvatarDefault = photo_200.contains("camera_") || photo_200.contains(".gif") || photo_200.contains("deactivated") || photo_200.contains("community");
    }

    public String getPhotoFor(int size) {
        if (size > 100) if (!photo_200.isEmpty()) return photo_200;
        if (size > 50) if (!photo_100.isEmpty()) return photo_100;
        return photo_50;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWiki_page() {
        return wiki_page;
    }

    public void setWiki_page(String wiki_page) {
        this.wiki_page = wiki_page;
    }

    public String getFoundation_date() {
        return foundation_date;
    }

    public void setFoundation_date(String foundation_date) {
        this.foundation_date = foundation_date;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getLinks() {
        return links;
    }

    public void setLinks(String links) {
        this.links = links;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public long getMembers_count() {
        return members_count;
    }

    public void setMembers_count(long members_count) {
        this.members_count = members_count;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public long getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(long finish_date) {
        this.finish_date = finish_date;
    }

    public boolean is_member() {
        return is_member;
    }

    public void setIs_member(boolean is_member) {
        this.is_member = is_member;
    }

    public boolean is_admin() {
        return is_admin;
    }

    public void setIs_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }

    public boolean isCan_post() {
        return can_post;
    }

    public void setCan_post(boolean can_post) {
        this.can_post = can_post;
    }

    public boolean isCan_message() {
        return can_message;
    }

    public void setCan_message(boolean can_message) {
        this.can_message = can_message;
    }

    public boolean isCan_see_all_posts() {
        return can_see_all_posts;
    }

    public void setCan_see_all_posts(boolean can_see_all_posts) {
        this.can_see_all_posts = can_see_all_posts;
    }

    public boolean isCan_upload_doc() {
        return can_upload_doc;
    }

    public void setCan_upload_doc(boolean can_upload_doc) {
        this.can_upload_doc = can_upload_doc;
    }

    public boolean isCan_upload_video() {
        return can_upload_video;
    }

    public void setCan_upload_video(boolean can_upload_video) {
        this.can_upload_video = can_upload_video;
    }

    public boolean isCan_create_topic() {
        return can_create_topic;
    }

    public void setCan_create_topic(boolean can_create_topic) {
        this.can_create_topic = can_create_topic;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean is_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(boolean is_favorite) {
        this.is_favorite = is_favorite;
    }

    public Closed getIs_closed() {
        return is_closed;
    }

    public void setIs_closed(Closed is_closed) {
        this.is_closed = is_closed;
    }

    public Deactivated getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(Deactivated deactivated) {
        this.deactivated = deactivated;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public AdminLevel getAdmin_level() {
        return admin_level;
    }

    public void setAdmin_level(AdminLevel admin_level) {
        this.admin_level = admin_level;
    }

    public boolean isAvatarDefault() {
        return isAvatarDefault;
    }

    public void setIsAvatarDefault(boolean isAvatarDefault) {
        this.isAvatarDefault = isAvatarDefault;
    }

    /**
     * Загружает юзера и выполняет
     *
     * @param runnable код
     */
    public static void load(long group_id, final ProfileOnLoad runnable) {
        RequestParams params = new RequestParams();
        params.put("group_id", Math.abs(group_id));
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.groupsGetById, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    JSONArray response = object.getJSONArray("response");
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject grouprObject = response.getJSONObject(i);
                        Group p = Group.parseGroup(grouprObject);
                        runnable.run(p);
                    }
                } catch (JSONException e) {
                    Log.e("NET", object.toString());
                    e.printStackTrace();
                }
            }
        });
    }

    public enum Closed {OPEN, CLOSED, PRIVATE}

    public enum Type {GROUP, PAGE, EVENT}

    public enum AdminLevel {NONE, MODERATOR, EDITOR, ADMIN}

    public static class BanInfo {
        private long end_date;
        private String comment = "";

        public static BanInfo parseBanInfo(JSONObject object) {
            BanInfo banInfo = new BanInfo();
            try {
                if (object.has("end_date")) banInfo.setEnd_date(object.getLong("end_date"));
                if (object.has("comment")) banInfo.setComment(object.getString("comment"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return banInfo;
        }

        public long getEnd_date() {
            return end_date;
        }

        public void setEnd_date(long end_date) {
            this.end_date = end_date;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

    public static class Contact {
        private long user_id;
        private String desc = "";

        public static Contact parseBanInfo(JSONObject object) {
            Contact contact = new Contact();
            try {
                if (object.has("user_id")) contact.setUser_id(object.getLong("user_id"));
                if (object.has("desc")) contact.setDesc(object.getString("desc"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return contact;
        }

        public long getUser_id() {
            return user_id;
        }

        public void setUser_id(long user_id) {
            this.user_id = user_id;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    public static class Link {
        private long user_id;
        private String desc = "";

        public static Link parseBanInfo(JSONObject object) {
            Link link = new Link();
            try {
                if (object.has("user_id")) link.setUser_id(object.getLong("user_id"));
                if (object.has("desc")) link.setDesc(object.getString("desc"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return link;
        }

        public long getUser_id() {
            return user_id;
        }

        public void setUser_id(long user_id) {
            this.user_id = user_id;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    public static class Place {
        private long id;
        private String title = "";
        private String address = "";
        private int latitude;
        private int longitude;
        private long city;
        private long country;
        private Type type;

        public static Place parsePlace(JSONObject object) {
            Place place = new Place();
            try {
                if (object.has("title")) place.setTitle(object.getString("title"));
                if (object.has("address")) place.setAddress(object.getString("address"));
                if (object.has("latitude")) place.setLatitude(object.getInt("latitude"));
                if (object.has("longitude")) place.setLongitude(object.getInt("longitude"));
                if (object.has("id")) place.setId(object.getInt("id"));
                if (object.has("city")) place.setCity(object.getInt("city"));
                if (object.has("country")) place.setCountry(object.getInt("country"));
                if (object.has("type")) {
                    for (Type type : Type.values()) {
                        if (type.ordinal() == object.getInt("type")) {
                            place.setType(type);
                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return place;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getLatitude() {
            return latitude;
        }

        public void setLatitude(int latitude) {
            this.latitude = latitude;
        }

        public int getLongitude() {
            return longitude;
        }

        public void setLongitude(int longitude) {
            this.longitude = longitude;
        }

        public long getCity() {
            return city;
        }

        public void setCity(long city) {
            this.city = city;
        }

        public long getCountry() {
            return country;
        }

        public void setCountry(long country) {
            this.country = country;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        enum Type {}
    }

    public static class Counters {
        private int photos;
        private int albums;
        private int audios;
        private int videos;
        private int topics;
        private int docs;

        public static Counters parseCounters(JSONObject object) {
            Counters counters = new Counters();
            try {
                if (object.has("photos")) counters.setPhotos(object.getInt("photos"));
                if (object.has("albums")) counters.setAlbums(object.getInt("albums"));
                if (object.has("audios")) counters.setAudios(object.getInt("audios"));
                if (object.has("videos")) counters.setVideos(object.getInt("videos"));
                if (object.has("topics")) counters.setTopics(object.getInt("topics"));
                if (object.has("docs")) counters.setDocs(object.getInt("docs"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return counters;
        }

        public int getPhotos() {
            return photos;
        }

        public void setPhotos(int photos) {
            this.photos = photos;
        }

        public int getAlbums() {
            return albums;
        }

        public void setAlbums(int albums) {
            this.albums = albums;
        }

        public int getAudios() {
            return audios;
        }

        public void setAudios(int audios) {
            this.audios = audios;
        }

        public int getVideos() {
            return videos;
        }

        public void setVideos(int videos) {
            this.videos = videos;
        }

        public int getTopics() {
            return topics;
        }

        public void setTopics(int topics) {
            this.topics = topics;
        }

        public int getDocs() {
            return docs;
        }

        public void setDocs(int docs) {
            this.docs = docs;
        }
    }
}
