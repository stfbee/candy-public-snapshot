package ru.kadabeld.candy.entities.meta;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * User: Vlad
 * Date: 12.11.2015
 * Time: 4:42
 */
public class Product {
    private Price price = new Price();

    public static Product parseProduct(JSONObject object) {
        Product product = new Product();
        try {
            if (object.has("price"))
                product.setPrice(Price.parsePrice(object.getJSONObject("price")));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return product;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public static class Price {
        private long amount;
        private String text = "";
        private Currency currency = new Currency();

        public static Price parsePrice(JSONObject object) {
            Price price = new Price();
            try {
                if (object.has("amount")) price.setAmount(object.getLong("amount"));
                if (object.has("text")) price.setText(object.getString("text"));
                if (object.has("currency"))
                    price.setCurrency(Currency.parseCurrency(object.getJSONObject("currency")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return price;
        }

        public long getAmount() {
            return amount;
        }

        public void setAmount(long amount) {
            this.amount = amount;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Currency getCurrency() {
            return currency;
        }

        public void setCurrency(Currency currency) {
            this.currency = currency;
        }

        public static class Currency {
            private long id;
            private String name = "";

            public static Currency parseCurrency(JSONObject object) {
                Currency currency = new Currency();

                try {
                    if (object.has("id")) currency.setId(object.getLong("id"));
                    if (object.has("name")) currency.setName(object.getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return currency;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }
        }
    }
}
