package ru.kadabeld.candy.entities;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * User: Vlad
 * Date: 23.08.2015
 * Time: 21:59
 */
public class Sticker {

    private long id = 0;
    private long product_id = 0;
    private String photo_64 = "";
    private String photo_128 = "";
    private String photo_256 = "";
    private String photo_512 = "";
    private int width;
    private int height;

    public static Sticker parseSticker(JSONObject stickerObject) {
        final Sticker sticker = new Sticker();
        try {
            if (stickerObject.has("id")) sticker.setId(stickerObject.getInt("id"));
            if (stickerObject.has("product_id"))
                sticker.setProduct_id(stickerObject.getInt("product_id"));
            if (stickerObject.has("height")) sticker.setHeight(stickerObject.getInt("height"));
            if (stickerObject.has("width")) sticker.setWidth(stickerObject.getInt("width"));
            if (stickerObject.has("photo_64"))
                sticker.setPhoto_64(stickerObject.getString("photo_64"));
            if (stickerObject.has("photo_128"))
                sticker.setPhoto_128(stickerObject.getString("photo_128"));
            if (stickerObject.has("photo_352"))
                sticker.setPhoto_256(stickerObject.getString("photo_352"));
            if (stickerObject.has("photo_512"))
                sticker.setPhoto_512(stickerObject.getString("photo_512"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return sticker;
    }

    public Sticker() {
    }

    public Sticker(long id) {
        setId(id);
    }

    public long getId() {
        return id;
    }

    public Sticker setId(long id) {
        this.id = id;
        photo_64 = "https://vk.com/images/stickers/" + id + "/64b.png";
        photo_128 = "https://vk.com/images/stickers/" + id + "/128b.png";
        photo_256 = "https://vk.com/images/stickers/" + id + "/256b.png";
        photo_512 = "https://vk.com/images/stickers/" + id + "/512b.png";
        return this;
    }

    public long getProduct_id() {
        return product_id;
    }

    public Sticker setProduct_id(long product_id) {
        this.product_id = product_id;
        return this;
    }

    public String getPhoto_64() {
        return photo_64;
    }

    public Sticker setPhoto_64(String photo_64) {
        this.photo_64 = photo_64;
        return this;
    }

    public String getPhoto_128() {
        return photo_128;
    }

    public Sticker setPhoto_128(String photo_128) {
        this.photo_128 = photo_128;
        return this;
    }

    public String getPhoto_256() {
        return photo_256;
    }

    public Sticker setPhoto_256(String photo_256) {
        this.photo_256 = photo_256;
        return this;
    }

    public String getPhoto_512() {
        return photo_512;
    }

    public Sticker setPhoto_512(String photo_512) {
        this.photo_512 = photo_512;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public Sticker setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public Sticker setHeight(int height) {
        this.height = height;
        return this;
    }

    public String getPhotoFor(int height) {
        if (height < 64) {
            return photo_64;
        } else if (height < 128) {
            return photo_128;
        } else if (height < 256) {
            return photo_256;
        } else if (height <= 512) {
            return photo_512;
        } else {
            return photo_128;
        }
    }


}
