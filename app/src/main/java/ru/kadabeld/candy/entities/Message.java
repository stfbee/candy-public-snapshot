package ru.kadabeld.candy.entities;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.entities.meta.Geo;
import ru.kadabeld.candy.utils.parsers.Attachments;

/**
 * User: Vlad
 * Date: 26.02.2015
 * Time: 17:36
 */
public class Message {
    public int FLAGS = 0;
    private long id;
    private long user_id;
    private long from_id;
    private long chat_id;
    private long date;
    private long users_count;
    private long action_mid;
    private boolean read_state;
    private boolean out;
    private String title = "";
    private String body = "";
    private String action_text = "";
    private String action_email = "";
    private Geo geo = new Geo();
    private Attachments attachments = new Attachments();
    private Action action;
    private boolean emoji;
    private boolean important;
    private boolean deleted;
    private boolean sent;
    private boolean chat;
    private boolean group;
    private boolean technical;
    private ArrayList<Message> fwd_messages = new ArrayList<>();

    public static Message parseMessage(final JSONObject object) {
        final Message message = new Message();
        try {
            if (object.has("id")) message.setId(object.getLong("id"));
            if (object.has("user_id")) message.setUser_id(object.getLong("user_id"));
            if (object.has("action_mid")) message.setAction_mid(object.getLong("action_mid"));
            if (object.has("from_id")) message.setFrom_id(object.getLong("from_id"));
            if (object.has("users_count")) message.setUsers_count(object.getLong("users_count"));
            if (object.has("chat_id")) {
                message.setChat_id(object.getLong("chat_id"));
                message.setChat(true);
            }
            if (object.has("date")) message.setDate(object.getLong("date"));
            if (object.has("read_state")) message.setRead_state(object.getInt("read_state") != 0);
            if (object.has("out")) message.setOut(object.getInt("out") != 0);
            if (object.has("body"))
                message.setBody(StringEscapeUtils.unescapeHtml(object.getString("body")));
            if (object.has("attachments"))
                message.setAttachments(Attachments.parseJson(object.getJSONArray("attachments")));
            if (object.has("title")) message.setTitle(object.getString("title"));
            if (object.has("action_email"))
                message.setAction_email(object.getString("action_email"));
            if (object.has("action_text")) message.setAction_text(object.getString("action_text"));
            if (object.has("geo")) {
                message.setGeo(Geo.parseGeo(object.getJSONObject("geo")));
            }
            if (object.has("fwd_messages")) {
                JSONArray fwd_messagesObjects = object.getJSONArray("fwd_messages");
                ArrayList<Message> fwd_messages = new ArrayList<>();
                for (int i = 0; i < fwd_messagesObjects.length(); i++) {
                    JSONObject o = fwd_messagesObjects.getJSONObject(i);
                    Message m = parseQuote(o);
                    fwd_messages.add(m);
                }
                message.setFwd_messages(fwd_messages);
            }
            if (object.has("emoji")) message.setEmoji(object.getInt("emoji") != 0);
            if (object.has("important")) message.setImportant(object.getInt("important") != 0);
            if (object.has("deleted")) message.setDeleted(object.getInt("deleted") != 0);

            if (object.has("action")) {
                message.setAction(Action.valueOf(object.getString("action").toUpperCase()));
                message.setTechnical(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }

    public static Message parseQuote(final JSONObject object) {
        final Message message = new Message();
        try {
            if (object.has("user_id")) message.setUser_id(object.getLong("user_id"));
            if (object.has("date")) message.setDate(object.getLong("date"));
            if (object.has("body"))
                message.setBody(StringEscapeUtils.unescapeHtml(object.getString("body")));
            if (object.has("attachments"))
                message.setAttachments(Attachments.parseJson(object.getJSONArray("attachments")));
            if (object.has("geo")) {
                message.setGeo(Geo.parseGeo(object.getJSONObject("geo")));
            }
            if (object.has("fwd_messages")) {
                JSONArray fwd_messagesObjects = object.getJSONArray("fwd_messages");
                ArrayList<Message> fwd_messages = new ArrayList<>();
                for (int i = 0; i < fwd_messagesObjects.length(); i++) {
                    JSONObject o = fwd_messagesObjects.getJSONObject(i);
                    Message m = parseQuote(o);
                    fwd_messages.add(m);
                }
                message.setFwd_messages(fwd_messages);
            }
            if (object.has("emoji")) message.setEmoji(object.getInt("emoji") != 0);

            return message;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }

    public long getChat_id() {
        return chat_id;
    }

    public void setChat_id(long chat_id) {
        this.chat_id = chat_id;
    }

    public boolean isChat() {
        return chat;
    }

    public void setChat(boolean chat) {
        this.chat = chat;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        if (user_id > 1000000000) {
            group = true;
            this.user_id = -user_id % 1000000000;
        } else if (user_id < 0) {
            group = true;
            this.user_id = user_id;
        } else {
            this.user_id = user_id;
        }
    }

    public long getFrom_id() {
        return from_id;
    }

    public void setFrom_id(long from_id) {
        this.from_id = from_id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isRead_state() {
        return read_state;
    }

    public void setRead_state(boolean read_state) {
        this.read_state = read_state;
    }

    public boolean isOut() {
        return out;
    }

    public void setOut(boolean out) {
        this.out = out;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Geo getGeo() {
        return geo;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    public Attachments getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }

    public ArrayList<Message> getFwd_messages() {
        return fwd_messages;
    }

    public void setFwd_messages(ArrayList<Message> fwd_messages) {
        this.fwd_messages = fwd_messages;
    }

    public boolean isEmoji() {
        return emoji;
    }

    public void setEmoji(boolean emoji) {
        this.emoji = emoji;
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public long getUsers_count() {
        return users_count;
    }

    public void setUsers_count(long users_count) {
        this.users_count = users_count;
    }

    public boolean isTechnical() {
        return technical;
    }

    public void setTechnical(boolean technical) {
        this.technical = technical;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getAction_email() {
        return action_email;
    }

    public void setAction_email(String action_email) {
        this.action_email = action_email;
    }

    public String getAction_text() {
        return action_text;
    }

    public void setAction_text(String action_text) {
        this.action_text = action_text;
    }

    public long getAction_mid() {
        return action_mid;
    }

    public void setAction_mid(long action_mid) {
        this.action_mid = action_mid;
    }

    public enum Action {
        CHAT_PHOTO_UPDATE, CHAT_PHOTO_REMOVE, CHAT_CREATE, CHAT_TITLE_UPDATE, CHAT_INVITE_USER, CHAT_KICK_USER
    }
}
