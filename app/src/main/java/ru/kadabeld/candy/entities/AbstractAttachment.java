package ru.kadabeld.candy.entities;

/**
 * User: Vlad
 * Date: 10.02.2016
 * Time: 14:57
 */
public interface AbstractAttachment {
    long getDate();
}
