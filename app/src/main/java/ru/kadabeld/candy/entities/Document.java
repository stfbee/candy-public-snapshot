package ru.kadabeld.candy.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Document implements AbstractAttachment {
    private long id;
    private long owner_id;
    private long date;
    private String title = "";
    private long size;
    private String ext = "";
    private String url = "";
    private String photo_m = "";
    private String photo_s = "";
    private String photo_x = "";
    private String photo_y = "";
    private String photo_z = "";
    private String photo_o = "";
    private Type type;

    private int width;
    private int height;

    public static Document parseDocument(JSONObject documentObject) {
        final Document document = new Document();
        try {
            if (documentObject.has("id")) document.setId(documentObject.getLong("id"));
            if (documentObject.has("owner_id"))
                document.setOwner_id(documentObject.getLong("owner_id"));
            if (documentObject.has("title")) document.setTitle(documentObject.getString("title"));
            if (documentObject.has("size")) document.setSize(documentObject.getLong("size"));
            if (documentObject.has("date")) document.setDate(documentObject.getLong("date"));
            if (documentObject.has("ext")) document.setExt(documentObject.getString("ext"));
            if (documentObject.has("url")) document.setUrl(documentObject.getString("url"));
            if (documentObject.has("photo_m"))
                document.setPhoto_m(documentObject.getString("photo_m"));
            if (documentObject.has("photo_s"))
                document.setPhoto_s(documentObject.getString("photo_s"));

            if (documentObject.has("type")) {
                int type = documentObject.getInt("type");
                for (int i = 0; i < Type.values().length; i++) {
                    if (i == type - 1) {
                        document.setType(Type.values()[i]);
                        break;
                    }
                }
            }

            if (documentObject.has("preview")) {
                JSONObject previewObject = documentObject.getJSONObject("preview");
                if (previewObject.has("photo")) {
                    JSONObject photoObject = previewObject.getJSONObject("photo");
                    JSONArray sizes = photoObject.getJSONArray("sizes");
                    for (int i = 0; i < sizes.length(); i++) {
                        JSONObject sizeObject = sizes.getJSONObject(i);
                        String type = sizeObject.getString("type");
                        switch (type) {
                            case "m":
                                document.setPhoto_m(sizeObject.getString("src"));
                                break;
                            case "s":
                                document.setPhoto_s(sizeObject.getString("src"));
                                break;
                            case "x":
                                document.setPhoto_x(sizeObject.getString("src"));
                                break;
                            case "y":
                                document.setPhoto_y(sizeObject.getString("src"));
                                break;
                            case "z":
                                document.setPhoto_z(sizeObject.getString("src"));
                                break;
                            case "o":
                                document.setPhoto_o(sizeObject.getString("src"));
                                document.setHeight(sizeObject.getInt("height"));
                                document.setWidth(sizeObject.getInt("width"));
                                break;
                        }
                    }

                    if (previewObject.has("graffiti")) {
                        document.setType(Type.GRAFFITI);
                        document.setPhoto_o(previewObject.getJSONObject("graffiti").getString("src"));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return document;
    }

    public static int size_m = 100;
    public static int size_s = 130;
    public static int size_x = 604;
    public static int size_y = 807;
    public static int size_z = 1280;

    public String getPhotoFor(int height) {
        if (height < size_s) {
            return photo_s;
        } else if (height < size_m) {
            return photo_m;
        } else if (height < size_x) {
            return photo_x;
        } else if (height < size_y) {
            return photo_y;
        } else if (height < size_z) {
            return photo_z;
        } else {
            return photo_o;
        }
    }

    public String getPhoto_m() {
        return photo_m;
    }

    public void setPhoto_m(String photo_m) {
        this.photo_m = photo_m;
    }

    public String getPhoto_s() {
        return photo_s;
    }

    public void setPhoto_s(String photo_s) {
        this.photo_s = photo_s;
    }

    public String getPhoto_x() {
        return photo_x;
    }

    public void setPhoto_x(String photo_x) {
        this.photo_x = photo_x;
    }

    public String getPhoto_y() {
        return photo_y;
    }

    public void setPhoto_y(String photo_y) {
        this.photo_y = photo_y;
    }

    public String getPhoto_z() {
        return photo_z;
    }

    public void setPhoto_z(String photo_z) {
        this.photo_z = photo_z;
    }

    public String getPhoto_o() {
        return photo_o;
    }

    public void setPhoto_o(String photo_o) {
        this.photo_o = photo_o;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public enum Type {
        TEXT, ZIP, GIF, IMAGE, AUDIO, VIDEO, BOOK, OTHER, GRAFFITI
    }
}
