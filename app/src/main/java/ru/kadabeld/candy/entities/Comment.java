package ru.kadabeld.candy.entities;

import org.json.JSONException;
import org.json.JSONObject;

import ru.kadabeld.candy.utils.parsers.Attachments;

public class Comment {
    private long time;
    private long id;
    private long date;
    private long countLikes;
    private long reply_to_comment;
    private String text = "";
    private boolean liked;
    private boolean can_like;
    private long from_id;
    private long reply_to_user;
    private long where_is;
    private Attachments attachments = new Attachments();

    public static Comment parseComment(JSONObject object) {
        Comment comment = new Comment();
        try {
            if (object.has("time")) comment.setTime(object.getLong("time"));
            if (object.has("id")) comment.setId(object.getLong("id"));
            if (object.has("date")) comment.setDate(object.getLong("date"));
            if (object.has("reply_to_comment"))
                comment.setReply_to_comment(object.getLong("reply_to_comment"));
            if (object.has("text")) {
                String text = object.getString("text");
                comment.setText(text.replaceFirst("\\[id[\\d]*\\|([^\\]]*)\\]", "$1"));
            }
            if (object.has("from_id")) comment.setFrom_id(object.getLong("from_id"));
            if (object.has("reply_to_user"))
                comment.setReply_to_user(object.getLong("reply_to_user"));

            if (object.has("likes")) {
                JSONObject likesObject = object.getJSONObject("likes");
                if (likesObject.has("countLikes"))
                    comment.setCountLikes(likesObject.getLong("countLikes"));
                if (likesObject.has("can_like"))
                    comment.setCan_like(likesObject.getInt("can_like") == 1);
                if (likesObject.has("liked")) comment.setLiked(likesObject.getBoolean("liked"));
            }

            if (object.has("attachments"))
                comment.setAttachments(Attachments.parseJson(object.getJSONArray("attachments")));

//            comment.setFrom_user(Cache.getAbstractProfile(comment.getFrom_id()));
//                comment.setReceiver_user(Cache.getAbstractProfile(comment.getReply_to_user()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return comment;
    }

    public long getFrom_id() {
        return from_id;
    }

    public void setFrom_id(long from_id) {
        this.from_id = from_id;
    }

    public long getReply_to_user() {
        return reply_to_user;
    }

    public void setReply_to_user(long reply_to_user) {
        this.reply_to_user = reply_to_user;
    }

    public Attachments getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getCountLikes() {
        return countLikes;
    }

    public void like() {
        liked = !liked;
        if (liked) {
            countLikes++;
        } else {
            countLikes--;
        }
    }

    public void setCountLikes(long countLikes) {
        this.countLikes = countLikes;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean isCan_like() {
        return can_like;
    }

    public void setCan_like(boolean can_like) {
        this.can_like = can_like;
    }

    public long getReply_to_comment() {
        return reply_to_comment;
    }

    public void setReply_to_comment(long reply_to_comment) {
        this.reply_to_comment = reply_to_comment;
    }

    public long getWhere_is() {
        return where_is;
    }

    public void setWhere_is(long where_is) {
        this.where_is = where_is;
    }
}