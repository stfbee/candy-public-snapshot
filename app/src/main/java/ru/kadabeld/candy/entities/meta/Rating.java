package ru.kadabeld.candy.entities.meta;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * User: Vlad
 * Date: 12.11.2015
 * Time: 4:55
 */
public class Rating {
    private double stars;
    private long reviews_count;

    public static Rating parseRating(JSONObject object) {
        Rating rating = new Rating();

        try {
            if (object.has("reviews_count"))
                rating.setReviews_count(object.getLong("reviews_count"));
            if (object.has("stars")) rating.setStars(object.getDouble("stars"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rating;
    }

    public long getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(long reviews_count) {
        this.reviews_count = reviews_count;
    }

    public double getStars() {
        return stars;
    }

    public void setStars(double stars) {
        this.stars = stars;
    }
}
