package ru.kadabeld.candy.entities.meta;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * User: Vlad
 * Date: 21.10.2015
 * Time: 12:29
 */
public class Geo {
    private String type = "";
    private String coordinates = "";
    private Place place = new Place();

    public static Geo parseGeo(JSONObject object) {
        Geo geo = new Geo();
        try {
            if (object.has("type")) geo.setType(object.getString("type"));
            if (object.has("coordinates")) geo.setCoordinates(object.getString("coordinates"));
            if (object.has("place")) {
                geo.setPlace(Place.parsePlace(object.getJSONObject("place")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return geo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public static class Place {
        private long id;
        private String title = "";
        private String icon = "";
        private String country = "";
        private String city = "";
        private String address = "";
        private double latitude;
        private double longitude;
        private double created;
        private double group_photo;
        private double group_id;
        private double checkins;
        private double updated;

        public static Place parsePlace(JSONObject object) {
            Place place = new Place();
            try {
                if (object.has("id")) place.setId(object.getLong("id"));
                if (object.has("title")) place.setTitle(object.getString("title"));
                if (object.has("icon")) place.setIcon(object.getString("icon"));
                if (object.has("country")) place.setCountry(object.getString("country"));
                if (object.has("city")) place.setCity(object.getString("city"));
                if (object.has("address")) place.setAddress(object.getString("address"));
                if (object.has("latitude")) place.setLatitude(object.getDouble("latitude"));
                if (object.has("longitude")) place.setLongitude(object.getDouble("longitude"));
                if (object.has("created")) place.setCreated(object.getDouble("created"));
                if (object.has("group_photo"))
                    place.setGroup_photo(object.getDouble("group_photo"));
                if (object.has("group_id")) place.setGroup_id(object.getDouble("group_id"));
                if (object.has("checkins")) place.setCheckins(object.getDouble("checkins"));
                if (object.has("updated")) place.setUpdated(object.getDouble("updated"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return place;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getCreated() {
            return created;
        }

        public void setCreated(double created) {
            this.created = created;
        }

        public double getGroup_photo() {
            return group_photo;
        }

        public void setGroup_photo(double group_photo) {
            this.group_photo = group_photo;
        }

        public double getGroup_id() {
            return group_id;
        }

        public void setGroup_id(double group_id) {
            this.group_id = group_id;
        }

        public double getCheckins() {
            return checkins;
        }

        public void setCheckins(double checkins) {
            this.checkins = checkins;
        }

        public double getUpdated() {
            return updated;
        }

        public void setUpdated(double updated) {
            this.updated = updated;
        }
    }
}
