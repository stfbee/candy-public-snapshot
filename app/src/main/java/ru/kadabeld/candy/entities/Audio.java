package ru.kadabeld.candy.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class Audio implements AbstractAttachment {
    private long owner_id;
    private long id;
    private long duration;
    private long lyrics_id;
    private long album_id;
    private long date;
    private String artist = "";
    private String title = "";
    private String url = "";
    private Genre genre_id;

    private boolean isPlaying = false;

    public static Audio parseAudio(JSONObject object) {
        Audio audio = new Audio();
        try {
            if (object.has("owner_id")) audio.setOwner_id(object.getLong("owner_id"));
            if (object.has("id")) audio.setId(object.getLong("id"));
            if (object.has("duration")) audio.setDuration(object.getLong("duration"));
            if (object.has("lyrics_id")) audio.setLyrics_id(object.getLong("lyrics_id"));
            if (object.has("album_id")) audio.setAlbum_id(object.getLong("album_id"));
            if (object.has("date")) audio.setDate(object.getLong("date"));
            if (object.has("artist")) audio.setArtist(object.getString("artist"));
            if (object.has("title")) audio.setTitle(object.getString("title"));
            if (object.has("url")) audio.setUrl(object.getString("url"));
            if (object.has("genre_id")) {
                for (Genre genre_id : Genre.values()) {
                    if (genre_id.ordinal() == object.getInt("genre_id")) {
                        audio.setGenre_id(genre_id);
                        break;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return audio;
    }

    public long getLyrics_id() {
        return lyrics_id;
    }

    public void setLyrics_id(long lyrics_id) {
        this.lyrics_id = lyrics_id;
    }

    public long getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(long album_id) {
        this.album_id = album_id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Genre getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(Genre genre_id) {
        this.genre_id = genre_id;
    }

    public void setIsPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdP() {
        return getOwner_id() + "_" + getId();
    }

    enum Genre {
        Rock,
        Pop,
        Rap,
        Easy,
        Dance,
        Instrumental,
        Metal,
        Alternative,
        Dubstep,
        Jazz,
        DrumBass,
        Trance,
        Chanson,
        Ethnic,
        AcousticVocal,
        Reggae,
        Classical,
        IndiePop,
        Speech,
        Electropop,
        Other
    }

}
