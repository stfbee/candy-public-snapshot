package ru.kadabeld.candy.entities;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.LongSparseArray;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.account.AccountInfo;

/**
 * User: Vlad
 * Date: 04.09.2015
 * Time: 11:47
 */
@SuppressWarnings({"SpellCheckingInspection", "unused"})
public class User extends AbstractProfile {
    private static final String TAG = User.class.getSimpleName();

    private long id;
    private int followers_count;
    private int common_count;
    private int timezone;

    //enums
    private Deactivated deactivated = Deactivated.NONE;
    private Sex sex = Sex.NONE;
    private Relation relation = Relation.NONE;
    private FriendStatus friend_status = FriendStatus.NOPE;

    //strings
    private String photo_id = "";
    private String bdate = "";
    private String home_town = "";
    private String photo_50 = "";
    private String photo_100 = "";
    private String photo_200_orig = "";
    private String photo_200 = "";
    private String photo_400_orig = "";
    private String photo_max = "";
    private String photo_max_orig = "";
    private String domain = "";
    private String site = "";
    private String status = "";
    private String activities = "";
    private String interests = "";
    private String music = "";
    private String movies = "";
    private String tv = "";
    private String books = "";
    private String games = "";
    private String about = "";
    private String quotes = "";
    private String mail = "";
    private String nickname = "";
    private String screen_name = "";

    //bool
    private boolean hidden;
    private boolean verified;
    private boolean blacklisted;
    private boolean is_hidden_from_feed;
    private boolean blacklisted_by_me;
    private boolean has_photo;
    private boolean online;
    private boolean online_mobile;
    private boolean online_app;
    private boolean has_mobile;
    private boolean wall_comments;
    private boolean can_post;
    private boolean can_see_all_posts;
    private boolean can_see_audio;
    private boolean can_write_private_message;
    private boolean can_send_friend_request;
    private boolean is_favorite;
    private boolean isAvatarDefault = true;
    private boolean is_friend;

    //lists
    private HashMap<NameCase, String> first_names = new HashMap<>();
    private HashMap<NameCase, String> last_names = new HashMap<>();
    private ArrayList<Long> lists = new ArrayList<>();
    private ArrayList<University> universities = new ArrayList<>();
    private ArrayList<School> schools = new ArrayList<>();
    private ArrayList<Relative> relatives = new ArrayList<>();
    private ArrayList<Career> career = new ArrayList<>();
    private ArrayList<Military> military = new ArrayList<>();
    private LongSparseArray<Long> chat_invited = new LongSparseArray<>();

    //objects
    private City city = new City();
    private Education education = new Education();
    private Country country = new Country();
    private Contacts contacts = new Contacts();
    private LastSeen last_seen = new LastSeen();
    private Counters counters = new Counters();
    private Occupation occupation = new Occupation();
    private Personal personal = new Personal();
    private RelationPartner relation_partner = new RelationPartner();
    private Audio status_audio = new Audio();

    public User() {
        first_names.put(NameCase.NOM, "");
        first_names.put(NameCase.ABL, "");
        first_names.put(NameCase.ACC, "");
        first_names.put(NameCase.DAT, "");
        first_names.put(NameCase.GEN, "");
        first_names.put(NameCase.INS, "");
        last_names.put(NameCase.NOM, "");
        last_names.put(NameCase.ABL, "");
        last_names.put(NameCase.ACC, "");
        last_names.put(NameCase.DAT, "");
        last_names.put(NameCase.GEN, "");
        last_names.put(NameCase.INS, "");
    }

    @Override
    public String toString() {
        return getId() + " @ " + getName();
    }

    public static User parseUser(JSONObject object) {
        User user = null;

        try {
            // id
            long id = 0;
            if (object.has("id")) id = object.getLong("id");
            user = (User) Cache.getAbstractProfile(id);
            if (user == null) user = new User();

            user.setId(id);

            if (id < -2000000000) {
                user.setName("@");
            }

            // names
            if (object.has("first_name"))
                user.setName(NameCase.NOM, object.getString("first_name"));
            if (object.has("first_name_nom"))
                user.setName(NameCase.NOM, object.getString("first_name_nom"));
            if (object.has("first_name_abl"))
                user.setName(NameCase.ABL, object.getString("first_name_abl"));
            if (object.has("first_name_acc"))
                user.setName(NameCase.ACC, object.getString("first_name_acc"));
            if (object.has("first_name_dat"))
                user.setName(NameCase.DAT, object.getString("first_name_dat"));
            if (object.has("first_name_gen"))
                user.setName(NameCase.GEN, object.getString("first_name_gen"));
            if (object.has("first_name_ins"))
                user.setName(NameCase.INS, object.getString("first_name_ins"));
            if (object.has("last_name"))
                user.setSurname(NameCase.NOM, object.getString("last_name"));
            if (object.has("last_name_nom"))
                user.setSurname(NameCase.NOM, object.getString("last_name_nom"));
            if (object.has("last_name_abl"))
                user.setSurname(NameCase.ABL, object.getString("last_name_abl"));
            if (object.has("last_name_acc"))
                user.setSurname(NameCase.ACC, object.getString("last_name_acc"));
            if (object.has("last_name_dat"))
                user.setSurname(NameCase.DAT, object.getString("last_name_dat"));
            if (object.has("last_name_gen"))
                user.setSurname(NameCase.GEN, object.getString("last_name_gen"));
            if (object.has("last_name_ins"))
                user.setSurname(NameCase.INS, object.getString("last_name_ins"));


            if (object.has("photo_50")) user.setPhoto_50(object.getString("photo_50"));
            if (object.has("photo_rec")) user.setPhoto_50(object.getString("photo_rec"));
            if (object.has("photo_100")) user.setPhoto_100(object.getString("photo_100"));
            if (object.has("photo_medium_rec"))
                user.setPhoto_100(object.getString("photo_medium_rec"));
            if (object.has("photo_200_orig"))
                user.setPhoto_200_orig(object.getString("photo_200_orig"));
            if (object.has("photo_200")) user.setPhoto_200(object.getString("photo_200"));
            if (object.has("photo_big")) user.setPhoto_200(object.getString("photo_big"));
            if (object.has("photo_400_orig"))
                user.setPhoto_400_orig(object.getString("photo_400_orig"));
            if (object.has("photo_max")) user.setPhoto_max(object.getString("photo_max"));
            if (object.has("photo_max_orig"))
                user.setPhoto_max_orig(object.getString("photo_max_orig"));

            // strings
            if (object.has("deactivated")) {
                for (Deactivated deactivated : Deactivated.values()) {
                    user.setDeactivated(Deactivated.valueOf(object.getString("deactivated").toUpperCase()));
                }
            }
            if (object.has("photo_id")) user.setPhoto_id(object.getString("photo_id"));
            if (object.has("home_town")) user.setHome_town(object.getString("home_town"));
            if (object.has("activities")) user.setActivities(object.getString("activities"));
            if (object.has("interests")) user.setInterests(object.getString("interests"));
            if (object.has("music")) user.setMusic(object.getString("music"));
            if (object.has("movies")) user.setMovies(object.getString("movies"));
            if (object.has("tv")) user.setTv(object.getString("tv"));
            if (object.has("books")) user.setBooks(object.getString("books"));
            if (object.has("games")) user.setGames(object.getString("games"));
            if (object.has("about")) user.setAbout(object.getString("about"));
            if (object.has("quotes")) user.setQuotes(object.getString("quotes"));
            if (object.has("domain")) user.setDomain(object.getString("domain"));
            if (object.has("site")) user.setSite(object.getString("site"));
            if (object.has("status"))
                user.setStatus(object.getString("status"));
            if (object.has("status_audio"))
                user.setStatus_audio(Audio.parseAudio(object.getJSONObject("status_audio")));


            if (object.has("bdate")) user.setBdate(object.getString("bdate"));
            if (object.has("screen_name")) user.setScreen_name(object.getString("screen_name"));
            if (object.has("nickname")) user.setNickname(object.getString("nickname"));
            if (object.has("note")) user.setNote(object.getString("note"));

            // booleans
            if (object.has("hidden")) user.setHidden(object.getInt("hidden") == 1);
            if (object.has("verified")) user.setVerified(object.getInt("verified") == 1);
            if (object.has("blacklisted")) user.setBlacklisted(object.getInt("blacklisted") == 1);
            if (object.has("has_photo")) user.setHas_photo(object.getInt("has_photo") == 1);
            if (object.has("blacklisted_by_me"))
                user.setBlacklisted_by_me(object.getInt("blacklisted_by_me") == 1);
            if (object.has("is_hidden_from_feed"))
                user.setIs_hidden_from_feed(object.getInt("is_hidden_from_feed") == 1);
            if (object.has("online")) user.setOnline(object.getInt("online") == 1);
            if (object.has("online_mobile"))
                user.setOnline_mobile(object.getInt("online_mobile") == 1);
            if (object.has("online_app")) user.setOnline_app(object.getInt("online_app") == 1);
            if (object.has("has_mobile")) user.setHas_mobile(object.getInt("has_mobile") == 1);
            if (object.has("wall_comments"))
                user.setWall_comments(object.getInt("wall_comments") == 1);
            if (object.has("can_post")) user.setCan_post(object.getInt("can_post") == 1);
            if (object.has("can_see_all_posts"))
                user.setCan_see_all_posts(object.getInt("can_see_all_posts") == 1);
            if (object.has("can_see_audio"))
                user.setCan_see_audio(object.getInt("can_see_audio") == 1);
            if (object.has("can_write_private_message"))
                user.setCan_write_private_message(object.getInt("can_write_private_message") == 1);
            if (object.has("can_send_friend_request"))
                user.setCan_send_friend_request(object.getInt("can_send_friend_request") == 1);
            if (object.has("is_favorite"))
                user.setIs_favorite(object.getInt("is_favorite") == 1);

            // ints
            if (object.has("timezone")) user.setTimezone(object.getInt("timezone"));
            if (object.has("followers_count"))
                user.setFollowers_count(object.getInt("followers_count"));
            if (object.has("common_count")) user.setCommon_count(object.getInt("common_count"));

            if (object.has("lists")) {
                JSONArray listsObjects = object.getJSONArray("lists");
                for (int i = 0; i < listsObjects.length(); i++) {
                    user.lists.add(listsObjects.getLong(i));
                }
            }

            if (object.has("universities")) {
                JSONArray universitiesObjects = object.getJSONArray("universities");
                for (int i = 0; i < universitiesObjects.length(); i++) {
                    user.universities.add(University.parseUniversity(universitiesObjects.getJSONObject(i)));
                }
            }
            if (object.has("schools")) {
                JSONArray schoolsObjects = object.getJSONArray("schools");
                for (int i = 0; i < schoolsObjects.length(); i++) {
                    user.schools.add(School.parseSchool(schoolsObjects.getJSONObject(i)));
                }
            }
            if (object.has("relatives")) {
                JSONArray relativesObjects = object.getJSONArray("relatives");
                for (int i = 0; i < relativesObjects.length(); i++) {
                    user.relatives.add(Relative.parseRelative(relativesObjects.getJSONObject(i)));
                }
            }
            if (object.has("career")) {
                JSONArray careerObjects = object.getJSONArray("career");
                for (int i = 0; i < careerObjects.length(); i++) {
                    user.career.add(Career.parseCareer(careerObjects.getJSONObject(i)));
                }
            }
            if (object.has("military")) {
                JSONArray militaryObjects = object.getJSONArray("military");
                for (int i = 0; i < militaryObjects.length(); i++) {
                    user.military.add(Military.parseMilitary(militaryObjects.getJSONObject(i)));
                }
            }

            // enums
            if (object.has("sex")) {
                for (Sex sex : Sex.values()) {
                    if (sex.ordinal() == object.getInt("sex")) {
                        user.setSex(sex);
                        break;
                    }
                }
            }
            if (object.has("relation")) {
                for (Relation relation : Relation.values()) {
                    if (relation.ordinal() == object.getInt("relation")) {
                        user.setRelation(relation);
                        break;
                    }
                }
            }
            if (object.has("friend_status")) {
                for (FriendStatus friend_status : FriendStatus.values()) {
                    if (friend_status.ordinal() == object.getInt("friend_status") - 1) {
                        user.setFriend_status(friend_status);
                        break;
                    }
                }
            }

            // objects
            if (object.has("city")) {
                user.setCity(City.parseCity(object.getJSONObject("city")));
            }
            if (object.has("country")) {
                user.setCountry(Country.parseCountry(object.getJSONObject("country")));
            }
            if (object.has("education")) {
                user.setEducation(Education.parseEducation(object.getJSONObject("education")));
            }
            if (object.has("last_seen")) {
                user.setLast_seen(LastSeen.parseLastSeen(object.getJSONObject("last_seen")));
            }
            if (object.has("counters")) {
                user.setCounters(Counters.parseCounters(object.getJSONObject("counters")));
            }
            if (object.has("occupation")) {
                user.setOccupation(Occupation.parseOccupation(object.getJSONObject("occupation")));
            }
            if (object.has("personal") && object.get("personal") instanceof JSONObject) {
                user.setPersonal(Personal.parsePersonal(object.getJSONObject("personal")));
            }
            if (object.has("relation_partner")) {
                user.setRelation_partner(RelationPartner.parseRelationPartner(object.getJSONObject("relation_partner")));
            }

            Contacts contacts = new Contacts();
            if (object.has("mobile_phone"))
                contacts.setMobile_phone(object.getString("mobile_phone"));
            if (object.has("home_phone"))
                contacts.setHome_phone(object.getString("home_phone"));
            if (object.has("skype"))
                contacts.setSkype(object.getString("skype"));
            if (object.has("facebook"))
                contacts.setFacebook(object.getString("facebook"));
            if (object.has("facebook_name"))
                contacts.setFacebook_name(object.getString("facebook_name"));
            if (object.has("twitter"))
                contacts.setTwitter(object.getString("twitter"));
            if (object.has("instagram"))
                contacts.setInstagram(object.getString("instagram"));
            user.setContacts(contacts);
        } catch (JSONException e) {
            e.printStackTrace();
            System.out.println(object);
        }
        Cache.putAbstractProfile(user);
        return user;
    }

    /**
     * Загружает юзера и выполняет
     *
     * @param runnable код
     */
    public static void load(long user_id, final ProfileOnLoad runnable) {
        RequestParams params = new RequestParams();
        params.put("user_ids", user_id);
        params.put("fields", API.userFields);
        params.put("v", API.version);
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE_without_setonline + API.usersGet, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    if (object.has("response")) {
                        JSONArray response = object.getJSONArray("response");
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject userObject = response.getJSONObject(i);
                            User p = User.parseUser(userObject);
                            runnable.run(p);
                        }
                    } else {
                        Thread.sleep(1000);
                        sendRetryMessage(10);
                    }
                } catch (JSONException e) {
                    Log.e("NET", object.toString());
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static User parseUser(JSONObject object, long chat_id) {
        User _user = parseUser(object);
        try {
            if (object.has("invited_by")) {
                _user.addInvation(chat_id, object.getLong("invited_by"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            System.out.println(object);
        }
        return _user;
    }

    public void addInvation(long chat_id, long date) {
        chat_invited.put(chat_id, date);
    }

    public long getInvation(long chat_id) {
        return chat_invited.get(chat_id, 0L);
    }

    public boolean isCan_send_friend_request() {
        return can_send_friend_request;
    }

    public void setCan_send_friend_request(boolean can_send_friend_request) {
        this.can_send_friend_request = can_send_friend_request;
    }

    public boolean is_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(boolean is_favorite) {
        this.is_favorite = is_favorite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public HashMap<NameCase, String> getFirst_names() {
        return first_names;
    }

    public void setFirst_names(HashMap<NameCase, String> first_names) {
        this.first_names = first_names;
    }

    public HashMap<NameCase, String> getLast_names() {
        return last_names;
    }

    public void setLast_names(HashMap<NameCase, String> last_names) {
        this.last_names = last_names;
    }

    public Deactivated getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(Deactivated deactivated) {
        this.deactivated = deactivated;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(String photo_id) {
        this.photo_id = photo_id;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(boolean blacklisted) {
        this.blacklisted = blacklisted;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getBdate() {
        return bdate;
    }

    public void setBdate(String bdate) {
        this.bdate = bdate;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getHome_town() {
        return home_town;
    }

    public void setHome_town(String home_town) {
        this.home_town = home_town;
    }

    public String getPhoto_50() {
        return photo_50;
    }

    public void setPhoto_50(String photo_50) {
        this.photo_50 = photo_50;
        isAvatarDefault = (photo_50.contains("camera_") || photo_100.contains("contact_info") || photo_50.contains(".gif")) && !photo_50.contains("deactivated");
    }

    public String getPhoto_100() {
        return photo_100;
    }

    public void setPhoto_100(String photo_100) {
        this.photo_100 = photo_100;
        isAvatarDefault = (photo_100.contains("camera_") || photo_100.contains("contact_info") || photo_100.contains(".gif")) && !photo_100.contains("deactivated");
    }

    public String getPhoto_200_orig() {
        return photo_200_orig;
    }

    public void setPhoto_200_orig(String photo_200_orig) {
        this.photo_200_orig = photo_200_orig;
        isAvatarDefault = (photo_200_orig.contains("camera_") || photo_100.contains("contact_info") || photo_200_orig.contains(".gif")) && !photo_200_orig.contains("deactivated");
    }

    public String getPhoto_200() {
        return photo_200;
    }

    public void setPhoto_200(String photo_200) {
        this.photo_200 = photo_200;
        isAvatarDefault = (photo_200.contains("camera_") || photo_100.contains("contact_info") || photo_200.contains(".gif")) && !photo_200.contains("deactivated");
    }

    public String getPhotoFor(int size) {
        if (size > 400) if (!photo_max_orig.isEmpty()) return photo_max_orig;
        if (size > 200) if (!photo_400_orig.isEmpty()) return photo_400_orig;
        if (size > 100) if (!photo_200.isEmpty()) return photo_200;
        if (size > 50) if (!photo_100.isEmpty()) return photo_100;
        return photo_50;
    }

    public String getPhoto_400_orig() {
        return photo_400_orig;
    }

    public void setPhoto_400_orig(String photo_400_orig) {
        this.photo_400_orig = photo_400_orig;
        isAvatarDefault = (photo_400_orig.contains("camera_") || photo_100.contains("contact_info") || photo_400_orig.contains(".gif")) && !photo_400_orig.contains("deactivated");
    }

    public String getPhoto_max() {
        return photo_max;
    }

    public void setPhoto_max(String photo_max) {
        this.photo_max = photo_max;
        isAvatarDefault = (photo_max.contains("camera_") || photo_100.contains("contact_info") || photo_max.contains(".gif")) && !photo_max.contains("deactivated");
    }

    public String getPhoto_max_orig() {
        return photo_max_orig;
    }

    public void setPhoto_max_orig(String photo_max_orig) {
        this.photo_max_orig = photo_max_orig;
        isAvatarDefault = (photo_max_orig.contains("camera_") || photo_100.contains("contact_info") || photo_max_orig.contains(".gif")) && !photo_max_orig.contains("deactivated");
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public boolean isOnline_mobile() {
        return online_mobile;
    }

    public void setOnline_mobile(boolean online_mobile) {
        this.online_mobile = online_mobile;
    }

    public boolean isOnline_app() {
        return online_app;
    }

    public void setOnline_app(boolean online_app) {
        this.online_app = online_app;
    }

    public ArrayList<Long> getLists() {
        return lists;
    }

    public void setLists(ArrayList<Long> lists) {
        this.lists = lists;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isHas_mobile() {
        return has_mobile;
    }

    public void setHas_mobile(boolean has_mobile) {
        this.has_mobile = has_mobile;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public ArrayList<University> getUniversities() {
        return universities;
    }

    public void setUniversities(ArrayList<University> universities) {
        this.universities = universities;
    }

    public ArrayList<School> getSchools() {
        return schools;
    }

    public void setSchools(ArrayList<School> schools) {
        this.schools = schools;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LastSeen getLast_seen() {
        return last_seen;
    }

    public void setLast_seen(LastSeen last_seen) {
        this.last_seen = last_seen;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public int getCommon_count() {
        return common_count;
    }

    public void setCommon_count(int common_count) {
        this.common_count = common_count;
    }

    public Counters getCounters() {
        return counters;
    }

    public void setCounters(Counters counters) {
        this.counters = counters;
    }

    public Occupation getOccupation() {
        return occupation;
    }

    public void setOccupation(Occupation occupation) {
        this.occupation = occupation;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public ArrayList<Relative> getRelatives() {
        return relatives;
    }

    public void setRelatives(ArrayList<Relative> relatives) {
        this.relatives = relatives;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }

    public ArrayList<Career> getCareer() {
        return career;
    }

    public void setCareer(ArrayList<Career> career) {
        this.career = career;
    }

    public ArrayList<Military> getMilitary() {
        return military;
    }

    public void setMilitary(ArrayList<Military> military) {
        this.military = military;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public boolean isWall_comments() {
        return wall_comments;
    }

    public void setWall_comments(boolean wall_comments) {
        this.wall_comments = wall_comments;
    }

    public String getActivities() {
        return activities;
    }

    public void setActivities(String activities) {
        this.activities = activities;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getMovies() {
        return movies;
    }

    public void setMovies(String movies) {
        this.movies = movies;
    }

    public String getTv() {
        return tv;
    }

    public void setTv(String tv) {
        this.tv = tv;
    }

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }

    public String getGames() {
        return games;
    }

    public void setGames(String games) {
        this.games = games;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getQuotes() {
        return quotes;
    }

    public void setQuotes(String quotes) {
        this.quotes = quotes;
    }

    public boolean isCan_post() {
        return can_post;
    }

    public void setCan_post(boolean can_post) {
        this.can_post = can_post;
    }

    public boolean isCan_see_all_posts() {
        return can_see_all_posts;
    }

    public void setCan_see_all_posts(boolean can_see_all_posts) {
        this.can_see_all_posts = can_see_all_posts;
    }

    public boolean isCan_see_audio() {
        return can_see_audio;
    }

    public void setCan_see_audio(boolean can_see_audio) {
        this.can_see_audio = can_see_audio;
    }

    public boolean isCan_write_private_message() {
        return can_write_private_message;
    }

    public void setCan_write_private_message(boolean can_write_private_message) {
        this.can_write_private_message = can_write_private_message;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public FriendStatus getFriend_status() {
        return friend_status;
    }

    public void setFriend_status(FriendStatus friend_status) {
        this.friend_status = friend_status;
    }

    public RelationPartner getRelation_partner() {
        return relation_partner;
    }

    public void setRelation_partner(RelationPartner relation_partner) {
        this.relation_partner = relation_partner;
    }

    @Deprecated
    public boolean isAvatarDefault() {
        return isAvatarDefault;
    }

    public void setIsAvatarDefault(boolean isAvatarDefault) {
        this.isAvatarDefault = isAvatarDefault;
    }

    public String getFullName() {
        return getName() + ((getSurname() == null) ? "" : " " + getSurname());
    }

    public String getFullName(NameCase nameCase) {
        return getName(nameCase) + ((getSurname(nameCase) == null) ? "" : " " + getSurname(nameCase));
    }

    public void setName(NameCase nameCase, String name) {
        this.first_names.put(nameCase, name);
    }

    public void setSurname(NameCase nameCase, String surname) {
        this.last_names.put(nameCase, surname);
    }

    public Drawable getAvatarDrawable() {
        ColorGenerator colorGenerator = ColorGenerator.MATERIAL;
        return TextDrawable.builder().buildRound((getName().isEmpty() ? "?" : getName().substring(0, 1)) + (getSurname().isEmpty() ? "?" : getSurname().substring(0, 1)), colorGenerator.getColor(id));
    }

    public Drawable getAvatarDrawable(int color) {
        return TextDrawable.builder().buildRound((getName().isEmpty() ? "?" : getName().substring(0, 1)) + (getSurname().isEmpty() ? "?" : getSurname().substring(0, 1)), color);
    }

    public String getSurname() {
        return getSurname(NameCase.NOM);
    }

    public void setSurname(String surname) {
        setSurname(NameCase.NOM, surname);
    }

    public String getSurname(NameCase nameCase) {
        return last_names.get(nameCase);
    }

    public String getName() {
        return getName(NameCase.NOM);
    }

    public void setName(String name) {
        setName(NameCase.NOM, name);
    }

    public String getName(NameCase nameCase) {
        return first_names.get(nameCase);
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
        this.setName(NameCase.GEN, mail);
        this.setName(NameCase.NOM, mail);
        this.setName(NameCase.DAT, mail);
        this.setName(NameCase.ACC, mail);
        this.setName(NameCase.INS, mail);
        this.setName(NameCase.ABL, mail);
//        setName(mail);
    }

    public void setIs_hidden_from_feed(boolean is_hidden_from_feed) {
        this.is_hidden_from_feed = is_hidden_from_feed;
    }

    public boolean isIs_hidden_from_feed() {
        return is_hidden_from_feed;
    }

    public boolean isBlacklisted_by_me() {
        return blacklisted_by_me;
    }

    public void setBlacklisted_by_me(boolean blacklisted_by_me) {
        this.blacklisted_by_me = blacklisted_by_me;
    }

    public void setHas_photo(boolean has_photo) {
        this.has_photo = has_photo;
    }

    public boolean isHas_photo() {
        return has_photo;
    }

    public boolean is_friend() {
        return is_friend;
    }

    public void setIs_friend(boolean is_friend) {
        this.is_friend = is_friend;
    }

    public void setStatus_audio(Audio status_audio) {
        this.status_audio = status_audio;
    }

    public Audio getStatus_audio() {
        return status_audio;
    }

    public static User fromAccountInfo(AccountInfo info) {
        User user = (User) Cache.getNullableAbstractProfile(info.getUserId());
        if (user == null) {
            user = new User();
            user.setId(info.getUserId());
            user.setPhoto_200(info.getPhoto200());
            user.setName(info.getFirstName());
            user.setSurname(info.getLastName());
            Cache.putAbstractProfile(user);
        }
        return user;
    }

    public enum NameCase {NOM, GEN, DAT, ACC, INS, ABL}

    public enum Sex {NONE, FEMALE, MALE}

    public enum Relation {NONE, SINGLE, RELATIONSHIP, ENGAGED, MARRIED, COMPLICATED, SEARCHING, LOVE}

    public enum FriendStatus {NOPE, SEND, YEP}

    public static class Personal {
        private String religion = "";
        private String inspired_by = "";
        private ArrayList<String> langs = new ArrayList<>();
        private PeopleMain people_main = PeopleMain.NONE;
        private LifeMain life_main = LifeMain.NONE;
        private Political political = Political.NONE;
        private ViewOn smoking = ViewOn.NONE;
        private ViewOn alcohol = ViewOn.NONE;

        public static Personal parsePersonal(JSONObject object) {
            Personal personal = new Personal();
            try {
                if (object.has("religion")) personal.setReligion(object.getString("religion"));
                if (object.has("inspired_by"))
                    personal.setInspired_by(object.getString("inspired_by"));

                if (object.has("langs")) {
                    JSONArray langsObjects = object.getJSONArray("langs");
                    for (int i = 0; i < langsObjects.length(); i++) {
                        personal.langs.add(langsObjects.getString(i));
                    }
                }

                if (object.has("people_main")) {
                    for (PeopleMain people_main : PeopleMain.values()) {
                        if (people_main.ordinal() == object.getInt("people_main")) {
                            personal.setPeople_main(people_main);
                            break;
                        }
                    }
                }

                if (object.has("life_main")) {
                    for (LifeMain life_main : LifeMain.values()) {
                        if (life_main.ordinal() == object.getInt("life_main")) {
                            personal.setLife_main(life_main);
                            break;
                        }
                    }
                }

                if (object.has("political")) {
                    for (Political political : Political.values()) {
                        if (political.ordinal() == object.getInt("political")) {
                            personal.setPolitical(political);
                            break;
                        }
                    }
                }

                if (object.has("smoking")) {
                    for (ViewOn smoking : ViewOn.values()) {
                        if (smoking.ordinal() == object.getInt("smoking")) {
                            personal.setSmoking(smoking);
                            break;
                        }
                    }
                }

                if (object.has("alcohol")) {
                    for (ViewOn alcohol : ViewOn.values()) {
                        if (alcohol.ordinal() == object.getInt("alcohol")) {
                            personal.setAlcohol(alcohol);
                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return personal;
        }

        public PeopleMain getPeople_main() {
            return people_main;
        }

        public void setPeople_main(PeopleMain people_main) {
            this.people_main = people_main;
        }

        public LifeMain getLife_main() {
            return life_main;
        }

        public void setLife_main(LifeMain life_main) {
            this.life_main = life_main;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getInspired_by() {
            return inspired_by;
        }

        public void setInspired_by(String inspired_by) {
            this.inspired_by = inspired_by;
        }

        public ArrayList<String> getLangs() {
            return langs;
        }

        public void setLangs(ArrayList<String> langs) {
            this.langs = langs;
        }

        public Political getPolitical() {
            return political;
        }

        public void setPolitical(Political political) {
            this.political = political;
        }

        public ViewOn getSmoking() {
            return smoking;
        }

        public void setSmoking(ViewOn smoking) {
            this.smoking = smoking;
        }

        public ViewOn getAlcohol() {
            return alcohol;
        }

        public void setAlcohol(ViewOn alcohol) {
            this.alcohol = alcohol;
        }

        public enum LifeMain {
            NONE,
            FAMILYANDCHILDREN,
            CAREERANDMONEY,
            ENTERTAINMENTANDLEISURE,
            SCIENCEANDRESEARCH,
            IMPROVINGTHEWORLD,
            PERSONALDEVELOPMEN,
            BEAUTYANDART,
            FAMEANDINFLUENCE,
        }

        public enum PeopleMain {
            NONE,
            INTELLECTANDCREATIVITY,
            KINDNESSANDHONESTY,
            HEALTHANDBEAUTY,
            WEALTHANDPOWER,
            COURAGEANDPERSISTANCE,
            HUMORANDLOVEFORLIFE,
        }

        public enum ViewOn {NONE, VERYNEGATIVE, NEGATIVE, NEUTRAL, COMPROMISABLE, POSITIVE}

        public enum Political {NONE, COMMUNIST, SOCIALIST, MODERATE, LIBERAL, CONSERVATIVE, MONARCHIST, ULTRACONSERVATIVE, APATHETIC, LIBERTIAN}
    }

    public static class Occupation {
        private Type type = Type.NOPE;
        private long id;
        private String name = "";

        public static Occupation parseOccupation(JSONObject object) {
            Occupation occupation = new Occupation();
            try {
                if (object.has("id")) occupation.setId(object.getLong("id"));
                if (object.has("name")) occupation.setName(object.getString("name"));
                if (object.has("type")) {
                    occupation.setType(Type.valueOf(object.getString("type").toUpperCase()));
                }
            } catch (JSONException e) {
                Log.d(TAG, object.toString());
                e.printStackTrace();
            }
            return occupation;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public enum Type {
            WORK, SCHOOL, UNIVERSITY, NOPE
        }
    }

    public static class Education {
        private long university;
        private String university_name = "";
        private long faculty;
        private String faculty_name = "";
        private int graduation;

        public static Education parseEducation(JSONObject object) {
            Education education = new Education();
            try {
                if (object.has("university")) education.setUniversity(object.getLong("university"));
                if (object.has("university_name"))
                    education.setUniversity_name(object.getString("university_name"));
                if (object.has("faculty")) education.setFaculty(object.getLong("faculty"));
                if (object.has("faculty_name"))
                    education.setFaculty_name(object.getString("faculty_name"));
                if (object.has("graduation")) education.setGraduation(object.getInt("graduation"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return education;
        }

        public long getUniversity() {
            return university;
        }

        public void setUniversity(long university) {
            this.university = university;
        }

        public String getUniversity_name() {
            return university_name;
        }

        public void setUniversity_name(String university_name) {
            this.university_name = university_name;
        }

        public long getFaculty() {
            return faculty;
        }

        public void setFaculty(long faculty) {
            this.faculty = faculty;
        }

        public String getFaculty_name() {
            return faculty_name;
        }

        public void setFaculty_name(String faculty_name) {
            this.faculty_name = faculty_name;
        }

        public int getGraduation() {
            return graduation;
        }

        public void setGraduation(int graduation) {
            this.graduation = graduation;
        }
    }

    public static class Counters {
        private int albums;
        private int videos;
        private int audios;
        private int photos;
        private int notes;
        private int friends;
        private int groups;
        private int online_friends;
        private int mutual_friends;
        private int user_videos;
        private int followers;
        private int user_photos;
        private int subscriptions;
        private int gifts;
        private int pages;

        public static Counters parseCounters(JSONObject object) {
            Counters counters = new Counters();
            try {
                if (object.has("albums")) counters.setAlbums(object.getInt("albums"));
                if (object.has("videos")) counters.setVideos(object.getInt("videos"));
                if (object.has("audios")) counters.setAudios(object.getInt("audios"));
                if (object.has("photos")) counters.setPhotos(object.getInt("photos"));
                if (object.has("gifts")) counters.setGifts(object.getInt("gifts"));
                if (object.has("pages")) counters.setPages(object.getInt("pages"));
                if (object.has("notes")) counters.setNotes(object.getInt("notes"));
                if (object.has("friends")) counters.setFriends(object.getInt("friends"));
                if (object.has("groups")) counters.setGroups(object.getInt("groups"));
                if (object.has("online_friends"))
                    counters.setOnline_friends(object.getInt("online_friends"));
                if (object.has("mutual_friends"))
                    counters.setMutual_friends(object.getInt("mutual_friends"));
                if (object.has("user_videos"))
                    counters.setUser_videos(object.getInt("user_videos"));
                if (object.has("followers")) counters.setFollowers(object.getInt("followers"));
                if (object.has("user_photos"))
                    counters.setUser_photos(object.getInt("user_photos"));
                if (object.has("subscriptions"))
                    counters.setSubscriptions(object.getInt("subscriptions"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return counters;
        }

        public int getGifts() {
            return gifts;
        }

        public void setGifts(int gifts) {
            this.gifts = gifts;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getAlbums() {
            return albums;
        }

        public void setAlbums(int albums) {
            this.albums = albums;
        }

        public int getVideos() {
            return videos;
        }

        public void setVideos(int videos) {
            this.videos = videos;
        }

        public int getAudios() {
            return audios;
        }

        public void setAudios(int audios) {
            this.audios = audios;
        }

        public int getPhotos() {
            return photos;
        }

        public void setPhotos(int photos) {
            this.photos = photos;
        }

        public int getNotes() {
            return notes;
        }

        public void setNotes(int notes) {
            this.notes = notes;
        }

        public int getFriends() {
            return friends;
        }

        public void setFriends(int friends) {
            this.friends = friends;
        }

        public int getGroups() {
            return groups;
        }

        public void setGroups(int groups) {
            this.groups = groups;
        }

        public int getOnline_friends() {
            return online_friends;
        }

        public void setOnline_friends(int online_friends) {
            this.online_friends = online_friends;
        }

        public int getMutual_friends() {
            return mutual_friends;
        }

        public void setMutual_friends(int mutual_friends) {
            this.mutual_friends = mutual_friends;
        }

        public int getUser_videos() {
            return user_videos;
        }

        public void setUser_videos(int user_videos) {
            this.user_videos = user_videos;
        }

        public int getFollowers() {
            return followers;
        }

        public void setFollowers(int followers) {
            this.followers = followers;
        }

        public int getUser_photos() {
            return user_photos;
        }

        public void setUser_photos(int user_photos) {
            this.user_photos = user_photos;
        }

        public int getSubscriptions() {
            return subscriptions;
        }

        public void setSubscriptions(int subscriptions) {
            this.subscriptions = subscriptions;
        }
    }

    public static class Career {
        private long group_id;
        private String company = "";

        private String city_name = "";
        private long country_id;
        private long city_id;
        private int from;

        private int until;
        private String position = "";

        public static Career parseCareer(JSONObject object) {
            Career career = new Career();
            try {
                if (object.has("group_id")) career.setGroup_id(object.getLong("group_id"));
                if (object.has("company")) career.setCompany(object.getString("company"));
                if (object.has("city_name")) career.setCity_name(object.getString("city_name"));
                if (object.has("country_id")) career.setCountry_id(object.getLong("country_id"));
                if (object.has("city_id")) career.setCity_id(object.getLong("city_id"));
                if (object.has("from")) career.setFrom(object.getInt("from"));
                if (object.has("until")) career.setUntil(object.getInt("until"));
                if (object.has("position")) career.setPosition(object.getString("position"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return career;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public int getUntil() {
            return until;
        }

        public void setUntil(int until) {
            this.until = until;
        }

        public long getGroup_id() {
            return group_id;
        }

        public void setGroup_id(long group_id) {
            this.group_id = group_id;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public long getCountry_id() {
            return country_id;
        }

        public void setCountry_id(long country_id) {
            this.country_id = country_id;
        }

        public long getCity_id() {
            return city_id;
        }

        public void setCity_id(long city_id) {
            this.city_id = city_id;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }
    }

    public static class Military {
        private String unit = "";
        private long unit_id;
        private long country_id;
        private int from;
        private int until;

        public static Military parseMilitary(JSONObject object) {
            Military military = new Military();
            try {
                if (object.has("unit")) military.setUnit(object.getString("unit"));
                if (object.has("unit_id")) military.setUnit_id(object.getLong("unit_id"));
                if (object.has("country_id")) military.setCountry_id(object.getLong("country_id"));
                if (object.has("from")) military.setFrom(object.getInt("from"));
                if (object.has("until")) military.setUntil(object.getInt("until"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return military;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public long getUnit_id() {
            return unit_id;
        }

        public void setUnit_id(long unit_id) {
            this.unit_id = unit_id;
        }

        public long getCountry_id() {
            return country_id;
        }

        public void setCountry_id(long country_id) {
            this.country_id = country_id;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getUntil() {
            return until;
        }

        public void setUntil(int until) {
            this.until = until;
        }
    }

    public static class Contacts {
        private String mobile_phone = "";
        private String home_phone = "";
        private String skype = "";
        private String facebook = "";
        private String facebook_name = "";
        private String twitter = "";
        private String instagram = "";

        public static Contacts parseContacts(JSONObject object) {
            Contacts contacts = new Contacts();
            try {
                if (object.has("mobile_phone"))
                    contacts.setMobile_phone(object.getString("mobile_phone"));
                if (object.has("home_phone"))
                    contacts.setHome_phone(object.getString("home_phone"));
                if (object.has("skype"))
                    contacts.setSkype(object.getString("skype"));
                if (object.has("facebook"))
                    contacts.setFacebook(object.getString("facebook"));
                if (object.has("facebook_name"))
                    contacts.setFacebook_name(object.getString("facebook_name"));
                if (object.has("twitter"))
                    contacts.setTwitter(object.getString("twitter"));
                if (object.has("instagram"))
                    contacts.setInstagram(object.getString("instagram"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return contacts;
        }

        public String getSkype() {
            return skype;
        }

        public void setSkype(String skype) {
            this.skype = skype;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getFacebook_name() {
            return facebook_name;
        }

        public void setFacebook_name(String facebook_name) {
            this.facebook_name = facebook_name;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }

        public String getHome_phone() {
            return home_phone;
        }

        public void setHome_phone(String home_phone) {
            this.home_phone = home_phone;
        }

        public String getMobile_phone() {
            return mobile_phone;
        }

        public void setMobile_phone(String mobile_phone) {
            this.mobile_phone = mobile_phone;
        }
    }

    public static class LastSeen {
        private long time;
        private Platform platform;

        public static LastSeen parseLastSeen(JSONObject object) {
            LastSeen lastSeen = new LastSeen();
            try {
                if (object.has("time")) lastSeen.setTime(object.getLong("time"));
                if (object.has("platform")) {
                    for (Platform platform : Platform.values()) {
                        if (platform.ordinal() == object.getInt("platform")) {
                            lastSeen.setPlatform(platform);
                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return lastSeen;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public Platform getPlatform() {
            return platform;
        }

        public void setPlatform(Platform platform) {
            this.platform = platform;
        }

        public enum Platform {NONE, MOBILE, IPHONE, IPAD, ANDROID, WPHONE, WINDOWS, WEB}
    }

    public static class Relative {
        private long id;
        private String type = "";
        private String name = "";

        public static Relative parseRelative(JSONObject object) {
            Relative relative = new Relative();
            try {
                if (object.has("id")) relative.setId(object.getLong("id"));
                if (object.has("type")) relative.setType(object.getString("type"));
                if (object.has("name")) relative.setName(object.getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return relative;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class School {
        private long id;
        private long country;
        private long city;
        private String name = "";
        private int year_from;
        private int year_to;
        private int year_graduated;
        private String class_letter = ""; //class
        private String speciality = "";
        private long type;
        private String type_str = "";

        public static School parseSchool(JSONObject object) {
            School school = new School();
            try {
                if (object.has("id")) school.setId(object.getLong("id"));
                if (object.has("country")) school.setCountry(object.getLong("country"));
                if (object.has("city")) school.setCity(object.getLong("city"));
                if (object.has("type")) school.setType(object.getLong("type"));
                if (object.has("year_from"))
                    school.setYear_from(object.getInt("year_from"));
                if (object.has("year_to")) school.setYear_to(object.getInt("year_to"));
                if (object.has("year_graduated"))
                    school.setYear_graduated(object.getInt("year_graduated"));
                if (object.has("name")) school.setName(object.getString("name"));
                if (object.has("class"))
                    school.setClass_letter(object.getString("class"));
                if (object.has("speciality"))
                    school.setSpeciality(object.getString("speciality"));
                if (object.has("type_str"))
                    school.setType_str(object.getString("type_str"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return school;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getCountry() {
            return country;
        }

        public void setCountry(long country) {
            this.country = country;
        }

        public long getCity() {
            return city;
        }

        public void setCity(long city) {
            this.city = city;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getYear_from() {
            return year_from;
        }

        public void setYear_from(int year_from) {
            this.year_from = year_from;
        }

        public int getYear_to() {
            return year_to;
        }

        public void setYear_to(int year_to) {
            this.year_to = year_to;
        }

        public int getYear_graduated() {
            return year_graduated;
        }

        public void setYear_graduated(int year_graduated) {
            this.year_graduated = year_graduated;
        }

        public String getClass_letter() {
            return class_letter;
        }

        public void setClass_letter(String class_letter) {
            this.class_letter = class_letter;
        }

        public String getSpeciality() {
            return speciality;
        }

        public void setSpeciality(String speciality) {
            this.speciality = speciality;
        }

        public long getType() {
            return type;
        }

        public void setType(long type) {
            this.type = type;
        }

        public String getType_str() {
            return type_str;
        }

        public void setType_str(String type_str) {
            this.type_str = type_str;
        }
    }

    public static class RelationPartner {
        private long id;
        private String first_name = "";
        private String last_name = "";

        public static RelationPartner parseRelationPartner(JSONObject object) {
            RelationPartner relationPartner = new RelationPartner();
            try {
                if (object.has("id")) relationPartner.setId(object.getLong("id"));
                if (object.has("first_name"))
                    relationPartner.setFirst_name(object.getString("first_name"));
                if (object.has("last_name"))
                    relationPartner.setLast_name(object.getString("last_name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return relationPartner;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }
    }

    public static class CropPhoto {
        //мне похуй, все паблик, потом передалю если надо будет
        public Photo photo;
        public Crop crop;
        public Rect rect;

        public static CropPhoto parseCropPhoto(JSONObject object) {
            CropPhoto cropPhoto = new CropPhoto();
            try {
                if (object.has("photo"))
                    cropPhoto.photo = Photo.parsePhoto(object.getJSONObject("photo"));
                if (object.has("crop"))
                    cropPhoto.crop = Crop.parseCrop(object.getJSONObject("crop"));
                if (object.has("rect"))
                    cropPhoto.rect = Rect.parseRect(object.getJSONObject("rect"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return cropPhoto;
        }

        public static class Crop {
            public double x;
            public double y;
            public double x2;
            public double y2;

            public static Crop parseCrop(JSONObject object) {
                Crop crop = new Crop();
                try {
                    if (object.has("x")) crop.x = (object.getDouble("x"));
                    if (object.has("y")) crop.y = (object.getDouble("y"));
                    if (object.has("x2")) crop.x2 = (object.getDouble("x2"));
                    if (object.has("y2")) crop.y2 = (object.getDouble("y2"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return crop;
            }
        }

        public static class Rect {
            public double x;
            public double y;
            public double x2;
            public double y2;

            public static Rect parseRect(JSONObject object) {
                Rect rect = new Rect();
                try {
                    if (object.has("x")) rect.x = (object.getDouble("x"));
                    if (object.has("y")) rect.y = (object.getDouble("y"));
                    if (object.has("x2")) rect.x2 = (object.getDouble("x2"));
                    if (object.has("y2")) rect.y2 = (object.getDouble("y2"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return rect;
            }
        }
    }

    public static class University {
        private long id;
        private long country;
        private long city;
        private String name = "";
        private long faculty;
        private String faculty_name = "";
        private long chair;
        private String chair_name = "";
        private int graduation;
        private String education_form = "";
        private String education_status = "";

        public static University parseUniversity(JSONObject object) {
            University university = new University();
            try {
                if (object.has("id")) university.setId(object.getLong("id"));
                if (object.has("country"))
                    university.setCountry(object.getLong("country"));
                if (object.has("city"))
                    university.setCity(object.getLong("city"));
                if (object.has("faculty"))
                    university.setFaculty(object.getLong("faculty"));
                if (object.has("chair"))
                    university.setChair(object.getLong("chair"));
                if (object.has("graduation"))
                    university.setGraduation(object.getInt("graduation"));
                if (object.has("name"))
                    university.setName(object.getString("name"));
                if (object.has("faculty_name"))
                    university.setFaculty_name(object.getString("faculty_name"));
                if (object.has("chair_name"))
                    university.setChair_name(object.getString("chair_name"));
                if (object.has("education_form"))
                    university.setEducation_form(object.getString("education_form"));
                if (object.has("education_status"))
                    university.setEducation_status(object.getString("education_status"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return university;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getCountry() {
            return country;
        }

        public void setCountry(long country) {
            this.country = country;
        }

        public long getCity() {
            return city;
        }

        public void setCity(long city) {
            this.city = city;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getFaculty() {
            return faculty;
        }

        public void setFaculty(long faculty) {
            this.faculty = faculty;
        }

        public String getFaculty_name() {
            return faculty_name;
        }

        public void setFaculty_name(String faculty_name) {
            this.faculty_name = faculty_name;
        }

        public long getChair() {
            return chair;
        }

        public void setChair(long chair) {
            this.chair = chair;
        }

        public String getChair_name() {
            return chair_name;
        }

        public void setChair_name(String chair_name) {
            this.chair_name = chair_name;
        }

        public int getGraduation() {
            return graduation;
        }

        public void setGraduation(int graduation) {
            this.graduation = graduation;
        }

        public String getEducation_form() {
            return education_form;
        }

        public void setEducation_form(String education_form) {
            this.education_form = education_form;
        }

        public String getEducation_status() {
            return education_status;
        }

        public void setEducation_status(String education_status) {
            this.education_status = education_status;
        }
    }
}