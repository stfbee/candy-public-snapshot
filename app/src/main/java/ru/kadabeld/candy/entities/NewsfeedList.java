package ru.kadabeld.candy.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: Vlad
 * Date: 06.09.2015
 * Time: 2:05
 */
public class NewsfeedList implements Serializable {
    private long id;
    private boolean no_reposts;
    private String title = "";
    private ArrayList<Long> source_ids = new ArrayList<>();

    public static NewsfeedList parseNewsfeedList(JSONObject object) {
        NewsfeedList newsfeedList = new NewsfeedList();
        try {
            if (object.has("id")) newsfeedList.setId(object.getLong("id"));
            if (object.has("no_reposts"))
                newsfeedList.setNo_reposts(object.getInt("no_reposts") == 1);
            if (object.has("title")) newsfeedList.setTitle(object.getString("title"));
            if (object.has("source_ids")) {
                JSONArray source_idsObjects = object.getJSONArray("source_ids");
                for (int i = 0; i < source_idsObjects.length(); i++) {
                    newsfeedList.addSource_id(source_idsObjects.getLong(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newsfeedList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Long> getSource_ids() {
        return source_ids;
    }

    public String getSource_ids_asString() {
        StringBuilder builder = new StringBuilder();
        for (Long source_id : source_ids) {
            builder.append(source_id).append(",");
        }
        return builder.toString();
    }

    public void setSource_ids(ArrayList<Long> source_ids) {
        this.source_ids = source_ids;
    }

    public void addSource_id(Long source_id) {
        this.source_ids.add(source_id);
    }

    public boolean isNo_reposts() {
        return no_reposts;
    }

    public void setNo_reposts(boolean no_reposts) {
        this.no_reposts = no_reposts;
    }

}
