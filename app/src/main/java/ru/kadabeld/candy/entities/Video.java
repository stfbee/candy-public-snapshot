package ru.kadabeld.candy.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class Video implements AbstractAttachment {

    private String title = "";
    private String description = "";
    private String player = "";
    private String photo_130 = "";
    private String photo_320 = "";
    private String photo_640 = "";
    private String access_key = "";
    private long id;
    private long duration;
    private long date;
    private long adding_date;
    private long views;
    private long comments;
    private long owner_id;
    private boolean processing;
    private boolean can_edit;
    private boolean can_repost;
    private boolean repeat;
    private int position;

    //likes
    private long user_likes;
    private long count_likes;

    //files
    private String mp4_240 = "";
    private String mp4_360 = "";
    private String mp4_480 = "";
    private String mp4_720 = "";
    private String external = "";

    public static Video parseVideo(JSONObject videoObject) {
        final Video video = new Video();
        try {
            if (videoObject.has("id")) video.setId(videoObject.getLong("id"));
            if (videoObject.has("owner_id")) video.setOwner_id(videoObject.getLong("owner_id"));
            if (videoObject.has("title")) video.setTitle(videoObject.getString("title"));
            if (videoObject.has("description"))
                video.setDescription(videoObject.getString("description"));
            if (videoObject.has("duration")) video.setDuration(videoObject.getLong("duration"));
            if (videoObject.has("photo_130"))
                video.setPhoto_130(videoObject.getString("photo_130"));
            if (videoObject.has("photo_320"))
                video.setPhoto_320(videoObject.getString("photo_320"));
            if (videoObject.has("photo_640"))
                video.setPhoto_640(videoObject.getString("photo_640"));
            if (videoObject.has("date")) video.setDate(videoObject.getLong("date"));
            if (videoObject.has("adding_date"))
                video.setAdding_date(videoObject.getLong("adding_date"));
            if (videoObject.has("views")) video.setViews(videoObject.getLong("views"));
            if (videoObject.has("comments")) video.setComments(videoObject.getLong("comments"));
            if (videoObject.has("player")) video.setPlayer(videoObject.getString("player"));
            if (videoObject.has("access_key"))
                video.setAccess_key(videoObject.getString("access_key"));
            if (videoObject.has("can_repost"))
                video.setCan_repost(videoObject.getInt("can_repost") == 1);
            if (videoObject.has("can_edit")) video.setCan_edit(videoObject.getInt("can_edit") == 1);
            if (videoObject.has("repeat")) video.setRepeat(videoObject.getInt("repeat") == 1);
            video.setProcessing(videoObject.has("processing"));

            if (videoObject.has("files")) {
                JSONObject files = videoObject.getJSONObject("files");
                if (files.has("mp4_240")) video.setMp4_240(files.getString("mp4_240"));
                if (files.has("mp4_360")) video.setMp4_360(files.getString("mp4_360"));
                if (files.has("mp4_480")) video.setMp4_480(files.getString("mp4_480"));
                if (files.has("mp4_720")) video.setMp4_720(files.getString("mp4_720"));
                if (files.has("external")) video.setExternal(files.getString("external"));
            }

            if (videoObject.has("likes")) {
                JSONObject likes = videoObject.getJSONObject("likes");
                if (likes.has("user_likes")) video.setUser_likes(likes.getLong("user_likes"));
                if (likes.has("count")) video.setCount_likes(likes.getLong("count"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return video;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public long getUser_likes() {
        return user_likes;
    }

    public void setUser_likes(long user_likes) {
        this.user_likes = user_likes;
    }

    public long getCount_likes() {
        return count_likes;
    }

    public void setCount_likes(long count_likes) {
        this.count_likes = count_likes;
    }

    public boolean isCan_edit() {
        return can_edit;
    }

    public void setCan_edit(boolean can_edit) {
        this.can_edit = can_edit;
    }

    public boolean isCan_repost() {
        return can_repost;
    }

    public void setCan_repost(boolean can_repost) {
        this.can_repost = can_repost;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public String getPhoto_130() {
        return photo_130;
    }

    public void setPhoto_130(String photo_130) {
        this.photo_130 = photo_130;
    }

    public String getPhoto_320() {
        return photo_320;
    }

    public void setPhoto_320(String photo_320) {
        this.photo_320 = photo_320;
    }

    public String getPhoto_640() {
        return photo_640;
    }

    public void setPhoto_640(String photo_640) {
        this.photo_640 = photo_640;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getAdding_date() {
        return adding_date;
    }

    public void setAdding_date(long adding_date) {
        this.adding_date = adding_date;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public long getComments() {
        return comments;
    }

    public void setComments(long comments) {
        this.comments = comments;
    }

    public boolean isProcessing() {
        return processing;
    }

    public void setProcessing(boolean processing) {
        this.processing = processing;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMp4_240() {
        return mp4_240;
    }

    public void setMp4_240(String mp4_240) {
        this.mp4_240 = mp4_240;
    }

    public String getMp4_360() {
        return mp4_360;
    }

    public void setMp4_360(String mp4_360) {
        this.mp4_360 = mp4_360;
    }

    public String getMp4_480() {
        return mp4_480;
    }

    public void setMp4_480(String mp4_480) {
        this.mp4_480 = mp4_480;
    }

    public String getMp4_720() {
        return mp4_720;
    }

    public void setMp4_720(String mp4_720) {
        this.mp4_720 = mp4_720;
    }

    public String getExternal() {
        return external;
    }

    public void setExternal(String external) {
        this.external = external;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getMaxImage() {
        if (!photo_640.isEmpty()) return photo_640;
        if (!photo_320.isEmpty()) return photo_320;
        return photo_130;
    }
}


