package ru.kadabeld.candy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by VOnishchenko
 * 02.03.2016
 * 14:49
 */
public class LinkOpener extends AbstractActivity {
    private static final String TAG = LinkOpener.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CandyApplication.setTheme(this);
        super.onCreate(savedInstanceState);
        finish();
        final Intent intent = getIntent();
        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            if (segments.size() > 0) {
                String link = segments.get(0);
                Log.d(TAG, link);
                if (link.matches("(id)[-0-9]+")) {
                    Matcher matcher = Pattern.compile("(?:id|wall)([-0-9]+)").matcher(link);
                    if (matcher.find()) {
                        long id = Long.parseLong(matcher.group(1));
                        openProfile(id);
                    }
                } else if (link.matches("(club)[-0-9]+")) {
                    Matcher matcher = Pattern.compile("(?:club|wall)([-0-9]+)").matcher(link);
                    if (matcher.find()) {
                        long id = Long.parseLong(matcher.group(1));
                        openProfile(-id);
                    }
                } else if (link.matches("[A-Za-z0-9\\._]+")) {
                    Matcher matcher = Pattern.compile("[A-Za-z0-9\\._]+").matcher(link);
                    if (matcher.find()) {
                        String id = matcher.group(0);
                        if (!id.isEmpty()) {
                            RequestParams requestParams = new RequestParams();
                            requestParams.put("screen_name", id);
                            requestParams.put("access_token", CandyApplication.access_token());
                            requestParams.put("v", API.version);
                            CandyApplication.client.get(this, API.BASE() + API.pollsDeleteVote, requestParams, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    try {
                                        if (response.has("response")) {
                                            response = response.getJSONObject("response");
                                            if (response.has("type")) {
                                                switch (response.getString("type")) {
                                                    case "user":
                                                        if (response.has("object_id")) {
                                                            long id = response.getLong("object_id");
                                                            openProfile(id);
                                                        }
                                                        break;
                                                }
                                            }
                                        } else if (response.has("error")) {
                                            Toast.makeText(LinkOpener.this, getString(R.string.wtf_toast_text), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                } else {
                    //just open link
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, intent.getData());
                    startActivity(browserIntent);
                }
            }
        }
    }

    private void openProfile(long id) {
        Intent intent1 = new Intent(LinkOpener.this, MainActivity.class);
        intent1.putExtra("action", "open_profile");
        intent1.putExtra("user_id", id);
        startActivity(intent1);
    }
}
