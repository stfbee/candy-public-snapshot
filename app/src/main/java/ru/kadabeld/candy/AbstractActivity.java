package ru.kadabeld.candy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.squareup.picasso.Picasso;
import com.trello.rxlifecycle.ActivityEvent;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import ru.kadabeld.candy.fragments.AudiosFragment;
import ru.kadabeld.candy.fragments.ChatFragment;
import ru.kadabeld.candy.fragments.DialogsFragment;
import ru.kadabeld.candy.fragments.DocsFragment;
import ru.kadabeld.candy.fragments.FeedFragment;
import ru.kadabeld.candy.fragments.FriendsFragment;
import ru.kadabeld.candy.fragments.GroupsFragment;
import ru.kadabeld.candy.fragments.NotificationsFragment;
import ru.kadabeld.candy.fragments.PhotosFragment;
import ru.kadabeld.candy.fragments.ProfileFragment;
import ru.kadabeld.candy.fragments.VideosFragment;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;
import ru.kadabeld.candy.utils.transformations.StackBlurTransformation;
import rx.android.schedulers.AndroidSchedulers;

public abstract class AbstractActivity extends RxAppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FragmentManager.OnBackStackChangedListener {

    protected NavigationView navigationView;
    protected DrawerLayout drawer;
    protected ActionBar supportActionBar;
    protected Toolbar toolbar;
    protected ActionBarDrawerToggle toggle;
    protected View shadow_toolbar;
    protected SharedPreferences sPref;
    protected Fragment currentFragment;

    public static final String DRAWER_UPDATE_COUNTERS = "ru.cd.candy.DRAWER_UPDATE_COUNTERS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CandyApplication.setTheme(this);
        super.onCreate(savedInstanceState);
        sPref = PreferenceManager.getDefaultSharedPreferences(this);

        Global.isSmall = sPref.getBoolean("small_drawer", false);
        Global.displayDensity = getResources().getDisplayMetrics().density;

        setContentView(R.layout.activity_base);

        getSupportFragmentManager().addOnBackStackChangedListener(this);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (navigationView != null) {
            if (!Global.isSmall) {
                navigationView.addHeaderView(getLayoutInflater().inflate(R.layout.drawer_header, null));
                navigationView.getLayoutParams().width = DrawerLayout.LayoutParams.WRAP_CONTENT;
            } else {
                RelativeLayout view = (RelativeLayout) getLayoutInflater().inflate(R.layout.drawer_header_small, null);
                navigationView.addHeaderView(view);
                navigationView.getLayoutParams().width = Global.dp(76);

                int statusBarHeight = getStatusBarHeight();
                navigationView.getHeaderView(0).getLayoutParams().height = Global.dp(76) + statusBarHeight;
            }

            navigationView.setVisibility(View.VISIBLE);
            navigationView.setNavigationItemSelectedListener(this);

            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }

            RxView.clicks(navigationView.getHeaderView(0).findViewById(R.id.header))
                    .withLatestFrom(CandyApplication.getInstance().getAccountsBus().current(), (f, s) -> s)
                    .compose(bindToLifecycle())
                    .subscribe(accountInfo -> {
                        if (drawer != null) drawer.closeDrawer(GravityCompat.START);
                        ProfileFragment f = new ProfileFragment();
                        f.init(accountInfo.getUserId());
                        openFragment(f);
                    });

            updateProfileInfo();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkAlpha));
            if (sPref.getBoolean("colorizeNavbar", true)) {
                getWindow().setNavigationBarColor(ColorUtils.getThemeColor(R.attr.colorPrimary));
            }
        } else {
            //Затемняем статусбар и навбар на девайсах меньше лоллипопа
            SystemBarTintManager tintManager = new SystemBarTintManager(this);// enable status bar tint
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setNavigationBarTintEnabled(true);
            tintManager.setTintColor(Color.parseColor("#20000000"));
        }
    }

    public void makeActionBar(View view) {
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        shadow_toolbar = view.findViewById(R.id.shadow_toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            supportActionBar = getSupportActionBar();

            if (supportActionBar != null) {
                supportActionBar.setDisplayShowHomeEnabled(true);
                supportActionBar.setDisplayHomeAsUpEnabled(true);
            }

            //Делаем отступ в тулбаре под статусбар
            int statusBarHeight = getStatusBarHeight();
            toolbar.setPadding(0, statusBarHeight, 0, 0);
            toolbar.getLayoutParams().height += statusBarHeight;

            if (drawer != null) {
                toggle = new ActionBarDrawerToggle(this, drawer, R.string.app_name, R.string.app_name) {
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        syncActionBarArrowState();
                        hideIME();
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        toggle.setDrawerIndicatorEnabled(true);
                        hideIME();
                    }
                };

                drawer.addDrawerListener(toggle);
                toggle.syncState();
            }

        }

        colorize();
    }

    public void hideIME() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = getCurrentFocus();
        inputMethodManager.hideSoftInputFromWindow((currentFocus != null) ? currentFocus.getWindowToken() : null, 0);
        if (currentFocus != null) {
            currentFocus.clearFocus();
        }
    }

    public void setDrawerCounter(int item_res, int count) {
        if (count > 0) {
            View counter = getLayoutInflater().inflate(R.layout.drawer_counter, null);
            TextView counter_text = (TextView) counter.findViewById(R.id.notify_count);
            if (count < 100) {
                counter_text.setText(String.valueOf(count));
            } else {
                counter_text.setText(R.string.drawer_counter_limit);
            }
            navigationView.getMenu().findItem(item_res).setActionView(counter);
        } else {
            navigationView.getMenu().findItem(item_res).setActionView(null);
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (currentFragment instanceof ChatFragment) {
            if (((ChatFragment) currentFragment).onBackPressed()) super.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    public void updateProfileInfo() {
        CandyApplication.getInstance()
                .getAccountsBus()
                .current()
                .filter(info1 -> info1 != null)
                .compose(bindUntilEvent(ActivityEvent.DESTROY))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(info -> {
                    ImageView avatar = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.avatar);

                    if (!Global.isSmall) {
                        TextView name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.name);
                        TextView status_text = (TextView) navigationView.getHeaderView(0).findViewById(R.id.status_text);
                        //MaterialIconView status_music_icon = (MaterialIconView) navigationView.getHeaderView(0).findViewById(R.id.status_music_icon);

                        name.setText(info.getFullName());
                        //TODO: not works temporary!!! @arktic
                        status_text.setTypeface(status_text.getTypeface(), Typeface.ITALIC);
                        status_text.setText(R.string.dev_status_placeholder);
                    }

                    Picasso.with(this).load(info.getPhoto200()).transform(new CropCircleTransformation()).into(avatar);

                    if (!Global.isSmall) {
                        if (info.isAvatarDefault()) {
                            ImageView cover = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.cover);
                            Picasso.with(this).load(Global.getHeader(this)).into(cover);
                        } else {
                            ImageView cover = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.cover);
                            if (sPref.getString("background", "blur").equals("googlenow")) {
                                Picasso.with(this).load(Global.getHeader(this)).into(cover);
                            } else if (sPref.getString("background", "blur").equals("blur")) {
                                Picasso.with(this).load(info.getPhoto200()).transform(new StackBlurTransformation(15)).into(cover);
                            }
                        }
                    }
                });
    }

    public void colorize() {
        colorize(ColorUtils.getThemeColor(R.attr.colorPrimary), 255);
    }

    public void colorize(int colorPrimary) {
        colorize(colorPrimary, 255);
    }

    public void colorize(int colorPrimary, int toolBarAlpha) {
        if (toolbar != null) {
            toolbar.setBackgroundColor(colorPrimary);
            toolbar.getBackground().setAlpha(toolBarAlpha);
//			toolbar.setAlpha(toolBarAlpha / 255F);
        }
        if (shadow_toolbar != null) shadow_toolbar.getBackground().setAlpha(toolBarAlpha);

        if (Build.VERSION.SDK_INT >= 21) {
            if (sPref.getBoolean("colorizeNavbar", true)) {
                getWindow().setNavigationBarColor(colorPrimary);
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;

        switch (id) {
            case R.id.drawer_news:
                fragment = new FeedFragment();
                break;
            case R.id.drawer_feedback:
                fragment = new NotificationsFragment();
                break;
            case R.id.drawer_messages:
                fragment = new DialogsFragment();
                break;
            case R.id.drawer_friends:
                fragment = new FriendsFragment();
                break;
            case R.id.drawer_communities:
                fragment = new GroupsFragment();
                break;
            case R.id.drawer_photos:
                fragment = new PhotosFragment();
                break;
            case R.id.drawer_videos:
                fragment = new VideosFragment();
                break;
            case R.id.drawer_music:
                fragment = new AudiosFragment();
                break;
            case R.id.drawer_documents:
                fragment = new DocsFragment();
                break;
            case R.id.drawer_settings:
                startActivity(new Intent(AbstractActivity.this, SettingsActivity.class));
                break;
            default:
                fragment = new MainActivity.FragmentIndex();
                break;
        }
        if (fragment != null) {

            openFragment(fragment, false);
            colorize();

            if (drawer != null) drawer.closeDrawer(GravityCompat.START);

            return true;
        } else {
            return false;
        }
    }

    public void openFragment(Fragment fragment) {
        currentFragment = fragment;
        openFragment(fragment, true);
    }

    public void openFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (!addToBackStack)
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment);
        if (addToBackStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackStackChanged() {
        syncActionBarArrowState();
    }

    private void syncActionBarArrowState() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (toggle != null) toggle.setDrawerIndicatorEnabled(backStackEntryCount == 0);
    }

    private BroadcastReceiver drawerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case DRAWER_UPDATE_COUNTERS:
                    int MESSAGES_COUNT = intent.getIntExtra("MESSAGES_COUNT", 0);
                    setDrawerCounter(R.id.drawer_messages, MESSAGES_COUNT);
                    break;
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("onOptionsItemSelected", String.valueOf(item.getItemId()));
        if (toggle != null) {
            if (toggle.isDrawerIndicatorEnabled() &&
                    toggle.onOptionsItemSelected(item)) {
                return true;
            } else if (item.getItemId() == android.R.id.home) {
                onBackPressed();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        API.setOffline();
        getSupportFragmentManager().removeOnBackStackChangedListener(this);
        try {
            unregisterReceiver(drawerReceiver);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter drawerIntent = new IntentFilter();
        drawerIntent.addAction(DRAWER_UPDATE_COUNTERS);
        registerReceiver(drawerReceiver, drawerIntent);
    }
}
