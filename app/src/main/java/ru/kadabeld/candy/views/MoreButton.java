package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Locale;

import ru.kadabeld.candy.R;

public class MoreButton extends LinearLayout {
    private ProgressBar progressBar;
    private TextView textView;
    private Context context;

    public MoreButton(Context context) {
        super(context);
        this.context = context;
        ((Activity) context)
                .getLayoutInflater()
                .inflate(R.layout.element_more_button, this, true);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        textView = (TextView) findViewById(R.id.text);
    }

    public void setCount(long count) {
        textView.setText(String.format(Locale.getDefault(), "%s (%d)", context.getString(R.string.com_show_comments), count));
    }

    public void setVisible(boolean visible) {
        if (visible) {
            setClickable(true);
            progressBar.setVisibility(GONE);
            textView.setVisibility(VISIBLE);
        } else {
            setClickable(false);
            progressBar.setVisibility(VISIBLE);
            textView.setVisibility(GONE);
        }
    }
}
