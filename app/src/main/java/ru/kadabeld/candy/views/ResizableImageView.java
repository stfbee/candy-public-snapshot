package ru.kadabeld.candy.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import ru.kadabeld.candy.Global;

public class ResizableImageView extends ImageView {

    private Context context;
    private int mMaxHeight;
    private int mMaxWidth;


    public ResizableImageView(Context var1) {
        super(var1);
        this.context = var1;
    }

    public ResizableImageView(Context var1, AttributeSet var2) {
        super(var1, var2);
        this.context = var1;
    }

    public ResizableImageView(Context var1, AttributeSet var2, int var3) {
        super(var1, var2, var3);
        this.context = var1;
    }

    protected void onMeasure(int var1, int var2) {
        super.onMeasure(var1, var2);
        Drawable var3 = this.getDrawable();
        if (var3 != null) {
            int var4 = MeasureSpec.getMode(var1);
            int var5 = MeasureSpec.getMode(var2);
            if (var4 != 1073741824 && var5 != 1073741824) {
                int var6;
                if (var4 == Integer.MIN_VALUE) {
                    var6 = Math.min(MeasureSpec.getSize(var1), this.mMaxWidth);
                } else {
                    var6 = this.mMaxWidth;
                }

                int var7;
                if (var5 == Integer.MIN_VALUE) {
                    var7 = Math.min(MeasureSpec.getSize(var2), this.mMaxHeight);
                } else {
                    var7 = this.mMaxHeight;
                }

                int var8 = Math.round(Global.convertDpToPixel(this.context, (float) var3.getIntrinsicWidth()));
                int var9 = Math.round(Global.convertDpToPixel(this.context, (float) var3.getIntrinsicHeight()));
                float var10 = (float) var8 / (float) var9;
                int var11 = Math.min(Math.max((int) ((float) Math.min(Math.max(var8, this.getSuggestedMinimumWidth()), var6) / var10), this.getSuggestedMinimumHeight()), var7);
                int var12 = (int) (var10 * (float) var11);
                if (var12 > var6) {
                    var12 = var6;
                    var11 = (int) ((float) var6 / var10);
                }

                this.setMeasuredDimension(var12, var11);
                return;
            }
        }

    }

    public void setMaxHeight(int var1) {
        super.setMaxHeight(var1);
        this.mMaxHeight = var1;
    }

    public void setMaxWidth(int var1) {
        super.setMaxWidth(var1);
        this.mMaxWidth = var1;
    }
}
