package ru.kadabeld.candy.views;


import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.steamcrafted.materialiconlib.MaterialIconView;

import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Comment;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

@SuppressWarnings("unused")
public class WriteBar extends LinearLayout {
    private EditText editText;
    private FrameLayout sendButton;
    private Comment comment;
    private Context ctx;
    private ImageView imageView;
    private View circleView;
    private MaterialIconView emojiButton;
    private MaterialIconView attachButton;

    public WriteBar(Context context) {
        super(context);
        ctx = context;
        init();
    }

    public WriteBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        ctx = context;
        init();
    }

    public WriteBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ctx = context;
        init();
    }

    void init() {
        setOrientation(VERTICAL);
        setId(R.id.writebar);
        View.inflate(getContext(), R.layout.element_writebar, this);
        editText = (EditText) findViewById(R.id.writebar_edit);
        imageView = (ImageView) findViewById(R.id.imageView);
        sendButton = (FrameLayout) findViewById(R.id.writebar_send);
        circleView = findViewById(R.id.circleView);
        emojiButton = (MaterialIconView) findViewById(R.id.button_emoji);
        attachButton = (MaterialIconView) findViewById(R.id.button_attach);

        if (!isInEditMode()) {
            ((GradientDrawable) circleView.getBackground()).setColor(ColorUtils.getThemeColor(R.attr.colorPrimary));
        }
        setAvatar();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editText.getText().toString().isEmpty()) {
                    setAvatar();
                } else {
                    setDrawable();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        attachButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                IconizedMenu menu = new IconizedMenu(ctx, v);
                menu.inflate(R.menu.attach_im);
                menu.setOnMenuItemClickListener(new IconizedMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(ctx, "пффф, наивный", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                });

                menu.show();
            }
        });
    }

    public void setAvatar() {
        if (!isInEditMode()) {
            if (CandyApplication.current().isAvatarDefault()) {
                int fivezero = Math.round(Global.convertDpToPixel(getContext(), 40));
                ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                layoutParams.height = fivezero;
                layoutParams.width = fivezero;
                imageView.setLayoutParams(layoutParams);
                Picasso.with(getContext()).load(CandyApplication.current().getPhoto200()).resize(fivezero, fivezero).transform(new CropCircleTransformation()).into(imageView);
            } else {
                int fivezero = Math.round(Global.convertDpToPixel(getContext(), 50));
                Picasso.with(getContext()).load(CandyApplication.current().getPhoto200()).resize(fivezero, fivezero).transform(new CropCircleTransformation()).into(imageView);
            }
            circleView.setVisibility(View.GONE);
        }
    }

    public void setDrawable() {
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        layoutParams.height = -2;
        layoutParams.width = -2;
        imageView.setLayoutParams(layoutParams);
        imageView.setBackground(null);
        imageView.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.ic_send_light));
//        Picasso.with(getContext()).load(R.drawable.ic_send_light).into(imageView);
        circleView.setVisibility(View.VISIBLE);
    }

    public EditText getEditText() {
        return editText;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public FrameLayout getSendButton() {
        return sendButton;
    }

    public void setOnSendClickListener(OnClickListener onSendClickListener) {
        sendButton.setOnClickListener(onSendClickListener);
    }

    public void setOnEmojiClickListener(OnClickListener onEmojiClickListener) {
        emojiButton.setOnClickListener(onEmojiClickListener);
    }

    public void setOnAttachClickListener(OnClickListener onAttachClickListener) {
        attachButton.setOnClickListener(onAttachClickListener);
    }

    public Comment getComment() {
        return comment;
    }

    public MaterialIconView getEmojiButton() {
        return emojiButton;
    }
}
