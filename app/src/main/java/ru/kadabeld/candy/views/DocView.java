package ru.kadabeld.candy.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Document;

public class DocView extends RelativeLayout {
    private Context ctx;
    private String tag;
    private ImageView thumb;
    private TextView title;
    private TextView size;
    private String url;
    private RelativeLayout doc;
    private String ext;
    private Document document;

    public DocView(Context context) {
        super(context);
        ctx = context;
        ((Activity) ctx).getLayoutInflater().inflate(R.layout.element_document, this, true);
        this.thumb = (ImageView) this.findViewById(R.id.thumb);
        this.title = (TextView) this.findViewById(R.id.title);
        this.size = (TextView) this.findViewById(R.id.size);
        this.doc = (RelativeLayout) this.findViewById(R.id.doc);
        doc.setOnClickListener(v -> {
            final ArrayList<String> actions = new ArrayList<>();
            actions.clear();
            if (ext.equals("gif") || ext.equals("png") || ext.equals("jpg")) {
                actions.add(ctx.getString(R.string.view));
            }

            actions.add(ctx.getString(R.string.download));
            actions.add(ctx.getString(R.string.add_documents));
            ListAdapter adapter = new ArrayAdapter<String>(ctx, android.R.layout.select_dialog_item, android.R.id.text1, actions) {
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setTitle(ctx.getString(R.string.chs_action))
                    .setAdapter(adapter, (dialog, item) -> {
                        if (actions.get(item).equals(ctx.getString(R.string.view))) {
                            Global.gif(ctx, url);
                        }
                    })
                    .setCancelable(true);
            AlertDialog alert = builder.create();
            alert.show();
        });
    }

    private static String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    private void setThumb(String thumb) {
        if (thumb.isEmpty()) {
//            this.thumb.setImageResource(R.drawable.ic_launcher);
            int img;
            switch (ext) {
                case "apk": {
                    img = R.drawable.ext_apk;
                    break;
                }
                case "doc": {
                    img = R.drawable.ext_doc;
                    break;
                }
                case "mp3": {
                    img = R.drawable.ext_mp3;
                    break;
                }
                case "pdf": {
                    img = R.drawable.ext_pdf;
                    break;
                }
                case "ppt": {
                    img = R.drawable.ext_ppt;
                    break;
                }
                case "psd": {
                    img = R.drawable.ext_psd;
                    break;
                }
                case "rar": {
                    img = R.drawable.ext_rar;
                    break;
                }
                case "txt": {
                    img = R.drawable.ext_txt;
                    break;
                }
                case "zip": {
                    img = R.drawable.ext_zip;
                    break;
                }
                default: {
                    img = R.drawable.ext_file;
                    break;
                }
            }

            this.thumb.setImageDrawable(ContextCompat.getDrawable(ctx, img));
        } else {
            Picasso.with(ctx).load(thumb).tag(tag).into(this.thumb);
        }
    }

    public void setDocument(Document document) {
        this.document = document;
        this.title.setText(document.getTitle());
        this.ext = document.getExt();
        this.url = document.getUrl();
        this.size.setText(readableFileSize(document.getSize()));
        this.setThumb(document.getPhoto_m());
    }
}
