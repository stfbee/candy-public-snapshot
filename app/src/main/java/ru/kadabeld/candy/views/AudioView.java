package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Audio;

public class AudioView extends RelativeLayout {
    private TextView artist;
    private TextView title;
    private TextView duration;

    public AudioView(Context context) {
        super(context);
        ((Activity) context)
                .getLayoutInflater()
                .inflate(R.layout.element_audio, this, true);
        this.title = (TextView) this.findViewById(R.id.title);
        this.artist = (TextView) this.findViewById(R.id.artist);
        this.duration = (TextView) this.findViewById(R.id.duration);

        setOnClickListener(v -> Toast.makeText(getContext(), "Аудио отключены, ждите включения в будущем", Toast.LENGTH_SHORT).show());
    }

    public void setAudio(Audio audio) {
        this.duration.setText(String.format(Locale.getDefault(), "%d:%02d", audio.getDuration() / 60, audio.getDuration() % 60));
        this.title.setText(audio.getTitle());
        this.artist.setText(audio.getArtist());
    }
}
