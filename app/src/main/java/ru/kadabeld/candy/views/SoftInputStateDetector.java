package ru.kadabeld.candy.views;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.View;
import android.view.View.OnLayoutChangeListener;

import ru.kadabeld.candy.Global;

public abstract class SoftInputStateDetector implements OnLayoutChangeListener {

    private Activity activity;
    private int lastHeight = 0;
    private boolean lastState = false;


    public SoftInputStateDetector(Activity var1) {
        this.activity = var1;
    }

    public int getHeight() {
        return this.lastHeight;
    }

    public int getSoftInputHeight() {
        return this.lastHeight > 0 ? this.lastHeight : this.activity.getSharedPreferences("emoji", 0).getInt("kbd_height" + this.activity.getResources().getDisplayMetrics().widthPixels + "_" + this.activity.getResources().getDisplayMetrics().heightPixels, 0);
    }

    public boolean isShown() {
        return this.lastState;
    }

    public void onContentViewResize(int var1, int var2, int var3, int var4) {
        boolean var5 = true;
        Rect var6 = new Rect();
        this.activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(var6);
        int var7 = var6.top;
        int var8 = this.activity.getWindow().getDecorView().getHeight() - var7;
        if (VERSION.SDK_INT >= 20) {
            var8 -= Global.getNavigationBarHeight(this.activity);
        }

        if (this.activity.getActionBar() != null) {
            var2 += this.activity.getActionBar().getHeight();
        }

        int var9 = var8 - var2;
        this.lastHeight = var9;
        boolean var10;
        if (var9 > Global.scale(100.0F)) {
            var10 = var5;
        } else {
            var10 = false;
        }

        this.lastState = var10;
        if (var9 > Global.scale(100.0F)) {
            this.activity.getSharedPreferences("emoji", 0).edit().putInt("kbd_height" + this.activity.getResources().getDisplayMetrics().widthPixels + "_" + this.activity.getResources().getDisplayMetrics().heightPixels, var9).commit();
        }

        if (var9 <= Global.scale(100.0F)) {
            var5 = false;
        }

        this.onKeyboardStateChanged(var5, var9);
    }

    public abstract void onKeyboardStateChanged(boolean var1, int var2);

    public void onLayoutChange(View var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
        this.onContentViewResize(var4 - var2, var5 - var3, var8 - var6, var9 - var7);
    }
}
