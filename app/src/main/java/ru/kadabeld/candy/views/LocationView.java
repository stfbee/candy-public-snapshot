package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.overlay.Icon;
import com.mapbox.mapboxsdk.overlay.Marker;

import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.meta.Geo;

/**
 * User: Vlad
 * Date: 15.10.2015
 * Time: 13:55
 */
public class LocationView extends LinearLayout {
    private Context ctx;
    private String tag;
    private TextView location_text;
    private Geo location;
    private MapViewNoControl mv;

    public LocationView(Context context) {
        super(context);
        ctx = context;
        ((Activity) ctx).getLayoutInflater().inflate(R.layout.element_map, this, true);
        this.location_text = (TextView) this.findViewById(R.id.location_text);

        mv = (MapViewNoControl) findViewById(R.id.mapview);
        mv.setMinZoomLevel(mv.getTileProvider().getMinimumZoomLevel());
        mv.setMaxZoomLevel(mv.getTileProvider().getMaximumZoomLevel());
        mv.setCenter(mv.getTileProvider().getCenterCoordinate());
        mv.setPanShouldEnabled(false);
        mv.setUserLocationEnabled(true);
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setLocation(Geo location, int content_width) {
        this.location = location;
        double lat = Double.parseDouble(location.getCoordinates().split(" ")[0]);
        double lng = Double.parseDouble(location.getCoordinates().split(" ")[1]);

        System.out.println("lat " + lat + ", lng " + lng);

        ViewGroup.LayoutParams layoutParams = mv.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new LayoutParams(content_width, (int) Global.convertDpToPixel(ctx, 250));
        } else {
            layoutParams.width = content_width;
        }
        mv.setLayoutParams(layoutParams);

        LatLng aLatLng = new LatLng(lat, lng);
        Marker m = new Marker(mv, "Отмеченная точка", location.getPlace().getTitle(), aLatLng);
        m.setIcon(new Icon(ctx, Icon.Size.SMALL, "marker-stroked", "d32d32"));
        mv.addMarker(m);
        mv.setCenter(aLatLng);
        mv.setZoom(18);

        location_text.setText(location.getPlace().getTitle());
    }
}
