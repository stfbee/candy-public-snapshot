package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.kadabeld.candy.R;

public class ColorCircleCounter extends RelativeLayout {
    private ImageView circle;
    private ImageView icon;
    private TextView count;
    private TextView name;

    public ColorCircleCounter(Context context) {
        super(context);
        ((Activity) getContext())
                .getLayoutInflater()
                .inflate(R.layout.element_counter, this, true);
        circle = (ImageView) this.findViewById(R.id.circle);
        icon = (ImageView) this.findViewById(R.id.icon);
        count = (TextView) this.findViewById(R.id.count);
        name = (TextView) this.findViewById(R.id.name);
    }

    public void setCircleColor(int color) {
        circle.setColorFilter(new LightingColorFilter(color, 1));
    }

    public void setDrawable(Drawable drawable) {
        icon.setImageDrawable(drawable);
    }

    public void setCount(String count) {
        this.count.setText(count);
    }

    public void setName(String name) {
        this.name.setText(name);
    }
}
