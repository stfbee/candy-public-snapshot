package ru.kadabeld.candy.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class FixedScrollListView extends ListView {

    private static final int SCROLLBAR_SIZE = 200;


    public FixedScrollListView(Context context) {
        super(context);
    }

    public FixedScrollListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FixedScrollListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent() < super.computeVerticalScrollRange() ? 200 : super.computeVerticalScrollExtent();
    }

    public int computeVerticalScrollOffset() {
        int var1 = -100 + (super.computeVerticalScrollRange() - super.computeVerticalScrollExtent());
        return (int) ((float) Math.max(super.computeVerticalScrollOffset(), 0) / (float) var1 * (float) (-SCROLLBAR_SIZE + super.computeVerticalScrollRange()));
    }
}

