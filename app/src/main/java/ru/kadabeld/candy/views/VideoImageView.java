package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;

public class VideoImageView extends RelativeLayout {
    private Context ctx;
    private ImageView image;
    private TextView title;
    private TextView duration;
    private String tag = "news";
    private long owner_id;
    private long vid;
    private String access_key;
    private ru.kadabeld.candy.entities.Video video;

    public VideoImageView(Context context) {
        super(context);
        ctx = context;
        ((Activity) getContext())
                .getLayoutInflater()
                .inflate(R.layout.video, this, true);
        this.title = (TextView) this.findViewById(R.id.title);
        this.image = (ImageView) this.findViewById(R.id.imageView);
        this.duration = (TextView) this.findViewById(R.id.duration);
        image.setOnClickListener(v -> {
            Toast.makeText(getContext(), "Видео отключены, ждите включения в будущем", Toast.LENGTH_SHORT).show();
//                String video_url = String.format("%s_%s", owner_id, vid);
//                RequestParams params = new RequestParams();
//                //params.put("owner_id", owner_id);
//                params.put("videos", video_url);
//                if (!access_key.isEmpty()) {
//                    params.put("access_key", access_key);
//                }
//                params.put("access_token", Candy.access_token());
//                Candy.client.post(API.BASE() + API.videoGet, params, new TextHttpResponseHandler() {
//
//                    @Override
//                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                        Toast toast = Toast.makeText(ctx,
//                                "" + responseString, Toast.LENGTH_SHORT);
//                        toast.show();
//                    }
//
//                    @Override
//                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                        try {
//                            JSONObject object = new JSONObject(responseString);
//                            JSONArray response = object.getJSONArray("response");
//                            JSONObject videoObject = response.getJSONObject(1);
//                            JSONObject files = videoObject.getJSONObject("files");
//                            if (!files.isNull("mp4_240")) {
//                                Intent intent = new Intent(ctx, VideoPlayerActivity.class);
//                                ArrayList<Video> quality = new ArrayList<>();
//                                if (!files.isNull("mp4_240")) {
//                                    quality.add(new Video("240", files.getString("mp4_240")));
//                                }
//                                if (!files.isNull("mp4_360")) {
//                                    quality.add(new Video("360", files.getString("mp4_360")));
//                                }
//                                if (!files.isNull("mp4_480")) {
//                                    quality.add(new Video("480", files.getString("mp4_480")));
//                                }
//                                if (!files.isNull("mp4_720")) {
//                                    quality.add(new Video("720", files.getString("mp4_720")));
//                                }
//                                intent.putParcelableArrayListExtra("quality", quality);
//                                ctx.startActivity(intent);
//                            } else {
//                                String url = files.getString("external");
//                                Intent i = new Intent(Intent.ACTION_VIEW);
//                                i.setData(Uri.parse(url));
//                                ctx.startActivity(i);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
        });
    }


    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setImage(String source, int w, int h) {
        Picasso.with(ctx).load(source).resize(w, h).tag(tag).into(image);
        //image.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    public void setAdjustViewBnds(boolean adjust) {
        if (this.image != null) {
            this.image.setAdjustViewBounds(adjust);
        }
    }

    public void setVideo(ru.kadabeld.candy.entities.Video video) {
        this.video = video;
        this.title.setText(video.getTitle());
        this.duration.setText(Global.getMinutes(video.getDuration()));
        this.owner_id = video.getOwner_id();
        this.vid = video.getId();
        this.access_key = video.getAccess_key();
    }
}
