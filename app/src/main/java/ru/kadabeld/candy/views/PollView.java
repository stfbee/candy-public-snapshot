package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kadabeld.candy.API;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Poll;

/**
 * User: Vlad
 * Date: 15.10.2015
 * Time: 13:55
 */
public class PollView extends RelativeLayout {
    private Context ctx;
    private boolean answered = false;
    private long answered_id = 0;
    private Poll poll;

    private TextView question_text;
    private TextView info_text;
    private TextView open_text;
    private LinearLayout answers_list;

    public PollView(Context context) {
        super(context);
        ctx = context;
        ((Activity) ctx)
                .getLayoutInflater()
                .inflate(R.layout.element_poll, this, true);

        this.question_text = (TextView) this.findViewById(R.id.question_text);
        this.info_text = (TextView) this.findViewById(R.id.info_text);
        this.open_text = (TextView) this.findViewById(R.id.open_text);
        this.answers_list = (LinearLayout) this.findViewById(R.id.answers_list);
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
        answered_id = poll.getAnswer_id();
        answered = (answered_id > 0);
        info_text.setText(ctx.getString(R.string.poll_info, poll.getVotes()));
        question_text.setText(poll.getQuestion());
        open_text.setText(ctx.getString(poll.isAnonymous() ? R.string.poll_closed : R.string.poll_opened));

        PollView.this.invalidate();
        ArrayList<Poll.Answer> answers = poll.getAnswers();
        AnswerAdapter answerAdapter = new AnswerAdapter(answers);
        answers_list.removeAllViews();
        for (int i = 0, answersSize = answers.size(); i < answersSize; i++) {
            answers_list.addView(answerAdapter.getView(i));
        }
    }

    class AnswerAdapter {
        private ArrayList<Poll.Answer> answers;


        AnswerAdapter(ArrayList<Poll.Answer> answers) {
            this.answers = answers;
        }

        public View getView(int position) {
            LayoutInflater inflater = LayoutInflater.from(ctx);
            View view = inflater.inflate(R.layout.list_item_poll_answer, null);
            final ViewHolder viewHolder = new ViewHolder();
            final Poll.Answer answer = answers.get(position);

            boolean this_answer = answer.getId() == answered_id;

            viewHolder.answer_bar = (RelativeLayout) view.findViewById(R.id.answer_bar);
            viewHolder.answer_name = (TextView) view.findViewById(R.id.answer_name);
            viewHolder.answer_rate_text = (TextView) view.findViewById(R.id.answer_rate_text);
            viewHolder.answer_rate = (ProgressLayout) view.findViewById(R.id.answer_rate);

            viewHolder.answer_name.setText(answer.getText());

            CandyApplication.client.setLoggingEnabled(false);

            if (answered) {
                if (this_answer) {
                    viewHolder.answer_name.setTypeface(null, Typeface.BOLD);

                    view.setClickable(false);
                    view.setLongClickable(true);
                    view.setOnLongClickListener(v -> {
                        RequestParams params = new RequestParams();
                        params.put("poll_id", poll.getId());
                        params.put("answer_id", answer.getId());
                        params.put("owner_id", poll.getOwner_id());
                        params.put("access_token", CandyApplication.access_token());
                        params.put("v", API.version);
                        CandyApplication.client.get(ctx, API.BASE() + API.pollsDeleteVote, params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                if (response.has("response")) {
                                    Toast.makeText(ctx, ctx.getString(R.string.poll_unvoted), Toast.LENGTH_SHORT).show();
                                    updateInfo();
                                } else if (response.has("error")) {
                                    Toast.makeText(ctx, ctx.getString(R.string.wtf_toast_text), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                Toast.makeText(ctx, ctx.getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            }
                        });
                        return false;
                    });
                } else {
                    viewHolder.answer_name.setTypeface(null, Typeface.NORMAL);
                }

                viewHolder.answer_rate_text.setText(String.format("%d%%", (int) answer.getRate()));
                viewHolder.answer_rate.setMaxProgress(100);
                viewHolder.answer_rate.setCurrentProgress((int) answer.getRate());

            } else {
                view.setClickable(true);
                view.setLongClickable(false);
                view.setOnClickListener(v -> {
                    RequestParams params = new RequestParams();
                    params.put("poll_id", poll.getId());
                    params.put("owner_id", poll.getOwner_id());
                    params.put("answer_id", answer.getId());
                    params.put("access_token", CandyApplication.access_token());
                    params.put("v", API.version);
                    CandyApplication.client.get(ctx, API.BASE() + API.pollsAddVote, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            if (response.has("response")) {
                                Toast.makeText(ctx, ctx.getString(R.string.poll_voted), Toast.LENGTH_SHORT).show();
                                updateInfo();
                            } else if (response.has("error")) {
                                Toast.makeText(ctx, ctx.getString(R.string.wtf_toast_text), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            Toast.makeText(ctx, ctx.getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        }
                    });
                });
                viewHolder.answer_rate_text.setText(ctx.getString(R.string.poll_vote));
            }

            return view;
        }

        private void updateInfo() {
            RequestParams params = new RequestParams();
            params.put("poll_id", poll.getId());
            params.put("owner_id", poll.getOwner_id());
            params.put("access_token", CandyApplication.access_token());
            params.put("v", API.version);
            CandyApplication.client.get(ctx, API.BASE() + API.pollsGetById, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        setPoll(Poll.parsePoll(response.getJSONObject("response")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }


        protected class ViewHolder {
            private RelativeLayout answer_bar;
            private TextView answer_name;
            private TextView answer_rate_text;
            private ProgressLayout answer_rate;
        }
    }

}
