package ru.kadabeld.candy.views;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by casper on 11/8/14.
 * This is where the magic happens :D
 */
public class PaperRipple {
    private final int EASE_ANIM_DURATION = 200;
    private int animDuration = EASE_ANIM_DURATION;
    private final int MAX_RIPPLE_ALPHA = 255;
    private int mMaxAlpha = MAX_RIPPLE_ALPHA;
    private final int MAX_RIPPLE_RADIUS = 300;
    private int mMaxRadius = MAX_RIPPLE_RADIUS;
    private View mView;
    private RectF mDrawRect;
    private int mClipRadius;
    private Path mCirclePath = new Path();
    private boolean hasRippleEffect = false;
    private int mEffectColor = Color.LTGRAY;
    private AnimationSet mAnimationSet = null;

    private ArrayList<RippleWave> mWavesList = new ArrayList<>();
    private RippleWave mCurrentWave = null;

    public PaperRipple(View mView) {
        this.mView = mView;
        this.mDrawRect = new RectF(0, 0, mView.getWidth(), mView.getHeight());
        this.mAnimationSet = new AnimationSet(true);
    }

    public void setHasRippleEffect(boolean hasRippleEffect) {
        this.hasRippleEffect = hasRippleEffect;
    }

    public void setAnimDuration(int animDuration) {
        this.animDuration = animDuration;
    }

    public void setEffectColor(int effectColor) {
        mEffectColor = effectColor;
    }

    public void setClipRadius(int mClipRadius) {
        this.mClipRadius = mClipRadius;
    }

    public void setRippleSize(int radius) {
        mMaxRadius = radius;
    }

    public void setMaxAlpha(int alpha) {
        mMaxAlpha = alpha;
    }

    public void onTouchEvent(final MotionEvent event) {

        if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_CANCEL) {
            mCurrentWave.fadeOutWave();
        } else if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_UP) {
            mCurrentWave.fadeOutWave();
        } else if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
            mCurrentWave = new RippleWave(mView, mAnimationSet, animDuration, mEffectColor);
            mCurrentWave.setMaxAlpha(mMaxAlpha);
            mCurrentWave.setWaveMaxRadius(mMaxRadius);
            mCurrentWave.startWave(event.getX(), event.getY());
            mWavesList.add(mCurrentWave);
        }
    }

    public void onDraw(final Canvas canvas) {

        if (hasRippleEffect && mWavesList.size() > 0) {
            mDrawRect.set(0, 0, mView.getWidth(), mView.getHeight());
            mCirclePath.reset();
            mCirclePath.addRoundRect(this.mDrawRect,
                    mClipRadius, mClipRadius, Path.Direction.CW);
            canvas.clipPath(mCirclePath);

            // Draw ripples
            for (Iterator<RippleWave> iterator = mWavesList.iterator(); iterator.hasNext(); ) {
                RippleWave wave = iterator.next();
                if (wave.isFinished()) {
                    iterator.remove();
                } else {
                    wave.onDraw(canvas);
                }
            }
        }

    }

    interface InterpolatedTimeCallback {
        void onTimeUpdate(float interpolatedTime);
    }

    class ValueGeneratorAnim extends Animation {

        private InterpolatedTimeCallback interpolatedTimeCallback;

        ValueGeneratorAnim(InterpolatedTimeCallback interpolatedTimeCallback) {
            this.interpolatedTimeCallback = interpolatedTimeCallback;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            this.interpolatedTimeCallback.onTimeUpdate(interpolatedTime);
        }
    }

    private class RippleWave {
        private final float opacityDecayVelocity = 1.75f;
        private boolean waveFinished = false;
        private int waveMaxRadius = MAX_RIPPLE_RADIUS;
        private AnimationSet mAnimationSet = new AnimationSet(true);
        private View mView;
        private Paint mCirclePaint = new Paint();
        private float mDownX;
        private int animDuration = EASE_ANIM_DURATION;
        private float mDownY;
        private int mCircleAlpha = MAX_RIPPLE_ALPHA;
        private float mRadius;
        private int mMaxAlpha = MAX_RIPPLE_ALPHA;
        private Animation.AnimationListener radiusAnimationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        };
        private Animation.AnimationListener opacityAnimationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                waveFinished = true;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        };

        public RippleWave(View view, AnimationSet animationSet, int animDuration, int effectColor) {
            this.mView = view;
            this.mAnimationSet = animationSet;
            this.animDuration = animDuration;
            this.mCirclePaint.setColor(effectColor);
        }

        private int waveOpacity(float interpolatedTime) {
            return (int) Math.max(0, mMaxAlpha - (mMaxAlpha * interpolatedTime * this.opacityDecayVelocity));
        }

        private float waveRadius(float interpolatedTime) {
            float radius = waveMaxRadius * 1.1f + 5;
            return radius * (1 - (float) Math.pow(80, -interpolatedTime));
        }

        public boolean isFinished() {
            return waveFinished;
        }

        public void startWave(float x, float y) {
            mDownX = x;
            mDownY = y;

            mCircleAlpha = mMaxAlpha;

            ValueGeneratorAnim valueGeneratorAnim = new ValueGeneratorAnim(interpolatedTime -> {
                mRadius = RippleWave.this.waveRadius(interpolatedTime);
                mView.invalidate();
            });
            valueGeneratorAnim.setInterpolator(new DecelerateInterpolator());
            valueGeneratorAnim.setDuration(animDuration);
            valueGeneratorAnim.setAnimationListener(radiusAnimationListener);
            mAnimationSet.addAnimation(valueGeneratorAnim);
            if (!mAnimationSet.hasStarted() || mAnimationSet.hasEnded()) {
                mView.startAnimation(mAnimationSet);
            }
        }

        public void fadeOutWave() {
            ValueGeneratorAnim valueGeneratorAnim = new ValueGeneratorAnim(interpolatedTime -> {
                mCircleAlpha = RippleWave.this.waveOpacity(interpolatedTime);
                mView.invalidate();
            });
            valueGeneratorAnim.setDuration(animDuration);
            valueGeneratorAnim.setAnimationListener(opacityAnimationListener);

            // If all animations stopped
            if (mAnimationSet.hasEnded()) {
                mAnimationSet.getAnimations().clear();
                mAnimationSet.addAnimation(valueGeneratorAnim);
                mView.startAnimation(mAnimationSet);
            } else {
                // Add new animation to current set
                mAnimationSet.addAnimation(valueGeneratorAnim);
            }
        }

        public void onDraw(final Canvas canvas) {
            mCirclePaint.setAlpha(mCircleAlpha);
            canvas.drawCircle(mDownX, mDownY, mRadius, mCirclePaint);
        }

        public void setWaveMaxRadius(int radius) {
            waveMaxRadius = radius;
        }

        public void setMaxAlpha(int alpha) {
            mMaxAlpha = alpha;
        }


    }
}
