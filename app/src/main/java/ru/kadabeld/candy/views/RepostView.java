package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

import ru.kadabeld.candy.AbstractActivity;
import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.adapters.PostAdapter;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.fragments.PostFragment;
import ru.kadabeld.candy.fragments.ProfileFragment;
import ru.kadabeld.candy.utils.parsers.Attachments;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

/**
 * User: Vlad
 * Date: 13.10.2015
 * Time: 12:12
 */
public class RepostView extends LinearLayout {
    private final PostAdapter.ViewType viewType;
    private final SharedPreferences sPref;
    private Context ctx;
    private LinearLayout repost_layout;
    private ImageView repost_avatar;
    private FLinearLayout repost_header;
    private TextView repost_name;
    private TextView repost_time;
    private LinearLayout repost_body;
    private LinearLayout repost_content_wrapper;
    private TextView repost_text;
    private FLinearLayout repost_read_more;
    private TextView repost_read_more_text;
    private FrameLayout repost_attachments_frame;
    private FrameLayout repost_repost_frame;
    private boolean noPadding;

    public RepostView(Context context) {
        super(context);
        ctx = context;
        ((Activity) ctx)
                .getLayoutInflater()
                .inflate(R.layout.element_repost, this, true);
        repost_layout = (LinearLayout) this.findViewById(R.id.repost_layout);
        repost_header = (FLinearLayout) this.findViewById(R.id.repost_header);
        repost_avatar = (ImageView) this.findViewById(R.id.repost_avatar);
        repost_name = (TextView) this.findViewById(R.id.repost_name);
        repost_time = (TextView) this.findViewById(R.id.repost_time);
        repost_body = (LinearLayout) this.findViewById(R.id.repost_body);
        repost_content_wrapper = (LinearLayout) this.findViewById(R.id.repost_content_wrapper);
        repost_text = (TextView) this.findViewById(R.id.repost_text);
        repost_read_more = (FLinearLayout) this.findViewById(R.id.repost_read_more);
        repost_read_more_text = (TextView) this.findViewById(R.id.repost_read_more_text);
        repost_attachments_frame = (FrameLayout) this.findViewById(R.id.repost_attachments_frame);
        repost_repost_frame = (FrameLayout) this.findViewById(R.id.repost_repost_frame);

        repost_read_more.setVisibility(View.GONE);
        repost_attachments_frame.setVisibility(View.GONE);
        repost_repost_frame.setVisibility(View.GONE);
        repost_content_wrapper.setVisibility(View.GONE);

        this.viewType = PostAdapter.ViewType.FEED;
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setPost(final Post post, int content_width, ArrayList<Post> copy_history, int index) {
//        repost_frame.addView(getRepostView(n.getCopy_history(), 0));
        if (post != null) {
            final long from_id = post.getFrom_id();
            AbstractProfile from_user = Cache.getAbstractProfile(from_id);
            AbstractProfile.ProfileOnLoad onLoad = from_user1 -> {
                repost_name.setText(from_user1.getFullName());
                repost_avatar.setImageDrawable(from_user1.getAvatarDrawable());
                if (!from_user1.isAvatarDefault()) {
                    Picasso.with(ctx).load(from_user1.getPhotoFor(100)).tag("news").transform(new CropCircleTransformation()).into(repost_avatar);
                }
                repost_header.setOnClickListener(v -> {
                    ProfileFragment f = new ProfileFragment();
                    f.init(from_id);
                    ((AbstractActivity) getContext()).openFragment(f);
                });
            };

            if (from_user == null) {
                repost_name.setText(ctx.getResources().getString(R.string.loading));
                User.load(from_id, onLoad);
            } else {
                onLoad.run(from_user);
            }

            repost_time.setText(CandyApplication.p.format(new Date(post.getDate() * 1000L)));

            if (index >= 2) {
                repost_layout.setPadding(0, 0, 0, 0);
                repost_body.setVisibility(View.GONE);
            } else {
                while (repost_body.getChildCount() > 1) {
                    repost_body.removeViewAt(1);
                }

                if (viewType == PostAdapter.ViewType.SINGLE) {
                    if (!post.getText().isEmpty()) {
                        repost_content_wrapper.setVisibility(View.VISIBLE);
                        repost_text.setText(post.getText());
                    }
                } else {
                    if (!post.getText().isEmpty()) {
                        repost_content_wrapper.setVisibility(View.VISIBLE);
                        String small = String.valueOf(PostAdapter.truncatePost(post.getText()));
                        repost_text.setText(small);
                        repost_read_more_text.setText((sPref.getBoolean("expandtext", false)) ? ctx.getResources().getString(R.string.expand_text) : ctx.getResources().getString(R.string.read_more));
                        repost_read_more.setVisibility(small.equals(post.getText()) ? View.GONE : View.VISIBLE);
                        repost_read_more.setOnClickListener(v -> {
                            if (sPref.getBoolean("expandtext", false)) {
                                repost_text.setText(post.getText());
                                repost_read_more.setVisibility(View.GONE);
                            } else {
                                PostFragment f = new PostFragment();
                                f.init(post.getOwner_id(), post.getId(), post.getComments().isCan_post(), 0);
                                ((AbstractActivity) ctx).openFragment(f);
                            }
                        });
                    }
                }

                Attachments attachments = post.getAttachments();
                if (!post.getGeo().getCoordinates().isEmpty()) {
                    attachments.addGeos(post.getGeo());
                }
                if (!attachments.isEmpty()) {
                    putHr(repost_body, ctx);
                    repost_body.addView(repost_attachments_frame);
                    attachments.getView(ctx, repost_attachments_frame, content_width);
                }

                if (copy_history.size() > index + 1) {
                    putHr(repost_body, ctx);
                    repost_body.addView(repost_repost_frame);
                    repost_repost_frame.setVisibility(View.VISIBLE);
                    RepostView view = new RepostView(ctx);
                    view.setPost(copy_history.get(index + 1), content_width, copy_history, index + 1);
                    view.setTag("news");
                    repost_repost_frame.addView(view);
                }
            }
        }
    }

    private void putHr(LinearLayout layout, Context context) {
        if (layout.getChildCount() > 0) {
            View view = new View(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) Global.convertDpToPixel(context, 4));
            view.setLayoutParams(layoutParams);
            layout.addView(view);
        }
    }

    public void removePadding() {
        repost_layout.setPadding(0, 0, 0, 0);
    }
}
