package ru.kadabeld.candy.views;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.utils.ColorUtils;

public class IndeterminateHorizontalDrawable extends Drawable {

    private Paint bgPaint;
    private Paint fgPaint = new Paint();
    private int maxOffset;
    private int offset;


    public IndeterminateHorizontalDrawable() {
        this.fgPaint.setColor(Color.parseColor("#EAEBED"));
        this.fgPaint.setAntiAlias(true);
        this.fgPaint.setStrokeWidth((float) Global.scale(10.0F));
        this.fgPaint.setStrokeCap(Cap.BUTT);
        this.bgPaint = new Paint();
        this.bgPaint.setColor(ColorUtils.getThemeColor(R.attr.colorPrimaryDark));
        this.bgPaint.setAntiAlias(true);
        this.maxOffset = Global.scale(40.0F);
    }

    public void draw(Canvas var1) {
        int var2 = Global.scale(3.0F);
        Rect var3 = new Rect();
        this.copyBounds(var3);
        var3.top = var3.centerY() - var2 / 2;
        var3.bottom = var3.top + var2;
        var1.drawRect(new RectF(var3), this.bgPaint);
        var1.save();
        var1.clipRect(var3);
        var1.translate((float) var3.left, (float) var3.top);

        for (var2 = -1; var2 < 100 && Global.scale(40.0F) * var2 <= var3.width(); ++var2) {
            var1.drawLine((float) (Global.scale(40.0F) * var2 - Global.scale(20.0F) + this.offset), (float) (var3.height() + Global.scale(20.0F)), (float) ((var2 + 1) * Global.scale(40.0F) + Global.scale(20.0F) + this.offset), (float) (-Global.scale(20.0F)), this.fgPaint);
        }

        this.offset += Global.scale(1.0F);
        this.offset %= this.maxOffset;
        var1.restore();
        if (this.isVisible()) {
            this.invalidateSelf();
        }

    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int var1) {
    }

    public void setColorFilter(ColorFilter var1) {
    }
}

