package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

import ru.kadabeld.candy.Cache;
import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Comment;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.entities.User;
import ru.kadabeld.candy.entities.meta.Notification;
import ru.kadabeld.candy.utils.transformations.CropCircleTransformation;

/**
 * User: Vlad
 * Date: 13.11.2015
 * Time: 23:58
 */
public class NotificationView extends LinearLayout {
    private Context ctx;
    private SharedPreferences sPref;

    private int dp4, dp25;

    public NotificationView(Context context) {
        super(context);
        this.ctx = context;
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
        dp25 = Math.round(Global.convertDpToPixel(ctx, 25));
        dp4 = Math.round(Global.convertDpToPixel(ctx, 4));
    }

    public void setNotification(Notification notification, boolean is_last) {
        ((Activity) ctx).getLayoutInflater().inflate(R.layout.list_item_notification, this, true);
        ImageView avatar = (ImageView) findViewById(R.id.avatar);
        ImageView icon = (ImageView) findViewById(R.id.icon);
        TextView content = (TextView) findViewById(R.id.content);
        TextView time = (TextView) findViewById(R.id.time);
        TextView title_text = (TextView) findViewById(R.id.title_text);
        TextView description = (TextView) findViewById(R.id.description);
        LinearLayout more_avatars = (LinearLayout) findViewById(R.id.more_avatars);
        View hr = findViewById(R.id.hr);
        hr.setVisibility(is_last? View.GONE : View.VISIBLE);
        more_avatars.removeAllViews();
        more_avatars.setVisibility(GONE);
        icon.setVisibility(GONE);
        description.setVisibility(GONE);
        content.setVisibility(GONE);

        time.setText(CandyApplication.p.format(new Date(notification.getDate() * 1000L)));

        ArrayList<Long> from_ids;
        AbstractProfile profile;
        Notification.Feedback feedback;
        Post post;
        Comment comment;

        // TODO: 7?
        int max_avatars = 7;

        switch (notification.getType()) {
            case FOLLOW:
                icon.setVisibility(VISIBLE);
                title_text.setVisibility(VISIBLE);
                icon.setImageResource(R.drawable.notification_subscribe);
                from_ids = notification.getFeedback().getFrom_ids();
                for (int i = 0; i < Math.min(from_ids.size(), max_avatars); i++) {
                    profile = Cache.getAbstractProfile(from_ids.get(i));
                    Picasso.with(ctx).load(Cache.getAbstractProfile(from_ids.get(0)).getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                    if (i == 0) {
                        if (from_ids.size() == 1) {
                            if (((User) profile).getSex() == User.Sex.MALE) {
                                String s = ctx.getString(R.string.notification_followed_single_male, profile.getFullName());
                                title_text.setText(s);
                            } else if (((User) profile).getSex() == User.Sex.FEMALE) {
                                String s = ctx.getString(R.string.notification_followed_single_female, profile.getFullName());
                                title_text.setText(s);
                            }
                        } else {
                            String s = ctx.getString(R.string.notification_followed_many, profile.getFullName(), ctx.getResources().getQuantityString(R.plurals.people_count, from_ids.size() - 1, from_ids.size() - 1));
                            title_text.setText(s);
                        }
                    } else {
                        more_avatars.setVisibility(VISIBLE);
                        ImageView more_avatar = new ImageView(ctx);
                        MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                        params.setMargins(dp4, dp4, dp4, dp4);
                        more_avatar.setLayoutParams(params);
                        Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(more_avatar);
                        more_avatars.addView(more_avatar);
                    }
                }
                if (from_ids.size() > max_avatars) {
                    ImageView more_users = new ImageView(ctx);
                    MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                    params.setMargins(dp4, dp4, dp4, dp4);
                    more_users.setLayoutParams(params);
                    more_users.setImageResource(R.drawable.more_circle);
                    more_avatars.addView(more_users);
                }
                break;
            case FRIEND_ACCEPTED:
                icon.setVisibility(VISIBLE);
                icon.setImageResource(R.drawable.notification_friend);
                from_ids = notification.getFeedback().getFrom_ids();
                title_text.setVisibility(VISIBLE);
                for (int i = 0; i < Math.min(from_ids.size(), max_avatars); i++) {
                    profile = Cache.getAbstractProfile(from_ids.get(i));
                    Picasso.with(ctx).load(Cache.getAbstractProfile(from_ids.get(0)).getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                    if (i == 0) {
                        if (from_ids.size() == 1) {
                            if (((User) profile).getSex() == User.Sex.MALE) {
                                String s = ctx.getString(R.string.notification_friend_accepted_single_male, profile.getFullName());
                                title_text.setText(s);
                            } else if (((User) profile).getSex() == User.Sex.FEMALE) {
                                String s = ctx.getString(R.string.notification_friend_accepted_single_female, profile.getFullName());
                                title_text.setText(s);
                            }
                        } else {
                            String s = ctx.getString(R.string.notification_friend_accepted_many, profile.getFullName(), ctx.getResources().getQuantityString(R.plurals.people_count, from_ids.size() - 1, from_ids.size() - 1));
                            title_text.setText(s);
                        }
                    } else {
                        more_avatars.setVisibility(VISIBLE);
                        ImageView more_avatar = new ImageView(ctx);
                        MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                        params.setMargins(dp4, dp4, dp4, dp4);
                        more_avatar.setLayoutParams(params);
                        Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(more_avatar);
                        more_avatars.addView(more_avatar);
                    }
                }
                if (from_ids.size() > max_avatars) {
                    ImageView more_users = new ImageView(ctx);
                    MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                    params.setMargins(dp4, dp4, dp4, dp4);
                    more_users.setLayoutParams(params);
                    more_users.setImageResource(R.drawable.more_circle);
                    more_avatars.addView(more_users);
                }
                break;
            case MENTION:
            case MENTION_COMMENTS:
            case WALL:
            case WALL_PUBLISH:
                doit(avatar, title_text, time, content);
                break;
            case COMMENT_POST:
                feedback = notification.getFeedback();
                profile = Cache.getAbstractProfile(feedback.getFrom_id());
                title_text.setText(profile.getFullName());
                title_text.setVisibility(VISIBLE);
                content.setVisibility(VISIBLE);
                description.setVisibility(VISIBLE);
                if (feedback.getAttachments() != null) {
                    if (feedback.getText().isEmpty()) {
                        content.setText(ctx.getString(R.string.attachments_many));
                    } else {
                        CharSequence s = Html.fromHtml(feedback.getText()).toString().trim();
                        content.setText(String.format("%s\n%s", s, ctx.getString(R.string.attachments_many)));
                    }
                } else {
                    CharSequence s = Html.fromHtml(feedback.getText()).toString().trim();
                    content.setText(s);
                }
                post = (Post) notification.getParent();
                description.setText(ctx.getString(R.string.to_post, post.getText()));
                description.setSingleLine(true);
                description.setEllipsize(TextUtils.TruncateAt.END);
                Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                break;
            case COMMENT_PHOTO:
            case COMMENT_VIDEO:
                doit(avatar, title_text, time, content);
                break;
            case REPLY_COMMENT:
                feedback = notification.getFeedback();
                comment = (Comment) notification.getParent();
                profile = Cache.getAbstractProfile(feedback.getFrom_id());
                title_text.setText(profile.getFullName());
                title_text.setVisibility(VISIBLE);
                content.setVisibility(VISIBLE);
                description.setVisibility(VISIBLE);
                if (feedback.getAttachments() != null) {
                    if (feedback.getText().isEmpty()) {
                        content.setText(ctx.getString(R.string.attachments_many));
                    } else {
                        CharSequence s = Html.fromHtml(feedback.getText()).toString().trim();
                        content.setText(String.format("%s\n%s", s, ctx.getString(R.string.attachments_many)));
                    }
                } else {
                    CharSequence s = Html.fromHtml(feedback.getText()).toString().trim();
                    content.setText(s);
                }
                description.setText(ctx.getString(R.string.to_comment, comment.getText()));
                description.setSingleLine(true);
                description.setEllipsize(TextUtils.TruncateAt.END);
                Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                break;
            case REPLY_COMMENT_PHOTO:
            case REPLY_COMMENT_VIDEO:
            case REPLY_TOPIC:
                doit(avatar, title_text, time, content);
                break;
            case LIKE_POST:
                icon.setVisibility(VISIBLE);
                icon.setImageResource(R.drawable.notification_like);

                feedback = notification.getFeedback();
                profile = Cache.getAbstractProfile(feedback.getFrom_id());
                from_ids = notification.getFeedback().getFrom_ids();
                post = (Post) notification.getParent();
                title_text.setVisibility(VISIBLE);

                for (int i = 0; i < Math.min(from_ids.size(), max_avatars); i++) {
                    profile = Cache.getAbstractProfile(from_ids.get(i));
                    Picasso.with(ctx).load(Cache.getAbstractProfile(from_ids.get(0)).getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                    if (i == 0) {
                        if (from_ids.size() == 1) {
                            if (((User) profile).getSex() == User.Sex.MALE) {
                                String s = ctx.getString(R.string.notification_like_post_single_male, profile.getFullName(), post.getText());
                                title_text.setText(s);
                            } else if (((User) profile).getSex() == User.Sex.FEMALE) {
                                String s = ctx.getString(R.string.notification_like_post_single_female, profile.getFullName(), post.getText());
                                title_text.setText(s);
                            }
                        } else {
                            String s = ctx.getString(R.string.notification_like_post_many, profile.getFullName(), post.getText(), ctx.getResources().getQuantityString(R.plurals.people_count, from_ids.size() - 1, from_ids.size() - 1));
                            title_text.setText(s);
                        }
                    } else {
                        more_avatars.setVisibility(VISIBLE);
                        ImageView more_avatar = new ImageView(ctx);
                        MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                        params.setMargins(dp4, dp4, dp4, dp4);
                        more_avatar.setLayoutParams(params);
                        Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(more_avatar);
                        more_avatars.addView(more_avatar);
                    }
                }
                if (from_ids.size() > max_avatars) {
                    ImageView more_users = new ImageView(ctx);
                    MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                    params.setMargins(dp4, dp4, dp4, dp4);
                    more_users.setLayoutParams(params);
                    more_users.setImageResource(R.drawable.more_circle);
                    more_avatars.addView(more_users);
                }

                title_text.setMaxLines(2);
                title_text.setEllipsize(TextUtils.TruncateAt.END);

                break;
            case LIKE_COMMENT:
                icon.setVisibility(VISIBLE);
                icon.setImageResource(R.drawable.notification_like);

                feedback = notification.getFeedback();
                from_ids = feedback.getFrom_ids();
                comment = (Comment) notification.getParent();

                for (int i = 0; i < Math.min(from_ids.size(), max_avatars); i++) {
                    profile = Cache.getAbstractProfile(from_ids.get(i));
                    Picasso.with(ctx).load(Cache.getAbstractProfile(from_ids.get(0)).getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                    if (i == 0) {
                        if (from_ids.size() == 1) {
                            if (((User) profile).getSex() == User.Sex.MALE) {
                                String s = ctx.getString(R.string.notification_like_comment_single_male, profile.getFullName(), comment.getText());
                                title_text.setText(s);
                            } else if (((User) profile).getSex() == User.Sex.FEMALE) {
                                String s = ctx.getString(R.string.notification_like_comment_single_female, profile.getFullName(), comment.getText());
                                title_text.setText(s);
                            }
                        } else {
                            String s = ctx.getString(R.string.notification_like_comment_many, profile.getFullName(), ctx.getResources().getQuantityString(R.plurals.people_count, from_ids.size() - 1, from_ids.size() - 1), comment.getText());
                            title_text.setText(s);
                        }
                    } else {
                        more_avatars.setVisibility(VISIBLE);
                        ImageView more_avatar = new ImageView(ctx);
                        MarginLayoutParams params = new MarginLayoutParams(dp25 + dp4 + dp4, dp25 + dp4 + dp4);
                        more_avatar.setPadding(dp4, dp4, dp4, dp4);
                        more_avatar.setLayoutParams(params);
                        Picasso.with(ctx).load(profile.getPhotoFor(100)).placeholder(R.drawable.avatar).transform(new CropCircleTransformation()).into(more_avatar);
                        more_avatars.addView(more_avatar);
                    }
                }
                if (from_ids.size() > max_avatars) {
                    ImageView more_users = new ImageView(ctx);
                    MarginLayoutParams params = new MarginLayoutParams(dp25 + dp4 + dp4, dp25 + dp4 + dp4);
                    more_users.setPadding(dp4, dp4, dp4, dp4);
                    more_users.setLayoutParams(params);
                    more_users.setImageResource(R.drawable.more_circle);
                    more_avatars.addView(more_users);
                }

                title_text.setMaxLines(2);
                title_text.setEllipsize(TextUtils.TruncateAt.END);

                break;
            case LIKE_PHOTO:
            case LIKE_VIDEO:
                doit(avatar, title_text, time, content);
                break;
            case LIKE_COMMENT_PHOTO:
                icon.setVisibility(VISIBLE);
                icon.setImageResource(R.drawable.notification_like);

                feedback = notification.getFeedback();
                profile = Cache.getAbstractProfile(feedback.getFrom_id());
                from_ids = notification.getFeedback().getFrom_ids();
                comment = (Comment) notification.getParent();

                for (int i = 0; i < Math.min(from_ids.size(), max_avatars); i++) {
                    profile = Cache.getAbstractProfile(from_ids.get(i));
                    Picasso.with(ctx).load(Cache.getAbstractProfile(from_ids.get(0)).getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                    if (i == 0) {
                        if (from_ids.size() == 1) {
                            if (((User) profile).getSex() == User.Sex.MALE) {
                                String s = ctx.getString(R.string.notification_like_comment_photo_single_male, profile.getFullName(), comment.getText());
                                title_text.setText(s);
                            } else if (((User) profile).getSex() == User.Sex.FEMALE) {
                                String s = ctx.getString(R.string.notification_like_comment_photo_single_female, profile.getFullName(), comment.getText());
                                title_text.setText(s);
                            }
                        } else {
                            String s = ctx.getString(R.string.notification_like_comment_photo_many, profile.getFullName(), ctx.getResources().getQuantityString(R.plurals.people_count, from_ids.size() - 1, from_ids.size() - 1), comment.getText());
                            title_text.setText(s);
                        }
                    } else {
                        more_avatars.setVisibility(VISIBLE);
                        ImageView more_avatar = new ImageView(ctx);
                        MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                        params.setMargins(dp4, dp4, dp4, dp4);
                        more_avatar.setLayoutParams(params);
                        Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(more_avatar);
                        more_avatars.addView(more_avatar);
                    }
                }
                if (from_ids.size() > max_avatars) {
                    ImageView more_users = new ImageView(ctx);
                    MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                    params.setMargins(dp4, dp4, dp4, dp4);
                    more_users.setLayoutParams(params);
                    more_users.setImageResource(R.drawable.more_circle);
                    more_avatars.addView(more_users);
                }

                title_text.setMaxLines(2);
                title_text.setEllipsize(TextUtils.TruncateAt.END);

                break;
            case LIKE_COMMENT_VIDEO:
                icon.setVisibility(VISIBLE);
                icon.setImageResource(R.drawable.notification_like);

                feedback = notification.getFeedback();
                profile = Cache.getAbstractProfile(feedback.getFrom_id());
                from_ids = notification.getFeedback().getFrom_ids();
                comment = (Comment) notification.getParent();

                for (int i = 0; i < Math.min(from_ids.size(), max_avatars); i++) {
                    profile = Cache.getAbstractProfile(from_ids.get(i));
                    Picasso.with(ctx).load(Cache.getAbstractProfile(from_ids.get(0)).getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                    if (i == 0) {
                        if (from_ids.size() == 1) {
                            if (((User) profile).getSex() == User.Sex.MALE) {
                                String s = ctx.getString(R.string.notification_like_comment_video_single_male, profile.getFullName(), comment.getText());
                                title_text.setText(s);
                            } else if (((User) profile).getSex() == User.Sex.FEMALE) {
                                String s = ctx.getString(R.string.notification_like_comment_video_single_female, profile.getFullName(), comment.getText());
                                title_text.setText(s);
                            }
                        } else {
                            String s = ctx.getString(R.string.notification_like_comment_video_many, profile.getFullName(), ctx.getResources().getQuantityString(R.plurals.people_count, from_ids.size() - 1, from_ids.size() - 1), comment.getText());
                            title_text.setText(s);
                        }
                    } else {
                        more_avatars.setVisibility(VISIBLE);
                        ImageView more_avatar = new ImageView(ctx);
                        MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                        params.setMargins(dp4, dp4, dp4, dp4);
                        more_avatar.setLayoutParams(params);
                        Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(more_avatar);
                        more_avatars.addView(more_avatar);
                    }
                }
                if (from_ids.size() > max_avatars) {
                    ImageView more_users = new ImageView(ctx);
                    MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                    params.setMargins(dp4, dp4, dp4, dp4);
                    more_users.setLayoutParams(params);
                    more_users.setImageResource(R.drawable.more_circle);
                    more_avatars.addView(more_users);
                }

                title_text.setMaxLines(2);
                title_text.setEllipsize(TextUtils.TruncateAt.END);

                break;
            case LIKE_COMMENT_TOPIC:
                icon.setVisibility(VISIBLE);
                icon.setImageResource(R.drawable.notification_like);

                feedback = notification.getFeedback();
                profile = Cache.getAbstractProfile(feedback.getFrom_id());
                from_ids = notification.getFeedback().getFrom_ids();
                comment = (Comment) notification.getParent();

                for (int i = 0; i < Math.min(from_ids.size(), max_avatars); i++) {
                    profile = Cache.getAbstractProfile(from_ids.get(i));
                    Picasso.with(ctx).load(Cache.getAbstractProfile(from_ids.get(0)).getPhotoFor(100)).transform(new CropCircleTransformation()).into(avatar);
                    if (i == 0) {
                        if (from_ids.size() == 1) {
                            if (((User) profile).getSex() == User.Sex.MALE) {
                                String s = ctx.getString(R.string.notification_like_comment_topic_single_male, profile.getFullName(), comment.getText());
                                title_text.setText(s);
                            } else if (((User) profile).getSex() == User.Sex.FEMALE) {
                                String s = ctx.getString(R.string.notification_like_comment_topic_single_female, profile.getFullName(), comment.getText());
                                title_text.setText(s);
                            }
                        } else {
                            String s = ctx.getString(R.string.notification_like_comment_topic_many, profile.getFullName(), ctx.getResources().getQuantityString(R.plurals.people_count, from_ids.size() - 1, from_ids.size() - 1), comment.getText());
                            title_text.setText(s);
                        }
                    } else {
                        more_avatars.setVisibility(VISIBLE);
                        ImageView more_avatar = new ImageView(ctx);
                        MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                        params.setMargins(dp4, dp4, dp4, dp4);
                        more_avatar.setLayoutParams(params);
                        Picasso.with(ctx).load(profile.getPhotoFor(100)).transform(new CropCircleTransformation()).into(more_avatar);
                        more_avatars.addView(more_avatar);
                    }
                }
                if (from_ids.size() > max_avatars) {
                    ImageView more_users = new ImageView(ctx);
                    MarginLayoutParams params = new MarginLayoutParams(dp25, dp25);
                    params.setMargins(dp4, dp4, dp4, dp4);
                    more_users.setLayoutParams(params);
                    more_users.setImageResource(R.drawable.more_circle);
                    more_avatars.addView(more_users);
                }

                title_text.setMaxLines(2);
                title_text.setEllipsize(TextUtils.TruncateAt.END);

                break;
            case COPY_POST:
                icon.setVisibility(VISIBLE);
                icon.setImageResource(R.drawable.notification_repost);
                title_text.setText("%1$s поделился вашей записью %2$s");

                break;
            case COPY_PHOTO:
            case COPY_VIDEO:
            case MENTION_COMMENT_PHOTO:
            case MENTION_COMMENT_VIDEO:
                doit(avatar, title_text, time, content);
                break;
        }

    }

    private void doit(ImageView avatar, TextView title_text, TextView time, TextView content) {
        avatar.setImageResource(R.drawable.grust);
        title_text.setText("Этот пункт ответов пока не готов.");

        avatar.setVisibility(VISIBLE);
        time.setVisibility(GONE);
        content.setVisibility(GONE);
        title_text.setVisibility(VISIBLE);
    }
}
