package ru.kadabeld.candy.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ru.kadabeld.candy.CandyApplication;
import ru.kadabeld.candy.Global;
import ru.kadabeld.candy.R;
import ru.kadabeld.candy.entities.Link;
import ru.kadabeld.candy.utils.transformations.StackBlurTransformation;

public class LinkView extends RelativeLayout {
    private Context ctx;
    private SharedPreferences sPref;

    public LinkView(Context context) {
        super(context);
        this.ctx = context;
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setLink(final Link link, int content_width) {
        ((Activity) ctx)
                .getLayoutInflater()
                .inflate(link.getPhoto() == null ? R.layout.element_link_nophoto : R.layout.element_link, this, true);

        TextView title = (TextView) this.findViewById(R.id.title);
        TextView description = (TextView) this.findViewById(R.id.description);
        TextView price = (TextView) this.findViewById(R.id.price);
        RelativeLayout blacker = (RelativeLayout) this.findViewById(R.id.blacker);
        RelativeLayout market_row = (RelativeLayout) this.findViewById(R.id.market_row);
        Button buy_button = (Button) this.findViewById(R.id.buy_button);
        RatingBar rating = (RatingBar) this.findViewById(R.id.rating);
        View view = this.findViewById(R.id.link);
        title.setText(link.getTitle());
        description.setText(link.getDescription());
        description.setTypeface(CandyApplication.light);

        title.setVisibility(link.getTitle().isEmpty() ? GONE : VISIBLE);
        description.setVisibility(link.getDescription().isEmpty() ? GONE : VISIBLE);

        if (link.getPhoto() != null) {
            ImageView thumb = (ImageView) this.findViewById(R.id.thumb);
            int w, h;
            w = content_width;
            h = Global.getResizedDimensions(link.getPhoto().getWidth(), link.getPhoto().getHeight(), w, 0, true).second;
            thumb.setLayoutParams(new RelativeLayout.LayoutParams(w, h / 2));
            thumb.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Picasso.with(ctx).load(link.getPhoto().getMaxImage()).resize(w, h).transform(new StackBlurTransformation(12)).into(thumb);

            if (sPref.getBoolean("linksfullfill", false)) {
                blacker.setLayoutParams(new RelativeLayout.LayoutParams(w, h / 2));
            }

            if (link.getProduct() != null) {
                market_row.setVisibility(VISIBLE);

                if (link.getRating() != null) {
                    rating.setRating((float) link.getRating().getStars());
                }

                if (link.getProduct().getPrice() != null) {
                    price.setText(String.format("%d %s", link.getProduct().getPrice().getAmount(), link.getProduct().getPrice().getCurrency().getName()));
                }

                if (link.getButton() != null) {
                    buy_button.setVisibility(VISIBLE);
                    buy_button.setText(link.getButton().getTitle());
                    buy_button.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getButton().getUrl()));
                            ctx.startActivity(browserIntent);
                        }
                    });
                } else {
                    buy_button.setVisibility(GONE);
                }
            } else {
                market_row.setVisibility(GONE);
            }
        }

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(link.getUrl()));
                ctx.startActivity(i);
            }
        });
    }
}
