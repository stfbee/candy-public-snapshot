package ru.kadabeld.candy.views;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.Toast;

import com.mapbox.mapboxsdk.tileprovider.MapTileLayerBase;
import com.mapbox.mapboxsdk.views.MapView;

public class MapViewNoControl extends MapView implements OnGestureListener, OnDoubleTapListener {

    private GestureDetectorCompat mDetector;
    public boolean panShouldEnabled = true;

    public boolean isPanShouldEnabled() {
        return panShouldEnabled;
    }

    public void setPanShouldEnabled(boolean panShouldEnabled) {
        this.panShouldEnabled = panShouldEnabled;
    }

    public MapViewNoControl(Context aContext, AttributeSet attrs) {
        super(aContext, attrs);
        init(aContext);
    }

    private void init(Context mContext) {
        mDetector = new GestureDetectorCompat(mContext, this);
        // Set the gesture detector as the double tap
        // listener.
        mDetector.setOnDoubleTapListener(this);
    }

    public MapViewNoControl(Context aContext, int tileSizePixels,
                            MapTileLayerBase tileProvider, Handler tileRequestCompleteHandler,
                            AttributeSet attrs) {
        super(aContext, tileSizePixels, tileProvider,
                tileRequestCompleteHandler, attrs);
        init(aContext);
    }

    public MapViewNoControl(Context aContext, int tileSizePixels,
                            MapTileLayerBase aTileProvider) {
        super(aContext, tileSizePixels, aTileProvider);
        init(aContext);
    }

    public MapViewNoControl(Context aContext) {
        super(aContext);
        init(aContext);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        return !panShouldEnabled || super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        if (!panShouldEnabled) {
            if (detector.onTouchEvent(event)) {
                return true;
            } else {
                mDetector.onTouchEvent(event);
            }
        }
        return !panShouldEnabled || super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onLongPress(MotionEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
                            float arg3) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onShowPress(MotionEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onSingleTapUp(MotionEvent arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        zoomIn();
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        return false;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent arg0) {
        return false;
    }

    SimpleTwoFingerDoubleTapDetector detector = new SimpleTwoFingerDoubleTapDetector() {

        @Override
        public void onTwoFingerDoubleTap() {
            Toast.makeText(getContext(), "Two Finger Double Tap", Toast.LENGTH_SHORT).show();
            zoomOut();
        }
    };

    public abstract class SimpleTwoFingerDoubleTapDetector {
        private final int TIMEOUT = ViewConfiguration.getDoubleTapTimeout() + 100;
        private long mFirstDownTime = 0;
        private boolean mSeparateTouches = false;
        private byte mTwoFingerTapCount = 0;

        private void reset(long time) {
            mFirstDownTime = time;
            mSeparateTouches = false;
            mTwoFingerTapCount = 0;
        }

        public boolean onTouchEvent(MotionEvent event) {
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    if (mFirstDownTime == 0 || event.getEventTime() - mFirstDownTime > TIMEOUT)
                        reset(event.getDownTime());
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    if (event.getPointerCount() == 2)
                        mTwoFingerTapCount++;
                    else
                        mFirstDownTime = 0;
                    break;
                case MotionEvent.ACTION_UP:
                    if (!mSeparateTouches)
                        mSeparateTouches = true;
                    else if (mTwoFingerTapCount == 2 && event.getEventTime() - mFirstDownTime < TIMEOUT) {
                        onTwoFingerDoubleTap();
                        mFirstDownTime = 0;
                        return true;
                    }
            }

            return false;
        }

        public abstract void onTwoFingerDoubleTap();
    }
}