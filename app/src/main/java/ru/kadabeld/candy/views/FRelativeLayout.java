package ru.kadabeld.candy.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import ru.kadabeld.candy.R;

public class FRelativeLayout extends RelativeLayout {

    private Drawable mForegroundSelector;
    private Rect mRectPadding;
    private boolean mUseBackgroundPadding = false;
    private PaperRipple paperRipple;

    public FRelativeLayout(Context context) {
        super(context);
        init(context, null);
    }

    public FRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(context, attrs);
    }

    public FRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FRelativeLayout,
                defStyle, 0);

        final Drawable d = a.getDrawable(R.styleable.FRelativeLayout_foreground);
        if (d != null) {
            setForeground(d);
        }

        a.recycle();

        if (this.getBackground() instanceof NinePatchDrawable) {
            final NinePatchDrawable npd = (NinePatchDrawable) this.getBackground();
            if (npd != null) {
                mRectPadding = new Rect();
                if (npd.getPadding(mRectPadding)) {
                    mUseBackgroundPadding = true;
                }
            }
        }
    }

    public void setHasRippleEffect(boolean rippleEffect) {
        paperRipple.setHasRippleEffect(rippleEffect);
    }

    private void init(Context context, AttributeSet attrs) {
        // you should set a background to view for effect to be visible. in this sample, this
        // linear layout contains a transparent background which is set inside the XML
        paperRipple = new PaperRipple(this);
        paperRipple.setHasRippleEffect(false);
        // giving the view to animate on
        if (Build.VERSION.SDK_INT >= 20) {
            int[] ints = {android.R.attr.selectableItemBackground};
            TypedArray ta = context.getTheme().obtainStyledAttributes(ints);
            Drawable indicator = ta.getDrawable(0);
            setForeground(indicator);
        }

        // enabling ripple effect. it only performs ease effect without enabling ripple effect

        int rippleColor = Color.LTGRAY;
        int animationDuration = 700;
        int clipRadius = 0;

        // Remove this if you don't use custom style attrs via XML
        if (attrs != null) {
            //TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PaperRippleLayout);

            //rippleColor = a.getColor(R.styleable.PaperRippleLayout_ripple_color, rippleColor);

            //animationDuration = a.getInt(R.styleable.PaperRippleLayout_duration, animationDuration);

            //clipRadius = a.getInt(R.styleable.PaperRippleLayout_clip_radius, clipRadius);
        }
        // setting the effect color
        paperRipple.setEffectColor(rippleColor);

        // setting the duration
        paperRipple.setAnimDuration(animationDuration);

        // setting radius to clip the effect. use it if you have a rounded background
        paperRipple.setClipRadius(clipRadius);

        // the view must contain an onClickListener to receive UP touch events. paperRipple
        // doesn't return any value in onTouchEvent for flexibility. so it is developers
        // responsibility to add a listener
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // let animator show the animation by applying changes to view's canvas
        paperRipple.onDraw(canvas);
        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        // send the touch event to animator
        paperRipple.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();

        if (mForegroundSelector != null && mForegroundSelector.isStateful()) {
            mForegroundSelector.setState(getDrawableState());
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        if (mForegroundSelector != null) {
            if (mUseBackgroundPadding) {
                mForegroundSelector.setBounds(mRectPadding.left, mRectPadding.top, w - mRectPadding.right, h - mRectPadding.bottom);
            } else {
                mForegroundSelector.setBounds(0, 0, w, h);
            }
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if (mForegroundSelector != null) {
            mForegroundSelector.draw(canvas);
        }
    }

    @Override
    protected boolean verifyDrawable(Drawable who) {
        return super.verifyDrawable(who) || (who == mForegroundSelector);
    }

    @Override
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        if (mForegroundSelector != null) mForegroundSelector.jumpToCurrentState();
    }

    public void setForeground(Drawable drawable) {
        if (mForegroundSelector != drawable) {
            if (mForegroundSelector != null) {
                mForegroundSelector.setCallback(null);
                unscheduleDrawable(mForegroundSelector);
            }

            mForegroundSelector = drawable;

            if (drawable != null) {
                setWillNotDraw(false);
                drawable.setCallback(this);
                if (drawable.isStateful()) {
                    drawable.setState(getDrawableState());
                }
            } else {
                setWillNotDraw(true);
            }
            requestLayout();
            invalidate();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void drawableHotspotChanged(float x, float y) {
        super.drawableHotspotChanged(x, y);
        if (mForegroundSelector != null) {
            mForegroundSelector.setHotspot(x, y);
        }
    }
}
