package ru.kadabeld.candy;

import android.util.LongSparseArray;

import ru.kadabeld.candy.entities.AbstractProfile;
import ru.kadabeld.candy.entities.Audio;
import ru.kadabeld.candy.entities.Dialog;
import ru.kadabeld.candy.entities.Message;
import ru.kadabeld.candy.entities.Photo;
import ru.kadabeld.candy.entities.Post;
import ru.kadabeld.candy.entities.meta.PurchasedStickers;

/**
 * User: Vlad
 * Date: 06.04.2015
 * Time: 17:02
 */
public class Cache {

    private static final LongSparseArray<AbstractProfile> usersList = new LongSparseArray<>();
    private static final LongSparseArray<Dialog> dialogsList = new LongSparseArray<>();
    private static final LongSparseArray<Dialog> chatsList = new LongSparseArray<>();
    private static final LongSparseArray<Message> messagesList = new LongSparseArray<>();
    private static final LongSparseArray<LongSparseArray<Photo>> photosList = new LongSparseArray<>();
    private static final LongSparseArray<LongSparseArray<Post>> postsList = new LongSparseArray<>();
    private static final LongSparseArray<LongSparseArray<Audio>> audiosList = new LongSparseArray<>();

    private static PurchasedStickers purchasedStickers = null;

    public Cache() {
        syncUsersDB();
    }

    public static void putAbstractProfile(AbstractProfile user) {
        if (usersList.get(user.getId()) != null) {
            // TODO: 07.09.2015 magic?
        } else {
            usersList.put(user.getId(), user);
        }
        syncUsersDB();
    }

    public static void putDialog(Dialog dialog) {
        if (dialogsList.get(dialog.getUser_id()) != null) {
            // TODO: 07.09.2015 magic?
        } else {
            dialogsList.put(dialog.getUser_id(), dialog);
        }
        syncUsersDB();
    }

    public static void putChat(Dialog dialog) {
        if (chatsList.get(dialog.getChat_id()) != null) {
            // TODO: 07.09.2015 magic?
        } else {
            chatsList.put(dialog.getChat_id(), dialog);
        }
        syncUsersDB();
    }

    public static void putMessage(Message message) {
        if (messagesList.get(message.getId()) != null) {
            // TODO: 07.09.2015 magic?
        } else {
            messagesList.put(message.getId(), message);
        }
        syncUsersDB();
    }

    public static void putPost(Post post) {
        if (postsList.get(post.getOwner_id()) != null && postsList.get(post.getOwner_id()).get(post.getId()) != null) {
            // TODO: 07.09.2015 magic?
        } else {
            if (postsList.get(post.getOwner_id()) == null) {
                postsList.put(post.getOwner_id(), new LongSparseArray<Post>());
            }
            postsList.get(post.getOwner_id()).put(post.getId(), post);
        }
        syncUsersDB();
    }

    public static void putPhoto(Photo photo) {
        if (photosList.get(photo.getOwner_id()) != null && photosList.get(photo.getOwner_id()).get(photo.getId()) != null) {
            // TODO: 07.09.2015 magic?
        } else {
            if (photosList.get(photo.getOwner_id()) == null) {
                photosList.put(photo.getOwner_id(), new LongSparseArray<Photo>());
            }
            photosList.get(photo.getOwner_id()).put(photo.getId(), photo);
        }
        syncUsersDB();
    }

    public static void putAudio(Audio audio) {
        if (audiosList.get(audio.getOwner_id()) != null && postsList.get(audio.getOwner_id()).get(audio.getId()) != null) {
            // TODO: 07.09.2015 magic?
        } else {
            if (audiosList.get(audio.getOwner_id()) == null) {
                audiosList.put(audio.getOwner_id(), new LongSparseArray<Audio>());
            }
            audiosList.get(audio.getOwner_id()).put(audio.getId(), audio);
        }
        syncUsersDB();
    }

    public static AbstractProfile getAbstractProfile(Long user_id) {
        AbstractProfile abstractProfile = usersList.get(user_id);
//        if (abstractProfile == null) {
//            if (user_id > 0 || user_id < -2000000000) {
//                abstractProfile = new User();
//            } else {
//                abstractProfile = new Group();
//            }
//            abstractProfile.setId(user_id);
//            putAbstractProfile(abstractProfile);
//        }
        return abstractProfile;
    }

    public static AbstractProfile getNullableAbstractProfile(Long user_id) {
        return usersList.get(user_id);
    }

    public static Dialog getDialog(Long user_id) {
        return dialogsList.get(user_id);
    }

    public static Dialog getChat(Long chat_id) {
        return chatsList.get(chat_id);
    }

    public static Post getPost(Long owner_id, Long post_id) {
        LongSparseArray<Post> postLongSparseArray = postsList.get(owner_id);
        if (postLongSparseArray != null) {
            return postLongSparseArray.get(post_id);
        } else {
            return null;
        }
    }

    public static Audio getAudio(Long owner_id, Long audio_id) {
        LongSparseArray<Audio> audioLongSparseArrayLongSparseArray = audiosList.get(owner_id);
        if (audioLongSparseArrayLongSparseArray != null) {
            return audioLongSparseArrayLongSparseArray.get(audio_id);
        } else {
            return null;
        }
    }

    public static Photo getPhoto(Long owner_id, Long photo_id) {
        LongSparseArray<Photo> photoLongSparseArrayLongSparseArray = photosList.get(owner_id);
        if (photoLongSparseArrayLongSparseArray != null) {
            return photoLongSparseArrayLongSparseArray.get(photo_id);
        } else {
            return null;
        }
    }

    public static PurchasedStickers getPurchasedStickers() {
        return purchasedStickers;
    }

    public static void setPurchasedStickers(PurchasedStickers purchasedStickers) {
        Cache.purchasedStickers = purchasedStickers;
    }

    private static void syncUsersDB() {
    }
}
