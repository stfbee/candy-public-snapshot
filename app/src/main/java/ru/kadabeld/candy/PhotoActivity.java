package ru.kadabeld.candy;

import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.kadabeld.candy.entities.Photo;
import ru.kadabeld.candy.fragments.PhotoCommentsFragment;
import ru.kadabeld.candy.utils.ColorUtils;
import ru.kadabeld.candy.viewpager.TouchImageView;

public class PhotoActivity extends RxAppCompatActivity {
    private static SlidingUpPanelLayout mLayout;
    private static boolean hide = false;
    private static RelativeLayout draggable;

    private ImageView like;
    private ImageView share;
    private ImageView comment;
    private TextView like_count;
    private TextView comment_count;
    private LinearLayout like_layout;
    private LinearLayout share_layout;
    private LinearLayout comment_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CandyApplication.setTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_pager);
        draggable = (RelativeLayout) findViewById(R.id.draggable);
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        like = (ImageView) findViewById(R.id.like);
        share = (ImageView) findViewById(R.id.share);
        comment = (ImageView) findViewById(R.id.comment);
        like_count = (TextView) findViewById(R.id.like_count);
        comment_count = (TextView) findViewById(R.id.comment_count);
        like_layout = (LinearLayout) findViewById(R.id.like_layout);
        share_layout = (LinearLayout) findViewById(R.id.share_layout);
        comment_layout = (LinearLayout) findViewById(R.id.comment_layout);

        mLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if (mLayout.getPanelState() != SlidingUpPanelLayout.PanelState.EXPANDED) {
                }
            }

            @Override
            public void onPanelExpanded(View panel) {
                //Log.i(TAG, "onPanelExpanded");
            }

            @Override
            public void onPanelCollapsed(View panel) {
                /*mLayout.setEnableDragViewTouchEvents(true);
                hamburger.setOnClickListener(null);*/
            }

            @Override
            public void onPanelAnchored(View panel) {
                //Log.i(TAG, "onPanelAnchored");
                Toast.makeText(PhotoActivity.this, "Anchored", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPanelHidden(View panel) {
                //Log.i(TAG, "onPanelHidden");
            }
        });

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        mLayout.setPanelHeight(0);

        like_count.setVisibility(View.GONE);
        comment_count.setVisibility(View.GONE);

        Bundle b = getIntent().getExtras();
        int current = b.getInt("photo_position");
        final ArrayList<String> photos_ids = b.getStringArrayList("photos_layout");

        final List<Fragment> fragments = new ArrayList<>();
        if (photos_ids != null) {
            for (String photo_string_id : photos_ids) {
                Bundle bundle = new Bundle();
                String[] split = photo_string_id.split("_");
                bundle.putLong("owner_id", Long.parseLong(split[0]));
                bundle.putLong("photo_id", Long.parseLong(split[1]));
                PhotoFragment photoFragment = new PhotoFragment();
                photoFragment.setArguments(bundle);
                fragments.add(photoFragment);
            }

            final FeedAdapter adapter = new FeedAdapter(getSupportFragmentManager(), fragments);
            pager.setAdapter(adapter);
            pager.setCurrentItem(current);
            String s = photos_ids.get(current);

            String[] split = s.split("_");
            Photo photo = Cache.getPhoto(Long.parseLong(split[0]), Long.parseLong(split[1]));
            if (photo != null) {
                change(photo.getOwner_id(), photo.getId(), photo.getAccess_key());
                loadComments(photo.getOwner_id(), photo.getId());
                pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        String s = photos_ids.get(position);
                        String[] split = s.split("_");
                        Photo photo = Cache.getPhoto(Long.parseLong(split[0]), Long.parseLong(split[1]));
                        if (photo != null) {
                            change(photo.getOwner_id(), photo.getId(), photo.getAccess_key());
                            loadComments(photo.getOwner_id(), photo.getId());
                            ((PhotoFragment) adapter.getItem(position)).resetZoom();
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            } else {
                // TODO: 10.03.2016
            }
        } else {
            Toast.makeText(PhotoActivity.this, "WTF with photo loading", Toast.LENGTH_SHORT).show();
        }


    }

    void loadComments(long owner_id, long photo_id) {
        PhotoCommentsFragment f1 = PhotoCommentsFragment.newInstance(owner_id, photo_id);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.comments_fragment, f1);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    void change(final long owner_id, final long photo_id, final String access_key) {
        RequestParams params = new RequestParams();
        params.put("photos", String.format("%s_%s", owner_id, photo_id));
        params.put("extended", 1);
        if (!access_key.isEmpty()) {
            params.put("access_key", access_key);
        }
        params.put("access_token", CandyApplication.access_token());
        params.put("lang", CandyApplication.lang);
        CandyApplication.client.get(API.BASE() + API.photosGetById, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    JSONObject item = response.getJSONObject(0);
                    final Photo photo = Photo.parsePhoto(item);

                    like.setColorFilter(null);

                    if (photo.getLikes().getCount() > 0) {
                        like_count.setText(String.valueOf(photo.getLikes().getCount()));
                        like_count.setVisibility(View.VISIBLE);
                    }

                    if (photo.getCount_comments() > 0) {
                        comment_count.setText(String.valueOf(photo.getCount_comments()));
                        comment_count.setVisibility(View.VISIBLE);
                    }

                    like(like, like_layout, photo.getLikes().isUser_likes());

                    like_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            photo.getLikes().like();
                            like(like, like_layout, photo.getLikes().isUser_likes());
                            like_count.setVisibility(photo.getLikes().getCount() == 0 ? View.GONE : View.VISIBLE);
                            like_count.setText(String.valueOf(photo.getLikes().getCount()));

                            RequestParams params = new RequestParams();
                            params.put("type", "photo");
                            params.put("owner_id", photo.getOwner_id());
                            params.put("item_id", photo.getId());
                            if (!access_key.isEmpty()) {
                                params.put("access_key", access_key);
                            }
                            params.put("access_token", CandyApplication.access_token());
                            params.put("lang", CandyApplication.lang);
                            CandyApplication.client.get(API.BASE() + API.likes(photo.getLikes().isUser_likes()), params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    try {
                                        response = response.getJSONObject("response");
                                        if (response.has("likes")) {
                                            int likes = response.getInt("likes");
                                            photo.getLikes().setCount(likes);
                                            like_count.setText(String.valueOf(likes));
                                        } else {
                                            photo.getLikes().like();
                                            like(like, like_layout, photo.getLikes().isUser_likes());
                                            like_count.setVisibility(photo.getLikes().getCount() == 0 ? View.GONE : View.VISIBLE);
                                            like_count.setText(String.valueOf(photo.getLikes().getCount()));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }


                                @Override
                                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                    photo.getLikes().like();
                                    like(like, like_layout, photo.getLikes().isUser_likes());
                                    like_count.setVisibility(photo.getLikes().getCount() == 0 ? View.GONE : View.VISIBLE);
                                    like_count.setText(String.valueOf(photo.getLikes().getCount()));
                                }
                            });
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void like(ImageView like, LinearLayout like_layout, boolean liked) {
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.button_news);
        like.setColorFilter(null);
        if (liked && drawable != null) {
            drawable.setColorFilter(ColorUtils.getThemeColor(R.attr.colorPrimary), PorterDuff.Mode.MULTIPLY);
            like.setColorFilter(new LightingColorFilter(ColorUtils.getThemeColor(R.attr.colorLightText), ColorUtils.getThemeColor(R.attr.colorLightText)));
        }
        like_layout.setBackgroundDrawable(drawable);
    }


    @Override
    public void onBackPressed() {
        if (mLayout.getPanelState() != SlidingUpPanelLayout.PanelState.EXPANDED) {
            finish();
        } else {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }
    }

    public static class PhotoFragment extends Fragment {
        TouchImageView imageView;
        float y1, y2;

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            Bundle b = getArguments();
            imageView = new TouchImageView(getActivity());
            imageView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            y1 = event.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            y2 = event.getY();
                            float deltaY = y2 - y1;
                            if (Math.abs(deltaY) > 200) {
                                if (y2 > y1 && !imageView.isZoomed()) {
                                    if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                                    } else {
                                        getActivity().finish();
                                    }
                                } else if (y2 < y1 && !imageView.isZoomed()) {
                                    //Toast.makeText(getActivity(), "Comments", Toast.LENGTH_SHORT).show();
                                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                                }
                            }
                            break;
                    }
                    return true;
                }
            });
            final Animation hideAnim = new AlphaAnimation(1.00f, 0.00f);
            hideAnim.setDuration(200);
            hideAnim.setAnimationListener(new Animation.AnimationListener() {

                public void onAnimationStart(Animation animation) {

                }

                public void onAnimationRepeat(Animation animation) {

                }

                public void onAnimationEnd(Animation animation) {
                    draggable.setVisibility(View.GONE);

                }
            });
            final Animation showAnim = new AlphaAnimation(0.00f, 1.00f);
            showAnim.setDuration(200);
            showAnim.setAnimationListener(new Animation.AnimationListener() {

                public void onAnimationStart(Animation animation) {

                }

                public void onAnimationRepeat(Animation animation) {

                }

                public void onAnimationEnd(Animation animation) {
                    draggable.setVisibility(View.VISIBLE);

                }
            });
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (hide) {
                        draggable.startAnimation(showAnim);
                        hide = false;
                    } else {
                        draggable.startAnimation(hideAnim);
                        hide = true;
                    }
                }
            });
            Photo photo = Cache.getPhoto(b.getLong("owner_id"), b.getLong("photo_id"));
            if (photo != null) {
                Picasso.with(getActivity()).load(photo.getMaxImage()).into(imageView);
            } else {
                // TODO: 10.03.2016
            }
            return imageView;
        }

        public void resetZoom() {
            imageView.resetZoom();
        }

        @Override
        public void onResume() {
            super.onResume();
        }
    }

    public class FeedAdapter extends FragmentPagerAdapter {

        private List<Fragment> myFragments;

        public FeedAdapter(FragmentManager fragmentManager, List<Fragment> myFrags) {
            super(fragmentManager);
            myFragments = myFrags;
        }

        @Override
        public Fragment getItem(int position) {
            return myFragments.get(position);
        }

        @Override
        public int getCount() {
            return myFragments.size();
        }
    }
}
