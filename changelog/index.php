<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf8">
    <title>CANDY Changelog</title>
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="http://vladonishchenko.ru/candy/favicon.ico">
    <link rel="icon" href="http://vladonishchenko.ru/candy/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
<div class="center">
    <h2>CANDY Changelog</h2>

    <div class="changelog">
        <?php
        $file = 'http://host.vladonishchenko.ru/job/Candy/lastSuccessfulBuild/artifact/changelog/changelog.txt';
        $contents = file($file);

        $regex_lines = "/(new|fix|upd|rem) (.*)/";
        $replacement_lines = "<li class=\"$1\">$2</li>";
        $regex_version = "/(Candy.*)/";
        $replacement_version = "<div class=\"box\"><img src=\"http://vladonishchenko.ru/candy/favicon.ico\" height=\"24\" style=\"display: block; overflow: auto; float: left; margin-right: 5px; margin-left: 46px\"><h4>$1</h4>";
        $replacement_block = "</ul></div>";

        foreach ($contents as $line) {
            if($line == "\n"){
                echo $replacement_block;
            }else if(substr($line, 0, 5)=="Candy"){
                echo preg_replace($regex_version, $replacement_version, $line);
            }else{
                echo preg_replace($regex_lines, $replacement_lines, $line);
            }
        }
        ?>

    </div>
</div>
</body>
</html>
<!--© APM.GG-->